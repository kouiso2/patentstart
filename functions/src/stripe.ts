// import * as admin from "firebase-admin";
// import * as functions from "firebase-functions";
// import Stripe from "stripe";
// /* Stripe
// ====================================================*/
// //command firebase functions:config:set stripe.test="sk_test_51HoRYHCA4dp751lOx2tDc1WkCcG2tjt8rNWkfyQqNyMGjNFwK2n6ORmMWXOmZhZMYmVcBsNsI78sdoWLMBeZo9Tf0007yNKx9i"
// //command firebase functions:config:set stripe.live="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"
// const stripe = new Stripe(functions.config().stripe.test, {
//   apiVersion: "2020-08-27",
// });

// type prop = functions.https.CallableContext | functions.EventContext;
// interface CustomerAccountModel {
//   context: prop;
//   create: boolean;
// }
// interface ConnectAccountModel extends CustomerAccountModel {
//   accept: boolean;
//   clientIp?: string;
// }

// /* Firebase Admin
// ====================================================*/
// const firebase = admin.initializeApp();

// const authAccountFunc = async(context: prop): Promise<admin.auth.UserRecord> => {
//   console.log("authAccountFunc", "start");
//   const auth = firebase.auth();
//   return await auth.getUser(context.auth?.uid!);
// }

// const userCollectionFunc = async(authUid: string): Promise<FirebaseFirestore.DocumentData | undefined> => {
//   console.log("userCollectionFunc", "start");
//   const userDb = firebase.firestore().collection('User').doc(authUid);
//   const userRef = await userDb.get();
//   return userRef.data();
// }

// /* Stripe Customer Account
// ====================================================*/
// export async function connectAccountFunc(params: ConnectAccountModel){
//   const {context, accept, create, clientIp} = params;
//   const auth = await authAccountFunc(context);
//   const snap = await userCollectionFunc(auth.uid);
//   console.log("connectAccountCreate", "start");
//   console.log("clientIp", clientIp);
//   let object: Stripe.AccountUpdateParams | Stripe.AccountCreateParams = {
//   business_profile: {
//     name: snap?.companyName,
//     support_email: snap?.email,
//     support_phone: snap?.phoneNumber,
//     support_url: "https://app.patentstart.jp/auth/contact",
//     url: snap?.homepage
//   },
//   business_type: snap?.businessType.company ? "company" : "individual",
//   metadata: {
//     firebaseId: auth.uid
//   },
//   individual: {
//     address: {
//       country: 'JP',
//       city: snap?.address.city,
//       postal_code: snap?.address.postalCode,
//       state: snap?.address.state,
//       line1: snap?.address.line1,
//       line2: snap?.address.line2
//     },
//     address_kana: {
//       country: 'JP',
//       city: "アダチク",
//       postal_code: snap?.address.postalCode,
//       state: "トウキョウト",
//       town: "トウワ5チョウメ",
//       line1: "13-24",
//       line2: "イトーピアキタアヤセ207"
//     },
//     address_kanji: {
//       country: 'JP',
//       city: "足立区",
//       postal_code: snap?.address.postalCode,
//       state: "東京都",
//       town: "東和5丁目",
//       line1: "13-24",
//       line2: "イトーピア北綾瀬207"
//     },
//     gender: "male",
//     email: snap?.email,
//     phone: "080-1249-5168",
//   },
//   company: {
//     address: {
//       country: 'JP',
//       city: snap?.address.city,
//       postal_code: snap?.address.postalCode,
//       state: snap?.address.state,
//       line1: snap?.address.line1,
//       line2: snap?.address.line2
//     },
//     address_kana: {
//       country: 'JP',
//       city: snap?.address.city,
//       postal_code: snap?.address.postalCode,
//       state: snap?.address.state,
//       line1: snap?.address.line1,
//       line2: snap?.address.line2
//     },
//     address_kanji: {
//       country: 'JP',
//       city: snap?.address.city,
//       postal_code: snap?.address.postalCode,
//       state: snap?.address.state,
//       line1: snap?.address.line1,
//       line2: snap?.address.line2
//     },
//     name: snap?.companyName,
//     name_kana: snap?.companyKana,
//     name_kanji: snap?.companyName,
//     phone: snap?.phoneNumber,
//   },
//   settings: {
//     card_payments: {
//       decline_on: {
//         avs_failure: false,
//         cvc_failure: true
//       }
//     },
//     payments: {
//       statement_descriptor: "PATENTSTART",
//       statement_descriptor_kana: "パテントスタート",
//       statement_descriptor_kanji: "パテントスタート"
//     },
//     payouts: {
//       debit_negative_balances: true,
//       schedule: {
//         delay_days: 4,
//         interval: "monthly",
//         monthly_anchor: 25
//       },
//     },
//   },
//   email: snap?.email,
//   default_currency: 'jpy',
//   capabilities: {
//     card_payments: { requested: true },
//     transfers: { requested: true },
//   }
// }

//   if (accept){
//     object.tos_acceptance = {
//       ip: clientIp,
//       date: Math.floor(Date.now() / 1000),
//     };
//   };
//   if (create){
//     return await stripe.accounts.create({
//       type: 'custom',
//       country: "JP",
//       ...object,
//     });
//   } else {
//     return await stripe.accounts.update(snap?.stripeId, {
//       ...object,
//     });
//   };
// }

// /* Stripe Customer Account
// ====================================================*/
// /* --- Create --- */
// export async function customerAccountFunc(params: CustomerAccountModel){
//   const {context, create} = params;
//   const auth = await authAccountFunc(context);
//   const snap = await userCollectionFunc(auth.uid);
//   console.log("customerAccountCreate", "start");
//   //stripe
//   let object: Stripe.CustomerCreateParams | Stripe.CustomerUpdateParams = {
//     name: snap?.displayName,
//     email: snap?.email,
//     address: {
//       city: snap?.address.city,
//       country: snap?.address.country,
//       line1: snap?.address.line1,
//       line2: snap?.address.line2,
//       postal_code: snap?.address.postalCode,
//       state: snap?.address.state,
//     },
//     shipping: {
//       address: {
//         city: snap?.address.city,
//         country: snap?.address.country,
//         line1: snap?.address.line1,
//         line2: snap?.address.line2,
//         postal_code: snap?.address.postalCode,
//         state: snap?.address.state,
//       },
//       phone: snap?.phoneNumber,
//       name: snap?.displayName
//     },
//     preferred_locales: ["ja"],
//     metadata: {
//       firebaseId: auth.uid,
//     },
//     phone: snap?.phoneNumber,
//   };
//   if (create){
//     return await stripe.customers.create(object);
//   } else {
//     return await stripe.customers.update(snap?.stripeId, object);
//   }
// }

// // import * as admin from "firebase-admin";
// // import * as functions from "firebase-functions";
// // import Stripe from "stripe";

// // /* Stripe
// // ====================================================*/
// // //command firebase functions:config:set stripe.test="sk_test_51HoRYHCA4dp751lOx2tDc1WkCcG2tjt8rNWkfyQqNyMGjNFwK2n6ORmMWXOmZhZMYmVcBsNsI78sdoWLMBeZo9Tf0007yNKx9i"
// // //command firebase functions:config:set stripe.live="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"
// // const stripe = new Stripe(functions.config().stripe.test, {
// //   apiVersion: "2020-08-27",
// // });

// // const firebase = admin.initializeApp();

// // /* --- クレカ登録のための準備（トークン発行） --- */
// // export const readyIntents = functions.https.onCall(async (data, context) => {
// //   console.log(data);
// //   const db = firebase.firestore().collection('User').doc(context.auth?.uid!);
// //   const dbRef = await db.get();

// //   let item = dbRef.data();

// //   const setupRes = await stripe.setupIntents.create({
// //     customer: item?.stripeId,
// //     usage: 'on_session'
// //   });

// //   return setupRes;
// // });

// // /* --- Customer Applicantのアカウント --- */
// // export const createApplicantAccount = functions.https.onCall(async (data, context) => {
// //   console.log(data);
// //   const auth = await firebase.auth().getUser(context.auth?.uid!);
// //   const userDb = firebase.firestore().collection('User').doc(auth.uid);

// //   //stripeにアカウントを作成
// //   const customerRes = await stripe.customers.create({
// //     name: auth.displayName,
// //     email: auth.email,
// //     phone: auth.phoneNumber,
// //     preferred_locales: ["ja"],
// //     metadata: {
// //       firebaseId: auth.uid,
// //     },
// //   });

// //   //firestoreにIdを登録
// //   await userDb.update({
// //     stripeId: customerRes.id
// //   });
// //   return customerRes;
// // });

// // /* --- Connect Attorneyのアカウント --- */
// // export const createAttorneyAccount = functions.https.onCall(async (data, context) => {
// //   console.log(data);
// //   const auth = await firebase.auth().getUser(context.auth?.uid!);
// //   const clientIp = context.rawRequest.headers['x-appengine-user-ip'] as string;

// //   //stripeにアカウントを作成
// //   const connectRes = await stripe.accounts.create({
// //     type: 'custom',
// //     country: 'JP',
// //     email: auth.email,
// //     default_currency: 'jpy',
// //     capabilities: {
// //       card_payments: { requested: true },
// //       transfers: { requested: true },
// //     },
// //     tos_acceptance: {
// //       ip: clientIp,
// //       date: Math.floor(Date.now() / 1000),
// //     }
// //   });

// //   //firestoreにstripeIdを登録
// //   await firebase.firestore().collection('User').doc(auth.uid).update({
// //     stripeId: connectRes.id
// //   });

// //   return connectRes;
// // });
// // /* Stripe 銀行口座
// // ====================================================*/
// // /* --- 銀行口座・作成 --- */
// // export const createAttorneyBank = functions.https.onCall(async (data, context) => {
// //   //data = token
// //   const db = firebase.firestore().collection('User').doc(context.auth?.uid!);
// //   const dbRef = await db.get();

// //   const item = dbRef.data();

// //   if (item?.bankId) {
// //     //銀行アカウントを持っている弁理士
// //     var updateBankRes = await stripe.accounts.updateExternalAccount(
// //       item?.stripeId,
// //       item?.bankId,
// //       data
// //     );
// //     await db.update({
// //       bankId: updateBankRes.id
// //     });
// //   } else {
// //     //銀行アカウントを持っていない弁理士
// //     var createBankRes = await stripe.accounts.createExternalAccount(item?.stripeId, {
// //       external_account: data
// //     });
// //     await db.update({
// //       bankId: createBankRes.id
// //     });
// //   }

// //   return null;
// // });

// // /* --- 銀行口座・取得 --- */
// // export const takeBank = functions.https.onCall(async (data, context) => {
// //   console.log(data);
// //   const db = firebase.firestore().collection('User').doc(context.auth?.uid!);
// //   const dbRef = await db.get();

// //   const item = dbRef.data();

// //   const bankRes = await stripe.accounts.retrieveExternalAccount(
// //     item?.stripeId,
// //     item?.bankId,
// //   );

// //   return bankRes;
// // });

// // /* --- サブスクリプション --- */
// // export const linkingSubscription = functions.https.onCall(async (data, context) => {
// //   //data = {price, attorneyPath, appPath}
// //   const price: number = data.price;
// //   const attorneyPath: string = data.attorneyPath;
// //   const appPath: string = data.appPath;

// //   const db = firebase.firestore();
// //   console.log(143, data.attorneyPath, data.appPath);

// //   const userDb = db.collection('User').doc(context.auth?.uid!);
// //   const attorneyDb = db.doc(attorneyPath);
// //   const appDb = db.doc(appPath);
// //   const userRef = await userDb.get();
// //   const attorneyRef = await attorneyDb.get();
// //   const appRef = await appDb.get();
// //   const customerItem = userRef.data();
// //   const sellerItem = attorneyRef.data();
// //   const appItem = appRef.data();
// //   const customerId = customerItem?.stripeId;
// //   const sellerId = sellerItem?.stripeId;

// //   console.log(157, data.attorneyPath, data.appPath);

// //   //特許申請のための契約内容を作成(productIdがfirestoreに無ければ作成)
// //   const productRes = await stripe.products.create({
// //     name: `${appItem?.title}の特許申請`,
// //     type: "service",
// //     description: `${customerItem?.personName}と${sellerItem?.personName}の${appItem?.title}特許申請契約。`,
// //     metadata: {
// //       applicationId: appDb.id
// //     }
// //   }, {
// //     stripeAccount: sellerId
// //   });

// //   console.log(170, productRes);

// //   console.log(171, customerId);

// //   //サブスクリプションのための計画（支払い間隔や料金など）
// //   const planRes = await stripe.plans.create({
// //     amount: price,
// //     interval: "month",
// //     interval_count: 1,
// //     product: productRes.id,
// //     currency: "jpy"
// //   }, {
// //     stripeAccount: sellerId
// //   });

// //   console.log(205, planRes);

// //   //売り手側に紐づける用の顧客アカウントを作るためのトークン
// //   const tokenRes = await stripe.tokens.create({
// //     customer: customerId,
// //   }, { stripeAccount: sellerId });

// //   console.log(182, tokenRes);

// //   //売り手側に紐づける用の顧客のコピーアカウント
// //   const customerRes = await stripe.customers.create({
// //     source: tokenRes.id,
// //     description: "shared customer",
// //   }, { stripeAccount: sellerId });

// //   console.log(191, customerRes);

// //   //サブスクリプションの契約処理
// //   return await stripe.subscriptions.create({
// //     customer: customerRes.id,
// //     items: [
// //       { plan: planRes.id }
// //     ],
// //     application_fee_percent: 10,
// //   }, { stripeAccount: sellerId });
// // });

// // /* --- サブスクリプション --- */
// // // export const linkingSubscription = functions.https.onCall(async(data, context) => {
// // //   const price = data;
// // //   const productId = "prod_J6jxjd5IM6RgAs";

// // //   let samePrice: boolean = false;
// // //   let priceId: string = "";

// // //   const userDb = firebase.firestore().collection('User').doc(context.auth?.uid!);
// // //   const userRef = await userDb.get();

// // //   const priceList = await stripe.prices.list({
// // //     active: true,
// // //     currency: "jpy",
// // //     product: productId,
// // //     type: "recurring"
// // //   });

// // //   for(let i = 0; i < priceList.data.length; i++){
// // //     if (priceList.data[i].unit_amount === price){
// // //       priceId = priceList.data[i].id;
// // //       samePrice = true;
// // //       break;
// // //     } else {
// // //       continue;
// // //     }
// // //   }
// // //   if (!samePrice){
// // //     let priceRes = await stripe.prices.create({
// // //       unit_amount: price,
// // //       currency: 'jpy',
// // //       recurring: {interval: 'month'},
// // //       product: productId,
// // //     });
// // //     priceId = priceRes.id;
// // //   }

// // //   return await stripe.subscriptions.create({
// // //     customer: userRef.data()?.stripeId,
// // //     items: [
// // //       {price: priceId}
// // //     ]
// // //   });
// // // })

// // /* --- クレカ --- */
// // //デフォルトのカードを登録
// // export const attachDefaultNewCredit = functions.https.onCall(async (data, context) => {
// //   const db = await firebase.firestore().collection('User').doc(context.auth?.uid!).get();
// //   const item = db.data();
// //   const paymethod = data;

// //   await stripe.paymentMethods.attach(paymethod, {
// //     customer: item?.stripeId
// //   });

// //   return await stripe.customers.update(item?.stripeId, {
// //     invoice_settings: {
// //       default_payment_method: paymethod
// //     },
// //   });
// // });

// // //登録クレジットの解除（デフォルトの解除はない⇒ほかのカードをデフォルトとすることで解除可能）
// // export const detachCredit = functions.https.onCall(async (data, context) => {
// //   return await stripe.paymentMethods.detach(data);
// // });

// // /* --- Stripe connectのための本人確認書類の提出---*/
// // // export const identifyDocumentUpload = functions.https.onCall(async(data, context) => {
// // //   const db = firebase.firestore().collection('User');
// // //   const dbRef = db.doc(context.auth?.uid!);
// // //   const item = await dbRef.get();
// // //   const sellerItem = item.data();
// // //   const sellerId = sellerItem?.stripeId;

// // //   const docRes = await stripe.files.create({
// // //     purpose: "identity_document",
// // //     file: {
// // //       data: "",
// // //       name: "",
// // //       type: ""
// // //     }
// // //   });

// // //   return await stripe.accounts.update(sellerId, {

// // //   });
// // // });

// // export const payoutList = functions.https.onCall(async (data, context) => {
// //   //{stripeId} = data;
// //   //売り上げ一覧を表示させるたびに、getメゾットが動いてしまうため引数から取得
// //   console.log(context);
// //   const beginDate: Date = new Date();
// //   const endDate: Date = new Date();
// //   const begin = Math.floor(beginDate.getTime() / 1000);
// //   const end = Math.floor(endDate.getTime() / 1000);
// //   const stripeId = data;
// //   const list = await stripe.payouts.list({
// //     created: { gte: begin, lte: end }
// //   }, { stripeAccount: stripeId });
// //   return list.data;
// // });
// // //申請者のカード情報をすべて取得
// // export const listApplicantCredit = functions.https.onCall(async (data, context) => {
// //   console.log(data);
// //   const db = firebase.firestore().collection('User').doc(context.auth?.uid!);
// //   const dbRef = await db.get();

// //   let item = dbRef.data();

// //   const res = await stripe.paymentMethods.list({
// //     customer: item?.stripeId,
// //     type: 'card'
// //   });

// //   return res.data;
// // });

// // //申請者のカード情報をすべて取得
// // export const aleadyCheckUser = functions.https.onCall(async (data, context) => {
// //   console.log(data);
// //   const res = await firebase.auth().getUser(context.auth?.uid!);
// //   if (res) {
// //     return true;
// //   } else {
// //     return false;
// //   }
// // });

// // export const authUserController = functions.firestore
// //   .document("User/{userId}")
// //   .onUpdate(async (change, context) => {
// //     const snap = change.after.data();

// //     const auth = await firebase.auth().getUser(context.auth?.uid!);
// //     if (
// //       auth.displayName !== snap.personName ||
// //       auth.email !== snap.email ||
// //       auth.phoneNumber !== snap.phoneNumber ||
// //       auth.photoURL !== snap.photoUrl
// //     ) {
// //       firebase.auth().updateUser(auth.uid, {
// //         displayName: snap.displayName,
// //         email: snap.email,
// //         phoneNumber: snap.phoneNumber,
// //         photoURL: snap.photoUrl
// //       });
// //     }
// //     return null;
// //   })

// // export const userUpdateController = functions.firestore
// //   .document("User/{userId}")
// //   .onUpdate(async (change, context) => {
// //     const snap = change.after.data();

// //     const auth = await firebase.auth().getUser(context.auth?.uid!);
// //     if (
// //       auth.displayName !== snap.personName ||
// //       auth.email !== snap.email ||
// //       auth.phoneNumber !== snap.phoneNumber ||
// //       auth.photoURL !== snap.photoUrl
// //     ) {
// //       firebase.auth().updateUser(auth.uid, {
// //         displayName: snap.displayName,
// //         email: snap.email,
// //         phoneNumber: snap.phoneNumber,
// //         photoURL: snap.photoUrl
// //       });
// //     }
// //     if (snap.kind === "Applicant") {
// //       //stripe
// //       const res = await stripe.customers.update(snap.stripeId, {
// //         name: snap.displayName,
// //         email: snap.email,
// //         address: {
// //           city: snap.address.city,
// //           country: snap.address.country,
// //           line1: snap.address.line1,
// //           line2: snap.address.line2,
// //           postal_code: snap.address.postalCode,
// //           state: snap.address.state,
// //         },
// //         shipping: {
// //           address: {
// //             city: snap.address.city,
// //             country: snap.address.country,
// //             line1: snap.address.line1,
// //             line2: snap.address.line2,
// //             postal_code: snap.address.postalCode,
// //             state: snap.address.state,
// //           },
// //           phone: snap.phoneNumber,
// //           name: snap.displayName
// //         },
// //         preferred_locales: ["ja"],
// //         metadata: {
// //           firebaseId: auth.uid,
// //         },
// //         phone: snap.phoneNumber,
// //       });

// //       //追加
// //       if (snap.stripeId === res.id) {
// //         return null;
// //       }
// //       return change.after.ref.update({
// //         stripeId: res.id
// //       });
// //     } else if (snap.kind === "Attorney") {
// //       const res = await stripe.accounts.update(snap.stripeId, {
// //         business_profile: {
// //           name: snap?.companyName,
// //           support_email: snap?.email,
// //           support_phone: snap?.phoneNumber,
// //           support_url: "https://app.patentstart.jp/auth/contact",
// //           url: snap?.homepage
// //         },
// //         business_type: snap?.businessType.company ? "company" : "individual",
// //         metadata: {
// //           firebaseId: auth.uid
// //         },
// //         company: {
// //           address: {
// //             country: 'JP',
// //             city: snap?.address.city,
// //             postal_code: snap?.address.postalCode,
// //             state: snap?.address.state,
// //             line1: snap?.address.line1,
// //             line2: snap?.address.line2
// //           },
// //           address_kana: {
// //             country: 'JP',
// //             city: snap?.address.city,
// //             postal_code: snap?.address.postalCode,
// //             state: snap?.address.state,
// //             line1: snap?.address.line1,
// //             line2: snap?.address.line2
// //           },
// //           address_kanji: {
// //             country: 'JP',
// //             city: snap?.address.city,
// //             postal_code: snap?.address.postalCode,
// //             state: snap?.address.state,
// //             line1: snap?.address.line1,
// //             line2: snap?.address.line2
// //           },
// //           name: snap?.companyName,
// //           name_kana: snap?.companyKana,
// //           name_kanji: snap?.companyName,
// //           phone: snap?.phoneNumber,
// //         },
// //         settings: {
// //           card_payments: {
// //             decline_on: {
// //               avs_failure: false,
// //               cvc_failure: true
// //             }
// //           },
// //           payments: {
// //             statement_descriptor: "PATENTSTART",
// //             statement_descriptor_kana: "パテントスタート",
// //             statement_descriptor_kanji: "パテントスタート"
// //           },
// //           payouts: {
// //             debit_negative_balances: true,
// //             schedule: {
// //               delay_days: 4,
// //               interval: "monthly",
// //               monthly_anchor: 25
// //             },
// //           },
// //         },
// //         email: auth.email,
// //         default_currency: 'jpy',
// //         capabilities: {
// //           card_payments: { requested: true },
// //           transfers: { requested: true },
// //         },
// //       });

// //       //追加
// //       if (snap.stripeId === res.id) {
// //         return null;
// //       }
// //       return change.after.ref.update({
// //         stripeId: res.id
// //       });
// //     } else {
// //       return null;
// //     }
// //   });

// // export const sameStatusApplication = functions.firestore
// //   .document("User/{userId}/Application/{appId}")
// //   .onUpdate(async (change, context) => {
// //     console.log(context);
// //     const snap = change.after.data();
// //     const roomRef = firebase.firestore().collection('Rooms').doc(change.after.id)
// //     const roomRes = await roomRef.get();
// //     const roomItem = roomRes.data();
// //     if (snap.status !== roomItem?.patentStatus) {
// //       await roomRef.update({
// //         patentStatus: snap.status
// //       });
// //     }

// //     return null;
// //   });
// // /* プッシュ通知
// // ====================================================*/
// // export const sendMessageNotification = functions.firestore
// //   .document("/Rooms/{roomId}/Messages/{messageId}")
// //   .onCreate(async (event, snap) => { // 公式ではeventとなっていたが、そうするとevent.params.roomIdのように値を取ってこれなかった。
// //     var roomId = snap.params.roomId; // こうすることで上で指定したドキュメントのパスの{chatroomId}部分が取得できる。
// //     var messageId = snap.params.messageId;
// //     const roomRef = firebase.firestore().collection('Rooms').doc(roomId);
// //     const roomRes = await roomRef.get();
// //     const roomItem = roomRes.data();
// //     const messageRef = roomRef.collection('Messages').doc(messageId);
// //     const messageRes = await messageRef.get();
// //     const messageItem = messageRes.data();

// //     const senderRef = firebase.firestore().collection('User').doc(messageItem?.senderId);
// //     const senderRes = await senderRef.get();
// //     const senderItem = senderRes.data();
// //     if (roomItem?.applicantId === messageItem?.senderId) {
// //       const receiverRef = firebase.firestore().collection('User').doc(roomItem?.attorneyId);
// //       const receiverRes = await receiverRef.get();
// //       const receiverItem = receiverRes.data();
// //       var token = receiverItem?.token;
// //       var payload = {
// //         notification: {
// //           title: senderItem?.personName, // この辺りを動的に取ってくるケースが多いかな？
// //           body: messageItem?.text,
// //         }
// //       };

// //       var options = {
// //         priority: "high"
// //       };

// //       // 指定したtopic宛てにメッセージを送る
// //       return firebase.messaging().sendToDevice(token, payload, options)
// //         .then(function (response) {
// //           return console.log("Successfully sent message:", response);
// //         })
// //         .catch(function (error) {
// //           return console.log("Error sending message:", error);
// //         });
// //     } else if (roomItem?.attorneyId === messageItem?.senderId) {
// //       const receiverRef = firebase.firestore().collection('User').doc(roomItem?.applicantId);
// //       const receiverRes = await receiverRef.get();
// //       const receiverItem = receiverRes.data();
// //       var token = receiverItem?.token;
// //       var payload = {
// //         notification: {
// //           title: senderItem?.personName, // この辺りを動的に取ってくるケースが多いかな？
// //           body: messageItem?.text,
// //         }
// //       };

// //       var options = {
// //         priority: "high"
// //       };

// //       // 指定したtopic宛てにメッセージを送る
// //       return firebase.messaging().sendToDevice(token, payload, options)
// //         .then(function (response) {
// //           return console.log("Successfully sent message:", response);
// //         })
// //         .catch(function (error) {
// //           return console.log("Error sending message:", error);
// //         });
// //     }

// //   });

// // export const sendChatStartNotification = functions.firestore
// //   .document("/Rooms/{roomId}")
// //   .onCreate(async (event, snap) => { // 公式ではeventとなっていたが、そうするとevent.params.roomIdのように値を取ってこれなかった。
// //     var roomId = snap.params.roomId; // こうすることで上で指定したドキュメントのパスの{chatroomId}部分が取得できる。
// //     const roomRef = firebase.firestore().collection('Rooms').doc(roomId);
// //     const roomRes = await roomRef.get();
// //     const roomItem = roomRes.data();

// //     const senderRef = firebase.firestore().collection('User').doc(roomItem?.attorneyId);
// //     const senderRes = await senderRef.get();
// //     const senderItem = senderRes.data();
// //     const receiverRef = firebase.firestore().collection('User').doc(roomItem?.applicantId);
// //     const receiverRes = await receiverRef.get();
// //     const receiverItem = receiverRes.data();
// //     var token = receiverItem?.token;
// //     var payload = {
// //       notification: {
// //         title: senderItem?.personName, // この辺りを動的に取ってくるケースが多いかな？
// //         body: "応募がありました。弁理士とチャットを開始しましょう。",
// //       }
// //     };

// //     var options = {
// //       priority: "high"
// //     };

// //     // 指定したtopic宛てにメッセージを送る
// //     return firebase.messaging().sendToDevice(token, payload, options)
// //       .then(function (response) {
// //         return console.log("Successfully sent message:", response);
// //       })
// //       .catch(function (error) {
// //         return console.log("Error sending message:", error);
// //       });

// //   });

