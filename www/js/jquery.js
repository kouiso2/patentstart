// ---------- FVだけSPでフェードイン削除 ----------
if ($(window).width() <= 768) {
  $(".p-top__mv__wrapper *").removeClass(
    "js-trigger fade-type-up fade-elem fade-scale fade-up-down"
  );
  $(".p-top__mv__wrapper *").removeClass(
    "js-trigger fade-type-up fade-elem fade-scale fade-up-down"
  );
  $(".p-top__app__bg").attr("id", "");
  $(".p-top__app__sm").attr("id", "download");
} else {
  $(".p-top__app__bg").attr("id", "download");
  $(".p-top__app__sm").attr("id", "");
}

// ---------- モーダルトリガーをクリックした時 ----------
$(".modal__trigger").click(function () {
  var modalTrigger_number = $(this).attr("modalTrigger-number");

  $(".modal__wrap__all").fadeIn();
  $("[modalContent-number=" + modalTrigger_number + "]").fadeIn();
});

// ---------- 閉じるボタンをクリックした時 ----------
$(".modal__close").click(function () {
  $(".modal__wrap__all").fadeOut();
  $(".modal__content").fadeOut();
});

// ---------- コンテンツ部分以外をクリックした時 ----------
$(".modal__wrap__all").on("click", function (e) {
  if (!$(e.target).closest(".modal__content").length) {
    // ターゲット要素の外側をクリックした時の操作
    $(this).fadeOut();
    $(".modal__content").fadeOut();
  } else {
    // ターゲット要素をクリックした時の操作
  }
});

// ---------- アコーディオン ----------
$(".js__acdTitle").on("click", function () {
  $(this).toggleClass("-active");
  $(this).next().slideToggle();
});

// ---------- アンカーリンクでスクロール ----------
$(function () {
  $('a[href^="#"]').click(function () {
    var adjust = -100;
    var speed = 500;
    var href = $(this).attr("href");
    var target = $(href == "#" || href == "" ? "html" : href);
    var position = target.offset().top + adjust;
    $("html, body").animate({ scrollTop: position }, speed, "swing");
    return false;
  });
});
