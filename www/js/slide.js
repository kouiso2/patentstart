//スクロールしたら色が変わるサイドバー
var pagiJS = $(".pagination-js"); // セクションにpagination-jsをクラスに付与する
var set = window.innerHeight / 2;
var boxTop = new Array();
var current = -1;

changeBox(0);

$(window).scroll(function () {
  var top = $(".slide").offset().top;
  var appSectionTop = $(".p-top__app__bg").offset().top;

  var position = top - $(window).height() * 0.5;
  var slideHeight = $(".slide").height();
  var mvHeight = $(".p-top__mv").height();
  var slideSectionHeight = $(".slide__section").height();
  var windowWidth = $(window).width();

  pagiJS.each(function (i) {
    boxTop[i] = $(this).offset().top;
  });

  // ---------- MV超えたらのヘッダーとフッターを変化 ----------
  if ($(window).scrollTop() > mvHeight) {
    // ---------- MVを超えたら ----------
    $(".l-header").addClass("-active");
    $(".l-header").find(".button__type1").removeClass("-white");
    $(".l-header__flex__img").addClass("-red");
    if (windowWidth < 769) {
      $(".sp__footer__link").fadeIn();
    }
  } else {
    // ---------- MVの範囲内だったら ----------
    $(".l-header").removeClass("-active");
    $(".l-header__flex__img").removeClass("-red");
    $(".l-header").find(".button__type1").addClass("-white");
    if (windowWidth < 769) {
      $(".sp__footer__link").fadeOut();
    }
  }

  // // ---------- slide部分の左のページネーションの表示・非表示 ----------
  if (
    $(window).scrollTop() > top - slideSectionHeight / 5 &&
    $(window).scrollTop() - top + slideSectionHeight / 1 < slideHeight
  ) {
    $(".pagination_js_wrap").addClass("-active");
  } else {
    $(".pagination_js_wrap").removeClass("-active");
  }

  var headerHeight = $(".l-header").height();
  var windowHeight = $(window).height();
  var slideHeight = $(".slide").height();
  var ImgBoxPC = $(".-ImgBoxPC").offset().top;
  var step1ImgBoxSP = $(".-step1ImgBoxSP").offset().top;
  var step5ImgBoxSP = $(".-step5ImgBoxSP").offset().top;

  // ---------- slideエリア ----------
  if (
    // ---------- 768px以上だった時 ----------
    windowWidth >= 768
  ) {
    if (
      // ---------- slideエリアに入る前 ----------
      ImgBoxPC <= step1ImgBoxSP
    ) {
      $(".slide__img.-fixed").css("opacity", 0);
      $("#step1 .slide__img").css("opacity", 1);
    } else if (
      // ---------- slideエリアの中 ----------
      ImgBoxPC > step1ImgBoxSP &&
      ImgBoxPC <= step5ImgBoxSP
    ) {
      $(".slide__img.-fixed").css("opacity", 1);
      $("#step1 .slide__img, #step5 .slide__img").css("opacity", 0);
    } else if (
      // ---------- slideエリアを出た後 ----------
      ImgBoxPC > step5ImgBoxSP
    ) {
      $(".slide__img.-fixed").css("opacity", 0);
      $("#step5 .slide__img").css("opacity", 1);
    }
  }

  for (var i = boxTop.length - 1; i >= 0; i--) {
    if ($(window).scrollTop() > boxTop[i] - set) {
      changeBox(i);
      break;
    }
  }
});

function changeBox(secNum) {
  if (secNum != current) {
    current = secNum;
    secNum2 = secNum + 1; //以下にクラス付与したい要素名と付与したいクラス名
    $(".pagination_js_child-dot").removeClass("dot-active");
    $(".slide__img__item__fixed").removeClass(
      "-active-step -fadeIn -fadeIn2 -fadeIn3"
    );

    if (current == 0) {
      // ---------- 左のドット部分 ----------
      $(".dot-top").addClass("dot-active");

      // ---------- STEP1の画像 ----------
      $(".slide__img__item--step1--message1").addClass("-active-step -fadeIn");
      $(".slide__img__item--step1--message2").addClass(
        "-active-step -fadeIn -fadeIn2"
      );
      $(".slide__img__item--step1--message3").addClass(
        "-active-step -fadeIn -fadeIn3"
      );
      $(".slide__img__item--step1--button").addClass(
        " -fadeIn button-active -fadeIn3"
      );
    } else if (current == 1) {
      // ---------- 左のドット部分 ----------
      $(".dot-about").addClass("dot-active");

      // ---------- STEP2の画像 ----------
      $(".slide__img__item--step2--message1").addClass("-active-step -fadeIn");
      $(".slide__img__item--step2--message2").addClass(
        "-active-step -fadeIn -fadeIn2"
      );

      $(".slide__img__item--step2--message3").addClass(
        "-active-step -fadeIn -fadeIn3"
      );
    } else if (current == 2) {
      // ---------- 左のドット部分 ----------
      $(".dot-advantage").addClass("dot-active");

      // ---------- STEP3の画像 ----------
      $(".slide__img__item__step3--phone").addClass("-active-step -fadeIn");
      $(".slide__img__item--step3--plan").addClass("-active-step -fadeIn");
      $(".slide__img__item--step3--button").addClass(
        "-active-step -fadeIn -fadeIn2"
      );
    } else if (current == 3) {
      // ---------- 左のドット部分 ----------
      $('a[href*="4"]').find("span").addClass("dot-active");

      // ---------- STEP4の画像 ----------
      $(".slide__img__item__step4--phone").addClass("-active-step -fadeIn");
      $(".slide__img__item--step4--message1").addClass("-active-step -fadeIn");

      $(".slide__img__item--step4--message2").addClass(
        "-active-step -fadeIn -fadeIn2"
      );

      $(".slide__img__item--step4--message3").addClass(
        "-active-step -fadeIn -fadeIn3"
      );
    } else if (current == 4) {
      // ---------- 左のドット部分 ----------
      $('a[href*="5"]').find("span").addClass("dot-active");

      // ---------- STEP5の画像 ----------
      $(".slide__img__item__step5--phone").addClass("-active-step -fadeIn");
      $(".slide__img__item--step5--message1").addClass("-active-step -fadeIn");
      $(".slide__img__item--step5--message2").addClass(
        "-active-step -fadeIn -fadeIn2"
      );
      $(".slide__img__item--step5--message3").addClass(
        "-active-step -fadeIn -fadeIn3"
      );
    }
  }
}
