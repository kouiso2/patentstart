module.exports = {
	"env": {
		"es6": true,
		"node": true,
		"browser": true
	},
	"parser": "babel-eslint",
	"plugins": [
		"airbnb",
		"react",
		"prettier",
		"react-hooks",
	],
	"parserOptions": {
		"version": 2018,
		"sourceType": "module",
		"ecmaFeatures": {
			"jsx": true
		}
	},
	"extends": [
		"eslint:recommended",
		"plugin:react/recommended",
		"prettier/react",
		"plugin:prettier/recommended",
    "prettier/@typescript-eslint",
	],
	"rules": {
		"prettier/prettier": "error",
		"react-hooks/rules-of-hooks": "error",
		"react-hooks/exhaustive-deps": "warn"
	}
}