import { bankKindChoice, tabChoice } from 'data/choice/index';
import { accountChoice, categoryChoice } from '../data/choice/index';

export function tabJpn(props: typeof tabChoice[number]) {
  switch (props) {
    case 'account':
      return "アカウント";
    case 'chat':
      return "チャット";
    case 'home':
      return "ホーム";
    case 'list':
      return "詳細リスト";
    default:
      return props;
  }
}

export function accountkindLow(props: typeof accountChoice[number]) {
  switch (props) {
    case 'Applicant':
      return "applicant";
    case 'Attorney':
      return "attorney";
    case 'Admin':
      return "admin";
    case 'None':
      return "none";
    default:
      return props;
  }
}
export function bankKind(props: typeof bankKindChoice[number]){
  switch(props){
    case 'Normal':
      return "普通預金";
    case 'Current':
      return "当座預金";
    case 'Saving':
      return "貯蓄預金";
    case 'Default':
      return "未登録";
    default:
      return props;
  }
}

export function patentCategoryJpn(param: typeof categoryChoice[number]) {
  switch (param) {
    case 'DailyNecessities':
      return "生活必需品";
    case 'Electrical':
      return "電気";
    case 'FixedStructure':
      return "固定構造物";
    case 'Machine':
      return "機械工学・証明・加熱・武器";
    case 'Paper':
      return "繊維・紙";
    case 'Physics':
      return "物理学";
    case 'Science':
      return "科学・治金";
    case 'Transportation':
      return "処理操作・運輸";
    case 'NoUnderstand':
      return "わからない方";
    default:
      return param;
  }
}

export function patentCategoryDescription(param: typeof categoryChoice[number]) {
  switch (param) {
    case 'DailyNecessities':
      return;
    case 'Electrical':
      return;
    case 'FixedStructure':
      return;
    case 'Machine':
      return;
    case 'Paper':
      return;
    case 'Physics':
      return;
    case 'Science':
      return;
    case 'Transportation':
      return;
    case 'NoUnderstand':
      return;
    default:
      return param;
  }
}

export default tabJpn;
