import { modalChoice } from "data/choice";

export interface ModalModel {
  isOpen: boolean;
  form: typeof modalChoice[number];
  id: string;
  idSub: string;
  imgUrl: string;
}
