import { ModalModel } from "./Modal";
export default interface ConfigModel {
  modal: ModalModel
  path: string;
  id: string;
  normal: {
    id: string;
    path: string;
  };
  room: {
    id: string;
    path: string;
  };
  application: {
    id: string;
    path: string;
  };
  attorney: {
    id: string;
    path: string;
  };
  applicant: {
    id: string;
    path: string;
  };
  user: {
    id: string;
    path: string;
  };
  offer: {
    id: string;
    path: string;
  };
  patent: {
    id: string;
    path: string;
  };
  bank: {
    id: string;
    path: string;
    name: string;
  }
}
