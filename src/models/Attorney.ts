import { prefectureChoice, userChoice as attorneyChoice } from 'data/choice/index';
import { genderChoice } from '../data/choice/index';

export interface CategoryModel {
  DailyNecessities: boolean,
  Transportation: boolean,
  Science: boolean,
  Paper: boolean,
  FixedStructure: boolean,
  Machine: boolean,
  Physics: boolean,
  Electrical: boolean,
};

interface businessTypeProp {
  individual: boolean;
  company: boolean;
};

export default interface AttorneyModel {
  image: any;
  companyName: string;
  companyKana: string;
  firstName: string;
  firstNameKatakana: string;
  lastName: string;
  lastNameKatakana: string;
  birthday: Date;
  gender: typeof genderChoice[number];
  businessType: businessTypeProp;
  history: number;
  registerNumber: string;
  correspondArea: typeof prefectureChoice[number][];
  kind: typeof attorneyChoice[number];
  homepage: string;
  email: string;
  phoneNumber: string;
  goodCategory: CategoryModel;
  address: {
    country: string;
    city: string;
    line1: string;
    line2: string;
    postalCode: string;
    state: string,
    town: string;
  },
  addressKatakana: {
    country: string;
    city: string;
    line1: string;
    line2: string;
    state: string,
    town: string;
  },
  self: string;
  identification: {
    isIdentified: boolean;
    isSubmitted: boolean;
    confirmationFile: {
      front: {
        path: any;//File&Stringを入れるのでany
        fullPath: string;
        name: string;
        type: string;
      }
      back: {
        path: any;//File&Stringを入れるのでany
        fullPath: string;
        name: string;
        type: string;
      }
    };
    qualificationFile: {
        path: any;//File&Stringを入れるのでany
        fullPath: string;
        name: string;
        type: string;
      }
  }
  tutorial: {
    home: boolean;
    chat: boolean;
  }
  stripeId: string;
  bankId: string;
}
