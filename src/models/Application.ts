import { applicationCategoryChoice } from "data/choice/index";
import firebase from "firebase";
import "firebase/firestore";
export default interface ApplicationModel {
  title: string;
  category: typeof applicationCategoryChoice[number] | null;
  explanation: string;
  document: any;
  recruit: [];
  status: "Recruit" | "Apply" | "Manage";
  price: number;
  subscriptionId: string;
  maintenancePrice: number;
  platform: string;
  creater: firebase.firestore.DocumentReference<firebase.firestore.DocumentData> | null;
  attorney: firebase.firestore.DocumentReference<firebase.firestore.DocumentData> | null;
  patent: firebase.firestore.DocumentReference<firebase.firestore.DocumentData> | null;
}
