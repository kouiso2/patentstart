import { categoryObjectChoice } from "data/choice";
import firebase from 'firebase';
import AttorneyModel from 'models/Attorney';
import ConfigModel from "./Config";

type CategoryTypes = keyof typeof categoryObjectChoice;
type SorterTypes = "asc" | "desc";
export interface TabContextModel {
  tab: boolean;
  setTab: (e: boolean) => void;
};
export interface PatentCategoryContextModel {
  patentCategory: CategoryTypes;
  setPatentCategory: (e: CategoryTypes) => void;
};
export interface SorterContextModel {
  sorter: SorterTypes;
  setSorter: (e: SorterTypes) => void;
};

export interface ConfigContextModel {
  config: ConfigModel;
  setConfig: (e: ConfigModel) => void;
};


export interface AttorneyProfileContextModel {
  profile: AttorneyModel | firebase.firestore.DocumentData;
  setProfile: (e: AttorneyModel) => void;
}
export interface CreditCardEventContextModel {
  signal: boolean;
  setSignal: (e: boolean) => void;
}
export interface CreditcardContextModel {
  credit: any;
  setCredit: (e: any) => void;
}

export default TabContextModel;
