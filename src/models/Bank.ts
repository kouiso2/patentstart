import { bankKindChoice } from "data/choice";
export interface BankInputModel {
  country: string;
  currency: string;
  bank_number: string;
  branch_number: string;
  kind: typeof bankKindChoice[number];
  account_holder_name: string;
  account_holder_type: string;
  account_number: string;
};

export interface BankDetailModel {
  bankName: string;
  branchName: string;
  kind: typeof bankKindChoice[number] | "未登録";
  number: string;
  yourName: string;
}

export default BankInputModel;
