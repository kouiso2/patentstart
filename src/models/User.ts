import ApplicantModel from './Applicant';
import AttorneyModel from './Attorney';

export default interface UserModel extends AttorneyModel, ApplicantModel {

}
