export default interface PatentModel {
  explanation: string;
  referenceNum: string;//文献番号
  fillingNum: string;//出願番号
  knownDate: Date;//公知日
  fillingDate: Date;//出願日
  developName: string;
  fi: string;
  jplatUrl: string;
  rightHolder: string;//uid 権利者・出願者
  document: File;
  application: any;
}
