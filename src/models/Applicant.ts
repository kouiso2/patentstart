import { genderChoice, userChoice as applicantChoice } from 'data/choice/index';

interface businessTypeProp {
  individual: boolean;
  company: boolean;
}
export default interface ApplicantModel {
  image: any;
  companyName: string;
  companyKana: string;
  personName: string;
  personKatakana: string;
  firstName: string;
  firstNameKatakana: string;
  lastName: string;
  lastNameKatakana: string;
  birthday: Date;
  gender: typeof genderChoice[number];
  businessType: businessTypeProp;
  kind: typeof applicantChoice[number];
  homepage: string;
  email: string;
  phoneNumber: string;
  address: {
    country: string;
    city: string;
    line1: string;
    line2: string;
    postalCode: string;
    state: string,
    town: string;
  },
  addressKatakana: {
    country: string;
    city: string;
    line1: string;
    line2: string;
    state: string,
    town: string;
  },
  self: string;
  tutorial: {
    home: boolean;
    chat: boolean;
  }
  stripeId: string;
}
