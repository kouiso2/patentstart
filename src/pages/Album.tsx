//ionic
import { IonContent, IonFooter, IonHeader, IonPage } from '@ionic/react';
import { makeStyles } from '@material-ui/core/styles';
import 'assets/css/Album.css';
import 'assets/css/style.css';
import AlbumList from 'components/Album/List';
import Footer from 'components/Footer/Index';
//original
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
import React from 'react';




const useStyles = makeStyles({
  root: {
    display: 'grid',
    height: '100vh',
    gridTemplateRows: '1fr auto',
  },
});

const Album: React.FC = () => {
  const classes = useStyles();

  return (

    <IonPage>
      <IonHeader>
        <Header title="弁理士名" left="back" center="title" right="close" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="chat" />
            <main className="content__wrap">
              <div className={classes.root}>
                <AlbumList />
              </div>
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Full" page="home" />
        </IonFooter>
      </IonContent>
    </IonPage>



  );
};

export default Album;
