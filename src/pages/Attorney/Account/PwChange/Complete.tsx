//ionic
import { IonContent, IonFooter, IonHeader, IonPage, IonRouterLink } from '@ionic/react';
import 'assets/css/haseryo.css';
// css
import 'assets/css/style.css';
//original
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
import React from 'react';

export const AttorneyPwChangeComplete: React.FC = () => {
  return (
    //XD55
    <IonPage>
      <IonHeader>
        <Header title="パスワード変更の完了" left="back" center="title" right="none" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="account" />
            <main className="content__wrap complete--page">
              <div className="complete__wrap">
                <h2 className="complete__title">変更が完了しました</h2>
                <p className="complete__text">パスワードの変更が完了しました。<br />
登録しているメールアドレスに確認メールを送信しましたのでご確認お願いいたします。</p>

                <div className="btn--center">
                  <IonRouterLink className="" href="/applicant/home/">
                    <div className="Btn__base PC__btn">ホームに戻る</div></IonRouterLink>
                </div>
              </div>
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Tabs" page="account" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};

export default AttorneyPwChangeComplete;
