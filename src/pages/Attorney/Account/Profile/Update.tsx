//ionic
import { IonContent, IonFooter, IonHeader, IonPage } from '@ionic/react';
import 'assets/css/Form.css';
import 'assets/css/haseryo.css';
// css
import 'assets/css/style.css';
import Footer from 'components/Footer/Index';
import { AttorneyProfileForm } from 'components/Form/Profile/Attorney';
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
import useAuth from 'hooks/auth';
import React from 'react';

//XD50
export const AttorneyProfileUpdate: React.FC = () => {
  const auth = useAuth();
  const CheckSidebar = React.useMemo(() => {
    if (!auth?.companyName || !auth.personName) {
      return;
    } else {
      return (
        <Sidebar page="account" />
      );
    }
  }, [auth?.companyName, auth?.personName]);
  return (
    <IonPage>
      <IonHeader>
        <Header title="プロフィール変更" left="back" center="title" right="none" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <section className="inner">
          <div className="main__wrap">
            {CheckSidebar}
            <main className="content__wrap">
              <AttorneyProfileForm />
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Tabs" page="account" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};

export default AttorneyProfileUpdate;
