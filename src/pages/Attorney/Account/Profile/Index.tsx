//ionic
import { IonContent, IonFooter, IonHeader, IonPage, IonRouterLink, isPlatform } from '@ionic/react';
import 'assets/css/Account.css';
import 'assets/css/Btn.css';
import 'assets/css/haseryo.css';
// css
import 'assets/css/style.css';
// img
import ProfileCamera from 'assets/img/noimage.svg';
//original
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
import firebase from 'firebase';
import 'firebase/auth';
import useProfile from 'hooks/profile';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import { RootState } from 'store/Combine';
import { modalConfigAction, pathConfigAction } from 'store/Config/action';
import { patentCategoryJpn } from 'translate';
import { noValueText } from '../../../../controller/index';

//XD49
export const AttorneyProfile: React.FC = () => {
  const history = useHistory();
  const { profile, profileIdentify } = useProfile();
  const dispatch = useDispatch();
  const config = useSelector((state: RootState) => state.config);

  const handleClick = React.useCallback(() => {
    return history.push('/attorney/profile/update');
  }, [history]);

  const handleLogoutClick = React.useCallback(() => {
    firebase.auth().signOut()
      .then(() => {
        history.push('/auth/login');
      })
  }, [history]);

  const currentCategory = React.useMemo(() => {
    return Object.keys(profile.goodCategory).map((value: any, key: number) => {
      if (Object.values(profile.goodCategory)[key]) {
        return patentCategoryJpn(value) + "/";
      } else {
        return null;
      }
    })
  }, [profile]);

  return (
    <IonPage>
      <IonHeader>
        <Header title="プロフィール" left={(isPlatform("ios") || isPlatform("android")) ? "none" : "back"} center="title" right="none" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="account" />
            <main className="content__wrap">
              <div className="profile__media">
                <figure className="profile__media__img" style={{ backgroundImage: `url(${profile.image ? profile.image : ProfileCamera})` }}></figure>
                {/* style={{ background: 'url(../src/assets/img/demo.jpg)' }} */}
                <div>
                  <p className="profile__media__company">{noValueText(profile.companyName)}</p>
                  <p className="profile__media__kana">{noValueText(profile.companyKana)}</p>
                </div>
              </div>

              <div className="profile__item">
                <p className="profile__item__title">担当者名</p>
                <p className="profile__item__text">{noValueText(profile.firstName + profile.lastName)}</p>
              </div>

              <div className="profile__item">
                <p className="profile__item__title">担当者名（フリガナ）</p>
                <p className="profile__item__text">{noValueText(profile.personKatakana)}</p>
              </div>

              <div className="profile__item">
                <p className="profile__item__title">ホームページ・SNS等</p>
                <p className="profile__item__text">{noValueText(profile.homepage)}</p>
              </div>

              <div className="profile__item">
                <p className="profile__item__title">メールアドレス</p>
                <p className="profile__item__text">{noValueText(profile.email)}</p>
              </div>
              <div className="profile__item">
                <p className="profile__item__title">得意な国際特許分類</p>
                <p className="profile__item__text">{currentCategory}</p>
              </div>
              <div className="profile__item">
                <p className="profile__item__title">住所</p>
                <p className="profile__item__text">〒{noValueText(profile.address?.postalCode)}</p>
                <p className="profile__item__text">{noValueText(profile.address?.line1, profile.address?.state + profile.address?.city + profile.address?.line1 + profile.address?.line2)}</p>
              </div>

              <div className="profile__item">
                <p className="profile__item__title">自己紹介</p>
                <p className="profile__item__text">{noValueText(profile.self)}</p>
              </div>
              <p className="profile__item__link" onClick={() => {
                dispatch(pathConfigAction(profileIdentify.path));
                dispatch(modalConfigAction({
                  ...config.modal,
                  isOpen: true,
                  form: "Profile",
                }));
              }}>自分のプロフィールを確認する</p>
              <div className=" SP__btn__center">
                <button className="Btn__base PC__btn" onClick={handleClick}>変更する</button>
              </div>
              <p className="account__title mt-40">アカウント設定</p>
              <IonRouterLink onClick={handleLogoutClick} href="" className="account__link">ログアウト</IonRouterLink>
              {//ios・androidの場合と、それ以外で条件分岐
                (isPlatform("ios") || isPlatform("android")) ?
                  <Link to="/attorney/pwchange/" className="account__link">パスワード変更</Link> :
                  <React.StrictMode></React.StrictMode>
              }
              <Link to="/attorney/contact/" className="account__link">退会のお手続き</Link>
              {//ios・androidの場合と、それ以外で条件分岐
                (isPlatform("ios") || isPlatform("android")) ?
                  <React.StrictMode>
                    {/* <Link to="" className="account__link">アプリの使い方</Link> */}
                    <a href="https://patentstart.jp/privacy.html" target="_blank" rel="noreferrer" className="account__link">
                      プライバシーポリシー
                    </a>
                    {/* <Link to="" className="account__link">よくある質問</Link> */}
                    <Link to="/attorney/contact/" className="account__link">お問い合わせ</Link>
                  </React.StrictMode> :
                  <React.StrictMode></React.StrictMode>
              }
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Tabs" page="account" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );

};

export default AttorneyProfile;
