//ionic
import { IonContent, IonFooter, IonHeader, IonPage } from '@ionic/react';
import 'assets/css/Account.css';
import 'assets/css/haseryo.css';
// css
import 'assets/css/style.css';
//original
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
import useAuth from 'hooks/auth';
import React from 'react';
import { Link } from 'react-router-dom';

export const AttorneyAccountPageList: React.FC = () => {
  //XD48
  const auth = useAuth();
  const identifyComponent = React.useMemo(() => {
    if (!auth?.identification?.isIdentified) {
      return (<Link to="/attorney/identification/" className="account__link">本人確認書類</Link>)
    }
  }, [auth?.identification?.isIdentified]);
  return (
    <IonPage>
      <IonHeader>
        <Header title="アカウント" left="none" center="title" right="none" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="account" />
            <main className="content__wrap">
              <p className="account__title">アカウント</p>
              <Link to="/attorney/profile/" className="account__link">プロフィール</Link>
              {/* 本人確認完了したら非表示にする */}
              {identifyComponent}
              <Link to="/attorney/bank/" className="account__link">振込先口座の確認</Link>
              <Link to="/attorney/proceeds/" className="account__link">売上確認</Link>
              <Link to="/attorney/pwchange/" className="account__link">パスワード変更</Link>
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Tabs" page="account" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};

export default AttorneyAccountPageList;
