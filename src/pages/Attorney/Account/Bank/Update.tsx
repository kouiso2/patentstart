//ionic
import { IonContent, IonFooter, IonHeader, IonInput, IonPage } from '@ionic/react';
import 'assets/css/Form.css';
import 'assets/css/haseryo.css';
// css
import 'assets/css/style.css';
//original
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
import 'firebase/functions';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { bankNumberAction, branchNumberAction, holderNameAction, kindAction, numberAction } from 'store/Bank/Input/action';
import { RootState } from 'store/Combine';
let zenginCode = require('zengin-code');

//XD60
export const BankAccountUpdate: React.FC = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const bank = useSelector((state: RootState) => state.bankInput);
  const [name, setName] = React.useState<string>("");//銀行名

  const handleSubmit = React.useCallback((e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    return history.push({
      pathname: '/attorney/bank/check',
      state: name
    });
  }, [history, name]);

  const bankList = React.useMemo(() => {
    return (Object.keys(zenginCode) as typeof zenginCode).map((element: any, key: number) => {
      return <option value={element} className="form__select__option" key={key}>{zenginCode[element].name}</option>
    });
  }, []);

  return (
    <IonPage>
      <IonHeader>
        <Header title="口座の登録・変更" left="back" center="title" right="none" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="account" />
            <main className="content__wrap">
              <form onSubmit={handleSubmit}>
                <div className="form__title__wrap__flex">
                  <span className="form__title__20">入金先口座を変更する</span><span className="form__badge--required">必須</span>
                </div>

                <p className="form--bank__formtype">銀行</p>
                <label htmlFor="1" className="form__select--deco">
                  <select className="form__select" name="銀行" id="1" onChange={(e) => {
                    e.preventDefault();

                    dispatch(bankNumberAction(e.currentTarget.value));
                    setName(zenginCode[e.target.value]?.name);
                  }}>
                    <option value="default" className="form__select__option">選択してください</option>
                    <option value="1100" className="form__select__option">テスト口座</option>
                    {bankList}
                  </select>
                </label>

                <p className="form--bank__formtype">口座種別</p>
                <label htmlFor="1" className="form__select--deco">
                  <select className="form__select" name="銀行" id="1" value={bank.kind} onChange={(e) => {
                    e.preventDefault();
                    const value = e.target.value;
                    if (value === "Normal") {
                      dispatch(kindAction("Normal"));
                    } else if (value === "Current") {
                      dispatch(kindAction("Current"));
                      window.alert("当座預金では登録できません、普通預金の口座をご登録下さい");
                      dispatch(kindAction("Normal"));
                    } else if (value === "Saving") {
                      dispatch(kindAction("Saving"));
                      window.alert("貯蓄預金では登録できません、普通預金の口座をご登録下さい");
                      dispatch(kindAction("Normal"));
                    } else {
                      dispatch(kindAction("Default"));
                    }
                  }}>
                    <option value="Default" className="form__select__option">選択してください</option>
                    <option value="Normal" className="form__select__option">普通預金</option>
                    <option value="Current" className="form__select__option">当座預金</option>
                    <option value="Saving" className="form__select__option">貯蓄預金</option>
                  </select>
                </label>
                <p className="form--bank__formtype">支店コード</p>
                <IonInput
                  placeholder="例)000"
                  className="form__input"
                  type="text"
                  value={bank.branch_number}
                  onIonChange={(e) => {
                    e.preventDefault();

                    dispatch(branchNumberAction(e.detail.value!));
                  }} />
                <p className="form--bank__formtype">口座番号</p>
                <IonInput
                  placeholder="例)000000"
                  className="form__input"
                  type="text"
                  value={bank.account_number}
                  onIonChange={(e) => {
                    e.preventDefault();
                    dispatch(numberAction(e.detail.value!));
                  }} />
                <p className="form--bank__formtype">口座名義</p>
                <IonInput
                  placeholder="例)ベンリシ タロウ"
                  className="form__input"
                  type="text"
                  value={bank.account_holder_name}
                  onIonChange={(e) => {
                    e.preventDefault();
                    dispatch(holderNameAction(e.detail.value!));
                  }} />
                <p className="form--bank__undertext">25日までに変更した場合、翌月の支払いは新しい口座に振り込まれます。26日以降の場合は翌々月以降となります。</p>
                <div className=" SP__btn__center">
                  <button className="Btn__base SP__btn" type="submit">
                    内容を確認する
                  </button>
                </div>
              </form>
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Tabs" page="account" />
        </IonFooter>
      </IonContent>
    </IonPage >
  );
};

export default BankAccountUpdate;
