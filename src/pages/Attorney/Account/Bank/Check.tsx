//ionic
import { IonContent, IonFooter, IonHeader, IonPage } from '@ionic/react';
import { Elements, useStripe } from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';
import 'assets/css/Bank.css';
import 'assets/css/haseryo.css';
// css
import 'assets/css/style.css';
//original
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
import { noValueText } from 'controller';
import { stripePublickeys } from 'data/choice';
//firebase
import firebase from 'firebase';
import 'firebase/functions';
import React from 'react';
import { useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router';
import { RootState } from 'store/Combine';
import { bankKind } from '../../../../translate/index';

interface Props {
  bankName: string;
}

const stripePromise = loadStripe(stripePublickeys.test);
const StripeBankForm: React.FC<Props> = ({ bankName }) => {
  const history = useHistory();
  const stripe = useStripe();
  const bank = useSelector((state: RootState) => state.bankInput);

  const handleFixClick = React.useCallback((e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault();
    return history.goBack();
  }, [history]);

  const handleSaveClick = React.useCallback(async (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault();

    const func = firebase.functions();
    const register = func.httpsCallable('createAttorneyBank');

    const res = await stripe?.createToken('bank_account', {
      country: bank.country,
      currency: bank.currency,
      routing_number: bank.bank_number + bank.branch_number,
      account_holder_name: bank.account_holder_name,
      account_holder_type: bank.account_holder_type,
      account_number: bank.account_number,
    });
    await register(res?.token?.id);
    return history.push('/attorney/bank/complete');
  }, [history, bank, stripe]);
  return (
    <div className="bank__info">
      <div className="bank__info__item bank__info__item--half">
        <p className="bank__info__item__title">銀行</p>
        <p className="bank__info__item__text">{noValueText(bankName)}</p>
      </div>
      <div className="bank__info__item bank__info__item--half">
        <p className="bank__info__item__title">口座種別</p>
        <p className="bank__info__item__text">{noValueText(bankKind(bank.kind))}</p>
      </div>
      <div className="bank__info__item">
        <p className="bank__info__item__title">支店コード</p>
        <p className="bank__info__item__text">{noValueText(bank.branch_number)}</p>
      </div>
      <div className="bank__info__item">
        <p className="bank__info__item__title">口座番号</p>
        <p className="bank__info__item__text">{noValueText(bank.account_number)}</p>
      </div>
      <div className="bank__info__item">
        <p className="bank__info__item__title">口座名義</p>
        <p className="bank__info__item__text">{noValueText(bank.account_holder_name)}</p>
      </div>
      <div className="bank__info__btn-box">
        <button className="bank__info__btn" onClick={handleFixClick}>訂正する</button>
        <button className="bank__info__btn" onClick={handleSaveClick}>保存する</button>
      </div>
    </div>
  )
}
const BankForm: React.FC<Props> = ({ bankName }) => (
  <Elements stripe={stripePromise}>
    <StripeBankForm bankName={bankName} />
  </Elements>
);

//XD61
export const BankAccountCheck: React.FC = () => {
  const location = useLocation<string>();
  const bankName = location.state;
  return (
    <IonPage>
      <IonHeader>
        <Header title="入力内容の確認" left="back" center="title" right="close" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="account" />
            <main className="content__wrap">
              <BankForm bankName={bankName} />
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Tabs" page="account" />
        </IonFooter>
      </IonContent>
    </IonPage >
  );
};





export default BankAccountCheck;
