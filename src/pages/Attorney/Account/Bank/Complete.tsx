//ionic
import { IonContent, IonFooter, IonHeader, IonPage, IonRouterLink } from '@ionic/react';
import 'assets/css/haseryo.css';
// css
import 'assets/css/style.css';
//original
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
import React from 'react';

export const BankAccountComplete: React.FC = () => {
  return (
    //XD62
    <IonPage>
      <IonHeader>
        <Header title="口座の登録完了" left="none" center="title" right="close" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="account" />
            <main className="content__wrap complete--page">
              <div className="complete__wrap">
                <h2 className="complete__title">登録が完了しました</h2>
                <p className="complete__text">売上金額の口座登録の手続きが完了しました。
                  登録しているメールアドレスに確認メールを送信しましたのでご確認お願いいたします。</p>
                <div className="btn--center">
                  <IonRouterLink className="" href="/attorney/home/">
                    <div className="Btn__base PC__btn">ホームに戻る</div>
                  </IonRouterLink>
                </div>
              </div>
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Tabs" page="account" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};

export default BankAccountComplete;
