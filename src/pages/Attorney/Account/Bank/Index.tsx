//ionic
import { IonContent, IonFooter, IonHeader, IonPage } from '@ionic/react';
import 'assets/css/Bank.css';
import 'assets/css/Btn.css';
import 'assets/css/haseryo.css';
// css
import 'assets/css/style.css';
//original
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
//firebase
import firebase from 'firebase';
import 'firebase/functions';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { allAction } from 'store/Bank/Detail/action';
import { RootState } from 'store/Combine';
import { noValueText } from '../../../../controller/index';

// XD58,XD59
export const BankAccount: React.FC = () => {
  const history = useHistory();
  const dispatch = useDispatch();

  const bank = useSelector((state: RootState) => state.bankDetail);

  function handleClick(e: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
    e.preventDefault();
    history.push('/attorney/bank/update');
  }

  React.useEffect(() => {
    let unmounted = true;
    if (unmounted) {
      const func = firebase.functions();
      const takeBank = func.httpsCallable('takeBank');
      takeBank().then(res => {
        dispatch(allAction({
          ...bank,
          bankName: res.data.bank_name,
          branchName: res.data.routing_number,
          kind: "Normal",
          number: res.data.routing_number,
          yourName: res.data.account_holder_name,
        }))
      }).catch((e) => {
        console.error(e, "maybe, If bank is none...");
      })
    }
    return () => {
      unmounted = false;
    }
  }, [bank, dispatch]);
  return (
    <IonPage>
      <IonHeader>
        <Header title="入金口座先の確認" left="back" center="title" right="none" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="account" />
            <main className="content__wrap">
              <section className="bank__list">
                <div className="bank__list__item">
                  <p className="bank__list__item__title">金融機関名</p>
                  <p className="bank__list__item__text">{noValueText(bank.bankName)}</p>
                </div>
                <div className="bank__list__item">
                  <p className="bank__list__item__title">支店名</p>
                  <p className="bank__list__item__text">{noValueText(bank.branchName)}</p>
                </div>
                <div className="bank__list__item">
                  <p className="bank__list__item__title">口座種別</p>
                  <p className="bank__list__item__text">{noValueText(bank.kind)}</p>
                </div>
                <div className="bank__list__item">
                  <p className="bank__list__item__title">口座番号</p>
                  <p className="bank__list__item__text">{noValueText(bank.number)}</p>
                </div>
                <div className="bank__list__item">
                  <p className="bank__list__item__title">口座名義</p>
                  <p className="bank__list__item__text">{noValueText(bank.yourName)}</p>
                </div>
              </section>
              <div className=" SP__btn__center">
                <button className="Btn__base PC__btn" onClick={handleClick}>内容を変更する</button>
              </div>
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Tabs" page="account" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};
export default BankAccount;
