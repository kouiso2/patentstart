//ionic
import { IonContent, IonFooter, IonHeader, IonPage } from '@ionic/react';
import 'assets/css/Bank.css';
import 'assets/css/haseryo.css';
// css
import 'assets/css/style.css';
//original
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
import firebase from 'firebase';
import useAuth from 'hooks/auth';
import React from 'react';
// XD56, XD57
interface PayoutsModel {
  amount: number;
  created: any;
}
export const Proceeds: React.FC = () => {
  const auth = useAuth();
  const [payout, setPayout] = React.useState<PayoutsModel[]>();
  const [payoutDate, setPayoutDate] = React.useState<Date>(new Date());
  const [count, setCount] = React.useState<number>(0);
  React.useEffect(() => {
    const today = new Date();
    today.setMonth(new Date().getMonth() + 1 + count);
    setPayoutDate(today);
  }, [setPayoutDate, count]);
  React.useEffect(() => {
    let unmounted = false;
    if (!unmounted) {
      const func = firebase.functions();
      const list = func.httpsCallable('payoutList');
      list(auth?.stripeId).then((res) => {
        setPayout(res.data);
      });
    }
    return () => {
      // cleanup
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      unmounted = true;
    }
  }, [setPayout, payoutDate, auth]);
  const payoutLists = React.useMemo(() => {
    return payout?.map((result: PayoutsModel, key: number) => {
      const arrivalDate = new Date(result.created * 1000).toLocaleDateString();
      return (
        <div className="bank__proceeds__box" key={key}>
          {/* 総額の塊 */}
          <div className="bank__proceeds__box--amount">
            <p className="date">{arrivalDate}</p>
            <div className="amount__text--flex">
              <p className="amount__text">総額</p>
              <div>
                <span className="amount__text--red">{result.amount}</span>
                <span className="amount__text amount__text--s">円</span>
              </div>
            </div>
          </div>
          <div className="bank__proceeds__box__title">内訳</div>
          {/* 内訳の塊 */}
          {/* <div className="bank__proceeds__box__item">
                  <p className="date">2021/01/11</p>
                  <div className="item__text--flex">
                    <div>
                      <span className="item__text">株式会社 特許申請</span>
                      <span className="item__text--s">様</span>
                    </div>
                    <div>
                      <span className="item__text--l">20,000</span>
                      <span className="item__text--s">円</span>
                    </div>
                  </div>
                </div> */}
          {/* 内訳の塊 */}
        </div>
      )
    })
  }, [payout]);
  return (
    <IonPage>
      <IonHeader>
        <Header title="売上確認" left="back" center="title" right="none" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="account" />
            <main className="content__wrap">
              <div className="bank__proceeds__bar">
                <button className="bank__proceeds__bar__btn" onClick={(e) => {
                  e.preventDefault();
                  //前の月
                  setCount(count + 1);
                }}>
                  {payoutDate.getMonth() - 1}月
                </button>
                <button className="bank__proceeds__bar--month">{`${payoutDate.getFullYear()}.${payoutDate.getMonth()}`}</button>
                <button className="bank__proceeds__bar__btn" onClick={(e) => {
                  e.preventDefault();
                  //次の月
                  setCount(count + 1);
                }}>
                  {payoutDate.getMonth() + 1}月
                </button>
              </div>
              {payoutLists}
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Tabs" page="account" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};
export default Proceeds;
