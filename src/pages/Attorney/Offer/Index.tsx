//ionic
import { IonContent, IonFooter, IonHeader, IonInput, IonPage } from '@ionic/react';
import 'assets/css/haseryo.css';
// css
import 'assets/css/style.css';
//original
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
import 'firebase/auth';
import 'firebase/firestore';
import useOffer from 'hooks/offer';
import React from 'react';
import { useParams } from 'react-router';
import { useLocation } from 'react-router-dom';
import AccordionContentsAbout from '../../../components/Accordion/AccordionContents/About';
import AccordionContentsApplicantInfo from '../../../components/Accordion/AccordionContents/ApplicantInfo';
import useApplication from '../../../hooks/application';


// XD39
/**
* AttorneyOffer:
* 弁理士のプランオファーページを表示する
*/
export const AttorneyOffer: React.FC = () => {
  const location = useLocation<string>();
  const { id } = useParams<{ id: string }>();
  const roomId = id;
  const appPath = location.state;
  const { app } = useApplication(appPath);
  const { offer, setOffer, handleSubmit, setRoomId } = useOffer(appPath);
  const [active, setActive] = React.useState<boolean>(false);

  React.useEffect(() => {
    setRoomId(roomId);
  }, [roomId, setRoomId]);

  const priceInput = React.useMemo(() => {
    if (active) {
      return (
        <div id="planBox" className="active" style={{ display: 'none' }}>
          <p className="form--plan--price--offer">オファー金額を入力してください</p>
          <div className="common__flex">
            <IonInput placeholder="料金" className="payment--card--new__input" type="number" value={offer?.price} onIonChange={(e) => {
              setOffer({
                ...offer,
                price: parseInt(e.detail.value!, 10)
              });
            }} /><span className="payment--card--new__yen">円</span>
          </div>
        </div>
      );
    }
  }, [active, setOffer, offer]);

  return (
    <IonPage>
      <IonHeader>
        <Header title="オファー" left="back" center="title" right="close" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="list" />
            <main className="content__wrap">
              <p className="form__title">{app.title}</p>
              <span className="form__badge--recruitment">募集中</span>
              <div className="accordion accordion__wrap"><AccordionContentsAbout appPath={location.state} /></div>
              <div className="accordion accordion__wrap"><AccordionContentsApplicantInfo applicantPath={app.creater?.path} /></div>
              <div className="form__title__wrap__flex">
                <span className="form__title">プラン</span><span className="form__badge--required">必須</span>
              </div>
              <form className="form" method="POST" onSubmit={handleSubmit}>
                <div className="form--plan">
                  <input type="radio" id="plan1" name="radio01" className="radioBtn__input" onClick={(): void => {
                    setOffer({
                      ...offer,
                      price: 30000
                    });
                    setActive(false);
                  }} />
                  <label htmlFor="plan1" className="radioBtn__label"></label>
                  <label htmlFor="plan1" className="form--plan--price">
                    <p className="form--plan--price__title">スタンダード</p>
                    {/* <p className="form--plan--price__text">スタンダードについての説明文を記載する</p> */}
                  </label>
                </div>
                <div className="form--plan">
                  <input className="radioBtn__input" type="radio" id="plan2" name="radio01" onClick={(): void => {
                    setOffer({
                      ...offer,
                      price: 50000
                    });
                    setActive(false);
                  }} />
                  <label htmlFor="plan2" className="radioBtn__label"></label>
                  <label htmlFor="plan2" className="form--plan--price">
                    <p className="form--plan--price__title">"ハイグレード"</p>
                    {/* <p className="form--plan--price__text">"ハイグレード"についての説明文を記載する</p> */}
                  </label>
                </div>
                <div className="form--plan" style={{ 'borderBottom': '1px solid #cccccc' }}>
                  <input type="radio" id="plan3" name="radio01" className="radioBtn__input" onClick={(): void => {
                    setOffer({
                      ...offer,
                      price: 100000
                    });
                    setActive(true);
                  }} />
                  <label htmlFor="plan3" className="radioBtn__label"></label>
                  <label htmlFor="plan3" className="form--plan--price">
                    <p className="form--plan--price__title">特別プラン</p>
                    {/* <p className="form--plan--price__text">特別プランについての説明文を記載する</p> */}

                    {/* チェックされたら表示される部分ここから */}
                    {priceInput}
                    {/* チェックされたら表示される部分ここまで */}
                  </label>
                </div>
                {/* <p className="form__title mt-30">備考欄</p>
                <IonTextarea className="form--plan--price__textarea"
                  placeholder="備考欄"
                  onIonChange={(e): void => {
                    setOffer({
                      ...offer,
                      remark: e.detail.value!
                    })
                  }} value={offer.remark}></IonTextarea> */}
                <div className=" SP__btn__center">
                  <button className="Btn__base SP__btn" type="submit">
                    オファーする
                  </button>
                </div>
              </form>
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Full" page="list" />
        </IonFooter>
      </IonContent>
    </IonPage >
  );
};

export default AttorneyOffer;
