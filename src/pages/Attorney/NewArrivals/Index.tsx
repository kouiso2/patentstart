//ionic
import { IonContent, IonFooter, IonHeader, IonPage } from '@ionic/react';
import 'assets/css/ArrivalsList.css';
// css
import 'assets/css/Home.css';
import 'assets/css/style.css';
// import ReactPaginate from 'react-paginate';
import { AllArrivalsListChild, MatchArrivalsListChild, NoneArrivalsListChild } from 'components/ArrivalsListChild';
//original
import Footer from 'components/Footer/Index';
import SorterSelect from 'components/Form/Select/SorterSelect';
import Header from 'components/Header/index';
import { Pagination, ThirdPagination } from 'components/Pagination/Index';
import Sidebar from 'components/SideBar/Index';
import { SorterContext, TabContext } from 'data/context';
import 'firebase/firestore';
import useMatter from 'hooks/matter';
import React, { useState } from 'react';
import { AppFireTypes } from '../../Applicant/Home';
//XD44
export const ArrivalsList: React.FC = () => {
  const {
    matter,
    newArrivals,
    sorter,
    setSorter,
    tab,
    setTab,
  } = useMatter();
  // paginationの番号,state管理
  const [page, setPage] = useState(1);
  const handlePages = (updatePage: number) => setPage(updatePage);
  // ページあたりに表示させる案件数
  const maxPage = 5;
  // totalPageNumberはif文以外使用しない
  // totalPageNumberとtotalPagesに案件数/ページあたりに表示させたい案件数を入れる
  const [totalPageNumber] = useState(Math.floor(newArrivals / maxPage));
  const totalPages = Math.floor(newArrivals / maxPage);
  // paginationの出力memo
  const pagination = React.useMemo(() => {
    // 5件以下の場合はページネーション非表示
    if (newArrivals < maxPage + 1) {
      return;
    };
    if (totalPageNumber === 3) {
      return (
        <ThirdPagination
          page={page}
          totalPages={totalPages}
          handlePagination={handlePages} />
      )
    } else {
      return (
        <Pagination
          page={page}
          totalPages={totalPages}
          handlePagination={handlePages} />
      )
    }
  }, [newArrivals, page, totalPageNumber, totalPages]);

  // 1ページに出力する案件数をsliceで出し分けてるのでその数字を管理
  const PageSlice = React.useMemo(() => {
    if (page === 1) {
      return 0;
    } else {
      return maxPage * page;
    }
  }, [page]);

  const matterItem = React.useMemo(() => {
    // tabのpropsで出し分け
    if (newArrivals <= 0) {
      return (
        <NoneArrivalsListChild />
      );
    };
    // sliceで案件出し分け
    return matter?.app?.slice(PageSlice, PageSlice + maxPage).map((value: AppFireTypes, key: number) => {
      if (tab) {//trueだったら条件に合う案件一覧
        // タブ切り替えた時にちゃんと表示されるようにする
        return (
          <MatchArrivalsListChild value={value} key={key} />
        );
      } else {//falseだったら全案件一覧
        return (
          <AllArrivalsListChild value={value} key={key} />
        );
      };
    });
  }, [newArrivals, matter, PageSlice, tab]);

  // const categorySelectForm = React.useMemo(() => {
  //   if (!tab) {
  //     return (
  //       <CategoryContext.Provider value={{ patentCategory, setPatentCategory }}>
  //         <CategorySelect />
  //       </CategoryContext.Provider>
  //     )
  //   }
  // }, [tab, patentCategory, setPatentCategory]);

  React.useEffect(() => {
    if (tab) {
      setPage(1);
    }
  }, [tab, setPage]);

  return (
    <TabContext.Provider value={{ tab, setTab }}>
      {/* <TabButton platform="sp" /> */}
      <IonPage>
        <IonHeader>
          <Header title="新規案件リスト" left="none" center="title" right="none" />
        </IonHeader>
        <IonContent scrollEvents={true}>
          <section className="inner">
            <div className="main__wrap">
              <Sidebar page="list" />
              <main className="content__wrap">
                <section className="arrivalsList ">
                  {/* <TabButton platform="pc" /> */}
                  <div className="arrivalsList__posts">
                    <h4 className="arrivalsList__posts__text arrivalsList__posts__text--show">表示案件数</h4>
                    <p className="arrivalsList__posts__text">{newArrivals}
                      <span className="arrivalsList__posts__text--unit">件</span>
                    </p>
                  </div>
                  <div className="arrivalsList__catOrder">
                    <div className="arrivalsList__catOrder__catWrap">
                      {/* {categorySelectForm} */}
                    </div>
                    <SorterContext.Provider value={{ sorter, setSorter }}>
                      <SorterSelect />
                    </SorterContext.Provider>
                  </div>
                  <div className="arrivalsList--hasResult">
                    {matterItem}
                  </div>
                  <div className="arrivalsList__pagenation">
                    {pagination}

                  </div>
                </section>
              </main>
            </div>
          </section>
          <IonFooter className="ion-no-border">
            <Footer format="Tabs" page="list" />
          </IonFooter>
        </IonContent>
      </IonPage>
    </TabContext.Provider>
  );
};

export default ArrivalsList;
