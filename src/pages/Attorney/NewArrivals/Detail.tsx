/* eslint-disable react-hooks/rules-of-hooks */
//ionic
import { IonContent, IonFooter, IonHeader, IonLoading, IonPage } from '@ionic/react';
import 'assets/css/Btn.css';
import 'assets/css/haseryo.css';
// css
import 'assets/css/style.css';
//original
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
import firebase from 'firebase/index';
import useApplication from 'hooks/application';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';
import { RootState } from 'store/Combine';
import { applicationConfigAction } from 'store/Config/action';
import AccordionContentsAbout from '../../../components/Accordion/AccordionContents/About';
import AccordionContentsApplicantInfo from '../../../components/Accordion/AccordionContents/ApplicantInfo';


//XD46
export const ArrivalsDetail: React.FC = () => {
  const dispatch = useDispatch();
  const config = useSelector((state: RootState) => state.config);
  const history = useHistory();
  const [showLoading, setShowLoading] = useState(false);
  const location = useLocation<string>();
  const path = location.state;

  const { app } = useApplication(path);
  const db = firebase.firestore();
  const auth = firebase.auth().currentUser;

  const RecruitPath = app?.recruit;
  const isRecruit = RecruitPath.map((elm: any) => {
    return elm?.id
  })

  const handleClick = React.useCallback(async (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault();
    const appRef = db.doc(path);
    const attorney = db.collection("User").doc(auth?.uid);
    const attorneyRef = await attorney.get();
    const attorneyItem = attorneyRef.data();
    const applicant = db.doc(appRef.parent.parent!.path);
    const roomRef = db.collection("Rooms");
    const timestamp = firebase.firestore.FieldValue.serverTimestamp();
    console.log(attorneyItem?.identification?.isIdentified);
    setShowLoading(true);
    if (!attorneyItem?.identification?.isIdentified || !attorneyItem?.identification?.isSubmitted) {//弁理士の本人確認が終わっていない場合
      history.push({
        pathname: `/attorney/reject`,
      });
    } else {
      console.log("app update start");
      await roomRef.add({
        applicantId: applicant.id,
        attorneyId: attorney.id,
        application: appRef,
        applicantUnreadNumber: 1,
        attorneyUnreadNumber: 1,
        lastMessageText: "チャットを開始しましょう",
        lastMessageTimestamp: timestamp,
        timestamp: timestamp,
        patentStatus: "Recruit"
      });

      await appRef.update({
        // console.log("room creating start");
        recruit: firebase.firestore.FieldValue.arrayUnion(firebase.firestore().collection("User").doc(auth?.uid)),
        updateDt: firebase.firestore.FieldValue.serverTimestamp(),
        updater: attorney,
      });

      console.log("room creating finish");
      dispatch(applicationConfigAction({
        ...config.application,
        path: app.path
      }))


      history.push({
        pathname: `/attorney/newarrivals/complete`,
      });
    }
    setShowLoading(false);

  }, [db, path, auth?.uid, config, app.path, history, dispatch]);

  // db.collection("User").doc(.parent.parent?.path).collection("Application").get().then(function (querySnapshot) {
  //   querySnapshot.forEach(function (doc) {
  //     console.log(doc.id, " => ", doc.data());
  //   });
  // });

  return (
    <IonPage>
      <IonHeader>
        <Header title={app.title} left="back" center="title" right="none" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <IonLoading
          cssClass='my-custom-class'
          isOpen={showLoading}
          onDidDismiss={() => setShowLoading(false)}
          message={'しばらくお待ち下さい...'}
          duration={500000}
        />
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="list" />
            <main className="content__wrap">
              <p className="form__title">{app.title}</p>
              <span className="form__badge--recruitment">募集中</span>
              <div className="accordion accordion__wrap">
                <AccordionContentsAbout appPath={app.path} />
              </div>
              <div className="accordion accordion__wrap">
                <AccordionContentsApplicantInfo applicantPath={app.creater?.path} />
              </div>
              {isRecruit.includes(auth?.uid) ?
                <div className="btn--center">
                  <button className="Btn__base__gray PC__btn" style={{ "pointerEvents": "none" }} onClick={handleClick}>応募済み</button>
                </div>
                :
                <React.StrictMode>
                  <div className="btn--center">
                    <button className="Btn__base PC__btn" onClick={handleClick}>応募する</button>
                  </div>
                  <div className="mt15"></div>
                  <span className="auth--regist__disc auth--regist__disc--center">応募したら、申請者にチャットを送ることができます。<br />チャットページから見積もりを依頼をすることができます。</span>
                </React.StrictMode>
              }


            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Tabs" page="list" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};

export default ArrivalsDetail;
