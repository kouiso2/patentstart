//ionic
import { IonContent, IonFooter, IonHeader, IonPage } from '@ionic/react';
import 'assets/css/haseryo.css';
// css
import 'assets/css/style.css';
//original
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
import React from 'react';
import { useHistory } from 'react-router-dom';
//XD47
export const ArrivalsComplete: React.FC = () => {
  //特許のタイトル
  // const {config, setConfig} = React.useContext(ConfigContext);
  // const location = useLocation<string>();
  // const roomId = location.state;
  const history = useHistory();
  // const { app } = useApplication(config.application.path);
  const handleClick = React.useCallback((e: React.MouseEvent) => {
    e.preventDefault();
    return history.push({
      pathname: `/attorney/home/`,
    });
  }, [history]);
  return (
    <IonPage>
      <IonHeader>
        <Header title={""} left="none" center="title" right="close" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="list" />
            <main className="content__wrap complete--page">
              <div className="complete__wrap">
                <h2 className="complete__title">応募が完了しました</h2>
                <p className="complete__text">応募が完了したので、申請者にチャットを送ることができます。<br />相談内容を確認し、最適なプランでオファーをしましょう。</p>
                <div className="btn--center">
                  <div className="Btn__base PC__btn" onClick={handleClick}>チャットを送る</div>
                </div>
              </div>
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Tabs" page="list" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};
export default ArrivalsComplete;
