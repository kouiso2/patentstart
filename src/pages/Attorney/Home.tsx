//ionic
import { IonContent, IonFooter, IonHeader, IonPage, isPlatform } from '@ionic/react';
// css
import 'assets/css/style.css';
import { AttorneyApplicationItem, NoneApplicationItem } from 'components/ApplicationItem';
//original
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
import firebase from 'firebase';
import 'firebase/auth';
import 'firebase/firestore';
import usePatentList from 'hooks/patentList/attorney';
import { AppFireTypes } from 'pages/Applicant/Home';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'store/Combine';
import { modalConfigAction } from 'store/Config/action';

//XD1,XD3
export const ApplicationList: React.FC = () => {
  const { recruit, apply, manage, recruitFunc, applyFunc, manageFunc, handleRecruitClick } = usePatentList();
  const dispatch = useDispatch();
  const config = useSelector((state: RootState) => state.config);
  const [ModalBoolean, setModalBoolean] = React.useState(Boolean);

  React.useEffect(() => {
    const auth = firebase.auth().currentUser;
    const dbRef = firebase.firestore().collection("User").doc(auth?.uid);
    // モーダルのbooleanを取得
    dbRef.onSnapshot((snap: firebase.firestore.DocumentSnapshot) => {
      setModalBoolean(snap.data()?.tutorial?.home);
    });
    if (ModalBoolean === true) {
      dbRef.update({
        tutorial: {
          home: false,
          chat: true
        }
      }).then(() => {
        dispatch(modalConfigAction({
          ...config.modal,
          isOpen: true,
          form: "AttorneyHomeTutorial"
        }));
      })
    } else {

    }
  }, [ModalBoolean, config, dispatch]);

  const recruitItem = React.useMemo(() => {
    recruitFunc();
    return recruit?.app?.map((item: AppFireTypes, key: number) => {
      if (item) {
        return (
          <div className="applicationList__bg" key={key} onClick={() => {
            handleRecruitClick(item.id, item.path);
          }}>
            <div>
              <span className="applicationList__status">応募中</span>
              <p className="applicationList__text">{item.title}</p>
            </div>
          </div>
        )
      } else {
        return (
          <React.StrictMode></React.StrictMode>
        );
      }
    });
  }, [recruit, recruitFunc, handleRecruitClick]);

  const applyItem = React.useMemo(() => {
    applyFunc();
    return apply?.app?.map((item: AppFireTypes, key: number) => {
      if (item) {
        return (
          <AttorneyApplicationItem item={item} kind="Apply" key={key} />
        );
      } else {
        return (
          <React.StrictMode></React.StrictMode>
        );
      }
    });
  }, [apply, applyFunc]);

  const manageItem = React.useMemo(() => {
    manageFunc();
    return manage?.app?.map((item: AppFireTypes, key: number) => {
      if (item) {
        return (
          <AttorneyApplicationItem item={item} kind="Manage" key={key} />
        )
      } else {
        return (
          <React.StrictMode></React.StrictMode>
        );
      }
    });
  }, [manage, manageFunc]);

  return (
    <IonPage>
      <IonHeader>
        <Header left="none" center="logo" right="none" />
      </IonHeader>
      <IonContent>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="home" />
            <main className="content__wrap">
              <div className="applicationList__requests">
                {//ios・androidの場合と、それ以外で条件分岐
                  (isPlatform("ios") || isPlatform("android")) ?
                    <React.StrictMode></React.StrictMode> :
                    <React.StrictMode>
                      <div className="recruitment__flex">
                        <h3 className="chatlist__wrap__title recruitment__title applicationList__requests__title">応募した特許一覧</h3>
                        <span className="recruitment__number">全{recruit?.count}件</span>
                      </div>
                      {recruitItem}
                      {recruit?.count === 0 ? <NoneApplicationItem /> : <React.StrictMode></React.StrictMode>}
                    </React.StrictMode>
                }
                <div className="recruitment__flex">
                  <h3 className="chatlist__wrap__title recruitment__title applicationList__requests__title">申請中の特許一覧</h3>
                  <span className="recruitment__number">全{apply?.count}件</span>
                </div>
                {applyItem}
                {apply?.count === 0 ? <NoneApplicationItem /> : <React.StrictMode></React.StrictMode>}
                <div className="recruitment__flex">
                  <h3 className="chatlist__wrap__title recruitment__title applicationList__requests__title">管理している特許一覧</h3>
                  <span className="recruitment__number">全{manage?.count}件</span>
                </div>
                {manageItem}
                {manage?.count === 0 ? <NoneApplicationItem /> : <React.StrictMode></React.StrictMode>}
              </div>
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Tabs" page="home" />
        </IonFooter>
      </IonContent>
    </IonPage>

  );
};

export default ApplicationList;
