//ionic
import { IonContent, IonFooter, IonHeader, IonPage } from '@ionic/react';
import 'assets/css/haseryo.css';
// css
import 'assets/css/style.css';
//original
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
import { noValueText } from 'controller';
//firebase
import firebase from 'firebase';
import 'firebase/firestore';
import 'firebase/storage';
import useAuth from 'hooks/auth';
import React from 'react';
import { useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router';
import { RootState } from 'store/Combine';
//XD32
export const PatentCheck: React.FC = () => {
  const history = useHistory();
  const patent = useSelector((state: RootState) => state.patent);
  const location = useLocation<string>();
  const patentPath = location.state;
  const auth = useAuth();
  console.log(patent);

  const knownDate = patent.knownDate?.getFullYear() + "-" + ("00" + (patent.knownDate?.getMonth() + 1)).slice(-2) + "-" + ("00" + patent.knownDate?.getDate()).slice(-2);
  const fillingDate = patent.fillingDate?.getFullYear() + "-" + ("00" + (patent.fillingDate?.getMonth() + 1)).slice(-2) + "-" + ("00" + patent.fillingDate?.getDate()).slice(-2);

  const handleFixClick = React.useCallback(async (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault();
    return history.goBack();
  }, [history]);

  const handleSaveClick = React.useCallback(async (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault();
    const db = firebase.firestore();
    const storage = firebase.storage();
    const patentRef = db.doc(patentPath);
    const docRef = storage.ref().child(`patent/document/${patent.document?.name}`);
    const docRes = await docRef.put(patent.document, { contentType: `${patent.document?.type}` });
    const docUrl = await docRes.ref.getDownloadURL();
    await patentRef.update({
      ...patent,
      application: firebase.firestore().doc(patent.application),
      document: docUrl,
      rightHolder: auth?.personName,
      createDt: firebase.firestore.FieldValue.serverTimestamp(),
      updateDt: firebase.firestore.FieldValue.serverTimestamp(),
    });
    return history.push({
      pathname: `/attorney/application/apply/${firebase.firestore().doc(patent.application).id}`,
      state: patent.application
    });
  }, [history, patentPath, auth, patent]);


  return (
    <IonPage>
      <IonHeader>
        <Header title={patent.developName} left="back" center="title" right="close" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="home" />
            <main className="content__wrap">
              <p className="common__title">特許の詳細</p>

              <div className="bank__info">
                <div className="bank__info__item">
                  <p className="common__form__title">特許内容の説明文</p>
                  <p className="common__form__title--gray--bg common__form__title--gray--bg--l">{noValueText(patent.explanation)}</p>
                </div>
                <p className="common__title">申請情報</p>
                <div className="bank__info__item bank__info__item--half">
                  <p className="common__form__title">文献番号</p>
                  <p className="common__form__title--gray--bg">{noValueText(patent.referenceNum)}</p>
                </div>
                <div className="bank__info__item">
                  <p className="common__form__title">出願番号</p>
                  <p className="common__form__title--gray--bg">{noValueText(patent.fillingNum)}</p>
                </div>

                <div className="bank__info__item">
                  <p className="common__form__title">出願日</p>
                  <p className="common__form__title--gray--bg">{noValueText(patent.fillingDate, fillingDate)}</p>
                </div>
                <div className="bank__info__item">
                  <p className="common__form__title">公知日</p>
                  <p className="common__form__title--gray--bg">{noValueText(patent.knownDate, knownDate)}</p>
                </div>

                <div className="bank__info__item">
                  <p className="common__form__title">発明の名称</p>
                  <p className="common__form__title--gray--bg">{noValueText(patent.developName)}</p>
                </div>

                <div className="bank__info__item">
                  <p className="common__form__title">出願人・権利者</p>
                  <p className="common__form__title--gray--bg">{patent.rightHolder ? patent.rightHolder : auth?.personName}</p>
                </div>

                <div className="bank__info__item">
                  <p className="common__form__title">FI</p>
                  <p className="common__form__title--gray--bg">{noValueText(patent.fi)}</p>

                </div>

                <div className="bank__info__item">
                  <p className="common__form__title">PDFファイルの添付</p>
                  <li className="file__item">{noValueText(patent.document, patent.document.name)}
                    {/* <Link to={`${patent.document}`} className="file__item__link">プレビュー</IonRouterLink> */}
                  </li>
                </div>
                <div className="bank__info__btn-box">
                  <button className="bank__info__btn" onClick={handleFixClick}>訂正する</button>
                  <button className="bank__info__btn" onClick={handleSaveClick}>保存する</button>
                </div>
              </div>
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Tabs" page="home" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};

export default PatentCheck;
