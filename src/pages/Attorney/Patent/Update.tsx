//ionic
import { IonContent, IonFooter, IonHeader, IonInput, IonPage, IonTextarea } from '@ionic/react';
import 'assets/css/Form.css';
import 'assets/css/haseryo.css';
// css
import 'assets/css/style.css';
//original
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
import firebase from 'firebase';
import 'firebase/firestore';
import 'firebase/storage';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router';
import { RootState } from 'store/Combine';
import { allPatentAction, developNameAction, documentAction, explanationAction, fiAction, fillingDateAction, fillingNumAction, jplatUrlAction, knownDateAction, referenceNumAction } from 'store/Patent/action';

//XD31
export const PatentUpdate: React.FC = () => {
  const history = useHistory();
  const location = useLocation<string>();
  const patentPath = location.state;

  const dispatch = useDispatch();
  const docInput = React.useRef<HTMLInputElement>(null);

  const patent = useSelector((state: RootState) => state.patent);

  const handleSubmit = React.useCallback((e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (docInput.current?.files![0]) {
      dispatch(documentAction(docInput.current?.files![0]));
    }
    return history.push({
      pathname: '/attorney/application/check/',
      state: patentPath
    });
  }, [history, dispatch, patentPath]);

  const handleFileChange = React.useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
    e.preventDefault();
    const file = e.target.files![0];
    const type = file.type;
    const size = file.size;
    const limit = 2000000;
    if (type !== "application/pdf") {
      e.target.value = "";
      alert("pdfの形式でアップロードして下さい");
      return;
    }

    if (size > limit) {
      e.target.value = "";
      alert('PDFは2MB以下で指定してください');
      return;
    };

    dispatch(documentAction(file));
  }, [dispatch]);

  React.useEffect(() => {
    if (patentPath) {
      const db = firebase.firestore().doc(patentPath);
      const storage = firebase.storage();
      db.onSnapshot((snap) => {
        const fillingDate: firebase.firestore.Timestamp = snap.data()?.fillingDate;
        const knownDate: firebase.firestore.Timestamp = snap.data()?.knownDate;
        if (snap) {
          dispatch(allPatentAction({
            ...snap.data(),
            id: snap.id,
            path: snap.ref.path,
            fillingDate: fillingDate?.toDate(),
            knownDate: knownDate?.toDate(),
            document: snap.data()?.document ? storage.refFromURL(snap.data()?.document) : ""
          }))
        }
      });
    }
  }, [dispatch, patentPath]);
  return (
    <IonPage>
      <IonHeader>
        <Header title={patent.developName} left="back" center="title" right="close" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="home" />
            <main className="content__wrap">
              <p className="common__title">特許の詳細</p>
              <form method="post" onSubmit={handleSubmit}>
                <p className="common__form__title mt-20">特許内容の説明文</p>
                <IonTextarea
                  placeholder="特許の内容を記載する"
                  className="form__input form__input--l"
                  value={patent.explanation} onIonChange={(e) => {
                    dispatch(explanationAction(e.detail.value!));
                  }} />

                <p className="common__title mt-56">申請情報</p>
                <p className="common__form__title">文献番号</p>
                <IonInput
                  placeholder="00000000000000000000"
                  className="form__input"
                  type="text" value={patent.referenceNum}
                  onIonChange={(e) => {
                    dispatch(referenceNumAction(e.detail.value!));
                  }} />
                <p className="common__form__title">出願番号</p>
                <IonInput
                  placeholder="00000000000000000000"
                  className="form__input"
                  type="text" value={patent.fillingNum}
                  onIonChange={(e) => {
                    dispatch(fillingNumAction(e.detail.value!));
                  }} />
                <p className="common__form__title">出願日</p>
                <IonInput
                  placeholder="2021/10/10"
                  className="form__input form__input--half"
                  type="date"
                  value={patent.fillingDate?.getFullYear() + "-" + ("00" + (patent.fillingDate?.getMonth() + 1)).slice(-2) + "-" + ("00" + patent.fillingDate?.getDate()).slice(-2)}
                  onIonChange={(e) => {
                    dispatch(fillingDateAction(new Date(e.detail.value!)));
                  }} />
                <p className="common__form__title">公知日</p>
                <IonInput placeholder="2021/10/20"
                  className="form__input form__input--half"
                  type="date"
                  value={patent.knownDate?.getFullYear() + "-" + ("00" + (patent.knownDate?.getMonth() + 1)).slice(-2) + "-" + ("00" + patent.knownDate?.getDate()).slice(-2)} onIonChange={(e) => {
                    dispatch(knownDateAction(new Date(e.detail.value!)));
                  }} />
                <p className="common__form__title">発明の名前</p>
                <IonInput placeholder="特許を申請できるアプリ"
                  className="form__input" type="text"
                  value={patent.developName} onIonChange={(e) => {
                    dispatch(developNameAction(e.detail.value!));
                  }} />
                <p className="common__form__title">FI</p>
                <IonInput placeholder="0000000000000000"
                  className="form__input" type="text"
                  value={patent.fi} onIonChange={(e) => {
                    dispatch(fiAction(e.detail.value!));
                  }} />

                <p className="common__form__title">J-plat pat URL</p>
                <IonInput
                  placeholder="https://www.j-platpat.inpit.go.jp/"
                  className="form__input" type="url" value={patent.jplatUrl} onIonChange={(e) => {
                    dispatch(jplatUrlAction(e.detail.value!));
                  }} />
                <p className="common__title mt-56">申請書類</p>

                <p className="common__form__title">ファイルの添付</p>
                <label htmlFor="file1" className="identification__input">ファイルをアップロード</label>
                <input type="file" name="document" id="file1" ref={docInput} onChange={handleFileChange} />
                <p className="identification__input__name">{patent.document?.name}</p>
                <p className="identification__disctext">申請内容に関する資料などPDFファイルをアップロードできます。<br />
                  ※2MB以下
                  </p>
                <div className=" SP__btn__center">
                  <button className="Btn__base SP__btn" type="submit">
                    確認する
                  </button>
                </div>
              </form>
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Tabs" page="home" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};

export default PatentUpdate;
