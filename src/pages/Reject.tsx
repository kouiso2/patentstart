//ionic
import { IonContent, IonFooter, IonHeader, IonPage } from '@ionic/react';
//css
import 'assets/css/style.css';
//original
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';


const Reject: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <Header title="プロフィールの変更" left="none" center="title" right="close" />
      </IonHeader>
      <IonContent>
        <section className="inner">
          <p>アプリから会員登録は出来ません</p>
          <p>お手数ですがWebサイトからお手続きください</p>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Full" page="home" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
}

export default Reject;
