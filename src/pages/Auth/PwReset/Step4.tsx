import { IonContent, IonFooter, IonHeader, IonPage } from '@ionic/react';
import 'assets/css/haseryo.css';
import 'assets/css/Login.css';
import 'assets/css/style.css';
import PwResetBtn from 'components/Button/PwResetBtn';
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
import SingleTerms from 'components/Terms';
//img
import 'firebase/auth';
import React from 'react';

//XD74
export const Step4: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <Header left="none" center="logo" right="none" />
      </IonHeader>
      <IonContent>
        <section className="inner inner--auth--sp">
          <div className="inner--auth">
            <h2 className="auth--select__title">再設定完了</h2>
            <p className="auth__heading">パスワードの再設定が完了しました。<br />下記よりログイン下さい。</p>
            <PwResetBtn />
          </div>
          <SingleTerms />
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Full" page="home" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};

export default Step4;
