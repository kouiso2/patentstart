import { IonContent, IonFooter, IonHeader, IonInput, IonPage } from '@ionic/react';
import 'assets/css/haseryo.css';
import 'assets/css/Login.css';
import 'assets/css/style.css';
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
import SingleTerms from 'components/Terms';
//img
import firebase from 'firebase';
import 'firebase/auth';
import React from 'react';
import { useHistory } from 'react-router';

interface PasswordFormModel {
  current: string;
  new: string;
  confirm: string;
}

//XD73
export const Step3: React.FC = () => {
  const history = useHistory();
  const [password, setPassword] = React.useState<PasswordFormModel>({
    current: "",
    confirm: "",
    new: ""
  });

  const [validate, setValidate] = React.useState<boolean>(false);

  const handleSubmit = React.useCallback((e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const currentPasswordCheck = async () => {
      const auth = firebase.auth().currentUser;
      const result = await firebase.auth().signInWithEmailAndPassword(auth?.email!, password.current);
      if (result.user) {
        return await result.user.updatePassword(password.new);
      };
    };
    currentPasswordCheck().then(() => {
      if (!validate) {
        return history.push('/auth/pwreset/fourth/');
      };
    })
  }, [history, password, validate]);

  const validatePassword = React.useCallback(() => {
    if (password.new !== password.confirm) {
      setValidate(true);
    } else {
      setValidate(false);
    }
  }, [password.new, password.confirm, setValidate]);
  return (
    <IonPage>
      <IonHeader>
        <Header left="none" center="logo" right="none" />
      </IonHeader>
      <IonContent>
        <section className="inner inner--auth--sp">
          <div className="inner--auth">
            <h2 className="auth--select__title">パスワード再設定</h2>
            <p className="auth__heading">認証がされました。<br />新しいパスワードを入力してください。</p>
            <form action="post" className="container" onSubmit={handleSubmit}>
              <p className="auth__input__text">新しいパスワード</p>
              <IonInput
                autocomplete="new-password"
                placeholder="パスワード"
                className="form__input"
                type="password"
                id="password"
                name="password"
                min="8"
                max="128"
                value={password?.new}
                required={true}
                onIonChange={(e) => {
                  e.preventDefault();
                  setPassword({
                    ...password,
                    new: e.detail.value!
                  })
                }}
              />
              <span className="auth--regist__disc">※半角英数字を含む8〜20桁で入力して下さい。</span>
              <p className="auth__input__text">新しいパスワード（確認用）</p>
              <IonInput
                autocomplete="new-password"
                placeholder="パスワード"
                className="form__input"
                type="password"
                id="password"
                name="password"
                min="8"
                max="128"
                value={password?.confirm}
                required={true}
                onIonBlur={validatePassword}
                onIonChange={(e) => {
                  e.preventDefault();
                  setPassword({
                    ...password,
                    confirm: e.detail.value!
                  })
                }}
              />
              <p className="validate__text">
                {validate ? "確認用のパスワードと一致しません" : ""}
              </p>
              <div style={{ marginTop: "4rem" }}>
                <div className="btn--center">
                  <button className="Btn__base PC__btn" type="submit">次へ</button>
                </div>
              </div>
            </form>

          </div>
          <SingleTerms />

        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Full" page="home" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};

export default Step3;
