//ionic
import { IonContent, IonFooter, IonHeader, IonInput, IonPage } from '@ionic/react';
import 'assets/css/Btn.css';
import 'assets/css/haseryo.css';
//css
import 'assets/css/Login.css';
import 'assets/css/style.css';
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
import SingleTerms from 'components/Terms';
//firebase
import firebase from 'firebase';
import 'firebase/auth';
import React from 'react';
import { Link, useHistory } from 'react-router-dom';


//XD72
export const Step2: React.FC = () => {
  const history = useHistory();
  const [email, setEmail] = React.useState<string>();

  async function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    if (email) {
      await firebase.auth().sendPasswordResetEmail(email);
      history.push("/auth/signup/complete/");
    } else {
    }
  }

  return (
    <IonPage>
      <IonHeader>
        <Header left="none" center="logo" right="none" />
      </IonHeader>
      <IonContent>
        <section className="innerinner--auth--sp ">
          <div className="inner--auth">
            <h2 className="auth--select__title">パスワード再設定</h2>
            <p className="auth__heading">ご登録いただいているメールアドレスを入力して下さい。</p>
            <form className="container" onSubmit={handleSubmit} >
              <p className="auth__input__text">認証キー</p>
              <IonInput type="email" className="auth__input" name="email" id="email" value={email} onIonChange={(e) => {
                e.preventDefault();
                setEmail(e.detail.value!);
              }} />
              <span className="auth--regist__disc">ご登録いただいているメールアドレスに認証キーを送信します。メールアドレスをお忘れの場合は、お手数ですが<Link to="/auth/contact/" className="auth--link--primary">こちら</Link>からお問い合わせ下さい。</span>
              <div>
                <div className="btn--center">
                  <button className="Btn__base PC__btn" type="submit">次へ</button>
                </div>
                <Link to="/auth/top/" style={{ textAlign: 'center', marginTop: '20px' }} className="auth--regist__disc">トップページに戻る</Link>
              </div>
            </form>

          </div>
          <SingleTerms />

        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Full" page="home" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};

export default Step2;
