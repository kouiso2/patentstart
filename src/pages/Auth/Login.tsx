//ionic
import { IonContent, IonFooter, IonHeader, IonInput, IonPage } from '@ionic/react';
import 'assets/css/Btn.css';
import 'assets/css/haseryo.css';
//css
import 'assets/css/Login.css';
import 'assets/css/style.css';
//original
import RegistBtn from 'components/Button/RegistBtn';
import SnsBtn from 'components/Button/SnsBtn';
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
import SingleTerms from 'components/Terms';
import 'firebase/auth';
import useLogin from 'hooks/login';
import React from 'react';
import { Link } from 'react-router-dom';
// import SnsBtn from '../../components/Button/SnsBtn';



//XD71
const Login: React.FC = () => {
  const { email, setEmail, password, setPassword, handleLoginSubmit } = useLogin();
  return (
    <IonPage>
      <IonHeader>
        <Header left="none" center="logo" right="none" />
      </IonHeader>
      <IonContent >
        <section className="inner">
          <div className="inner--auth">
            <p className="auth__heading">すでに会員登録済みの方はご登録のメールアドレス、<br />パスワードを入力し、ログインして下さい。</p>
            <form method="POST" className="container" onSubmit={handleLoginSubmit}>
              <p className="auth__input__text">メールアドレス</p>
              <IonInput
                autocomplete="email"
                placeholder="メールアドレス"
                className="auth__input"
                type="email"
                id="email"
                name="email"
                min="8"
                max="128"
                value={email}
                required={true}
                onIonChange={(e) => {
                  e.preventDefault();
                  setEmail(e.detail.value!);
                }}
              />
              <p className="auth__input__text">パスワード</p>
              <IonInput
                autocomplete="current-password"
                placeholder="パスワード"
                className="auth__input"
                type="password"
                id="password"
                name="password"
                min="8"
                max="128"
                value={password}
                required={true}
                onIonChange={(e) => {
                  e.preventDefault();
                  setPassword(e.detail.value!);
                }}
              />
              <Link to="/auth/pwreset/first/" className="auth--link--pass">パスワードお忘れの方はこちら</Link>
              <div className="btn--center">
                <button type="submit" className="Btn__base__login PC__btn">ログイン</button>
              </div>
            </form>

            {/* <SnsBtn /> */}

            {/* //ios・androidの場合と、それ以外で条件分岐} */}
            {/* (isPlatform("ios") || isPlatform("android")) ? */}
            <React.StrictMode></React.StrictMode> :
            <div>
              <h2 className="auth__title">別サービスでログイン</h2>
              <SnsBtn />
              <h2 className="auth__title">初めての方は登録手続きへお進みください</h2>
              <RegistBtn />
            </div>

          </div>
          <SingleTerms />

        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Full" page="home" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};
export default Login;
