//ionic
import { IonContent, IonFooter, IonHeader, IonPage, isPlatform } from '@ionic/react';
import 'assets/css/Btn.css';
import 'assets/css/LoginPassReset.css';
// css
import 'assets/css/style.css';
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
// originalComponent
import ApplicantIcon from 'components/Icon/Applicant';
import AttorneyIcon from 'components/Icon/Attorney';
//push notification
import { Push } from 'components/PushNotification/app/Push';
import { MultiTerms } from 'components/Terms';
import { accountChoice } from 'data/choice';
import AttorneyProfileInit from 'data/initial';
//firebase
import firebase from 'firebase';
import 'firebase/auth';
import 'firebase/firestore';
import useAuth from 'hooks/auth';
import React from 'react';
import { useHistory } from 'react-router';

export const SignupSelect: React.FC = () => {
  const auth = useAuth();
  const history = useHistory();
  const [select, setSelect] = React.useState<typeof accountChoice[number]>("None");

  const handleSubmit = React.useCallback((e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const user = firebase.auth().currentUser;
    firebase.firestore().collection("User").doc(user?.uid).set({
      ...AttorneyProfileInit,
      kind: select,
      createDt: firebase.firestore.FieldValue.serverTimestamp(),
      personName: user?.displayName,
      email: user?.email,
      phoneNumber: user?.phoneNumber,
      image: user?.photoURL,
      tutorial: {
        home: false,
        chat: false
      }
    }).then(() => {
      //push通知デバイストークン登録
      if(isPlatform('ios') || isPlatform('android')) {
        const push = new Push('');
        push.push();
      }

      if (select === "Attorney") {
        return history.push('/attorney/profile/update');
      } else if (select === "Applicant") {
        return history.push('/applicant/profile/update');
      } else {
        return history.push('/auth/login');
      }
    }).catch((err) => {
      console.error(err, "エラーが返されました");
    })

  }, [select, history]);

  const submitButton = React.useMemo(() => {
    if (select === "None") {
      return (<button id="FocusBtn" className="Btn__base PC__btn btn--regist__btn" type="submit">次へ</button>);
    } else {
      return (<button id="FocusBtn" className="Btn__base PC__btn btn--regist__btn selected" type="submit">次へ</button>)
    }
  }, [select]);


  React.useEffect(() => {
    if (
      auth?.kind === "Attorney" ||
      auth?.kind === "Applicant" ||
      auth?.kind === "Admin") {
      history.push('/auth/login');
    }
  }, [auth?.kind, history]);

  return (
    <IonPage className="bg-gray">
      <IonHeader>
        <Header left="none" center="logo" right="none" />
      </IonHeader>
      <IonContent>
        <section className="inner inner--auth--sp">
          <section className="inner--auth--select ">
            <h2 className="auth--select__title">新規登録</h2>
            <p className="auth--select__disc">どちらでご登録するか選択してください。</p>
            <form name="UserSelectForm" onSubmit={handleSubmit}>
              <div className="auth--select__btns">
                <input name="usertype" value="Applicant" id="select1" type="radio" onClick={() => {
                  setSelect("Applicant");
                }} />
                <label htmlFor="select1" className="auth--select__btns__btn">
                  <p className="btn__text">特許申請する方はこちら</p>
                  <ApplicantIcon />
                </label>
                <input name="usertype" value="Attorney" id="select2" type="radio" onClick={() => {
                  setSelect("Attorney");
                }} />
                <label htmlFor="select2" className="auth--select__btns__btn">
                  <p className="btn__text">弁理士の方はこちら</p>
                  <AttorneyIcon />
                </label>
              </div>
              <div className="btn--center btn--regist">
                {submitButton}
              </div>
            </form>
            <MultiTerms />
          </section>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Full" page="home" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};

export default SignupSelect;
