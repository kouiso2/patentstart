//ionic
import { IonContent, IonFooter, IonHeader, IonPage } from '@ionic/react';
// css
import 'assets/css/style.css';
import firebase from 'firebase';
//customhook
import useAuth from 'hooks/auth';
import React from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { RootState } from 'store/Combine';
import Footer from '../../components/Footer/Index';
//original
import Header from '../../components/Header/index';



//XD101
export const AuthContact: React.FC = () => {
  const history = useHistory();
  const auth = useAuth();
  const app = useSelector((state: RootState) => state.application);

  async function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    let summary = "";
    let content = (e.currentTarget.elements[4] as HTMLTextAreaElement).value;

    if ((e.currentTarget.elements[0] as HTMLInputElement).checked) {
      summary = "メールアドレスを忘れた";
    } else if ((e.currentTarget.elements[1] as HTMLInputElement).checked) {
      summary = "データ復旧のお問い合わせ";
    } else if ((e.currentTarget.elements[2] as HTMLInputElement).checked) {
      summary = "ご意見・ご要望";
    } else {
      summary = "その他のお問い合わせ";
    }

    const db = firebase.firestore().collection("Contact");
    await db.add({
      userId: auth?.uid,
      name: auth?.firstName+auth?.lastName,
      email: auth?.email,
      summary: summary,
      content: content,
      timestamp: firebase.firestore.FieldValue.serverTimestamp()
    });
    return history.push('/auth/login/');
  }

  return (
    <IonPage>
      <IonHeader>
        <Header title={app.title} left="back" center="title" right="none" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <section className="inner inner--auth--sp">
          <div className="inner--auth" style={{ minHeight: '550px' }}>
            <main>
              <form method="post" onSubmit={handleSubmit}>
                <div className="p-applicantContact__status">
                  <h3 className="p-applicantContact__title">お問い合わせ内容</h3>
                  <span className="form__badge--required">必須</span>
                </div>
                <div className="form">
                  <div className="form--plan p-applicantContact__form">
                    <input type="radio" id="plan1" name="contact" className="radioBtn__input" />
                    <label htmlFor="plan1" className="radioBtn__label"></label>
                    <label htmlFor="plan1" className="form--plan--price">
                      <p className="form--plan--price__title">メールアドレスを忘れた</p>
                    </label>
                  </div>
                  <div className="form--plan p-applicantContact__form">
                    <input type="radio" id="plan2" name="contact" className="radioBtn__input" />
                    <label htmlFor="plan2" className="radioBtn__label"></label>
                    <label htmlFor="plan2" className="form--plan--price">
                      <p className="form--plan--price__title">データ復旧のお問い合わせ</p>
                    </label>
                  </div>
                  <div className="form--plan p-applicantContact__form">
                    <input type="radio" id="card3" name="contact" />
                    <div className="form--plan--price">
                      <p className="form--plan--price__title">ご意見・ご要望</p>
                    </div>
                  </div>
                  <div className="form--plan p-applicantContact__form">
                    <input type="radio" id="card3" name="contact" className="radioBtn__input" />
                    <label htmlFor="card3" className="radioBtn__label"></label>
                    <label htmlFor="card3" className="form--plan--price">
                      <p className="form--plan--price__title">その他のお問い合わせ</p>
                    </label>
                  </div>
                  <div>
                    <div className="p-applicantContact__status--mt">
                      <p className="form__title p-applicantContact__form__title">具体的に内容を記入してください</p>
                      <span className="form__badge--required">必須</span>
                    </div>
                    <textarea className="form--plan--price__textarea p-applicantContact__textarea"></textarea>
                    <div className=" SP__btn__center p-applicantContact__btn">
                      <button className="Btn__base SP__btn p-applicantContact__btn--b" >
                        送信する
                      </button>
                    </div>
                  </div>
                </div>
              </form>
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Full" page="home" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};

export default AuthContact;
