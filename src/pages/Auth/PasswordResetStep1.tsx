import { IonContent, IonFooter, IonHeader, IonPage } from '@ionic/react';
import 'assets/css/haseryo.css';
//css
import 'assets/css/Login.css';
import 'assets/css/style.css';
import PwResetBtn from 'components/Button/PwResetBtn';
import SingleTerms from 'components/Terms';
import { Link } from 'react-router-dom';
import Footer from '../../components/Footer/Index';
import Header from '../../components/Header/index';




export const PasswordResetStep1: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <Header left="none" center="logo" right="none" />
      </IonHeader>
      <IonContent>
        <section className="inner">
          <div className="inner--auth">
            <h2 className="auth--select__title">パスワード再設定</h2>
            <p className="auth__heading">ご登録いただいているメールアドレスを入力して下さい。</p>
            <form action="post" className="container" >
              <p className="auth__input__text">メールアドレス</p>
              <input type="email" className="auth__input" name="email" id="email" />
              <span className="auth--regist__disc">ご登録いただいているメールアドレスに認証キーを送信します。メールアドレスをお忘れの場合は、お手数ですが<Link to="/auth/contact/" className="auth--link--primary">こちら</Link>からお問い合わせ下さい。</span>
              <PwResetBtn />
            </form>

          </div>
          <SingleTerms />
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Full" page="home" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};

export default PasswordResetStep1;
