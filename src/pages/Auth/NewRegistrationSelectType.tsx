import { IonContent, IonFooter, IonHeader, IonPage } from '@ionic/react';
import 'assets/css/LoginPassReset.css';
// css
import 'assets/css/style.css';
import RegistBtn from 'components/Button/RegistBtn';
import SingleTerms from 'components/Terms';
import React from 'react';
import Footer from '../../components/Footer/Index';
import Header from '../../components/Header/index';




export const NewRegistrationSelectType: React.FC = () => {
  function SwapMail() {
    document.getElementById('mail')?.classList.add('active');
    document.getElementById('sns')?.classList.remove('active');
    document.getElementById('mailBtn')?.classList.add('active__tab');
    document.getElementById('snsBtn')?.classList.remove('active__tab');
  }
  function SwapSns() {
    document.getElementById('sns')?.classList.add('active');
    document.getElementById('mail')?.classList.remove('active');
    document.getElementById('snsBtn')?.classList.add('active__tab');
    document.getElementById('mailBtn')?.classList.remove('active__tab');
  }
  return (
    <IonPage>
      <IonHeader>
        <Header left="none" center="logo" right="none" />
      </IonHeader>
      <IonContent>
        <section className="inner ">
          <div className="inner--auth">
            <div className="auth--regist__tabs">
              <button id="mailBtn" onClick={SwapMail} className="auth--regist__tabs__tab active__tab">メールアドレスで登録</button>
              <button id="snsBtn" onClick={SwapSns} className="auth--regist__tabs__tab">その他サービスで登録</button>
            </div>
            <p className="auth--regist__heading">ご登録のメールアドレス、パスワードを入力し、新規アカウントを登録してください。</p>


            <div id="mail" className="active display__none">
              <form action="post" className="container">
                <p className="auth__input__text">メールアドレス</p>
                <input type="email" className="auth__input" name="email" id="email" />
                <p className="auth__input__text">メールアドレス（確認用）</p>
                <input type="email" className="auth__input" name="email" id="email" />
                <p className="auth__input__text">パスワード</p>
                <input className="auth__input" type="password" name="password" id="password" placeholder="" />
                <span className="auth--regist__disc">※半角英数字を含む8〜20桁で入力して下さい。</span>
                <RegistBtn />
              </form>
              <div className="auth--regist--undertext">
                <span className="auth--regist__disc">「登録」ボタンをクリックすることで、<a href="https://patentstart.jp/terms.html" className="auth--link--primary">ご利用規約</a>・<a href="https://patentstart.jp/privacy.html" target="_blank" rel="noreferrer" className="auth--link--primary">プライバシーポリシー</a>に同意したものとみなします。</span>
              </div>
            </div>

            <div id="sns" className="display__none">
              <form action="">
                {/* <SnsBtn /> */}
                <div className="auth--regist--undertext">
                  <span className="auth--regist__disc auth--regist__disc--snsbtn">「登録」ボタンをクリックすることで、<span className="auth--link--primary">ご利用規約</span>・<span className="auth--link--primary">プライバシーポリシー</span><br />に同意したものとみなします。</span>
                </div>
                <RegistBtn />
              </form>
            </div>


          </div>
          <SingleTerms />
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Full" page="home" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};

export default NewRegistrationSelectType;
