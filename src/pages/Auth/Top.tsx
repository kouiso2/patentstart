import { IonContent, IonFooter, IonHeader, IonPage, isPlatform } from '@ionic/react';
import 'assets/css/Btn.css';
import 'assets/css/haseryo.css';
import 'assets/css/Login.css';
import 'assets/css/style.css';
//img
import TopLogo from 'assets/img/toplogo.svg';
import LoginBtn from 'components/Button/LoginBtn';
import RegistBtn from 'components/Button/RegistBtn';
import React from 'react';
import Footer from '../../components/Footer/Index';
import Header from '../../components/Header/index';
import Terms from '../../components/Terms';

const Top: React.FC = () => {
  //XD70
  return (
    <IonPage>
      <IonHeader>
        <Header left="none" center="logo" right="none" />
      </IonHeader>
      <IonContent>
        <section className="inner">
          <div className="inner--auth">
            <div className="auth--top">
              <img className="auth--top__logo" src={TopLogo} alt="" />

              {//ios・androidの場合と、それ以外で条件分岐
                (isPlatform("ios") || isPlatform("android")) ?
                  <React.StrictMode></React.StrictMode> :
                  <React.StrictMode>
                    <p className="auth--top__text">パテントスタートを初めてご利用の方</p>
                    <RegistBtn />
                    <p className="auth--top__text">アカウントを既にお持ちの方</p>
                  </React.StrictMode>
              }

              <LoginBtn />
              <div className="mb-40"></div>
            </div>
          </div>
          <Terms />
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Full" page="home" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
}

export default Top;
