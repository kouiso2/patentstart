import { IonContent, IonFooter, IonHeader, IonPage } from '@ionic/react';
import 'assets/css/haseryo.css';
import 'assets/css/Login.css';
import 'assets/css/style.css';
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
import SingleTerms from 'components/Terms';
import React from 'react';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';


//XD77
export const NewRegistrationComplete: React.FC = () => {
  const history = useHistory();

  return (
    <IonPage>
      <IonHeader>
        <Header left="none" center="logo" right="none" />
      </IonHeader>
      <IonContent>
        <section className="inner inner--auth--sp">
          <div className="inner--auth" style={{ minHeight: '550px' }}>
            <div className="" style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', minHeight: '550px' }}>
              <h2 className="auth--select__title">メールを送信しました</h2>
              <p className="auth__heading">入力されたアドレスにメールを送信しました。メールを確認して、メールアドレスの認証を行って下さい。</p>
              <div style={{ marginTop: "4rem" }}>
                <div className="btn--center">
                  <button className="Btn__base PC__btn" onClick={() => { history.push('/auth/login/') }}>ログインページへ</button>
                </div>
                <Link to="/auth/top/" style={{ textAlign: 'center', marginTop: '20px' }} className="auth--regist__disc">トップページに戻る</Link>
              </div>
            </div>
          </div>
          <SingleTerms />
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Full" page="home" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};

export default NewRegistrationComplete;
