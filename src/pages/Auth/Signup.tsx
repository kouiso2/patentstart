//ionic
import { IonContent, IonFooter, IonHeader, IonInput, IonPage, IonRouterLink } from '@ionic/react';
import 'assets/css/Btn.css';
import 'assets/css/LoginPassReset.css';
// css
import 'assets/css/style.css';
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
import Terms from 'components/Terms';
import 'firebase/auth';
import useSignup from 'hooks/signup';
import React from 'react';


//XD76 XD79
export const NewRegistrationSelect: React.FC = () => {
  const { signup, setSignup, handleSignupSubmit, validateEmail } = useSignup();
  //mailフォームにするか、snsフォームにするか
  // const [tab, setTab] = React.useState<typeof signupTabChoice[number]>("Mail");

  const registForm = React.useMemo(() => {
    return (
      <form className="container" onSubmit={handleSignupSubmit}>
        <p className="auth__input__text">メールアドレス</p>
        <IonInput
          autocomplete="email"
          placeholder="メールアドレス"
          className="auth__input"
          type="email"
          id="email"
          name="email"
          min="8"
          max="128"
          value={signup.mainEmail}
          required={true}
          onIonChange={(e) => {
            e.preventDefault();
            setSignup({
              ...signup,
              mainEmail: e.detail.value!
            })
          }}
        />
        <p className="auth__input__text">メールアドレス（確認用）</p>
        <IonInput
          autocomplete="email"
          placeholder="メールアドレス"
          className="auth__input"
          type="email"
          id="email"
          name="email"
          min="8"
          max="128"
          value={signup.checkEmail}
          required={true}
          onIonChange={(e) => {
            e.preventDefault();
            setSignup({
              ...signup,
              checkEmail: e.detail.value!
            })
          }}
          onIonBlur={validateEmail}
        />
        <p className="validate__text">
          {signup.validate}
        </p>
        <p className="auth__input__text">パスワード</p>
        <IonInput
          autocomplete="new-password"
          placeholder="パスワード"
          className="auth__input"
          type="password"
          id="password"
          name="password"
          min="8"
          max="128"
          value={signup.password}
          required={true}
          onIonChange={(e) => {
            e.preventDefault();
            setSignup({
              ...signup,
              password: e.detail.value!
            })
          }}
        />
        <span className="auth--regist__disc">※半角英数字を含む8〜20桁で入力して下さい。</span>
        <div className="btn--center">
          <button className="Btn__base PC__btn" type="submit">新規登録</button>
        </div>
      </form>
    )
    // if (tab === "Mail") {
    //   return (
    //     <form className="container" onSubmit={handleSignupSubmit}>
    //       <p className="auth__input__text">メールアドレス</p>
    //       <IonInput
    //         autocomplete="email"
    //         placeholder="メールアドレス"
    //         className="auth__input"
    //         type="email"
    //         id="email"
    //         name="email"
    //         min="8"
    //         max="128"
    //         value={signup.mainEmail}
    //         required={true}
    //         onIonChange={(e) => {
    //           e.preventDefault();
    //           setSignup({
    //             ...signup,
    //             mainEmail: e.detail.value!
    //           })
    //         }}
    //       />
    //       <p className="auth__input__text">メールアドレス（確認用）</p>
    //       <IonInput
    //         autocomplete="email"
    //         placeholder="メールアドレス"
    //         className="auth__input"
    //         type="email"
    //         id="email"
    //         name="email"
    //         min="8"
    //         max="128"
    //         value={signup.checkEmail}
    //         required={true}
    //         onIonChange={(e) => {
    //           e.preventDefault();
    //           setSignup({
    //             ...signup,
    //             checkEmail: e.detail.value!
    //           })
    //         }}
    //         onIonBlur={validateEmail}
    //       />
    //       <p className="validate__text">
    //         {signup.validate}
    //       </p>
    //       <p className="auth__input__text">パスワード</p>
    //       <IonInput
    //         autocomplete="new-password"
    //         placeholder="パスワード"
    //         className="auth__input"
    //         type="password"
    //         id="password"
    //         name="password"
    //         min="8"
    //         max="128"
    //         value={signup.password}
    //         required={true}
    //         onIonChange={(e) => {
    //           e.preventDefault();
    //           setSignup({
    //             ...signup,
    //             password: e.detail.value!
    //           })
    //         }}
    //       />
    //       <span className="auth--regist__disc">※半角英数字を含む8〜20桁で入力して下さい。</span>
    //       <div className="btn--center">
    //         <button className="Btn__base PC__btn" type="submit">新規登録</button>
    //       </div>
    //     </form>
    //   );
    // } else {
    //   return (
    //     <SnsBtn />
    //   );
    // }
  }, [signup, setSignup, validateEmail, handleSignupSubmit]);

  // const tabComponent = React.useMemo(() => {
  //   if (tab === "Mail") {
  //     return (
  //       <div className="auth--regist__tabs">
  //         <button id="mailBtn" onClick={(e: React.MouseEvent<HTMLButtonElement, MouseEvent>): void => {
  //           e.preventDefault();
  //           setTab("Mail");
  //         }} className="auth--regist__tabs__tab active__tab">メールアドレスで登録</button>
  //         <button id="snsBtn" onClick={(e: React.MouseEvent<HTMLButtonElement, MouseEvent>): void => {
  //           e.preventDefault();
  //           setTab("Sns");
  //         }} className="auth--regist__tabs__tab">その他サービスで登録</button>
  //       </div>
  //     )
  //   } else if (tab === "Sns") {
  //     return (
  //       <div className="auth--regist__tabs">
  //         <button id="mailBtn" onClick={(e: React.MouseEvent<HTMLButtonElement, MouseEvent>): void => {
  //           e.preventDefault();
  //           setTab("Mail");
  //         }} className="auth--regist__tabs__tab">メールアドレスで登録</button>
  //         <button id="snsBtn" onClick={(e: React.MouseEvent<HTMLButtonElement, MouseEvent>): void => {
  //           e.preventDefault();
  //           setTab("Sns");
  //         }} className="auth--regist__tabs__tab active__tab">その他サービスで登録</button>
  //       </div>
  //     )
  //   }
  // }, [tab]);

  return (
    <IonPage>
      <IonHeader>
        <Header left="none" center="logo" right="none" />
      </IonHeader>
      <IonContent>
        <section className="inner inner--auth--sp">
          <section className="inner ">
            <div className="inner--auth">
              {/* {tabComponent} */}
              <p className="auth--regist__heading">ご登録のメールアドレス、パスワードを入力し、新規アカウントを登録してください。</p>
              {registForm}
              <div className="auth--regist--undertext">
                <span className="auth--regist__disc">「登録」ボタンをクリックすることで、<IonRouterLink href="https://patentstart.jp/terms.html" target="_blank" className="auth--link--primary">ご利用規約</IonRouterLink>・<IonRouterLink href="https://patentstart.jp/privacy.html" target="_blank" className="auth--link--primary">プライバシーポリシー</IonRouterLink>に同意したものとみなします。</span>
              </div>
            </div>
            <Terms />
          </section>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Full" page="home" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};

export default NewRegistrationSelect;
