//ionic
import { IonContent, IonFooter, IonHeader, IonPage } from '@ionic/react';
// css
import 'assets/css/haseryo.css';
import 'assets/css/style.css';
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
//firebase
import firebase from 'firebase';
import 'firebase/firestore';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { RootState } from 'store/Combine';
import { attorneyConfigAction } from 'store/Config/action';

export const UserListAttorney: React.FC = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const config = useSelector((state: RootState) => state.config);
  type Attorney = {
    id: string,
    firstName: string,
    firstNameKatakana: string,
    lastName: string,
    lastNameKatakana: string,
    isIdentified: boolean,
    isSubmitted: boolean,
  }

  const [attorneys, setAttorneys] = React.useState<Attorney[]>([]);

  React.useEffect(() => {
      const db = firebase.firestore();
      const usersRef = db.collection("User").where("kind", "==", "Attorney");
      
      usersRef.onSnapshot((snap: firebase.firestore.QuerySnapshot) => {
        snap.docChanges().forEach(function (change) {
          var attorney = change.doc.data();
          setAttorneys(
            attorneys => [{ 
              id: change.doc.id, 
              firstName: attorney.firstName, 
              firstNameKatakana: attorney.firstNameKatakana, 
              lastName: attorney.lastName, 
              lastNameKatakana: attorney.lastNameKatakana,
              isIdentified: attorney.identification.isIdentified,
              isSubmitted: attorney.identification.isSubmitted,
              path: change.doc.ref.path,
            }, ...attorneys]
          );
        });
      });
    return () => {
      // cleanup
    }
  }, [setAttorneys]);
  
  return (
    <IonPage>
      <IonHeader>
        <Header left="none" center="logo" right="none" />
      </IonHeader>
      <IonContent>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="home" />
            <main className="content__wrap">
              <div className="chatlist__wrap">
                <h3 className="chatlist__wrap__title pc">弁理士一覧</h3>
                {
                  attorneys.map((attorney: any, index: any) => {
                    return (
                      <div key={index} className="chatlist__child" style={{ marginBottom: "1rem" }} onClick={() => {
                        dispatch(attorneyConfigAction({
                          ...config.attorney,
                          path: attorney.path,
                        }));
                        history.push({
                          pathname: `${"/admin/attorney/profile/"}`,
                        });
                      }}>
                        {/* <figure className="chatlist__child__thum" style={{ backgroundImage: `url(${profImage})` }}></figure> */}
                        <article className="chatlist__child__desc">
                          <div className="chatlist__child__desc__top">
                            <h4 className="chatlist__child__desc__top--name">
                              {attorney.firstName + " " + attorney.lastName}
                            </h4>
                            {
                              (attorney.isSubmitted && attorney.isIdentified)?
                              <span className="chatlist__child__desc__top--status status--applying">
                                承認済
                              </span>:
                              <span className="chatlist__child__desc__top--status status--recruiting">
                                未承認
                              </span>
                            }
                          </div>
                          <p className="chatlist__boxRead">{attorney.firstNameKatakana + " " + attorney.lastNameKatakana}</p>
                        </article>
                      </div>
                    );
                  })
                }
              </div>
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Tabs" page="home" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};

export default UserListAttorney;
