//ionic
import { IonContent, IonFooter, IonHeader, IonPage } from '@ionic/react';
// css
import 'assets/css/haseryo.css';
import 'assets/css/style.css';
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
//firebase
import firebase from 'firebase';
import 'firebase/firestore';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { RootState } from 'store/Combine';
import { attorneyConfigAction } from 'store/Config/action';


// 使わない
export const AttorneyApproval: React.FC = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const config = useSelector((state: RootState) => state.config);
  type Attorney = {
    id: string,
    firstName: string,
    lastName: string,
    frontPath: any,
    backPath: any,
    qualificationFilePath: any,
    isIdentified: boolean,
    isSubmitted: boolean
  }

  const [attorneys, setAttorneys] = React.useState<Attorney[]>([]);

  const handleClick = React.useCallback((id: string, isSubmitted: boolean, isIdentified: boolean) => {
    const db = firebase.firestore().collection('User').doc(id);
    db.update({
      'identification.isSubmitted': isSubmitted,
      'identification.isIdentified': isIdentified,
    }).then(() => { 
      history.push('/admin/attorney-list/');
    });
  }, []);

  React.useEffect(() => {
      const db = firebase.firestore();
      const usersRef = db.collection("User").where("kind", "==", "Attorney");
      
      usersRef.onSnapshot((snap: firebase.firestore.QuerySnapshot) => {
        snap.docChanges().forEach(function (change) {
          var attorney = change.doc.data();
          setAttorneys(
            attorneys => [{ 
              id: change.doc.id, 
              firstName: attorney.firstName, 
              lastName: attorney.lastName, 
              frontPath: attorney.identification.confirmationFile.front.path,
              backPath: attorney.identification.confirmationFile.back.path,
              qualificationFilePath: attorney.identification.qualificationFile.path,
              isIdentified: attorney.identification.isIdentified,
              isSubmitted: attorney.identification.isSubmitted,
            }, ...attorneys]
          );
        });
      });
    return () => {
      // cleanup
    }
    // Lintの設定にすると無限ループが発生する
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [setAttorneys]);

  
  return (
    <IonPage>
      <IonHeader>
        <Header left="none" center="logo" right="none" />
      </IonHeader>
      <IonContent>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="home" />
            <main className="content__wrap">
              <div className="chatlist__wrap">
                <h3 className="chatlist__wrap__title pc">弁理士の本人確認一覧</h3>
                {
                  attorneys.map((attorney: any, index: any) => {
                    return (
                      <div key={index} className="chatlist__child" style={{ marginBottom: "1rem" }} onClick={() => {
                        dispatch(attorneyConfigAction({
                          ...config.attorney,
                          path: attorney.path,
                        }));
                      }}>
                        {/* <figure className="chatlist__child__thum" style={{ backgroundImage: `url(${profImage})` }}></figure> */}
                        <article className="chatlist__child__desc">
                          <div className="chatlist__child__desc__top">
                            <h4 className="chatlist__child__desc__top--name">
                              {attorney.firstName + " " + attorney.lastName}
                            </h4>
                          </div>
                          <p className="chatlist__boxRead">
                            <a href={attorney.frontPath} style={{ color: "#4682b4", textDecoration: "underline" }} target="_blank" rel="noreferrer noopener">本人確認(表)</a>、
                            <a href={attorney.backPath} style={{ color: "#4682b4", textDecoration: "underline" }} target="_blank" rel="noreferrer noopener">本人確認(裏)</a>、
                            <a href={attorney.qualificationFilePath} style={{ color: "#4682b4", textDecoration: "underline" }} target="_blank" rel="noreferrer noopener">弁理士資格証明書</a>
                          </p>
                        </article>
                        <button className="offer__button" style={{ backgroundColor: "#CB0015", padding: "5px", marginLeft: "10px" }} onClick={() => {
                          handleClick(attorney.id, !attorney.isSubmitted, !attorney.isIdentified);
                        }}>
                          {
                            (attorney.isSubmitted && attorney.isIdentified)?
                            <span>承認を外す</span>:
                            <span>承認する</span>
                          }
                        </button>
                      </div>
                    );
                  })
                }
              </div>
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Tabs" page="home" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};

export default AttorneyApproval;
