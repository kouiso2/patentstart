//ionic
import { IonContent, IonFooter, IonHeader, IonPage } from '@ionic/react';
// css
import 'assets/css/haseryo.css';
import 'assets/css/style.css';
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
//firebase
import firebase from 'firebase';
import 'firebase/firestore';
import React from 'react';

export const ReportList: React.FC = () => {
  const [reports, setReports] = React.useState([{ summary: "", content: "", name: "", email: "" }]);

  React.useEffect(() => {
      const db = firebase.firestore();
      const reportsRef = db.collection("Report");
      
      reportsRef.onSnapshot((snap: firebase.firestore.QuerySnapshot) => {
        snap.docChanges().forEach(function (change) {
          if (change.type === 'added') {
            var report = change.doc.data();
            setReports(
              reports => [{ 
                summary: report.summary, 
                content: report.content, 
                name: report.reportedUserName, 
                email: report.reportedUserEmail, 
              }, ...reports]
            );
          }
        });
      });
    return () => {
      // cleanup
    }
  }, [setReports]);

  
  return (
    <IonPage>
      <IonHeader>
        <Header left="none" center="logo" right="none" />
      </IonHeader>
      <IonContent>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="home" />
            <main className="content__wrap">
              <div className="chatlist__wrap">
                <h3 className="chatlist__wrap__title pc">通報一覧</h3>
                {
                  reports.map((report: any, index: any) => {
                    if(report.name) {
                      return (
                        <div key={index} className="chatlist__child" style={{ marginBottom: "1rem" }}>
                          {/* <figure className="chatlist__child__thum" style={{ backgroundImage: `url(${profImage})` }}></figure> */}
                          <article className="chatlist__child__desc">
                            <div className="chatlist__child__desc__top">
                              <h4 className="chatlist__child__desc__top--name">
                                {report.name}さんが通報されました：<a href={'mailto:'+report.email} style={{ color: "#4682b4", textDecoration: "underline" }}>メールはこちら</a>
                              </h4>
                            </div>
                            <p className="chatlist__boxRead">{report.content}</p>
                          </article>
                        </div>
                      );
                    }
                  })
                }
              </div>
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Tabs" page="home" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};

export default ReportList;