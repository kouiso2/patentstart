//ionic
import { IonContent, IonFooter, IonHeader, IonPage } from '@ionic/react';
// css
import 'assets/css/haseryo.css';
import 'assets/css/style.css';
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
//firebase
import firebase from 'firebase';
import 'firebase/firestore';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { RootState } from 'store/Combine';
import { applicantConfigAction } from 'store/Config/action';

export const UserListApplicant: React.FC = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const config = useSelector((state: RootState) => state.config);
  type Applicant = {
    id: string,
    firstName: string,
    firstNameKatakana: string,
  }

  const [applicants, setApplicants] = React.useState<Applicant[]>([]);

  React.useEffect(() => {
      const db = firebase.firestore();
      const usersRef = db.collection("User").where("kind", "==", "Applicant");
      
      usersRef.onSnapshot((snap: firebase.firestore.QuerySnapshot) => {
        snap.docChanges().forEach(function (change) {
          var applicant = change.doc.data();
          setApplicants(
            applicants => [{ 
              id: change.doc.id, 
              firstName: applicant.firstName, 
              firstNameKatakana: applicant.firstNameKatakana, 
              lastName: applicant.lastName, 
              lastNameKatakana: applicant.lastNameKatakana,
              path: change.doc.ref.path,
            }, ...applicants]
          );
        });
      });
    return () => {
      // cleanup
    }
    // Lintの設定にすると無限ループが発生する
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [setApplicants]);

  
  return (
    <IonPage>
      <IonHeader>
        <Header left="none" center="logo" right="none" />
      </IonHeader>
      <IonContent>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="home" />
            <main className="content__wrap">
              <div className="chatlist__wrap">
                <h3 className="chatlist__wrap__title pc">申請者一覧</h3>
                {
                  applicants.map((applicant: any, index: any) => {
                    return (
                      <div key={index} className="chatlist__child" style={{ marginBottom: "1rem" }} onClick={() => {
                        dispatch(applicantConfigAction({
                          ...config.applicant,
                          path: applicant.path,
                        }));
                        history.push({
                          pathname: `${"/admin/applicant/profile/"}`,
                        });
                      }}>
                        {/* <figure className="chatlist__child__thum" style={{ backgroundImage: `url(${profImage})` }}></figure> */}
                        <article className="chatlist__child__desc">
                          <div className="chatlist__child__desc__top">
                            <h4 className="chatlist__child__desc__top--name">
                              {applicant.firstName + " " + applicant.lastName}
                            </h4>
                          </div>
                          <p className="chatlist__boxRead">{applicant.firstNameKatakana + " " + applicant.lastNameKatakana}</p>
                        </article>
                      </div>
                    );
                  })
                }
              </div>
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Tabs" page="home" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};

export default UserListApplicant;
