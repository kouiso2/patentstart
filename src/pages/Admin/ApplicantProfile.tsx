//ionic
import { IonContent, IonFooter, IonHeader, IonPage, isPlatform } from '@ionic/react';
import 'assets/css/Account.css';
import 'assets/css/Btn.css';
import 'assets/css/haseryo.css';
import 'assets/css/style.css';
// img
import ProfileCamera from 'assets/img/noimage.svg';
//original
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
import { noValueText } from 'controller/index';
import 'firebase/auth';
import useProfile from 'hooks/profile';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { RootState } from 'store/Combine';

export const ApplicantProfile: React.FC = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const config = useSelector((state: RootState) => state.config);
  const { profile } = useProfile(config.applicant.path);

  return (
    <IonPage>
      <IonHeader>
        <Header title="プロフィール" left={(isPlatform("ios") || isPlatform("android")) ? "none" : "back"} center="title" right="none" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="account" />
            <main className="content__wrap">
              <div className="profile__media">
                <figure className="profile__media__img" style={{ backgroundImage: `url(${profile.image ? profile.image : ProfileCamera})` }} ></figure>
                {/* style={{ background: 'url(../src/assets/img/demo.jpg)' }} */}
                <div>
                  <p className="profile__media__company">{noValueText(profile.companyName)}</p>
                  <p className="profile__media__kana">{noValueText(profile.companyKana)}</p>
                </div>
              </div>

              <div className="profile__item">
                <p className="profile__item__title">法人・個人</p>
                <p className="profile__item__text">{noValueText(
                  profile.businessType.company === true ? "法人" : "個人"
                )}</p>
              </div>

              <div className="profile__item">
                <p className="profile__item__title">担当者名</p>
                <p className="profile__item__text">{noValueText(profile.firstName + profile.lastName)}</p>
              </div>

              <div className="profile__item">
                <p className="profile__item__title">担当者名（フリガナ）</p>
                <p className="profile__item__text">{noValueText(profile.personKatakana)}</p>
              </div>

              <div className="profile__item">
                <p className="profile__item__title">ホームページ・SNS等</p>
                <p className="profile__item__text">{noValueText(profile.homepage)}</p>
              </div>

              <div className="profile__item">
                <p className="profile__item__title">メールアドレス</p>
                <p className="profile__item__text">{noValueText(profile.email)}</p>
              </div>

              <div className="profile__item">
                <p className="profile__item__title">住所</p>
                <p className="profile__item__text">〒{noValueText(profile.address?.postalCode)}</p>
                <p className="profile__item__text">{noValueText(profile.address?.line1, profile.address?.state + profile.address?.city + profile.address?.line1)}</p>
              </div>

              <div className="profile__item">
                <p className="profile__item__title">自己紹介</p>
                <p className="profile__item__text">{noValueText(profile.self)}</p>
              </div>
              {/* とりあえず非表示 */}
              {/* {//ios・androidの場合と、それ以外で条件分岐
                (isPlatform("ios") || isPlatform("android")) ?
                <React.Fragment></React.Fragment> :
                <div className="profile__item">
                  <p className="profile__item__title">決済情報</p>
                  <p className="profile__item__text">磯貝さん確認要</p>
                </div>
              } */}
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Tabs" page="account" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );

};

export default ApplicantProfile;