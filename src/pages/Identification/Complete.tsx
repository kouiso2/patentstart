//ionic
import { IonContent, IonFooter, IonHeader, IonPage, IonRouterLink } from '@ionic/react';
// css
import 'assets/css/Home.css';
import 'assets/css/style.css';
//original
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
import React from 'react';

/**
* IdentificationComplete:
* 本人確認完了ページを表示する
*/
export const IdentificationComplete: React.FC = () => {
  return (
    //XD53
    <IonPage>
      <IonHeader>
        <Header title="本人確認書類の完了" left="back" center="title" right="none" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="account" />
            <main className="content__wrap complete--page">
              <div className="complete__wrap">
                <h2 className="complete__title">送信が完了しました</h2>
                <p className="complete__text">担当者が確認し、情報の確認が取れたらプロフィールに本人認証バッヂと弁理士資格認証バッヂがつきます。</p>
                <p className="complete__text">反映に最大5営業日かかる場合がございます。</p>

                <div className="btn--center">
                  <IonRouterLink className="" href="/attorney/home/">
                    <div className="Btn__base PC__btn">ホームに戻る</div></IonRouterLink>
                </div>
              </div>
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Tabs" page="account" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};

export default IdentificationComplete;
