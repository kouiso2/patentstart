//ionic
import { IonContent, IonFooter, IonHeader, IonPage, IonRouterLink } from '@ionic/react';
//css
import 'assets/css/style.css';
//original
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
import React from 'react';

/**
* Reject:
* 本人確認が終わっていない弁理士が案件に応募しようとした時にリダイレクトさせるリジェクトページ
*/
const Reject: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <Header title="パスワード変更の完了" left="none" center="title" right="none" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="account" />
            <main className="content__wrap complete--page">
              <div className="complete__wrap">
                <h2 className="complete__title">本人確認が完了していません。</h2>
                <p className="complete__text">案件の応募は本人確認が必要です。<br />アカウント→本人確認書類から手続きをお願い致します。</p>
                <div className="btn--center">
                  <IonRouterLink className="" href="/attorney/identification/">
                    <div className="Btn__base PC__btn">本人確認書類ページへ</div></IonRouterLink>
                </div>
              </div>
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Tabs" page="account" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
}

export default Reject;
