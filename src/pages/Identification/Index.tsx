//ionic
import { IonContent, IonFooter, IonHeader, IonPage } from '@ionic/react';
//css
import 'assets/css/Home.css';
import 'assets/css/Identification.css';
import 'assets/css/style.css';
//original
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
import firebase from 'firebase';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/functions';
import React from 'react';
import { useHistory } from 'react-router';

//XD52
/**
* Identification:
* 弁理士の本人確認ページを表示する
*/
export const Identification: React.FC = () => {
  const history = useHistory();

  const frontFile = React.useRef<HTMLInputElement>(null);
  const backFile = React.useRef<HTMLInputElement>(null);
  const qualifiedFile = React.useRef<HTMLInputElement>(null);

  /**
  * handleSubmit():
  * 「本人確認書類を送信する」ボタンを押した時の処理を記述した関数
  * Cloud FirestoreとFirebase Storageにアップロードされたファイルの情報を保存し、完了ページにリダイレクトさせる
  */
  const handleSubmit = React.useCallback(async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const auth = firebase.auth().currentUser;
    const db = firebase.firestore();
    const func = firebase.functions();
    const identificationsRef = db.collection("User").doc(auth?.uid);
    const verification = func.httpsCallable('identifyDocumentUpload');

    const frontImage = frontFile.current!.files![0];
    const backImage = backFile.current!.files![0];
    const qualifiedImage = qualifiedFile.current!.files![0];
    const front = await firebase.storage().ref().child('identification/' + frontImage?.name).put(frontImage);
    const back = await firebase.storage().ref().child('identification/' + backImage?.name).put(backImage);
    const qualification = await firebase.storage().ref().child('identification/' + qualifiedImage?.name).put(qualifiedImage);
    const frontPath = await front.ref.getDownloadURL();
    const backPath = await back.ref.getDownloadURL();
    const qualifiedPath = await qualification.ref.getDownloadURL();

    await identificationsRef.update({
      identification: {
        isSubmitted: false,
        isIdentified: false,
        confirmationFile: {
          front: {
            name: front.ref.name,
            path: frontPath,
            fullPath: front.ref.fullPath,
            type: frontImage.type
          },
          back: {
            name: back.ref.name,
            path: backPath,
            fullPath: back.ref.fullPath,
            type: backImage.type
          }
        },
        qualificationFile: {
          name: qualification.ref.name,
          path: qualifiedPath,
          fullPath: qualification.ref.fullPath,
          type: qualifiedImage.type
        }
      },
      updateDt: firebase.firestore.FieldValue.serverTimestamp()
    });

    await verification({ front: frontPath ? true : false, back: backPath ? true : false });


    history.push('/attorney/identification/complete/');
  }, [history]);

  return (
    <IonPage>
      <IonHeader>
        <Header title="本人確認書類を入力" left="back" center="title" right="none" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="account" />
            <main className="content__wrap">
              <p className="identification__heading">本人確認書類をアップロードしてください。書類承認後、申請者の依頼に応募し、チャットを送ることができます。</p>
              <form action="post" className="" onSubmit={handleSubmit}>
                <h2 className="identification__title">1.本人確認書類</h2>
                <p className="form__title__12 mb-20">表面</p>

                <label htmlFor="front-file" className="identification__input">ファイルをアップロード</label>
                <input type="file" name="front-file" id="front-file" ref={frontFile}
                  onChange={(e: React.ChangeEvent<HTMLInputElement>): void => {
                    var file = e.target.files![0];
                    console.log(frontFile.current?.files![0].name);
                    if (file !== undefined) {
                      var type = file.type; // MIMEタイプ
                      var size = file.size; // ファイル容量（byte）
                    } else {
                      return;
                    }
                    var limit10M = 10000000; // byte, 10MB

                    //決められたファイル形式以外ははじくようにする。
                    if (type !== "image/png" && type !== "image/jpeg") {
                      e.target.value = "";
                      alert('ファイルはpng,jpegのいずれかの形式でアップロードしてください');
                      return;
                    }
                    if (size > limit10M) {
                      e.target.value = "";
                      alert('画像サイズは10MB以下で指定してください');
                      return;
                    }

                  }} />
                <p className="form__title__12 mb-20">裏面</p>
                <label htmlFor="back-file" className="identification__input">ファイルをアップロード</label>
                <input type="file" name="back-file" id="back-file" ref={backFile}
                  onChange={(e: React.ChangeEvent<HTMLInputElement>): void => {
                    var file = e.target.files![0];
                    console.log(backFile.current?.files![0].name);
                    if (file !== undefined) {
                      var type = file.type; // MIMEタイプ
                      var size = file.size; // ファイル容量（byte）
                    } else {
                      return;
                    }
                    var limit10M = 10000000; // byte, 10MB

                    //決められたファイル形式以外ははじくようにする。
                    if (type !== "image/png" && type !== "image/jpeg") {
                      e.target.value = "";
                      alert('ファイルはpng,jpegのいずれかの形式でアップロードしてください');
                      return;
                    }
                    if (size > limit10M) {
                      e.target.value = "";
                      alert('画像サイズは10MB以下で指定してください');
                      return;
                    }

                  }} />
                <p className="identification__disctext">本人確認書類は<br />
                  パスポート<br />
                  在留カード（外国籍の方）<br />
                  住基カード（顔写真付き）<br />
                  が利用可能です。<br />
                  ※10MB以下
                </p>

                <h2 className="identification__title">2.弁理士資格証明書</h2>
                <label htmlFor="qualified-file" className="identification__input">ファイルをアップロード</label>
                <input type="file" name="qualified-file" id="qualified-file" ref={qualifiedFile}
                  onChange={(e: React.ChangeEvent<HTMLInputElement>): void => {
                    var file = e.target.files![0];
                    console.log(qualifiedFile.current?.files![0].name);
                    if (file !== undefined) {
                      var type = file.type; // MIMEタイプ
                      var size = file.size; // ファイル容量（byte）
                    } else {
                      return;
                    }
                    var limit10M = 10000000; // byte, 10MB
                    //決められたファイル形式以外ははじくようにする。
                    if (type !== "image/png" && type !== "image/jpeg") {
                      e.target.value = "";
                      alert('ファイルはpng,jpegのいずれかの形式でアップロードしてください');
                      return;
                    }
                    if (size > limit10M) {
                      e.target.value = "";
                      alert('画像サイズは10MB以下で指定してください');
                      return;
                    }
                  }} />
                <p className="identification__disctext">申請内容に関する資料などPDFファイルをアップロードできます。<br />
    ※10MB以下
    </p>

                <div className=" SP__btn__center">
                  <button type="submit" className="Btn__base PC__btn">本人確認書類を送信する</button>
                </div>
              </form>
            </main>
          </div>
        </section>

        <IonFooter className="ion-no-border">
          <Footer format="Tabs" page="account" />
        </IonFooter>
      </IonContent>
    </IonPage>

  );
};

export default Identification;
