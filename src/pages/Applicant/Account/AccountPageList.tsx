//ionic
import { IonContent, IonFooter, IonHeader, IonPage } from '@ionic/react';
import 'assets/css/Account.css';
import 'assets/css/haseryo.css';
// css
import 'assets/css/style.css';
import ProfileCamera from 'assets/img/noimage.svg';
import Footer from 'components/Footer/Index';
//original
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
import { noValueText } from 'controller/index';
import useProfile from 'hooks/profile';
import React from 'react';
import { Link } from 'react-router-dom';


// XD23
export const ApplicantAccountPageList: React.FC = () => {
  const { profile } = useProfile();

  return (
    <IonPage>
      <IonHeader>
        <Header title="アカウント" left="none" center="title" right="none" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="account" />
            <main className="content__wrap">
              <div className="profile__media">
                <figure className="profile__media__img" style={{ backgroundImage: `url(${profile.image ? profile.image : ProfileCamera})` }} ></figure>
                {/* style={{ background: 'url(../src/assets/img/demo.jpg)' }} */}
                <div>
                  <span className="accordion__contents__profile__text__flex__child--applicant dislay__active" style={{ "display": "inline-block" }}>
                    {noValueText(
                      profile.businessType.company === true ? "法人" : "個人"
                    )}
                  </span>
                  <p className="profile__media__company">{noValueText(profile.companyName)}</p>
                  <p className="profile__media__kana">{noValueText(profile.companyKana)}</p>
                </div>
              </div>
              <p className="account__title">アカウント</p>
              <Link to="/applicant/profile/" className="account__link">プロフィール</Link>
              <Link to="/applicant/pwchange/" className="account__link">パスワード変更</Link>
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Tabs" page="account" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};

export default ApplicantAccountPageList;
