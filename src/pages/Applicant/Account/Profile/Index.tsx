//ionic
import { IonContent, IonFooter, IonHeader, IonPage, isPlatform } from '@ionic/react';
import 'assets/css/Account.css';
import 'assets/css/Btn.css';
import 'assets/css/haseryo.css';
import 'assets/css/style.css';
// img
import ProfileCamera from 'assets/img/noimage.svg';
//original
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
import { noValueText } from 'controller/index';
import firebase from 'firebase';
import 'firebase/auth';
import useProfile from 'hooks/profile';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';
import { RootState } from 'store/Combine';
import { modalConfigAction, pathConfigAction } from 'store/Config/action';

//XD24
export const ApplicantProfile: React.FC = () => {
  const history = useHistory();
  const { profile, profileIdentify } = useProfile();
  const dispatch = useDispatch();
  const config = useSelector((state: RootState) => state.config);

  const handleClick = React.useCallback(() => {
    return history.push('/applicant/profile/update');
  }, [history]);


  const handleLogoutClick = React.useCallback(async () => {
    await firebase.auth().signOut()
    console.log("ログアウト");
  }, []);
  return (
    <IonPage>
      <IonHeader>
        <Header title="プロフィール" left={(isPlatform("ios") || isPlatform("android")) ? "none" : "back"} center="title" right="none" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="account" />
            <main className="content__wrap">
              <div className="profile__media">
                <figure className="profile__media__img" style={{ backgroundImage: `url(${profile.image ? profile.image : ProfileCamera})` }} ></figure>
                {/* style={{ background: 'url(../src/assets/img/demo.jpg)' }} */}
                <div>
                  <p className="profile__media__company">{noValueText(profile.companyName)}</p>
                  <p className="profile__media__kana">{noValueText(profile.companyKana)}</p>
                </div>
              </div>

              <div className="profile__item">
                <p className="profile__item__title">法人・個人</p>
                <p className="profile__item__text">{noValueText(
                  profile.businessType.company === true ? "法人" : "個人"
                )}</p>
              </div>

              <div className="profile__item">
                <p className="profile__item__title">担当者名</p>
                <p className="profile__item__text">{noValueText(profile.firstName + profile.lastName)}</p>
              </div>

              <div className="profile__item">
                <p className="profile__item__title">担当者名（フリガナ）</p>
                <p className="profile__item__text">{noValueText(profile.personKatakana)}</p>
              </div>

              <div className="profile__item">
                <p className="profile__item__title">ホームページ・SNS等</p>
                <p className="profile__item__text">{noValueText(profile.homepage)}</p>
              </div>

              <div className="profile__item">
                <p className="profile__item__title">メールアドレス</p>
                <p className="profile__item__text">{noValueText(profile.email)}</p>
              </div>

              <div className="profile__item">
                <p className="profile__item__title">住所</p>
                <p className="profile__item__text">〒{noValueText(profile.address?.postalCode)}</p>
                <p className="profile__item__text">{noValueText(profile.address?.line1, profile.address?.state + profile.address?.city + profile.address?.line1)}</p>
              </div>

              <div className="profile__item">
                <p className="profile__item__title">自己紹介</p>
                <p className="profile__item__text">{noValueText(profile.self)}</p>
              </div>
              {/* とりあえず非表示 */}
              {/* {//ios・androidの場合と、それ以外で条件分岐
                (isPlatform("ios") || isPlatform("android")) ?
                <React.StrictMode></React.StrictMode> :
                <div className="profile__item">
                  <p className="profile__item__title">決済情報</p>
                  <p className="profile__item__text">磯貝さん確認要</p>
                </div>
              } */}
              <p className="profile__item__link" onClick={() => {
                dispatch(pathConfigAction(profileIdentify.path));
                dispatch(modalConfigAction({
                  ...config.modal,
                  isOpen: true,
                  form: "Profile",
                }));
              }}>自分のプロフィールを確認する</p>

              <div className=" SP__btn__center mt-30">
                <button className="Btn__base PC__btn" onClick={handleClick}>変更する</button>
              </div>

              <p className="account__title mt-40">アカウント設定</p>
              <Link onClick={handleLogoutClick} to="/auth/top" className="account__link">ログアウト</Link>
              {//ios・androidの場合と、それ以外で条件分岐
                (isPlatform("ios") || isPlatform("android")) ?
                  <Link to="/applicant/pwchange/" className="account__link">パスワード変更</Link> :
                  <React.StrictMode></React.StrictMode>
              }
              <Link to="/applicant/contact/" className="account__link">退会のお手続き</Link>
              {//ios・androidの場合と、それ以外で条件分岐
                (isPlatform("ios") || isPlatform("android")) ?
                  <React.StrictMode>
                    {/* <Link to="" className="account__link">アプリの使い方</Link> */}
                    <a href="https://patentstart.jp/privacy.html" target="_blank" rel="noreferrer" className="account__link">
                      プライバシーポリシー
                    </a>
                    {/* <Link to="" className="account__link">よくある質問</Link> */}
                    <Link to="/applicant/contact/" className="account__link">お問い合わせ</Link>
                  </React.StrictMode> :
                  <React.StrictMode></React.StrictMode>
              }
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Tabs" page="account" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );

};

export default ApplicantProfile;
