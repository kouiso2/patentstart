import { IAPProduct, InAppPurchase2 } from '@ionic-native/in-app-purchase-2';
import { IonContent, IonFooter, IonHeader, IonPage, isPlatform } from '@ionic/react';
import 'assets/css/haseryo.css';
// css
import 'assets/css/style.css';
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
import firebase from 'firebase';
import 'firebase/functions';
import useApplication from 'hooks/application';
import useAuth from 'hooks/auth';
import useOffer from 'hooks/offer';
import useProfile from 'hooks/profile';
import React from 'react';
import { useHistory } from 'react-router';
import { useLocation } from 'react-router-dom';
import AccordionContentsAbout from '../../../components/Accordion/AccordionContents/About';
import AccordionContentsAttorneyInfo from '../../../components/Accordion/AccordionContents/AttorneyInfo';
//XD17
export const ApplicantOfferCheck: React.FC = () => {
  const location = useLocation<string>();
  const offerPath = location.state;
  const { offer } = useOffer("", offerPath);
  const { app } = useApplication(offer.application?.path);
  const { profile, profileIdentify } = useProfile(offer.attorney?.path);
  const [fullName] = React.useState<string>(profile.firstName + profile.lastName);
  const history = useHistory();
  const auth = useAuth();
  const [priceCode, setPriceCode] = React.useState<string>("");
  const func = firebase.functions();
  const db = firebase.firestore();
  InAppPurchase2.verbosity = InAppPurchase2.DEBUG;

  const priceIosFunc = React.useCallback((price: number) => {
    if (price === 30000) {
      return 29800;
    } else if (price === 50000) {
      return 49800;
    } else {
      return price;
    }
  }, []);

  const handlePatentCreate = React.useCallback(async (id: string, platform: string) => {
    console.log("Patent Create");
    const patentRes = await db.doc(profileIdentify.path).collection('Patent').add({
      application: app.path,
      developName: app.title,
      explanation: app.explanation
    });
    console.log("App Update");
    await db.doc(app.path).update({
      subscriptionId: id,
      price: priceIosFunc(offer.price),
      status: "Apply",
      platform: platform,
      updateDt: firebase.firestore.FieldValue.serverTimestamp(),
      updater: auth?.path,
      attorney: db.doc(profileIdentify.path),
      patent: patentRes.path
    });
    return history.push({
      pathname: '/applicant/request/complete',
      state: offerPath
    });
  }, [db, app, offer, offerPath, history, auth?.path, profileIdentify.path, priceIosFunc]);

  React.useEffect(() => {
    if ((isPlatform('ios') || isPlatform('android')) && !isPlatform('mobileweb')) {
      InAppPurchase2.when(priceCode).updated((p: IAPProduct) => {
        if (p.state === "approved" && p.id === priceCode) {
          console.log("ID", p.id);
          handlePatentCreate(p.id, 'ios');
        }
      })
    }

  }, [history, offerPath, priceCode, handlePatentCreate]);

  React.useEffect(() => {
    let priceCoder: string = ""
    if (offer.price === 30000) {
      priceCoder = "jp.patentstart.www.standard";
    } else if (offer.price === 50000) {
      priceCoder = "jp.patentstart.www.highgrade";
    }
    InAppPurchase2.register({
      id: priceCoder,
      type: InAppPurchase2.NON_RENEWING_SUBSCRIPTION,
    });
    setPriceCode(priceCoder);
    InAppPurchase2.refresh();
  }, [offer.price]);

  InAppPurchase2.ready(() => {
    console.log('Store is ready');
    console.log('Products: ' + JSON.stringify(InAppPurchase2.products));
    console.log(JSON.stringify(InAppPurchase2.get(priceCode)));
  })

  const handleFixClick = React.useCallback((e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault();
    return history.goBack();
  }, [history]);

  const handleSaveClick = React.useCallback(async (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault();
    const register = func.httpsCallable('createApplicantAccount');
    const subscription = func.httpsCallable('linkingSubscription');
    if (isPlatform("ios")) {
      const priceItem = InAppPurchase2.get(priceCode);
      InAppPurchase2.order(priceItem).then((p: IAPProduct) => {
        console.log(p);
      }, (e: any) => {
        console.error(e)
      });
      // await handlePatentCreate();
    } else {
      try {
        if (!auth?.stripeId) {
          await register();
        }
        console.log('regist subscription');
        const sub = await subscription({ price: offer.price, attorneyPath: profileIdentify.path, appPath: app.path });
        await handlePatentCreate(sub.data.id, "web");
      } catch (e) {
        console.error(e, "失敗");
        window.alert("処理が失敗しました");
      }
    }
  }, [auth, app, offer, profileIdentify, func, handlePatentCreate, priceCode]);

  const priceIosVariable = React.useMemo(() => {
    return isPlatform('ios') ? priceIosFunc(offer.price) : offer.price;
  }, [offer.price, priceIosFunc]);
  return (
    <IonPage>
      <IonHeader>
        <Header title={fullName} left="back" center="namecase" right="close" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="home" />
            <main className="content__wrap">
              <p className="form__title">{app.title}</p>
              <span className="form__badge--recruitment">募集中</span>
              <div className="accordion accordion__wrap">
                <AccordionContentsAbout appPath={app.path} />
              </div>
              <div className="accordion accordion__wrap">
                <AccordionContentsAttorneyInfo attorneyPath={profileIdentify.path} />
              </div>
              <p className="form__title">プラン</p>
              <div className="gray__bg gray__bg__half">{priceIosVariable}プラン</div>
              {/* <p className="form__title">備考欄</p>
              <div className="gray__bg">{offer.remark}</div> */}
              <p className="form__title">決済方法</p>
              <div className="gray__bg gray__bg__half">{priceIosVariable}プラン</div>
              <div className="bank__info__btn-box">
                <button className="bank__info__btn" onClick={handleFixClick}>訂正する</button>
                <button className="bank__info__btn" onClick={handleSaveClick}>依頼する</button>
              </div>
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Full" page="home" />
        </IonFooter>
      </IonContent>
    </IonPage >
  );
};
export default ApplicantOfferCheck;
