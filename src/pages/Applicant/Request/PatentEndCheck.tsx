/**
 * @modules 保守管理確認ページ
 * route /applicant/patentend/check
 */

import { InAppPurchase2 } from '@ionic-native/in-app-purchase-2';
import { IonContent, IonFooter, IonHeader, IonPage } from '@ionic/react';
import 'assets/css/haseryo.css';
// css
import 'assets/css/style.css';
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
import 'firebase/functions';
import useApplication from 'hooks/application';
import useOffer from 'hooks/offer';
import useProfile from 'hooks/profile';
import React from 'react';
import { useLocation } from 'react-router-dom';
import AccordionContentsAbout from '../../../components/Accordion/AccordionContents/About';
import AccordionContentsAttorneyInfo from '../../../components/Accordion/AccordionContents/AttorneyInfo';
//XD17をコピー
export const PatentEndCheck: React.FC = () => {
  const location = useLocation<string>();
  const offerPath = location.state;
  const { offer } = useOffer("", offerPath);
  const { app } = useApplication(offer.application?.path);
  const { profile, profileIdentify } = useProfile(offer.attorney?.path);
  const fullName = profile.firstName + profile.lastName;

  InAppPurchase2.verbosity = InAppPurchase2.DEBUG;


  return (
    <IonPage>
      <IonHeader>
        <Header title={fullName} left="back" center="namecase" right="close" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="home" />
            <main className="content__wrap">
              <p className="form__title">{app.title}</p>
              <div className="accordion accordion__wrap">
                <AccordionContentsAbout appPath={app.path} />
              </div>
              <div className="accordion accordion__wrap">
                <AccordionContentsAttorneyInfo attorneyPath={profileIdentify.path} />
              </div>
              <div className="bank__info__btn-box">
                <button className="bank__info__btn">チャットに戻る</button>
                <button className="bank__info__btn">申請を完了する</button>
              </div>
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Full" page="home" />
        </IonFooter>
      </IonContent>
    </IonPage >
  );
};
export default PatentEndCheck;
