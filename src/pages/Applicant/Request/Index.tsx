import { IonContent, IonFooter, IonHeader, IonPage, isPlatform } from '@ionic/react';
import 'assets/css/haseryo.css';
// css
import 'assets/css/style.css';
import Footer from 'components/Footer/Index';
import CheckoutForm from 'components/Form/CreditCard';
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
import { ProfileSubmitContext } from 'data/context';
import 'firebase/functions';
import useApplication from 'hooks/application';
import useOffer from 'hooks/offer';
import useProfile from 'hooks/profile';
import React from 'react';
import { useHistory, useLocation } from 'react-router';
import AccordionContentsAbout from '../../../components/Accordion/AccordionContents/About';
import AccordionContentsAttorneyInfo from '../../../components/Accordion/AccordionContents/AttorneyInfo';
// interface cardProps {
//   id: string;
//   brand: string;
//   country: string;
//   exp_month: number;
//   exp_year: number;
//   last4: string;
// }
// XD16
export const ApplicantOffer: React.FC = () => {
  const location = useLocation<string>();
  const history = useHistory();

  const offerPath = location.state;

  const { offer, signal, setSignal } = useOffer("", offerPath);

  const { app } = useApplication(offer.application?.path);
  const { profile, profileIdentify } = useProfile(offer.attorney?.path);

  // const [card, setCard] = React.useState<cardProps[]>();
  // const [credit, setCredit] = React.useState<any>("");

  const handleClick = React.useCallback((e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault();
    setSignal(true);
    return history.push({
      pathname: "/applicant/request/check",
      state: offerPath
    })
  }, [history, offerPath, setSignal]);

  // const cardList = React.useMemo(() => {
  //   const func = firebase.functions();
  //   const list = func.httpsCallable('listApplicantCredit');
  //   let cards: cardProps[] = [];
  //   list().then((elements) => {
  //     elements.data?.forEach((value: any) => {
  //       cards.push({
  //         ...value?.card,
  //         id: value?.id
  //       });
  //     })
  //   });
  //   setCard(cards);
  //   return card?.map((value: cardProps, key: number) => (
  //     <div className="payment--card" key={key}>
  //       <input type="radio" id="card2" name={value.id} value={value.id} checked={value.id === credit} onClick={() => {
  //         setCredit(value.id);
  //       }} />
  //       <span className="card card--type">{value.brand}</span>
  //       <span className="card card-number">………{value.last4}</span>
  //     </div>
  //   ))
  // }, [card, setCard, credit, setCredit]);

  const stripeCreditForm = React.useMemo(() => {
    if (!isPlatform('ios') && !isPlatform('android')) {
      return (
        <React.StrictMode>
          <span className="form__title form__title--ib">決済方法を選択する</span><span className="form__badge--required">必須</span>
          {/* 決済ページここから */}
          <div className="payment">
            <ProfileSubmitContext.Provider value={{ signal, setSignal }}>
              <CheckoutForm>
                <span className="payment--card--new__title">その他の決済方法を追加する</span>
              </CheckoutForm>
            </ProfileSubmitContext.Provider>

          </div>
        </React.StrictMode>
      )
    }
  }, [signal, setSignal]);

  const priceIosFunc = React.useCallback((price: number) => {
    if (price === 30000) {
      return "スタンダード";
    } else if (price === 50000) {
      return "ハイグレード";
    }
  }, []);

  const priceIosVariable = React.useMemo(() => {
    return isPlatform('ios') ? priceIosFunc(offer.price) : offer.price;
  }, [offer.price, priceIosFunc]);

  return (
    <IonPage>
      <IonHeader>
        <Header title={profile.firstName + profile.lastName} cases={app.title} left="none" center="namecase" right="close" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="chat" />
            <main className="content__wrap">
              <p className="form__title">{app.title}</p>

              <span className="form__badge--recruitment">募集中</span>

              <div className="accordion accordion__wrap"><AccordionContentsAbout appPath={app.path} /></div>
              <div className="accordion accordion__wrap"><AccordionContentsAttorneyInfo attorneyPath={profileIdentify.path} /></div>
              <p className="form__title mb-10">プラン</p>
              <div className="gray__bg gray__bg__half">{priceIosVariable}プラン</div>
              {/* <p className="form__title mb-10">備考欄</p>
              <div className="gray__bg">{offer.remark}</div> */}

              {stripeCreditForm}
              <div className=" SP__btn__center">
                <button className="Btn__base SP__btn" onClick={handleClick}>
                  内容を確認する
                </button>
              </div>
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Full" page="home" />
        </IonFooter>
      </IonContent>
    </IonPage >
  );
};

export default ApplicantOffer;
