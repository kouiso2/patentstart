//ionic
import { IonContent, IonFooter, IonHeader, IonPage } from '@ionic/react';
import 'assets/css/haseryo.css';
// css
import 'assets/css/style.css';
import Footer from 'components/Footer/Index';
//original
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
import useApplication from 'hooks/application';
import useOffer from 'hooks/offer';
import useProfile from 'hooks/profile';
import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { RootState } from '../../../store/Combine';


export const ApplicantOfferComplete: React.FC = () => {
  const config = useSelector((state: RootState) => state.config);

  const { offer } = useOffer("", config.path);
  const { app } = useApplication(offer.application?.path);
  const { profile } = useProfile(offer.attorney?.path);
  return (
    //XD18
    <IonPage>
      <IonHeader>
        <Header title={profile.firstName + profile.lastName} cases={app.title} left="none" center="namecase" right="close" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="account" />
            <main className="content__wrap complete--page">
              <div className="complete__wrap">
                <h2 className="complete__title">依頼が完了しました</h2>
                <p className="complete__text">詳細な相談内容をやり取りして特許を取得しましょう</p>
                <div className="btn--center">
                  <Link to={{
                    pathname: `/applicant/chat/${config.room.id}`,
                  }} className="Btn__base PC__btn">チャットを送る</Link>
                </div>
              </div>
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Full" page="home" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};

export default ApplicantOfferComplete;
