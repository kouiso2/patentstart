//ionic
import { IonContent, IonFooter, IonHeader, IonPage, isPlatform } from '@ionic/react';
import 'assets/css/haseryo.css';
// css
import 'assets/css/style.css';
import Footer from 'components/Footer/Index';
//original
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
import useAuth from 'hooks/auth';
import React from 'react';
import { Link } from 'react-router-dom';

//XD100,XD101
/**
* ContactReportComplete:
* お問い合わせ、通報の完了ページを表示する
*/
export const ContactReportComplete: React.FC = () => {
  const auth = useAuth();
  return (
    //XD100,XD101
    <IonPage>
      <IonHeader>
        <Header title="パスワード変更の完了" left="back" center="title" right="none" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="home" />
            <main className="content__wrap complete--page">
              <div className="complete__wrap">
                <h2 className="complete__title">送信が完了しました</h2>
                <p className="complete__text">
                  ご意見を頂きましてありがとうございます。<br />
                  2~3営業日以内に担当者にて確認の上でご回答、対応を進めていきますので、今しばらくお待ち下さい。</p>
                <div className="btn--center">
                  <Link to={auth?.kind === "Applicant" ? "/applicant/home/" : "/attorney/home/"}>
                    <div className="Btn__base PC__btn">ホームに戻る</div>
                  </Link>
                </div>
              </div>
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Tabs" page={(isPlatform("ios") || isPlatform("android")) ? "account" : "home"} />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};

export default ContactReportComplete;
