//ionic
import { IonContent, IonFooter, IonHeader, IonPage, isPlatform } from '@ionic/react';
// css
import 'assets/css/style.css';
import Sidebar from 'components/SideBar/Index';
import firebase from 'firebase';
//customhook
import useAuth from 'hooks/auth';
import React from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { RootState } from 'store/Combine';
import Footer from '../../components/Footer/Index';
//original
import Header from '../../components/Header/index';

//XD101
/**
* ApplicantContact:
* お問い合わせページを表示する関数コンポーネント
*/
export const ApplicantContact: React.FC = () => {
  const history = useHistory();
  const auth = useAuth();
  const app = useSelector((state: RootState) => state.application);

  /**
  * handleSubmit():
  * お問い合わせページの「送信する」ボタンを押した時の処理が記述された関数
  * Cloud Firestoreにお問い合わせした人のユーザIDやお問い合わせ内容などを保存し、完了ページにリダイレクトさせる
  */
  async function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    let summary = "";
    let content = (e.currentTarget.elements[5] as HTMLTextAreaElement).value;

    if ((e.currentTarget.elements[0] as HTMLInputElement).checked) {
      summary = "不具合に関するお問い合わせ";
    } else if ((e.currentTarget.elements[1] as HTMLInputElement).checked) {
      summary = "データ復旧のお問い合わせ";
    } else if ((e.currentTarget.elements[2] as HTMLInputElement).checked) {
      summary = "ご意見・ご要望";
    } else if ((e.currentTarget.elements[3] as HTMLInputElement).checked) {
      summary = "退会のお問い合わせ";
    } else {
      summary = "その他のお問い合わせ";
    }
    const db = firebase.firestore().collection("Contact");
    await db.add({
      userId: auth?.uid,
      name: auth?.firstName+auth?.lastName,
      email: auth?.email,
      summary: summary,
      content: content,
      timestamp: firebase.firestore.FieldValue.serverTimestamp()
    });
    if (auth?.kind === "Applicant") {
      return history.push('/applicant/contact/complete');

    } else {
      return history.push('/attorney/contact/complete');

    }
  }

  return (
    <IonPage>
      <IonHeader>
        <Header title={app.title} left="back" center="title" right="none" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="home" />
            <main className="content__wrap">
              <form method="post" onSubmit={handleSubmit}>
                <div className="p-applicantContact__status">
                  <h3 className="p-applicantContact__title">お問い合わせ内容</h3>
                  <span className="form__badge--required">必須</span>
                </div>
                <div className="form">
                  <div className="form--plan p-applicantContact__form">
                    <input type="radio" id="plan1" name="contact" className="radioBtn__input" />
                    <label htmlFor="plan1" className="radioBtn__label"></label>
                    <label htmlFor="plan1" className="form--plan--price">
                      <p className="form--plan--price__title">不具合に関するお問い合わせ</p>
                    </label>
                  </div>

                  <div className="form--plan p-applicantContact__form">
                    <input type="radio" id="plan2" name="contact" className="radioBtn__input" />
                    <label htmlFor="plan2" className="radioBtn__label"></label>
                    <label htmlFor="plan2" className="form--plan--price">
                      <p className="form--plan--price__title">データ復旧のお問い合わせ</p>
                    </label>
                  </div>

                  <div className="form--plan p-applicantContact__form">
                    <input type="radio" id="card3" name="contact" className="radioBtn__input" />
                    <label htmlFor="card3" className="radioBtn__label"></label>
                    <label htmlFor="card3" className="form--plan--price">
                      <p className="form--plan--price__title">ご意見・ご要望</p>
                    </label>
                  </div>

                  <div className="form--plan p-applicantContact__form">
                    <input type="radio" id="plan5" name="contact" className="radioBtn__input" />
                    <label htmlFor="plan5" className="radioBtn__label"></label>
                    <label htmlFor="plan5" className="form--plan--price">
                      <p className="form--plan--price__title">退会のお問い合わせ</p>
                    </label>
                  </div>

                  <div className="form--plan p-applicantContact__form">
                    <input type="radio" id="card4" name="contact" className="radioBtn__input" />
                    <label htmlFor="card4" className="radioBtn__label"></label>
                    <label htmlFor="card4" className="form--plan--price">
                      <p className="form--plan--price__title">その他のお問い合わせ</p>
                    </label>
                  </div>

                  <div>
                    <div className="p-applicantContact__status--mt">
                      <p className="form__title p-applicantContact__form__title">具体的に内容を記入してください</p>
                      <span className="form__badge--required">必須</span>
                    </div>
                    <textarea className="form--plan--price__textarea p-applicantContact__textarea"></textarea>
                    <div className=" SP__btn__center p-applicantContact__btn">
                      <button className="Btn__base SP__btn p-applicantContact__btn--b" >
                        送信する
                      </button>
                    </div>
                  </div>
                </div>
              </form>
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Tabs" page={(isPlatform("ios") || isPlatform("android")) ? "account" : "home"} />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};

export default ApplicantContact;
