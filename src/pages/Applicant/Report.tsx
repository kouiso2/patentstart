//ionic
import { IonContent, IonFooter, IonHeader, IonPage, IonTextarea } from '@ionic/react';
// css
import 'assets/css/style.css';
import AcdContentsProf from 'assets/img/demo.jpg';
import Sidebar from 'components/SideBar/Index';
import firebase from 'firebase';
import useAuth from 'hooks/auth';
import useProfile from 'hooks/profile';
import React from 'react';
import { useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router';
import { RootState } from 'store/Combine';
import Footer from '../../components/Footer/Index';
//original
import Header from '../../components/Header/index';

//XD101
/**
* ApplicantReport:
* 通報ページを表示する関数コンポーネント
*/
export const ApplicantReport: React.FC = () => {
  const history = useHistory();
  const { id } = useParams<{ id: string }>();
  const app = useSelector((state: RootState) => state.application);
  const { profile } = useProfile(`User/${id}`);
  const auth = useAuth();

  /**
  * handleSubmit():
  * 通報ページの「送信する」ボタンを押した時の処理が記述された関数
  * Cloud Firestoreに通報した人のユーザIDやお問い合わせ内容などを保存し、完了ページにリダイレクトさせる
  */
  async function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    let summary = "";
    let content = (e.currentTarget.elements[5] as HTMLTextAreaElement).value;

    if ((e.currentTarget.elements[0] as HTMLInputElement).checked) {
      summary = "不審な内容・またはスパムである";
    } else if ((e.currentTarget.elements[1] as HTMLInputElement).checked) {
      summary = "私や他の利用者のなりすましをしている";
    } else if ((e.currentTarget.elements[2] as HTMLInputElement).checked) {
      summary = "攻撃的な内容や暴言、脅迫などが含まれる";
    } else if ((e.currentTarget.elements[3] as HTMLInputElement).checked) {
      summary = "ブロックしたい";
    } else {
      summary = "その他の通報";
    }

    const db = firebase.firestore().collection("Report");
    await db.add({
      reportedUserId: id,
      reportedUserName: profile.firstName+profile.lastName,
      reportedUserEmail: profile.email,
      summary: summary,
      content: content,
      timestamp: firebase.firestore.FieldValue.serverTimestamp()
    });
    return history.push(`${auth?.url}/report/complete`);
  }

  return (
    <IonPage>
      <IonHeader>
        <Header title={app.title} left="back" center="title" right="none" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="home" />
            <main className="content__wrap">
              <div className="p-applicantReport">
                <h3 className="p-applicantReport__reportTxt">こちらのユーザーを通報します</h3>
                <div className="accordion__contents__profile p-applicantReport__reportWrap">
                  <figure className="accordion__contents__profile__img p-applicantReport__reportWrap__img" style={{ backgroundImage: `url(${profile.image ? profile.image : AcdContentsProf})` }}></figure>
                  <div className="accordion__contents__profile__text p-applicantReport__reportWrap__text">
                    <h4 className="accordion__contents__commonTitle accordion__contents__profile__text__kanji p-applicantReport__reportWrap--fw">{profile.companyName}</h4>
                    <p className="accordion__contents__profile__text__kana">{profile.companyKana}</p>
                  </div>
                </div>
                <div className="p-applicantReport__name">
                  <div className="p-applicantReport__name__flex">
                    <p className="p-applicantReport__name__flex--w">担当者名</p>
                    <p className="p-applicantReport__name__flex--fw">{profile.firstName + profile.lastName}</p>
                  </div>
                  <div className="p-applicantReport__name__flex p-applicantReport__name__flex--mt">
                    <p className="p-applicantReport__name__flex--w">担当者名（フリガナ）</p>
                    <p className="p-applicantReport__name__flex--fw">{profile.personKatakana}</p>
                  </div>
                </div>
              </div>
              <form method="post" onSubmit={handleSubmit}>
                <div className="p-applicantContact__status">
                  <h3 className="p-applicantContact__title">通報の内容</h3>
                  <span className="form__badge--required">必須</span>
                </div>
                <div className="form">
                  <div className="form--plan p-applicantContact__form">
                    <input type="radio" id="plan1" name="report" className="radioBtn__input" />
                    <label htmlFor="plan1" className="radioBtn__label"></label>
                    <label htmlFor="plan1" className="form--plan--price">
                      <p className="form--plan--price__title">不審な内容・またはスパムである</p>
                    </label>
                  </div>
                  <div className="form--plan p-applicantContact__form">
                    <input type="radio" id="plan2" name="report" className="radioBtn__input" />
                    <label htmlFor="plan2" className="radioBtn__label"></label>
                    <label htmlFor="plan2" className="form--plan--price">
                      <p className="form--plan--price__title">私や他の利用者のなりすましをしている</p>
                    </label>
                  </div>
                  <div className="form--plan p-applicantContact__form">
                    <input type="radio" id="card3" name="report" className="radioBtn__input" />
                    <label htmlFor="card3" className="radioBtn__label"></label>
                    <label htmlFor="card3" className="form--plan--price">
                      <p className="form--plan--price__title">攻撃的な内容や暴言、脅迫などが含まれる</p>
                    </label>
                  </div>
                  <div className="form--plan p-applicantContact__form">
                    <input type="radio" id="card4" name="report" className="radioBtn__input" />
                    <label htmlFor="card4" className="radioBtn__label"></label>
                    <div className="form--plan--price">
                      <p className="form--plan--price__title">ブロックしたい</p>
                    </div>
                  </div>
                  <div className="form--plan p-applicantContact__form">
                    <input type="radio" id="card5" name="report" className="radioBtn__input" />
                    <label htmlFor="card5" className="radioBtn__label"></label>
                    <label htmlFor="card5" className="form--plan--price">
                      <p className="form--plan--price__title">その他の通報</p>
                    </label>
                  </div>
                  <div>
                    <div className="p-applicantContact__status--mt">
                      <p className="form__title p-applicantContact__form__title">具体的に内容を記入してください</p>
                      <span className="form__badge--required">必須</span>
                    </div>
                    <IonTextarea className="form--plan--price__textarea p-applicantContact__textarea"></IonTextarea>
                    <div className=" SP__btn__center p-applicantContact__btn">
                      <button className="Btn__base SP__btn p-applicantContact__btn--b" type="submit">
                        送信する
                      </button>
                    </div>
                  </div>
                </div>
              </form>
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Tabs" page="home" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};

export default ApplicantReport;
