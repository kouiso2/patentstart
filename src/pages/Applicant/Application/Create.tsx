//ionic
import { IonContent, IonFooter, IonHeader, IonInput, IonPage, IonRouterLink, IonTextarea } from '@ionic/react';
import 'assets/css/Form.css';
import 'assets/css/haseryo.css';
// css
import 'assets/css/style.css';
//original
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
import { categoryObjectChoice } from 'data/choice';
import React from 'react';
//redux
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { RootState } from 'store/Combine';
import { patentCategoryJpn } from 'translate';
import { categoryAction, documentAction, explanationAction, titleAction } from '../../../store/Application/action';

//XD9
type categoryKeyOnly = keyof typeof categoryObjectChoice;
export const ApplicationCreate: React.FC = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const app = useSelector((state: RootState) => state.application);
  const docInput = React.useRef<HTMLInputElement>(null);

  const handleSubmit = React.useCallback((e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    const docFile = docInput.current!.files![0];

    dispatch(documentAction(docFile));

    if (!app.category) {
      window.alert('カテゴリーを必ず一つ選択してください')
      return;
    };
    return history.push('/applicant/application/check/');
  }, [history, dispatch, app.category]);

  const categoryCheckbox = React.useMemo(() => {
    return (Object.keys(categoryObjectChoice) as (categoryKeyOnly)[]).map((category: categoryKeyOnly, key: number) => {
      return (
        <label htmlFor={String(key)} className="form__classification" key={String(key)} >
          <input
            type="radio"
            className=""
            id={String(key)}
            value={category}
            name={category}
            checked={app.category === category ? true : false}
            onClick={(e) => {
              e.preventDefault();

              dispatch(categoryAction(category));
            }}
          />
          {app.category === category ? (<div className="form__classification__checkbox--active"></div>) : <div className="form__classification__checkbox"></div>}
          <div className="">
            <p className="form__classification__title">{patentCategoryJpn(category)}</p>
            {/*
            <span className="form__classification__disc">

              {patentCategoryDescription(category)}</span> */}
          </div>
        </label>
      )
    })
  }, [app.category, dispatch]);

  const handleFileChange = React.useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
    e.preventDefault();
    const file = e.target.files![0];
    const type = file.type;
    const size = file.size;
    const limit = 2000000;
    if (type !== "application/pdf") {
      e.target.value = "";
      alert("pdfの形式でアップロードして下さい");
      return;
    }

    if (size > limit) {
      e.target.value = "";
      alert('PDFは2MB以下で指定してください');
      return;
    };

    dispatch(documentAction(file));
  }, [dispatch]);
  return (
    <IonPage>
      <IonHeader>
        <Header title="申請内容を作成" left="none" center="title" right="close" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="home" />
            <main className="content__wrap">
              <form method="post" onSubmit={handleSubmit}>
                <div className="">
                  <div className="form__title__wrap__flex mt-0">
                    <span className="form__title mt-0">特許タイトル</span>
                    <span className="form__badge--required">必須</span>
                  </div>
                  <IonInput
                    onIonChange={(e): void => {
                      e.preventDefault();
                      dispatch(titleAction(e.detail.value!))
                    }}
                    value={app.title}
                    placeholder="申請を行いたい特許について簡単に記載する"
                    className="form__input"
                    type="text"
                    required={true} />
                </div>
                <div className="form__title__wrap__flex">
                  <span className="form__title">カテゴリー</span><span className="form__badge--required">必須</span>
                </div>
                {categoryCheckbox}
                <span className="form__classification--undertext">カテゴリーは「<IonRouterLink className="form__classification--undertext__link" target="_blank" href="https://www.jpo.go.jp/system/patent/gaiyo/bunrui/ipc/ipc8wk.html">国際特許分類</IonRouterLink>」に従い、分野を選択してください。</span>

                <div className="form__title__wrap__flex">
                  <span className="form__title">特許の説明</span>
                  <span className="form__badge--required">必須</span>
                </div>
                <IonTextarea
                  onIonChange={(e): void => {
                    dispatch(explanationAction(e.detail.value!));
                  }}
                  className="form__input form__textarea"
                  placeholder="簡単な特許の説明"
                  value={app.explanation}
                  required={true} />

                <div className="form__title__wrap__flex">
                  <span className="common__form__title mt-0">PDFファイルの添付</span>
                  <span className="form__badge--required mb-10">必須</span>
                </div>
                <label htmlFor="file1" className="identification__input">ファイルをアップロード</label>
                <input type="file" name="document" id="file1" ref={docInput} onChange={handleFileChange} />
                <p className="identification__input__name">{app.document?.name}</p>
                <p className="identification__disctext">申請内容に関する資料などPDFファイルをアップロードできます。<br />
                  ※2MB以下
                  </p>
                <div className=" SP__btn__center">
                  <button className="Btn__base SP__btn" type="submit">
                    作成する
                  </button>
                </div>
              </form>
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Tabs" page="home" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};

export default ApplicationCreate;
