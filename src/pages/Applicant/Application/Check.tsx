//ionic
import { IonContent, IonFooter, IonHeader, IonLoading, IonPage } from '@ionic/react';
import 'assets/css/haseryo.css';
import 'assets/css/style.css';
import Footer from 'components/Footer/Index';
//original
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
//firebase
import firebase from 'firebase';
import 'firebase/firestore';
import 'firebase/storage';
// css
import React, { useState } from 'react';
//redux
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { RootState } from 'store/Combine';
import { noValueText } from '../../../controller/index';
import { patentCategoryJpn } from '../../../translate/index';
//XD10
export const ApplicationCreateCheck: React.FC = () => {
  const history = useHistory();

  const [showLoading, setShowLoading] = useState(false);
  const app = useSelector((state: RootState) => state.application);
  const handleFixClick = React.useCallback((e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault();
    return history.goBack();
  }, [history]);

  const handleSaveClick = React.useCallback(async (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault();
    const auth = firebase.auth().currentUser;
    const db = firebase.firestore();
    const storage = firebase.storage();
    setShowLoading(true);
    const dbRef = db.collection("User").doc(auth?.uid).collection("Application");
    if (app.document) {
      const docRef = storage.ref().child(`application/document/${app.document.name}`);
      const docRes = await docRef.put(app.document, { contentType: `${app.document.type}` });
      const docUrl = await docRes.ref.getDownloadURL();
      await dbRef.add({
        ...app,
        document: docUrl,
        createDt: firebase.firestore.FieldValue.serverTimestamp(),
        creater: firebase.firestore().collection("User").doc(auth?.uid)
      });
    } else {
      dbRef.add({
        ...app,
        createDt: firebase.firestore.FieldValue.serverTimestamp(),
        creater: firebase.firestore().collection("User").doc(auth?.uid)
      });
    }
    return history.push('/applicant/home/');
  }, [history, app]);
  return (
    <IonPage>
      <IonHeader>
        <Header title="特許申請の確認" left="back" center="title" right="close" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <IonLoading
          cssClass='my-custom-class'
          isOpen={showLoading}
          onDidDismiss={() => setShowLoading(false)}
          message={'しばらくお待ち下さい...'}
          duration={5000}
        />
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="home" />
            <main className="content__wrap">
              <div className="bank__info__item">
                <p className="common__form__title">特許タイトル</p>
                <p className="common__form__title--gray--bg">{app.title}</p>
              </div>
              <div className="bank__info__item bank__info__item--half">
                <p className="common__form__title">カテゴリー</p>
                <p className="common__form__title--gray--bg">{patentCategoryJpn(app.category!)}</p>
              </div>
              <div className="bank__info">
                <div className="bank__info__item">
                  <p className="common__form__title">特許の概要</p>
                  <p className="common__form__title--gray--bg common__form__title--gray--bg--l">{app.explanation}</p>
                </div>
                <div className="bank__info__item">
                  <p className="common__form__title">PDFファイルの添付</p>
                  <li className="file__item">{noValueText(app.document, app.document?.name)}
                    {/* <a target="_blank" href={app.document} className="file__item__link">プレビュー</a> */}
                  </li>
                </div>
                <div className="bank__info__btn-box">
                  <button className="bank__info__btn" onClick={handleFixClick}>訂正する</button>
                  <button className="bank__info__btn" onClick={handleSaveClick}>保存する</button>
                </div>
              </div>
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Full" page="home" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};
export default ApplicationCreateCheck;
