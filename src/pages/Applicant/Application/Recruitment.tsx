//ionic
import { IonContent, IonFooter, IonHeader, IonPage } from '@ionic/react';
// css
import 'assets/css/style.css';
import { NoneArrivalsListChild } from 'components/ArrivalsListChild';
import Footer from 'components/Footer/Index';
//original
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
import 'firebase/storage';
import useApplication from 'hooks/application';
import React from 'react';
import { useLocation } from 'react-router';
import "../../../assets/css/accordion.css";
import AccordionContentsAbout from '../../../components/Accordion/AccordionContents/About';
import ChatListChild from '../../../components/chat/ChatListChild';


//XD2,XD4
export const Recruitment: React.FC = () => {
  const location = useLocation<string>();
  const { app } = useApplication(location.state);
  const chatListChild = React.useMemo(() => {
    if (app.recruit?.length === 0) {
      return (
        <NoneArrivalsListChild />
      )
    } else {
      return app.recruit?.map((value: any) => {
        return (
          <ChatListChild appId={app.id} appPath={value.path} />
        );
      })
    }
  }, [app]);
  return (
    <IonPage>
      <IonHeader>
        <Header title={app.title} left="back" center="title" right="none" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="home" />
            <main className="content__wrap">
              <div className="">
                <div className="chatlist__wrap">
                  <div className="recruitment__flex">
                    <h3 className="chatlist__wrap__title recruitment__title">リクエスト一覧</h3>
                    <span className="recruitment__number">全{app.recruit?.length}件</span>
                  </div>
                  {chatListChild}
                </div>
                <span className="applying__container__flex__title">{app?.title}</span>
                <span className="applying__container__flex__status">申請中</span>
                {/* 依頼内容 */}
                <div className="accordion accordion__wrap">
                  <AccordionContentsAbout appPath={location.state} />
                </div>

              </div>
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Tabs" page="home" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};

export default Recruitment;
