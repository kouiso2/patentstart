//ionic
import { IonContent, IonFooter, IonHeader, IonPage } from '@ionic/react';
// css
import 'assets/css/style.css';
import AccordionContentsDetail from 'components/Accordion/AccordionContents/PatentDetail';
import AccordionContentsPlan from 'components/Accordion/AccordionContents/Plan';
//original
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
import 'firebase/firestore';
import useApplication from 'hooks/application';
import useAuth from 'hooks/auth';
import React from 'react';
import { useLocation } from 'react-router-dom';
import AccordionContentsApplicantInfo from '../../../components/Accordion/AccordionContents/ApplicantInfo';
import AccordionContentsAttorneyInfo from '../../../components/Accordion/AccordionContents/AttorneyInfo';


//XD33 XD7
export const Management: React.FC = () => {
  const location = useLocation<string>();
  const path = location.state;
  const { app } = useApplication(path);
  const auth = useAuth();

  const badge = React.useMemo(() => {
    if (app.status === "Apply") {
      return <span className="tokkyo--state--applying">申請中</span>
    } else if (app.status === "Manage") {
      return <span className="tokkyo--state--management">管理中</span>
    } else {
      return <span className="tokkyo--state--management">応募中</span>
    }
  }, [app.status]);

  const infoComponent = React.useMemo(() => {
    if (auth?.kind === "Attorney") {
      return (
        <div className="accordion accordion__wrap">
          <AccordionContentsApplicantInfo applicantPath={app.creater?.path} />
        </div>
      );
    } else if (auth?.kind === "Applicant") {
      return (
        <div className="accordion accordion__wrap">
          <AccordionContentsAttorneyInfo attorneyPath={app.attorney?.path} />
        </div>
      )
    }
  }, [auth?.kind, app.creater?.path, app.attorney?.path]);

  return (
    <IonPage>
      <IonHeader>
        <Header title={app.title} left={auth?.kind === "Applicant" ? "back" : "none"} center="title" right={auth?.kind === "Applicant" ? "none" : "close"} />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="home" />
            <main className="content__wrap">
              <p className="tokkyo__title">{app.title}</p>
              {badge}
              {infoComponent}
              <div className="accordion accordion__wrap"><AccordionContentsDetail patentPath={app.patent?.path} /></div>
              <div className="accordion accordion__wrap"><AccordionContentsPlan price={app.price} /></div>
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Tabs" page="home" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};

export default Management;
