//ionic
import { IonContent, IonFooter, IonHeader, IonPage } from '@ionic/react';
// css
import 'assets/css/style.css';
// img
// commonComponent
// import HeaderChatlistSP from 'components/Header/HeaderChatlistSP';
//chatlist
import ChatListWrap from 'components/chat/ChatListWrap';
//original
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
import React from 'react';



export const ChatList: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <Header title="チャット一覧" left="none" center="title" right="none" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="chat" />
            <main className="content__wrap">
              <ChatListWrap>
                {/* <ChatListChild status="None"  /> */}
              </ChatListWrap>
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Tabs" page="chat" />
        </IonFooter>
      </IonContent>
    </IonPage>

  );
};
export default ChatList;
