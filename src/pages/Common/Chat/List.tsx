//ionic
import { IonContent, IonFooter, IonHeader, IonPage } from '@ionic/react';
import 'assets/css/haseryo.css';
// css
import 'assets/css/style.css';
//original
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
import List from 'components/Room/List';
import Sidebar from 'components/SideBar/Index';
import React from 'react';



/**
* ChatList:
* トークルーム一覧ページを表示する
*/
export const ChatList: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <Header title="チャット一覧" left="none" center="title" right="none" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="chat" />
            <main className="content__wrap">

              <List />
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Tabs" page="chat" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );

};

export default ChatList;
