//ionic
import { IonContent, IonFooter, IonHeader, IonPage } from '@ionic/react';
// css
import 'assets/css/style.css';
import InfoIcon from 'assets/img/common/info__icon.svg';
//original
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
import MessageInputPC from 'components/Message/InputPC';
import { default as MessageInputSP } from 'components/Message/InputSP';
import MessageList from 'components/Message/List';
import Sidebar from 'components/SideBar/Index';
import firebase from 'firebase';
import 'firebase/auth';
import 'firebase/firestore';
import useApplication from 'hooks/application';
import useAuth from 'hooks/auth';
import useProfile from 'hooks/profile';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { RootState } from 'store/Combine';
import { modalConfigAction } from 'store/Config/action';

//XD12
/**
* Chat:
* チャットのトークルームページを表示する
* returnのメイン部分ではMessageListコンポーネント(複数のメッセージを表示するコンポーネント)とMessageInputコンポーネント(トークルームのインプットエリアを表示するコンポーネント)を表示している
*/
const Chat: React.FC = () => {
  const history = useHistory();
  const auth = useAuth();
  // チュートリアルモーダルの処理
  const dispatch = useDispatch();
  const config = useSelector((state: RootState) => state.config);
  const [ModalBoolean, setModalBoolean] = React.useState(Boolean);
  const { app } = useApplication(config.application.path);
  const handleAppDetail = React.useCallback(() => {

    if (auth?.kind === 'Applicant') {
      if (app.status === "Apply") {
        // 申請中
        return history.push({
          pathname: `/applicant/application/apply/${app.path.slice(-20)}/`,
          state: app.path
        });
      } else if (app.status === "Recruit") {
        // 応募中
        return history.push({
          pathname: `/applicant/recruitment/${app.path.slice(-20)}/`,
          state: app.path
        });
      } else {
        // 管理中
        return history.push({
          pathname: `/applicant/application/management/${app.path.slice(-20)}/`,
          state: app.path
        });
      }
    } else {
      // Attorney
      if (app.status === "Apply") {
        // 申請中
        return history.push({
          pathname: `/attorney/application/apply/${app.path.slice(-20)}/`,
          state: app.path
        });
      } else if (app.status === "Recruit") {
        // 応募中
        return history.push({
          pathname: `/attorney/newarrivals/detail/${app.path.slice(-20)}/`,
          state: app.path
        });
      } else {
        // 管理中
        console.log("管理中")
        return history.push({
          pathname: `/attorney/application/management/${app.path.slice(-20)}/`,
          state: app.path
        });
      }
    }
  }, [app.path, app.status, auth?.kind, history]);
  const { profile } = useProfile(config.attorney.path);


  React.useEffect(() => {
    let unmounted = true;
    const dbRef = firebase.firestore().collection("User").doc(firebase.auth().currentUser?.uid);

    // モーダルのbooleanを取得
    dbRef.onSnapshot((snap: firebase.firestore.DocumentSnapshot) => {
      setModalBoolean(snap.data()?.tutorial?.chat);
    });
    if (ModalBoolean === true) {
      dbRef.update({
        tutorial: {
          chat: false,
          home: false
        }
      }).then(() => {
        if (unmounted) {
          dispatch(modalConfigAction({
            ...config.modal,
            isOpen: true,
            form: "ChatTutorial"
          }));
        }
      })
    }
    return () => {
      // cleanup
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      unmounted = false;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [setModalBoolean, config, dispatch]);

  function isSmartPhone() {
    // UserAgentからのスマホ判定
    if (navigator.userAgent.match(/iPhone|Android.+Mobile/)) {
      return false;
    } else {
      return true;
    }
  }
  return (
    <IonPage>
      <IonHeader>
        <Header title={profile.firstName + profile.lastName} cases={app.title} left="back" center="namecase" right="info" />
      </IonHeader>
      <IonContent scrollEvents={false} scrollY={isSmartPhone()}>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="chat" />
            <main className="content__wrap content__wrap--chatPage" style={{ 'overflow': 'hidden' }}>
              <header className="header__base header__chat pc">
                <div className="header__desc">
                  <p className="header__desc--bold">{profile.firstName + profile.lastName}</p>
                  <p className="header__desc--text">{app.title}</p>
                </div>
                {app.path ?
                  <img className="header__chat--info hover__pointer" src={InfoIcon} alt="Information" onClick={handleAppDetail} />
                  :
                  <React.StrictMode></React.StrictMode>
                }
              </header>
              <MessageList />
              <MessageInputPC />
              <MessageInputSP />
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Full" page="chat" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};

export default Chat;
