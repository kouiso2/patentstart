//ionic
import { IonContent, IonFooter, IonHeader, IonPage } from '@ionic/react';
import 'assets/css/Chat.css';
// css
import 'assets/css/style.css';
import AlbumList from 'components/Album/List';
//original
import Footer from 'components/Footer/Index';
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
import React, { useState } from 'react';


/**
* SaveFolder:
* アルバムページを表示する関数コンポーネント
*/
export const SaveFolder: React.FC = () => {
  const [HeadText, setHeadText] = useState(false);
  const [HeadNum, setHeadNum] = useState(0);
  const changeHeadNum = (select: number) => {
    setHeadNum(select);
  }
  const changeHead = (isState: boolean) => {
    setHeadText(isState);
  }

  /**
  * return(戻り値):
  * メイン部分でAlbumListコンポーネントを表示している
  */
  return (
    <IonPage>
      <IonHeader>
        <Header title={HeadText ? HeadNum + "件選択中" : "保存用フォルダ"} left="none" center="title" right="close" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <section className="inner">
          <div className="main__wrap album__wrap">
            <Sidebar page="chat" />
            <main className="content__wrap">
              <AlbumList changeHead={changeHead} selectNum={changeHeadNum} />
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Full" page="chat" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};

export default SaveFolder;
