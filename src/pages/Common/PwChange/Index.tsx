//ionic
import { IonContent, IonFooter, IonHeader, IonInput, IonPage, IonRouterLink } from '@ionic/react';
// css
import 'assets/css/style.css';
import Footer from 'components/Footer/Index';
//original
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
import firebase from 'firebase';
import 'firebase/auth';
import useAuth from 'hooks/auth';
import React from 'react';
import { useHistory } from 'react-router';
interface PasswordFormModel {
  current: string;
  new: string;
  confirm: string;
}
// XD27
export const PasswordUpdate: React.FC = () => {
  const history = useHistory();
  const auth = useAuth();

  const [password, setPassword] = React.useState<PasswordFormModel>({
    current: "",
    confirm: "",
    new: ""
  });

  const [validate, setValidate] = React.useState<boolean>(false);

  const handleSubmit = React.useCallback(async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    const user = firebase.auth().currentUser;
    const result = await firebase.auth().signInWithEmailAndPassword(user?.email!, password.current);
    if (result.user) {
      return await result.user.updatePassword(password.new)
        .then(() => {
          if (auth?.kind === "Attorney") {
            history.push({
              pathname: '/attorney/pwchange/complete',
              state: "Attorney"
            });
          } else if (auth?.kind === "Applicant") {
            history.push({
              pathname: '/applicant/pwchange/complete',
              state: "Applicant"
            });
          }
        });
    }
  }, [history, password, auth?.kind]);

  const validatePassword = React.useCallback(() => {
    if (password.new !== password.confirm) {
      setValidate(true);
    } else {
      setValidate(false);
    }
  }, [password, setValidate]);

  return (
    <IonPage>
      <IonHeader>
        <Header title="パスワード変更" left="back" center="title" right="none" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="account" />
            <main className="content__wrap">
              <form onSubmit={handleSubmit}>
                <div className="form__title__wrap">
                  <div className="form__title__wrap__flex">
                    <span className="form__title">現在のパスワード</span>
                    <span className="form__badge--required">必須</span>
                  </div>
                  <IonInput
                    autocomplete="current-password"
                    placeholder="パスワード"
                    className="form__input"
                    type="password"
                    id="password"
                    name="password"
                    min="8"
                    max="128"
                    value={password?.current}
                    onIonChange={(e) => {
                      e.preventDefault();
                      setPassword({
                        ...password,
                        current: e.detail.value!
                      })
                    }}
                    required={true} />
                  <p className="identification__disctext mt-10">パスワードをお忘れの方は
                   <IonRouterLink className="auth--link--primary" rel="noopener noreferrer" href="/auth/pwreset/first/">こちら</IonRouterLink>
                  </p>
                </div>



                <div className="form__title__wrap">

                  <div className="form__title__wrap__flex">
                    <span className="form__title">新しいパスワード</span>
                    <span className="form__badge--required">必須</span>
                  </div>
                  <IonInput
                    autocomplete="new-password"
                    placeholder="パスワード"
                    className="form__input"
                    type="password"
                    id="password"
                    name="password"
                    min="8"
                    max="128"
                    value={password?.new}
                    required={true}
                    onIonChange={(e) => {
                      e.preventDefault();
                      setPassword({
                        ...password,
                        new: e.detail.value!
                      })
                    }}
                  />
                  <p className="identification__disctext mt-10">8文字以上のパスワードを入力してください。</p>
                </div>

                <div className="form__title__wrap">

                  <div className="form__title__wrap__flex">
                    <span className="form__title">確認用パスワード</span>
                    <span className="form__badge--required">必須</span>
                  </div>
                  <IonInput
                    autocomplete="new-password"
                    placeholder="パスワード"
                    className="form__input"
                    type="password"
                    id="password"
                    name="password"
                    min="8"
                    max="128"
                    value={password?.confirm}
                    required={true}
                    onIonBlur={validatePassword}
                    onIonChange={(e) => {
                      e.preventDefault();
                      setPassword({
                        ...password,
                        confirm: e.detail.value!
                      })
                    }}
                  />
                </div>
                <p className="validate__text">
                  {validate ? "確認用のパスワードと一致しません" : ""}
                </p>
                <div className=" SP__btn__center SP__btn__update">
                  <button className="Btn__base SP__btn" type="submit">
                    変更する
                  </button>
                </div>
              </form>
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Tabs" page="account" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};

export default PasswordUpdate;
