//ionic
import { IonContent, IonFooter, IonHeader, IonPage, IonRouterLink } from '@ionic/react';
import 'assets/css/haseryo.css';
// css
import 'assets/css/style.css';
import Footer from 'components/Footer/Index';
//original
import Header from 'components/Header/index';
import Sidebar from 'components/SideBar/Index';
import React from 'react';
import { useLocation } from 'react-router';

//XD28
export const PwChangeComplete: React.FC = () => {
  const location = useLocation<string>();
  const kind = location.state;

  const kindHomeLink = React.useMemo(() => {
    if (kind === "Attorney") {
      return (
        <IonRouterLink className="" href="/attorney/home/">
          <div className="Btn__base PC__btn">ホームに戻る</div></IonRouterLink>
      );
    } else if (kind === "Applicant") {
      return (
        <IonRouterLink className="" href="/applicant/home/">
          <div className="Btn__base PC__btn">ホームに戻る</div>
        </IonRouterLink>
      )
    }
  }, [kind])
  return (
    //XD28
    <IonPage>
      <IonHeader>
        <Header title="パスワード変更の完了" left="none" center="title" right="none" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="account" />
            <main className="content__wrap complete--page">
              <div className="complete__wrap">
                <h2 className="complete__title">変更が完了しました</h2>
                <p className="complete__text">パスワードの変更が完了しました。
登録しているメールアドレスに確認メールを送信しましたのでご確認お願いいたします。</p>
                <div className="btn--center">
                  {kindHomeLink}
                </div>
              </div>
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Tabs" page="account" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};

export default PwChangeComplete;
