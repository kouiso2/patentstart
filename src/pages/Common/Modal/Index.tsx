import ModalAlbum from 'components/Modal/ModalAlbum';
import { modalChoice } from 'data/choice';
import React from 'react';
import AccountProfileModal from './AccountProfileModal';
import OpponentProfileModal from './OpponentProfileModal';
import ApplicantHomeTutorial from './Tutorial/ApplicantHomeTutorial';
import AttorneyHomeTutorial from './Tutorial/AttorneyHomeTutorial';
import ChatTutorial from './Tutorial/ChatTutorial';


interface modalProps {
  kind: typeof modalChoice[number];
  id: string;
  idSub: string;
  imgUrl: string;
}

export const ModalPage: React.FC<modalProps> = ({ kind, id, idSub, imgUrl, }) => {
  const components = React.useMemo(() => {
    // モダール分岐
    if (kind === "Profile") {
      return (<AccountProfileModal />)
    } else if (kind === "OpponentProfile") {
      return (<OpponentProfileModal />)
    } else if (kind === "Chat") {
      return (<ModalAlbum roomId={id} messageId={idSub} imgUrl={imgUrl} />);
    } else if (kind === "Home" || kind === "ApplicantHomeTutorial") {
      return (<ApplicantHomeTutorial />);
    } else if (kind === "AttorneyHomeTutorial") {
      return (<AttorneyHomeTutorial />);
    } else if (kind === "ChatTutorial") {
      return (<ChatTutorial />);
    }
    else {
      return (<ChatTutorial />);
    }
  }, [kind, imgUrl, idSub, id]);

  return (
    <div >
      {components}
    </div>
  );
}

export default ModalPage;
