import 'assets/css/style.css';
import ModalApplicantProf from 'components/Modal/Applicant';
import ModalAttorneyProf from 'components/Modal/Attorney';
import 'firebase/firestore';
import useAuth from 'hooks/auth';
import React from 'react';

export const AccountProfileModal: React.FC = () => {
  const auth = useAuth();
  const ProfType = React.useMemo(() => {
    if (auth?.kind === "Applicant") {
      return (
        <ModalApplicantProf />
      );
    } else if (auth?.kind === "Attorney") {
      return (
        <ModalAttorneyProf />
      );
    }
  }, [auth?.kind]);
  return (
    <div>
      {ProfType}
    </div>

  );
};

export default AccountProfileModal;
