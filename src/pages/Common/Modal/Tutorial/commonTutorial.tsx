import { IonButton, IonButtons, IonContent, IonHeader, IonModal, IonPage, IonSlide, IonSlides, IonToolbar } from '@ionic/react';
import 'assets/css/Tutorial.css';
import "firebase/auth";
import "firebase/firestore";
import React from 'react';
import { useHistory } from 'react-router';




// モーダルの雛形を作成して使いまわし
// ファイル複製か１つのファイルで使い回すのかは後で決める
export const Pages: React.FC = () => {
  const history = useHistory();
  const [showSkip, setShowSkip] = React.useState(true);
  const [showModal, setShowModal] = React.useState(false);
  const slideRef = React.useRef<HTMLIonSlidesElement>(null);

  // useIonViewWillEnter(() => {
  //   setMenuEnabled(false);
  // });

  const startApp = async () => {
    history.push('/tabs/schedule/', { direction: 'none' });
  };

  const handleSlideChangeStart = () => {
    slideRef.current!.isEnd().then(isEnd => setShowSkip(!isEnd));
  };
  return (
    <IonPage id="tutorial-page"  >
      <IonHeader no-border>
        <IonToolbar>
          <IonButtons slot="end">
            {showSkip && <IonButton color='primary' onClick={startApp}>Skip</IonButton>}
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen >

        <IonSlides ref={slideRef} onIonSlideWillChange={handleSlideChangeStart} pager={false}>
          <IonSlide>
            <img src="assets/img/ica-slidebox-img-1.png" alt="" className="slide-image" />
            <h2 className="slide-title">
              <b>PatentStart</b>へようこそ
            </h2>
            <p>
              <b>PatentStart</b>は特許を申請するためのアプリです
            </p>
          </IonSlide>

          <IonSlide>
            <img src="assets/img/ica-slidebox-img-4.png" alt="" className="slide-image" />
            <h2 className="slide-title">さあ始めましょう！</h2>
            <IonButton fill="clear" onClick={() => { setShowModal(true) }}>
              今すぐアプリを使ってみる

            </IonButton>
          </IonSlide>
        </IonSlides>
        <IonModal isOpen={showModal} showBackdrop={true} >
          <IonButton onClick={() => { history.push('/admin/dashboard/') }}>ダッシュボードへ行く</IonButton>
        </IonModal>
      </IonContent>
    </IonPage>
  );
};

export default Pages;
