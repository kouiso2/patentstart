//ionic
import { IonContent, IonPage, IonSlide, IonSlides } from '@ionic/react';
import "assets/css/modal.css";
import "assets/css/style.css";
//css
import 'assets/css/Tutorial.css';
import addIcon from 'assets/img/tutorial-add.svg';
import STEP1PCimg from 'assets/img/tutorial/tutorial_applicant_home_1_pc.png';
import STEP1SPimg from 'assets/img/tutorial/tutorial_applicant_home_1_sp.png';
import STEP2PCimg from 'assets/img/tutorial/tutorial_applicant_home_2_pc.png';
import STEP2SPimg from 'assets/img/tutorial/tutorial_applicant_home_2_sp.png';
import STEP3PCimg from 'assets/img/tutorial/tutorial_applicant_home_3_pc.png';
import STEP3SPimg from 'assets/img/tutorial/tutorial_applicant_home_3_sp.png';
import STEP4PCimg from 'assets/img/tutorial/tutorial_applicant_home_4_pc.png';
import STEP4SPimg from 'assets/img/tutorial/tutorial_applicant_home_4_sp.png';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'store/Combine';
import { modalConfigAction } from 'store/Config/action';



// モーダルの雛形を作成して使いまわし
// ファイル複製か１つのファイルで使い回すのかは後で決める
export const ApplicantHomeTutorial: React.FC = () => {
  const [showSkip, setShowSkip] = React.useState<boolean>(true);
  const dispatch = useDispatch();
  const config = useSelector((state: RootState) => state.config);
  const slideRef = React.useRef<HTMLIonSlidesElement>(null);
  const handleNext = () => slideRef.current?.slideNext();
  const handleClose = React.useCallback(() => {
    dispatch(modalConfigAction({
      ...config.modal,
      isOpen: false,
    }))
  }, [dispatch, config]);

  const handleSlideChangeStart = () => {
    slideRef.current!.isEnd().then(isEnd => setShowSkip(!isEnd));
    console.log(showSkip);
  };

  return (
    <div className="tutorial">
      <IonPage>
        <IonContent className="tutorial__wrap">

          <IonSlides ref={slideRef} onIonSlideWillChange={handleSlideChangeStart} pager={true} >
            <IonSlide className="tutorial-page_wrap_item">
              <h2 className="modal--tutorial__title">STEP.1</h2>
              <article className="modal--tutorial__img modal--tutorial__img--pc" style={{ backgroundImage: `url(${STEP1PCimg})` }}></article>
              <article className="modal--tutorial__img modal--tutorial__img--sp" style={{ backgroundImage: `url(${STEP1SPimg})` }}></article>

              <p className="modal--tutorial__text">「アカウント」から<br />プロフィールを入力しましょう</p>
              <div className="btn--center modal--tutorial__button">
                <button className="Btn__base PC__btn" onClick={handleNext}>次へ</button>
              </div>
            </IonSlide>

            <IonSlide>
              <h2 className="modal--tutorial__title">STEP.2</h2>
              <article className="modal--tutorial__img modal--tutorial__img--pc" style={{ backgroundImage: `url(${STEP2PCimg})` }}></article>
              <article className="modal--tutorial__img modal--tutorial__img--sp" style={{ backgroundImage: `url(${STEP2SPimg})` }}></article>
              <p className="modal--tutorial__text">右下の<img className="modal--tutorial__text__img" src={addIcon} alt="addicon" />マークを選択し、<br />申請情報を作成しましょう</p>
              <div className="btn--center modal--tutorial__button">
                <button className="Btn__base PC__btn" onClick={handleNext}>次へ</button>
              </div>
            </IonSlide>

            <IonSlide>
              <h2 className="modal--tutorial__title">STEP.3</h2>
              <article className="modal--tutorial__img modal--tutorial__img--pc" style={{ backgroundImage: `url(${STEP3PCimg})` }}></article>
              <article className="modal--tutorial__img modal--tutorial__img--sp" style={{ backgroundImage: `url(${STEP3SPimg})` }}></article>
              <p className="modal--tutorial__text">リクエストが届いたら<br />弁理士にチャットで<br />申請内容を相談しましょう</p>
              <div className="btn--center modal--tutorial__button">
                <button className="Btn__base PC__btn" onClick={handleNext}>次へ</button>
              </div>
            </IonSlide>

            <IonSlide>
              <h2 className="modal--tutorial__title">STEP.4</h2>
              <article className="modal--tutorial__img modal--tutorial__img--pc" style={{ backgroundImage: `url(${STEP4PCimg})` }}></article>
              <article className="modal--tutorial__img modal--tutorial__img--sp" style={{ backgroundImage: `url(${STEP4SPimg})` }}></article>
              <p className="modal--tutorial__text">お願いする弁理士を選び<br />決済をして正式に<br />申請を依頼しましょう</p>
              <div className="btn--center modal--tutorial__button">
                <button className="Btn__base PC__btn" onClick={handleClose}>はじめる</button>
              </div>
            </IonSlide>
          </IonSlides>

        </IonContent>
      </IonPage>
    </div>
  );
};

export default ApplicantHomeTutorial;
