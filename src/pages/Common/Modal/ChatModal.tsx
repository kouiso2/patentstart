import 'assets/css/style.css';
import ModalApplicantProf from "components/Modal/Applicant";
import ModalAttorneyProf from "components/Modal/Attorney";
import 'firebase/firestore';
import useAuth from 'hooks/auth';
import React from 'react';


export const ChatModal: React.FC = () => {
  const auth = useAuth();
  const ProfType = React.useMemo(() => {
    if (auth?.kind === "Applicant") {

      return (
        <ModalAttorneyProf />
      );
    } else if (auth?.kind === "Attorney") {
      return (
        <ModalApplicantProf />
      );
    }
  }, [auth?.kind]);
  return (
    <div>
      {ProfType}
    </div>
  );
};

export default ChatModal;
