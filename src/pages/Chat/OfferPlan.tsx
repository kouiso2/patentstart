//ionic
import { IonContent, IonFooter, IonHeader, IonPage } from '@ionic/react';
//css
import 'assets/css/Home.css';
import Sidebar from 'components/SideBar/Index';
//firebase
import firebase from 'firebase';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';
import { allAction } from 'store/Application/action';
import { RootState } from 'store/Combine';
import { allUserAction } from 'store/User/action';
import Footer from '../../components/Footer/Index';
//original
import Header from '../../components/Header/index';

//XD
export const OfferPlan: React.FC = () => {
  const dispatch = useDispatch();

  const history = useHistory();

  const location = useLocation();
  const { path } = location.state as { path: string };

  const app = useSelector((state: RootState) => state.application);
  const user = useSelector((state: RootState) => state.user);

  const [attorneyPath, setAttorneyPath] = React.useState<string>();

  async function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    const state = "offer";
    var text = "";

    if ((e.currentTarget.elements[0] as HTMLInputElement).checked) {
      text = "スタンダード";
    } else if ((e.currentTarget.elements[1] as HTMLInputElement).checked) {
      text = "ハイグレード";
    } else {
      text = "特別プラン";
    }

  }

  React.useEffect(() => {
    const db = firebase.firestore();
    const dbRef = db.doc(path);
    dbRef.onSnapshot((snap: firebase.firestore.DocumentSnapshot) => {
      let item = snap.data();
      dispatch(allAction({
        ...app,
        ...item
      }))
      setAttorneyPath(item?.user.path);
    })
  }, [dispatch, app, path, setAttorneyPath]);

  React.useEffect(() => {
    if (attorneyPath) {
      const db = firebase.firestore();
      db.doc(attorneyPath).onSnapshot((snap: firebase.firestore.DocumentSnapshot) => {
        dispatch(allUserAction({
          ...user,
          ...snap.data()
        }))
      })
    }
  }, [attorneyPath, user, dispatch]);
  return (
    <IonPage>
      <IonHeader>
        <Header title={app.title} left="none" center="title" right="close" />
      </IonHeader>
      <IonContent scrollEvents={true}>
        <section className="inner">
          <div className="main__wrap">
            <Sidebar page="chat" />
            <main className="content__wrap">
              <form action="post" className="" onSubmit={handleSubmit}>
                <h2>プラン</h2>
                <input type="radio" name="plan" value="スタンダード" />スタンダード<br />
                <input type="radio" name="plan" value="ハイグレード"/>ハイグレード<br />
                <input type="radio" name="plan" value="特別プラン" />特別プラン<br />

                <button type="submit">オファーする</button>
              </form>
            </main>
          </div>
        </section>
        <IonFooter className="ion-no-border">
          <Footer format="Tabs" page="chat" />
        </IonFooter>
      </IonContent>
    </IonPage>
  );
};

export default OfferPlan;
