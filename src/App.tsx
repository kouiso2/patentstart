import {
  IonApp,
  IonModal, IonRouterOutlet
} from "@ionic/react";
import { IonReactRouter } from "@ionic/react-router";
/* Core CSS required for Ionic components to work properly */
import "@ionic/react/css/core.css";
import 'assets/css/reset.css';
//customhook
import useAuth from 'hooks/auth';
import ModalPage from 'pages/Common/Modal/Index';
import React from 'react';
import { useDispatch, useSelector } from "react-redux";
import { Route } from 'react-router-dom';
import { RootState } from "store/Combine";
import { modalConfigAction } from "store/Config/action";
import Admin from 'theme/Admin';
// import Message from "theme/Message";
import Applicant from 'theme/Applicant';
import Attorney from 'theme/Attorney';
import Auth from "theme/Auth";


// Events (iOS only)
window.addEventListener('statusTap', function () {

});

const App: React.FC = () => {
  const auth = useAuth();
  const dispatch = useDispatch();
  const config = useSelector((state: RootState) => state.config);

  return (
    <IonApp>
      <IonReactRouter>
        <IonRouterOutlet>
          <Route path="/auth" render={() => {
            return <Auth />;
          }} />
          <Route path="/applicant/" render={() => {
            return auth?.kind === "Applicant" ? <Applicant /> : auth?.redirect
          }} />
          <Route path="/attorney/" render={() => {
            return auth?.kind === "Attorney" ? <Attorney /> : auth?.redirect
          }} />
          <Route path="/admin/" render={() => {
            return auth?.kind === "Admin" ? <Admin /> : auth?.redirect
          }} />
          <Route path="/" render={() => auth?.redirect} exact={true} />
        </IonRouterOutlet>
      </IonReactRouter>
      <IonModal isOpen={config.modal.isOpen} swipeToClose={true} onDidDismiss={() => {
        dispatch(modalConfigAction({
          ...config.modal,
          isOpen: false,
        }))
      }}>
        <ModalPage kind={config.modal.form} id={config.modal.id} idSub={config.modal.idSub} imgUrl={config.modal.imgUrl} />
      </IonModal>
    </IonApp>);

}

export default App;
