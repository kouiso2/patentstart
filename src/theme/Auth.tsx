import {
  IonRouterOutlet
} from "@ionic/react";
import React from "react";
import { Redirect, Route } from "react-router-dom";
import routes from "routes/Auth";
import { allRouteProps } from 'routes/routes';



const Auth: React.FC = () => {

  const getRoute = React.useMemo(() => {
    return routes.map((first: allRouteProps, key: number) => {
      if (first.views) {
        return first.views?.map((second: allRouteProps, key: number) => {
          return (
            <Route
              exact={true}
              path={"/auth" + first.path + second.path}
              component={second.component}
              key={key}
            />
          );
        })
      } else {
        return (
          <Route
            exact={true}
            path={"/auth" + first.path}
            component={first.component}
            key={key}
          />
        );
      }
    });
  }, []);
  return (
    <IonRouterOutlet>
      {getRoute}
      <Redirect from="/auth" to="/auth/login/" exact />
    </IonRouterOutlet>
  );
};

export default Auth;
