import {
  IonRouterOutlet, isPlatform
} from "@ionic/react";
import React from "react";
import { Redirect, Route } from "react-router-dom";
import routes from "routes/Admin";
import { allRouteProps } from 'routes/routes';



const Admin: React.FC = () => {

  const getRoute = React.useMemo(() => {
    return routes.map((first: allRouteProps, key: number) => {
      if (first.views) {
        return first.views?.map((second: allRouteProps, key: number) => {
          return (
            <Route
              exact={true}
              path={"/admin" + first.path + second.path}
              component={second.component}
              key={key}
            />
          );
        })
      } else {
        return (
          <Route
            exact={true}
            path={"/admin" + first.path}
            component={first.component}
            key={key}
          />
        );
      }
    });
  }, []);
  return (
    <IonRouterOutlet>
      {getRoute}
      {//ios・androidの場合と、それ以外で条件分岐
        (isPlatform("ios") || isPlatform("android")) ? 
        <Redirect from="/admin" to="/admin/chat/" exact /> :
        <Redirect from="/admin" to="/admin/contact-list/" exact />
      }
    </IonRouterOutlet>
  );
};

export default Admin;
