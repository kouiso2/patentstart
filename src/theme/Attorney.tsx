import {
  IonRouterOutlet, isPlatform
} from "@ionic/react";
import useAuth from "hooks/auth";
import React from "react";
import { Redirect, Route, useHistory } from "react-router-dom";
import routes from "routes/Attorney";
import { allRouteProps } from 'routes/routes';

const Attorney: React.FC = () => {
  const history = useHistory();
  const auth = useAuth();

  const getRoute = React.useMemo(() => {
    return routes.map((first: allRouteProps, key: number) => {
      if (first.views) {
        return first.views?.map((second: allRouteProps, key: number) => {
          return (
            <Route
              exact={true}
              path={"/attorney" + first.path + second.path}
              component={second.component}
              key={key}
            />
          );
        })
      } else {
        return (
          <Route
            exact={true}
            path={"/attorney" + first.path}
            component={first.component}
            key={key}
          />
        );
      }
    });
  }, []);

  React.useEffect(() => {
    if (
      auth?.personName === null ||
      auth?.companyName === null
    ) {
      history.push('/attorney/profile/update');
    }
  }, [history, auth?.personName, auth?.companyName]);
  return (
    <IonRouterOutlet>
      {getRoute}
      {//ios・androidの場合と、それ以外で条件分岐
        (isPlatform("ios") || isPlatform("android")) ? 
        <Redirect from="/attorney" to="/attorney/chat/" exact /> :
        <Redirect from="/attorney" to="/attorney/home/" exact />
      }
    </IonRouterOutlet>
  );
};

export default Attorney;
