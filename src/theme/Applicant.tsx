import {
  IonRouterOutlet, isPlatform
} from "@ionic/react";
import useAuth from "hooks/auth";
import React from "react";
import { Redirect, Route, useHistory } from "react-router-dom";
import routes from "routes/Applicant";
import { allRouteProps } from 'routes/routes';




const Applicant: React.FC = () => {
  const history = useHistory();
  const auth = useAuth();

  const getRoute = React.useMemo(() => {
    return routes.map((first: allRouteProps, key: number) => {
      if (first.views) {
        return first.views?.map((second: allRouteProps, key: number) => {
          return (
            <Route
              exact={true}
              path={"/applicant" + first.path + second.path}
              component={second.component}
              key={key}
            />
          );
        })
      } else {
        return (
          <Route
            exact={true}
            path={"/applicant" + first.path}
            component={first.component}
            key={key}
          />
        );
      }
    });
  }, []);

  React.useEffect(() => {
    if (
      auth?.personName === null ||
      auth?.companyName === null
    ) {
      history.push('/applicant/profile/update');
    }
  }, [history, auth?.personName, auth?.companyName]);

  return (
    <IonRouterOutlet>
      {getRoute}
      {//ios・androidの場合と、それ以外で条件分岐
        (isPlatform("ios") || isPlatform("android")) ?
        <Redirect from="/applicant" to="/applicant/chat/" exact /> :
        <Redirect from="/applicant" to="/applicant/home/" exact />
      }
    </IonRouterOutlet>
  );
};

export default Applicant;
