export const applicationTypes = {
  'image': "IMAGE",
  'title': "TITLE",
  'category': "CATEGORY",
  'explanation': 'EXPLAINATION',
  'document': "DOCUMENT",
  'all': "ALL"
} as const;

export default applicationTypes;
