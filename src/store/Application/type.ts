import model from "models/Application";
import { Action } from "redux";
import types from 'store/Application/actionType';
interface titleAction extends Action {
  type: typeof types.title;
  payload: model["title"];
};
interface categoryAction extends Action {
  type: typeof types.category;
  payload: model["category"];
};
interface explanationAction extends Action {
  type: typeof types.explanation;
  payload: model["explanation"];
};
interface documentAction extends Action {
  type: typeof types.document;
  payload: model["document"];
};
interface documentAction extends Action {
  type: typeof types.document;
  payload: model["document"];
};

interface allAction extends Action {
  type: typeof types.all;
  payload: model;
}

export type applicationPattern =
| titleAction
| categoryAction
| explanationAction
| documentAction
| allAction;

export default applicationPattern;
