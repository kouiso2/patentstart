import model from "models/Application";
import types from "store/Application/actionType";
import pattern from "store/Application/type";

export const titleAction = (param: model["title"]): pattern => {
  return {
    type: types.title,
    payload: param
  }
}
export const categoryAction = (param: model["category"]): pattern => {
  return {
    type: types.category,
    payload: param
  }
}
export const explanationAction = (param: model["explanation"]): pattern => {
  return {
    type: types.explanation,
    payload: param
  }
}
export const documentAction = (param: model["document"]): pattern => {
  return {
    type: types.document,
    payload: param
  }
}
export const allAction = (param: model): pattern => {
  return {
    type: types.all,
  payload: param
}
}
