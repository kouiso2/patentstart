// img
import 'firebase/firestore';
import model from "models/Application";
import types from "store/Application/actionType";
import pattern from "store/Application/type";
import { appInit } from '../../data/initial/index';


const initialState: model = appInit;

const applicationReducer = (
  state = initialState,
  action: pattern
): model => {
  switch (action.type) {
    case types.title:
      return {
        ...state,
        title: action.payload
      };
    case types.category:
      return {
        ...state,
        category: action.payload
      };
    case types.explanation:
      return {
        ...state,
        explanation: action.payload
      };
    case types.document:
      return {
        ...state,
        document: action.payload
      };
    case types.all:
      return {
        ...state,
        ...action.payload
      }
    default:
      return state;
  }
};

export default applicationReducer;
