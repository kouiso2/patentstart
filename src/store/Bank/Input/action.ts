import model from "models/Bank";
import types from "store/Bank/Input/actionType";
import pattern from "store/Bank/Input/type";

export const countryAction = (param: model['country']): pattern => {
  return {
    type: types.country,
    payload: param
  }
}
export const currencyAction = (param: model['currency']): pattern => {
  return {
    type: types.currency,
    payload: param
  }
}
export const bankNumberAction = (param: model['bank_number']): pattern => {
  return {
    type: types.bank_number,
    payload: param
  }
}
export const branchNumberAction = (param: model['branch_number']): pattern => {
  return {
    type: types.branch_number,
    payload: param
  }
}
export const kindAction = (param: model['kind']): pattern => {
  return {
    type: types.kind,
  payload: param,
}
}
export const holderNameAction = (param: model['account_holder_name']): pattern => {
  return {
    type: types.account_holder_name,
    payload: param
  }
}
export const holderTypeAction = (param: model['account_holder_type']): pattern => {
  return {
    type: types.account_holder_type,
    payload: param
  }
}
export const numberAction = (param: model['account_number']): pattern => {
  return {
    type: types.account_number,
    payload: param
  }
}
export const allBankAction = (param: model): pattern => {
  return {
    type: types.all,
    payload: param
  }
}
