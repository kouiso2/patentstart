import model from "models/Bank";
import { Action } from "redux";
import types from 'store/Bank/Input/actionType';

interface countryAction extends Action {
  type: typeof types.country;
  payload: model['country'];
}
interface currencyAction extends Action {
  type: typeof types.currency;
  payload: model['currency'];
}
interface bankNumberAction extends Action {
  type: typeof types.bank_number;
  payload: model['bank_number'];
}
interface kindAction extends Action {
  type: typeof types.kind;
  payload: model["kind"]
}
interface branchNumberAction extends Action {
  type: typeof types.branch_number;
  payload: model['branch_number'];
}
interface holderNameAction extends Action {
  type: typeof types.account_holder_name;
  payload: model['account_holder_name'];
}
interface holderTypeAction extends Action {
  type: typeof types.account_holder_type;
  payload: model['account_holder_type'];
}
interface numberAction extends Action {
  type: typeof types.account_number;
  payload: model['account_number'];
}
interface allBankAction extends Action {
  type: typeof types.all;
  payload: model;
}

export type bankPattern =
countryAction |
currencyAction |
bankNumberAction |
kindAction |
branchNumberAction |
holderNameAction |
holderTypeAction |
allBankAction |
numberAction;

export default bankPattern;
