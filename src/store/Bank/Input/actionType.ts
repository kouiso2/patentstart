export const patentTypes = {
  country: "COUNTRY",
  currency: "CURRENCY",
  bank_number: "BANK_NUMBER",
  kind: "KIND",
  branch_number: "BRANCH_NUMBER",
  account_holder_name: "ACCOUNT_HOLDER_NAME",
  account_holder_type: "ACCOUNT_HOLDER_TYPE",
  account_number: "ACCOUNT_NUMBER",
  all: "ALL"
} as const;

export default patentTypes;
