import { bankInputInit } from "data/initial";
import model from "models/Bank";
import types from "store/Bank/Input/actionType";
import pattern from "store/Bank/Input/type";

const initialState: model = bankInputInit;

const bankReducer = (
  state = initialState,
  action: pattern
): model => {
  switch (action.type) {
    case types.bank_number:
      return {
        ...state,
        bank_number: action.payload,
      }
    case types.branch_number:
      return {
        ...state,
        branch_number: action.payload,
      }
    case types.account_number:
      return {
        ...state,
        account_number: action.payload,
      }
    case types.kind:
      return {
        ...state,
        account_number: action.payload,
      }
    case types.country:
      return {
        ...state,
        country: action.payload,
      }
    case types.currency:
      return {
        ...state,
        country: action.payload,
      }
    case types.account_holder_name:
      return {
        ...state,
        account_holder_name: action.payload,
      }
    case types.account_holder_type:
      return {
        ...state,
        account_holder_type: action.payload,
      }
    case types.all:
      return {
        ...state,
        ...action.payload
      }
    default:
      return state;
  }
};

export default bankReducer;
