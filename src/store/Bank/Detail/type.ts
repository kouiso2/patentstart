import { BankDetailModel as model } from "models/Bank";
import { Action } from "redux";
import types from 'store/Bank/Detail/actionType';

interface bankNameAction extends Action {
  type: typeof types.bankName;
  payload: model["bankName"]
}
interface branchNameAction extends Action {
  type: typeof types.branchName;
  payload: model["branchName"]
}
interface kindAction extends Action {
  type: typeof types.kind;
  payload: model["kind"]
}
interface numberAction extends Action {
  type: typeof types.number;
  payload: model["number"]
}
interface yourNameAction extends Action {
  type: typeof types.yourName;
  payload: model["yourName"]
}
interface allAction extends Action {
  type: typeof types.all;
  payload: model;
}

export type bankPattern =
| bankNameAction
| branchNameAction
| kindAction
| numberAction
| yourNameAction
| allAction;

export default bankPattern;
