export const BankDetailTypes = {
  'bankName': "BANKNAME",
  'branchName': "BRANCHNAME",
  'kind': "KIND",
  'number': "NUMBER",
  'yourName': "YOURNAME",
  'all': "ALL"
} as const;

export default BankDetailTypes;
