import { BankDetailModel as model } from "models/Bank";
import types from "store/Bank/Detail/actionType";
import pattern from "store/Bank/Detail/type";

const initialState: model = {
  bankName: "",
  branchName: "",
  kind: "未登録",
  number: "",
  yourName: "",
};

const bankReducer = (
  state = initialState,
  action: pattern
): model => {
  switch (action.type) {
    case types.bankName:
      return {
        ...state,
        bankName: action.payload,
      }
    case types.branchName:
      return {
        ...state,
        branchName: action.payload,
      }
    case types.kind:
      return {
        ...state,
        kind: action.payload,
      }
    case types.number:
      return {
        ...state,
        number: action.payload,
      }
    case types.yourName:
      return {
        ...state,
        yourName: action.payload,
      }
    case types.all:
      return {
        ...state,
        ...action.payload
      }
    default:
      return state;
  }
};

export default bankReducer;
