import { BankDetailModel as model } from "models/Bank";
import types from "store/Bank/Detail/actionType";
import pattern from "store/Bank/Detail/type";

export const bankNameAction = (param: model['bankName']): pattern => {
  return {
    type: types.bankName,
  payload: param,
}
}
export const branchNameAction = (param: model['branchName']): pattern => {
  return {
    type: types.branchName,
  payload: param,
}
}
export const kindAction = (param: model['kind']): pattern => {
  return {
    type: types.kind,
  payload: param,
}
}
export const numberAction = (param: model['number']): pattern => {
  return {
    type: types.number,
  payload: param,
}
}
export const yourNameAction = (param: model['yourName']): pattern => {
  return {
    type: types.yourName,
  payload: param,
}
}
export const allAction = (param: model): pattern => {
  return {
    type: types.all,
  payload: param
}
}
