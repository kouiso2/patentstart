// img
import model from "models/Config";
import types from "store/Config/actionType";
import pattern from "store/Config/type";
import { configInit } from '../../data/initial/index';

export const initialState: model = configInit;

const configReducer = (
  state = initialState,
  action: pattern
): model => {
  switch (action.type) {
    case types.modal:
      return {
        ...state,
        modal: {
          ...state.modal,
          form: action.payload.form,
          isOpen: action.payload.isOpen,
          id: action.payload.id,
          idSub: action.payload.idSub,
          imgUrl: action.payload.imgUrl
        },
      }
    case types.id:
      return {
        ...state,
        id: action.payload
      }
    case types.path:
      return {
        ...state,
        path: action.payload
      }
    case types.room:
      return {
        ...state,
        room: {
          id: action.payload.id,
          path: action.payload.path
        },
      }
    case types.application:
      return {
        ...state,
        application: {
          id: action.payload.id,
          path: action.payload.path
        },
      }
    case types.attorney:
      return {
        ...state,
        attorney: {
          id: action.payload.id,
          path: action.payload.path
        },
      }
    case types.applicant:
      return {
        ...state,
        applicant: {
          id: action.payload.id,
          path: action.payload.path
        },
      }
    case types.user:
      return {
        ...state,
        user: {
          id: action.payload.id,
          path: action.payload.path
        },
      }
    case types.offer:
      return {
        ...state,
        offer: {
          id: action.payload.id,
          path: action.payload.path
        },
      }
    case types.patent:
      return {
        ...state,
        patent: {
          id: action.payload.id,
          path: action.payload.path
        },
      }
    case types.bank:
      return {
        ...state,
        bank: {
          id: action.payload.id,
          path: action.payload.path,
          name: action.payload.name
        },
      }
    default:
      return state;
  }
};

export default configReducer;
