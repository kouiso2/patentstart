import model from "models/Config";
import types from "store/Config/actionType";
import pattern from "store/Config/type";

export const modalConfigAction = (param: model["modal"]): pattern => {
  return {
    type: types.modal,
    payload: param,
  };
};
//normal
export const idConfigAction = (param: model["id"]): pattern => {
  return {
    type: types.id,
    payload: param,
  };
};
export const pathConfigAction = (param: model["path"]): pattern => {
  return {
    type: types.path,
    payload: param,
  };
};
//normal
export const normalConfigAction = (param: model["normal"]): pattern => {
  return {
    type: types.normal,
    payload: param,
  };
};
export const normalIdConfigAction = (param: model["normal"]["id"]): pattern => {
  return {
    type: types.normalId,
    payload: param,
  };
};
export const normalPathConfigAction = (
  param: model["normal"]["path"]
): pattern => {
  return {
    type: types.normalPath,
    payload: param,
  };
};
//room
export const roomConfigAction = (param: model["room"]): pattern => {
  return {
    type: types.room,
    payload: param,
  };
};
export const roomIdConfigAction = (param: model["room"]["id"]): pattern => {
  return {
    type: types.roomId,
    payload: param,
  };
};
export const roomPathConfigAction = (param: model["room"]["path"]): pattern => {
  return {
    type: types.roomPath,
    payload: param,
  };
};
//application
export const applicationConfigAction = (
  param: model["application"]
): pattern => {
  return {
    type: types.application,
    payload: param,
  };
};
export const applicationIdConfigAction = (
  param: model["application"]["id"]
): pattern => {
  return {
    type: types.applicationId,
    payload: param,
  };
};
export const applicationPathConfigAction = (
  param: model["application"]["path"]
): pattern => {
  return {
    type: types.applicationPath,
    payload: param,
  };
};
//attorney
export const attorneyConfigAction = (param: model["attorney"]): pattern => {
  return {
    type: types.attorney,
    payload: param,
  };
};
export const attorneyIdConfigAction = (
  param: model["attorney"]["id"]
): pattern => {
  return {
    type: types.attorneyId,
    payload: param,
  };
};
export const attorneyPathConfigAction = (
  param: model["attorney"]["path"]
): pattern => {
  return {
    type: types.attorneyPath,
    payload: param,
  };
};
//applicant
export const applicantConfigAction = (param: model["applicant"]): pattern => {
  return {
    type: types.applicant,
    payload: param,
  };
};
export const applicantIdConfigAction = (
  param: model["applicant"]["id"]
): pattern => {
  return {
    type: types.applicantId,
    payload: param,
  };
};
export const applicantPathConfigAction = (
  param: model["applicant"]["path"]
): pattern => {
  return {
    type: types.applicantPath,
    payload: param,
  };
};
//user
export const userConfigAction = (param: model["user"]): pattern => {
  return {
    type: types.user,
    payload: param,
  };
};
export const userIdConfigAction = (param: model["user"]["id"]): pattern => {
  return {
    type: types.userId,
    payload: param,
  };
};
export const userPathConfigAction = (param: model["user"]["path"]): pattern => {
  return {
    type: types.userPath,
    payload: param,
  };
};
//offer
export const offerConfigAction = (param: model["offer"]): pattern => {
  return {
    type: types.offer,
    payload: param,
  };
};
export const offerIdConfigAction = (param: model["offer"]["id"]): pattern => {
  return {
    type: types.offerId,
    payload: param,
  };
};
export const offerPathConfigAction = (
  param: model["offer"]["path"]
): pattern => {
  return {
    type: types.offerPath,
    payload: param,
  };
};
//patent
export const patentConfigAction = (param: model["patent"]): pattern => {
  return {
    type: types.patent,
    payload: param,
  };
};
export const patentIdConfigAction = (param: model["patent"]["id"]): pattern => {
  return {
    type: types.patentId,
    payload: param,
  };
};
export const patentPathConfigAction = (
  param: model["patent"]["path"]
): pattern => {
  return {
    type: types.patentPath,
    payload: param,
  };
};
//bank
export const bankConfigAction = (param: model["bank"]): pattern => {
  return {
    type: types.bank,
    payload: param,
  };
};
export const bankIdConfigAction = (param: model["bank"]["id"]): pattern => {
  return {
    type: types.bankId,
    payload: param,
  };
};
export const bankPathConfigAction = (param: model["bank"]["path"]): pattern => {
  return {
    type: types.bankPath,
    payload: param,
  };
};
