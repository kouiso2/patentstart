import model from "models/Config";
import { Action } from "redux";
import types from 'store/Config/actionType';
import { pathConfigAction } from "./action";

interface modalConfigAction extends Action {
  type: typeof types.modal;
  payload: model['modal'];
}
interface idConfigAction extends Action {
  type: typeof types.id;
  payload: model['id'];
}
interface pathConfigAction extends Action {
  type: typeof types.path;
  payload: model['path'];
}
interface normalConfigAction extends Action {
  type: typeof types.normal;
  payload: model['normal'];
}
interface normalIdConfigAction extends Action {
  type: typeof types.normalId;
  payload: model['normal']['id'];
}
interface normalPathConfigAction extends Action {
  type: typeof types.normalPath;
  payload: model['normal']['path'];
}
interface roomConfigAction extends Action {
  type: typeof types.room;
  payload: model['room'];
}
interface roomIdConfigAction extends Action {
  type: typeof types.roomId;
  payload: model['room']['id'];
}
interface roomPathConfigAction extends Action {
  type: typeof types.roomPath;
  payload: model['room']['path'];
}
interface applicationConfigAction extends Action {
  type: typeof types.application;
  payload: model['application'];
}
interface applicationIdConfigAction extends Action {
  type: typeof types.applicationId;
  payload: model['application']['id'];
}
interface applicationPathConfigAction extends Action {
  type: typeof types.applicationPath
  payload: model['application']['path']
}
interface attorneyConfigAction extends Action {
  type: typeof types.attorney;
  payload: model['attorney'];
}
interface attorneyIdConfigAction extends Action {
  type: typeof types.attorneyId;
  payload: model['attorney']['id'];
}
interface attorneyPathConfigAction extends Action {
  type: typeof types.attorneyPath
  payload: model['attorney']['path']
}
interface applicantConfigAction extends Action {
  type: typeof types.applicant;
  payload: model['applicant'];
}
interface applicantIdConfigAction extends Action {
  type: typeof types.applicantId;
  payload: model['applicant']['id'];
}
interface applicantPathConfigAction extends Action {
  type: typeof types.applicantPath
  payload: model['applicant']['path']
}
interface userConfigAction extends Action {
  type: typeof types.user;
  payload: model['user'];
}
interface userIdConfigAction extends Action {
  type: typeof types.userId;
  payload: model['user']['id'];
}
interface userPathConfigAction extends Action {
  type: typeof types.userPath;
  payload: model['user']['path'];
}
interface offerConfigAction extends Action {
  type: typeof types.offer;
  payload: model['offer'];
}
interface offerIdConfigAction extends Action {
  type: typeof types.offerId;
  payload: model['offer']['id'];
}
interface offerPathConfigAction extends Action {
  type: typeof types.offerPath;
  payload: model['offer']['path'];
}
interface patentConfigAction extends Action {
  type: typeof types.patent;
  payload: model['patent'];
}
interface patentIdConfigAction extends Action {
  type: typeof types.patentId;
  payload: model['patent']['id'];
}
interface patentPathConfigAction extends Action {
  type: typeof types.patentPath;
  payload: model['patent']['path'];
}
interface bankConfigAction extends Action {
  type: typeof types.bank;
  payload: model['bank'];
}
interface bankIdConfigAction extends Action {
  type: typeof types.bankId;
  payload: model['bank']['id'];
}
interface bankPathConfigAction extends Action {
  type: typeof types.bankPath;
  payload: model['bank']['path'];
}

export type UserPattern =
  modalConfigAction |
  idConfigAction |
  pathConfigAction |
  normalConfigAction |
  normalIdConfigAction |
  normalPathConfigAction |
  roomConfigAction |
  roomIdConfigAction |
  roomPathConfigAction |
  applicationConfigAction |
  applicationIdConfigAction |
  applicationPathConfigAction |
  attorneyConfigAction |
  attorneyIdConfigAction |
  attorneyPathConfigAction |
  applicantConfigAction |
  applicantIdConfigAction |
  applicantPathConfigAction |
  userConfigAction |
  userIdConfigAction |
  userPathConfigAction |
  offerConfigAction |
  offerIdConfigAction |
  offerPathConfigAction |
  patentConfigAction |
  patentIdConfigAction |
  patentPathConfigAction |
  bankConfigAction |
  bankIdConfigAction |
  bankPathConfigAction;

export default UserPattern;
