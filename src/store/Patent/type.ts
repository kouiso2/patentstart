import firebase from 'firebase';
import model from "models/Patent";
import { Action } from "redux";
import types from 'store/Patent/actionType';

interface referenceNumAction extends Action {
  type: typeof types.referenceNum;
  payload: model["referenceNum"];
};
interface fillingNumAction extends Action {
  type: typeof types.fillingNum;
  payload: model["fillingNum"];
};
interface knownDateAction extends Action {
  type: typeof types.knownDate;
  payload: model["knownDate"];
};
interface fillingDateAction extends Action {
  type: typeof types.fillingDate;
  payload: model["fillingDate"];
};
interface explanationAction extends Action {
  type: typeof types.explanation;
  payload: model["explanation"];
};
interface developNameAction extends Action {
  type: typeof types.developName;
  payload: model["developName"];
};
interface fiAction extends Action {
  type: typeof types.fi;
  payload: model["fi"];
};
interface jplatUrlAction extends Action {
  type: typeof types.jplatUrl;
  payload: model["jplatUrl"];
};
interface documentAction extends Action {
  type: typeof types.document;
  payload: model["document"];
};

interface allPatentAction extends Action {
  type: typeof types.all;
  payload: model | firebase.firestore.DocumentData;
}

export type patentPattern =
  | referenceNumAction
  | fillingNumAction
  | knownDateAction
  | fillingDateAction
  | explanationAction
  | developNameAction
  | fiAction
  | jplatUrlAction
  | documentAction
  | allPatentAction;

export default patentPattern;
