export const patentTypes = {
  'referenceNum': 'LITERATURE',
  'fillingNum': 'FILLINGNUM',
  'knownDate': "KNOWNDATE",
  'fillingDate': "FILLINGDATE",
  'developName': "DEVELOPNAME",
  'explanation': "EXPLAINATION",
  'jplatUrl': 'JPLATURL',
  'fi': "FI",
  'document': "DOCUMENT",
  'all': "ALL",
} as const;

export default patentTypes;
