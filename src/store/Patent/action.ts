import firebase from 'firebase';
import model from "models/Patent";
import types from "store/Patent/actionType";
import pattern from "store/Patent/type";

export const referenceNumAction = (param: model['referenceNum']): pattern => {
  return {
    type: types.referenceNum,
    payload: param
  }
};
export const fillingNumAction = (param: model['fillingNum']): pattern => {
  return {
    type: types.fillingNum,
    payload: param
  }
};
export const knownDateAction = (param: model['knownDate']): pattern => {
  return {
    type: types.knownDate,
    payload: param
  }
};
export const fillingDateAction = (param: model['fillingDate']): pattern => {
  return {
    type: types.fillingDate,
    payload: param
  }
};
export const explanationAction = (param: model['explanation']): pattern => {
  return {
    type: types.explanation,
    payload: param
  }
};
export const developNameAction = (param: model['developName']): pattern => {
  return {
    type: types.developName,
    payload: param
  }
};
export const fiAction = (param: model['fi']): pattern => {
  return {
    type: types.fi,
    payload: param
  }
};
export const jplatUrlAction = (param: model['jplatUrl']): pattern => {
  return {
    type: types.jplatUrl,
    payload: param
  }
};
export const documentAction = (param: model['document']): pattern => {
  return {
    type: types.document,
    payload: param
  }
};
export const allPatentAction = (param: model | firebase.firestore.DocumentData): pattern => {
  return {
    type: types.all,
    payload: param
  }
}

