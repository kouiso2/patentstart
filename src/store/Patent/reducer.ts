// img
import model from "models/Patent";
import types from "store/Patent/actionType";
import pattern from "store/Patent/type";
import { patentInit } from '../../data/initial/index';

const initialState: model = patentInit;

const patentReducer = (
  state = initialState,
  action: pattern
): model => {
  switch (action.type) {
    case types.referenceNum:
      return {
        ...state,
        referenceNum: action.payload
      };
    case types.fillingNum:
      return {
        ...state,
        fillingNum: action.payload
      };
    case types.knownDate:
      return {
        ...state,
        knownDate: action.payload
      };
    case types.fillingDate:
      return {
        ...state,
        fillingDate: action.payload
      };
    case types.explanation:
      return {
        ...state,
        explanation: action.payload
      };
    case types.fi:
      return {
        ...state,
        fi: action.payload
      };
    case types.developName:
      return {
        ...state,
        developName: action.payload
      };
    case types.jplatUrl:
      return {
        ...state,
        jplatUrl: action.payload
      };
    case types.document:
      return {
        ...state,
        document: action.payload
      };
    case types.all:
      return {
        ...state,
        ...action.payload
      }
    default:
      return state;
  }
};

export default patentReducer;
