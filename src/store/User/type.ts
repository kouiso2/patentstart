import model from "models/User";
import { Action } from "redux";
import types from 'store/User/actionType';

interface imageAction extends Action {
  type: typeof types.image;
  payload: model["image"];
}

interface companyNameAction extends Action {
  type: typeof types.companyName;
  payload: model["companyName"];
}
interface companyKanaAction extends Action {
  type: typeof types.companyKana;
  payload: model["companyKana"];
}
interface personNameAction extends Action {
  type: typeof types.personName;
  payload: model["personName"];
}
interface personKatakanaAction extends Action {
  type: typeof types.personKatakana;
  payload: model["personKatakana"];
}
interface historyAction extends Action {
  type: typeof types.history;
  payload: model["history"];
}
interface registerNumberAction extends Action {
  type: typeof types.registerNumber;
  payload: model["registerNumber"];
}
interface correspondAreaAction extends Action {
  type: typeof types.correspondArea;
  payload: model["correspondArea"];
}
interface hpAction extends Action {
  type: typeof types.homepage;
  payload: model["homepage"];
}
interface emailAction extends Action {
  type: typeof types.email;
  payload: model["email"];
}
interface phoneNumberAction extends Action {
  type: typeof types.phoneNumber;
  payload: model["phoneNumber"];
}
interface goodCategoryAction extends Action {
  type: typeof types.goodCategory;
  payload: model["goodCategory"];
}
interface kindAction extends Action {
  type: typeof types.kind;
  payload: model["kind"];
}
interface selfAction extends Action {
  type: typeof types.self;
  payload: model["self"];
}
interface countryAction extends Action {
  type: typeof types.address.country;
  payload: model["address"]["country"];
}
interface stateAction extends Action {
  type: typeof types.address.state;
  payload: model["address"]["state"];
}
interface cityAction extends Action {
  type: typeof types.address.city;
  payload: model["address"]["city"];
}
interface line1Action extends Action {
  type: typeof types.address.line1;
  payload: model["address"]["line1"];
}
interface line2Action extends Action {
  type: typeof types.address.line2;
  payload: model["address"]["line2"];
}
interface postalCodeAction extends Action {
  type: typeof types.address.postalCode;
  payload: model["address"]["postalCode"];
}

interface allUserAction extends Action {
  type: typeof types.all;
  payload: model;
}

export type UserPattern =
  | imageAction
  | companyNameAction
  | companyKanaAction
  | personNameAction
  | personKatakanaAction
  | historyAction
  | registerNumberAction
  | correspondAreaAction
  | hpAction
  | emailAction
  | phoneNumberAction
  | goodCategoryAction
  | kindAction
  | selfAction
  | countryAction
  | stateAction
  | cityAction
  | line1Action
  | line2Action
  | postalCodeAction
  | allUserAction;

export default UserPattern;
