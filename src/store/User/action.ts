import model from "models/User";
import types from "store/User/actionType";
import pattern from "store/User/type";

export const imageAction = (param: model['image']): pattern => {
  return {
    type: types.image,
    payload: param
  }
}
export const companyNameAction = (param: model['companyName']): pattern => {
  return {
    type: types.companyName,
    payload: param,
  };
};
export const companyKanaAction = (param: model['companyKana']): pattern => {
  return {
    type: types.companyKana,
    payload: param,
  };
};
export const personNameAction = (param: model['personName']): pattern => {
  return {
    type: types.personName,
    payload: param,
  };
};
export const personKatakanaAction = (param: model['personKatakana']): pattern => {
  return {
    type: types.personKatakana,
    payload: param,
  };
};
export const historyAction = (param: model['history']): pattern => {
  return {
    type: types.history,
    payload: param,
  };
};
export const registerNumberAction = (param: model['registerNumber']): pattern => {
  return {
    type: types.registerNumber,
    payload: param,
  };
};
export const correspondAreaAction = (param: model['correspondArea']): pattern => {
  return {
    type: types.correspondArea,
    payload: param,
  };
};
export const hpAction = (param: model['homepage']): pattern => {
  return {
    type: types.homepage,
    payload: param,
  };
};
export const goodCategoryAction = (param: model['goodCategory']): pattern => {
  return {
    type: types.goodCategory,
    payload: param,
  };
};
export const emailAction = (param: model['email']): pattern => {
  return {
    type: types.email,
    payload: param,
  };
};
export const phoneNumberAction = (param: model['phoneNumber']): pattern => {
  return {
    type: types.phoneNumber,
    payload: param,
  };
};
export const kindAction = (param: model['kind']): pattern => {
  return {
    type: types.kind,
    payload: param,
  };
};
export const postalCodeAction = (param: model['address']['postalCode']): pattern => {
  return {
    type: types.address.postalCode,
    payload: param,
  };
};
export const countryAction = (param: model['address']['country']): pattern => {
  return {
    type: types.address.country,
    payload: param,
  };
};
export const stateAction = (param: model['address']['state']): pattern => {
  return {
    type: types.address.state,
    payload: param,
  };
};
export const cityAction = (param: model['address']['city']): pattern => {
  return {
    type: types.address.city,
    payload: param,
  };
};
export const line1Action = (param: model['address']['line1']): pattern => {
  return {
    type: types.address.line1,
    payload: param,
  };
};
export const line2Action = (param: model['address']['line2']): pattern => {
  return {
    type: types.address.line2,
    payload: param,
  };
};
export const selfAction = (param: model['self']): pattern => {
  return {
    type: types.self,
    payload: param,
  };
};
export const allUserAction = (param: model): pattern => {
  return {
    type: types.all,
    payload: param
  }
}
