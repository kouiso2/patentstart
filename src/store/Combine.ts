import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import createSagaMiddleware from "redux-saga";
import applicantReducer from "./Applicant/reducer";
import applicationReducer from "./Application/reducer";
import attorneyReducer from "./Attorney/reducer";
import bankDetailReducer from "./Bank/Detail/reducer";
import bankInputReducer from "./Bank/Input/reducer";
import configReducer from "./Config/reducer";
import patentReducer from "./Patent/reducer";
import userReducer from "./User/reducer";

////redux-saga
const sagaMiddleware = createSagaMiddleware();

const rootReducer = combineReducers({
  user: userReducer, //ログインしているユーザーの情報
  applicant: applicantReducer,
  attorney: attorneyReducer,
  application: applicationReducer,
  patent: patentReducer,
  bankDetail: bankDetailReducer,
  bankInput: bankInputReducer,
  config: configReducer,
});

export type RootState = ReturnType<typeof rootReducer>;

interface ExtendedWindow extends Window {
  __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
}
declare var window: ExtendedWindow;

const composeReduxDevToolsEnhancers =
  (typeof window === "object" && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) ||
  compose;

const store = createStore(
  rootReducer,
  composeReduxDevToolsEnhancers(applyMiddleware(sagaMiddleware))
);

export default store;
