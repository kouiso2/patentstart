import model from "models/Attorney";
import { Action } from "redux";
import types from 'store/Attorney/actionType';

interface imageAction extends Action {
  type: typeof types.image;
  payload: model["image"];
}

interface companyNameAction extends Action {
  type: typeof types.companyName;
  payload: model["companyName"];
}
interface companyKanaAction extends Action {
  type: typeof types.companyKana;
  payload: model["companyKana"];
}
interface firstNameAction extends Action {
  type: typeof types.firstName;
  payload: model["firstName"];
}
interface lastNameAction extends Action {
  type: typeof types.lastName;
  payload: model["lastName"];
}
interface firstNameKatakanaAction extends Action {
  type: typeof types.firstNameKatakana;
  payload: model["firstNameKatakana"];
}
interface lastNameKatakanaAction extends Action {
  type: typeof types.lastNameKatakana;
  payload: model["lastNameKatakana"];
}
interface genderAction extends Action {
  type: typeof types.gender;
  payload: model["gender"];
}
interface birthdayAction extends Action {
  type: typeof types.birthday;
  payload: model["birthday"];
}
interface historyAction extends Action {
  type: typeof types.history;
  payload: model["history"];
}
interface registerNumberAction extends Action {
  type: typeof types.registerNumber;
  payload: model["registerNumber"];
}
interface correspondAreaAction extends Action {
  type: typeof types.correspondArea;
  payload: model["correspondArea"];
}
interface hpAction extends Action {
  type: typeof types.homepage;
  payload: model["homepage"];
}
interface emailAction extends Action {
  type: typeof types.email;
  payload: model["email"];
}
interface phoneNumberAction extends Action {
  type: typeof types.phoneNumber;
  payload: model["phoneNumber"];
}
interface goodCategoryAction extends Action {
  type: typeof types.goodCategory;
  payload: model["goodCategory"];
}
interface kindAction extends Action {
  type: typeof types.kind;
  payload: model["kind"];
}
interface selfAction extends Action {
  type: typeof types.self;
  payload: model["self"];
}
interface countryAction extends Action {
  type: typeof types.address.country;
  payload: model["address"]["country"];
}
interface stateAction extends Action {
  type: typeof types.address.state;
  payload: model["address"]["state"];
}
interface cityAction extends Action {
  type: typeof types.address.city;
  payload: model["address"]["city"];
}
interface line1Action extends Action {
  type: typeof types.address.line1;
  payload: model["address"]["line1"];
}
interface line2Action extends Action {
  type: typeof types.address.line2;
  payload: model["address"]["line2"];
}
interface postalCodeAction extends Action {
  type: typeof types.address.postalCode;
  payload: model["address"]["postalCode"];
}

interface allAttorneyAction extends Action {
  type: typeof types.all;
  payload: model;
}

export type AttorneyPattern =
  | imageAction
  | companyNameAction
  | companyKanaAction
  | firstNameAction
  | lastNameAction
  | firstNameKatakanaAction
  | lastNameKatakanaAction
  | genderAction
  | birthdayAction
  | historyAction
  | registerNumberAction
  | correspondAreaAction
  | hpAction
  | emailAction
  | phoneNumberAction
  | goodCategoryAction
  | kindAction
  | selfAction
  | countryAction
  | stateAction
  | cityAction
  | line1Action
  | line2Action
  | postalCodeAction
  | allAttorneyAction;

export default AttorneyPattern;
