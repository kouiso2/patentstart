import model from "models/Attorney";
import types from "store/Attorney/actionType";
import pattern from "store/Attorney/type";

export const imageAction = (param: model['image']): pattern => {
  return {
    type: types.image,
    payload: param
  }
}
export const companyNameAction = (param: model['companyName']): pattern => {
  return {
    type: types.companyName,
    payload: param,
  };
};
export const companyKanaAction = (param: model['companyKana']): pattern => {
  return {
    type: types.companyKana,
    payload: param,
  };
};
export const firstNameAction = (param: model['firstName']): pattern => {
  return {
    type: types.firstName,
    payload: param,
  };
};
export const firstNameKatakanaAction = (param: model['firstNameKatakana']): pattern => {
  return {
    type: types.firstNameKatakana,
    payload: param,
  };
};
export const lastNameAction = (param: model['lastName']): pattern => {
  return {
    type: types.lastName,
    payload: param,
  };
};
export const lastNameKatakanaAction = (param: model['lastNameKatakana']): pattern => {
  return {
    type: types.lastNameKatakana,
    payload: param,
  };
};
export const genderAction = (param: model['gender']): pattern => {
  return {
    type: types.gender,
    payload: param,
  };
};
export const birthdayAction = (param: model['birthday']): pattern => {
  return {
    type: types.birthday,
    payload: param,
  };
};
export const historyAction = (param: model['history']): pattern => {
  return {
    type: types.history,
    payload: param,
  };
};
export const registerNumberAction = (param: model['registerNumber']): pattern => {
  return {
    type: types.registerNumber,
    payload: param,
  };
};
export const correspondAreaAction = (param: model['correspondArea']): pattern => {
  return {
    type: types.correspondArea,
    payload: param,
  };
};
export const hpAction = (param: model['homepage']): pattern => {
  return {
    type: types.homepage,
    payload: param,
  };
};
export const goodCategoryAction = (param: model['goodCategory']): pattern => {
  return {
    type: types.goodCategory,
    payload: param,
  };
};
export const emailAction = (param: model['email']): pattern => {
  return {
    type: types.email,
    payload: param,
  };
};
export const phoneNumberAction = (param: model['phoneNumber']): pattern => {
  return {
    type: types.phoneNumber,
    payload: param,
  };
};
export const kindAction = (param: model['kind']): pattern => {
  return {
    type: types.kind,
    payload: param,
  };
};
export const postalCodeAction = (param: model['address']['postalCode']): pattern => {
  return {
    type: types.address.postalCode,
    payload: param,
  };
};
export const countryAction = (param: model['address']['country']): pattern => {
  return {
    type: types.address.country,
    payload: param,
  };
};
export const stateAction = (param: model['address']['state']): pattern => {
  return {
    type: types.address.state,
    payload: param,
  };
};
export const cityAction = (param: model['address']['city']): pattern => {
  return {
    type: types.address.city,
    payload: param,
  };
};
export const line1Action = (param: model['address']['line1']): pattern => {
  return {
    type: types.address.line1,
    payload: param,
  };
};
export const line2Action = (param: model['address']['line2']): pattern => {
  return {
    type: types.address.line2,
    payload: param,
  };
};
export const selfAction = (param: model['self']): pattern => {
  return {
    type: types.self,
    payload: param,
  };
};
export const allAttorneyAction = (param: model): pattern => {
  return {
    type: types.all,
    payload: param
  }
}
