// img
import { ApplicantProfileInit } from 'data/initial';
import model from "models/Applicant";
import types from "store/Applicant/actionType";
import pattern from "store/Applicant/type";


export const initialState: model = ApplicantProfileInit;

const applicantReducer = (
  state = initialState,
  action: pattern
): model => {
  switch (action.type) {
    case types.image:
      return {
        ...state,
        image: action.payload
      };
    case types.companyName:
      return {
        ...state,
        companyName: action.payload
      };
    case types.companyKana:
      return {
        ...state,
        companyKana: action.payload
      };
    case types.personName:
      return {
        ...state,
        personName: action.payload
      };
    case types.personKatakana:
      return {
        ...state,
        personKatakana: action.payload
      };
    case types.homepage:
      return {
        ...state,
        homepage: action.payload
      };
    case types.email:
      return {
        ...state,
        email: action.payload
      };
    case types.phoneNumber:
      return {
        ...state,
        phoneNumber: action.payload
      };
    case types.address.country:
      return {
        ...state,
        address: {
          ...state.address,
          country: action.payload
        },
      };
    case types.address.city:
      return {
        ...state,
        address: {
          ...state.address,
          city: action.payload
        },
      };
    case types.address.state:
      return {
        ...state,
        address: {
          ...state.address,
          state: action.payload
        },
      };
    case types.address.line1:
      return {
        ...state,
        address: {
          ...state.address,
          line1: action.payload
        },
      };
    case types.address.line2:
      return {
        ...state,
        address: {
          ...state.address,
          line2: action.payload
        },
      };
    case types.address.postalCode:
      return {
        ...state,
        address: {
          ...state.address,
          postalCode: action.payload
        },
      };
    case types.self:
      return {
        ...state,
        self: action.payload,
      };
    case types.all:
      return {
        ...state,
        ...action.payload
      }
    default:
      return state;
  }
};

export default applicantReducer;
