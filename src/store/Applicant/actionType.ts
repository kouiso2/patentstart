export const applicantTypes = {
  image: "IMAGE",
  companyName: "COMPANY_NAME",
  companyKana: "COMPANY_KANA",
  personName: "PERSON_NAME",
  personKatakana: "PERSON_KANA",
  homepage: "HOMEPAGE",
  email: "EMAIL",
  phoneNumber: "PHONE",
  kind: "KIND",
  address: {
    country: "COUNTRY",
    city: "CITY",
    line1: "LINE1",
    line2: "LINE2",
    postalCode: "POSTALCODE",
    state: "STATE",
  },
  self: "SELF",
  all: "ALL_USER"
} as const;
export default applicantTypes;
