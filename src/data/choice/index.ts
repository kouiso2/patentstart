export const stripePublickeys = { live: "pk_live_51HoRYHCA4dp751lOdam8Lm1sGJOJFMbwSkPJLY1LvMhWzXfKdfnOldhnf6C0p0n6BEvvicAvc4pxHzaUm2KDKj8J00P2PzEEi5", test: "pk_test_51HoRYHCA4dp751lOyokwmOonwRrfyHwuVzFGl7hc3r5UeZftUR1cSHKn9IfQgZzJ58fsqXH4u6KBZGTgahKAemn300tGb655Ys" };
export const tabChoice = [
  'account',
  'home',
  'list',
  'chat'
] as const;

export const signupTabChoice = [
  'Mail',
  'Sns'
] as const;

export const formStatusChoice = [
  "Create",
  "Update",
  "Delete"
] as const; // JavaScript source code

export const layoutsChoice = [
  'Tabs',
  'Full'
] as const;

export const userChoice = [
  'Attorney',
  'Applicant',
  "Admin",
  "Auth"
] as const;

export const leftButtonChoice = [
  'back',
  'none'
] as const;

//logoの処理追加
export const centerTextChoice = [
  'title',
  'namecase',
  'logo'
] as const;

export const rightButtonChoice = [
  'close',
  'info',
  'none'
] as const;


export const statusChoice = [
  'Recruit',
  'Manage',
  'Apply',
  'None'
] as const;

export const accountChoice = [
  'Applicant',
  'Attorney',
  'Admin',
  'None'
];

export const genderChoice = [
  'male',
  'female'
];

export const rootUrlChoice = [
  '/admin',
  '/auth',
  '/applicant',
  '/attorney'
] as const;

export const modalChoice = [
  "Chat",
  "Home",
  "Profile",
  "OpponentProfile",
  "ChatTutorial",
  "ApplicantHomeTutorial",
  "AttorneyHomeTutorial",
] as const;

export const applicationCategoryChoice = [
  "DailyNecessities",
  "Transportation",
  "Science",
  "Paper",
  "FixedStructure",
  "Machine",
  "Physics",
  "Electrical",
  "NoUnderstand"
] as const;

export const bankKindChoice = [
  "Normal", "Current", "Saving", "Default"
] as const;
export const businessTypeChoice = [
  'company',
  'individual'
] as const;

export const prefectureChoice = [
  "北海道",
  "青森県",
  "岩手県",
  "宮城県",
  "秋田県",
  "山形県",
  "福島県",
  "茨城県",
  "栃木県",
  "群馬県",
  "埼玉県",
  "千葉県",
  "東京都",
  "神奈川県",
  "新潟県",
  "富山県",
  "石川県",
  "福井県",
  "山梨県",
  "長野県",
  "岐阜県",
  "静岡県",
  "愛知県",
  "三重県",
  "滋賀県",
  "京都府",
  "大阪府",
  "兵庫県",
  "奈良県",
  "和歌山県",
  "鳥取県",
  "島根県",
  "岡山県",
  "広島県",
  "山口県",
  "徳島県",
  "香川県",
  "愛媛県",
  "高知県",
  "福岡県",
  "佐賀県",
  "長崎県",
  "熊本県",
  "大分県",
  "宮崎県",
  "鹿児島県",
  "沖縄県",
] as const;

export const categoryObjectChoice = {
  DailyNecessities: "生活必需品",
  Transportation: "処理操作・運輸",
  Science: "科学・冶金",
  Paper: "繊維・紙",
  FixedStructure: "固定構造物",
  Machine: "機械工学・証明・加熱・武器",
  Physics: "物理学",
  Electrical: "電気",
  NoUnderstand: "分からない方",
} as const;
export const sorterChoice = {
  desc: "新着順",
  asc: "古い順",
  // descending: "応募が多い順",
  // ascending: "応募が少ない順"
} as const;

export const categoryChoice = [
  "DailyNecessities",
  "Transportation",
  "Science",
  "Paper",
  "FixedStructure",
  "Machine",
  "Physics",
  "Electrical",
  "NoUnderstand"
] as const;

export const NewArrivalsChoice = [
  'All',
  'Match'
] as const;


export const SideBarChoice = [
  'home',
  'chat',
  'account',
  'list',
] as const;

export const FooterChoice = [
  'home',
  'chat',
  'account',
  'list',
] as const;
