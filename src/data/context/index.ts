import {
  AttorneyProfileContextInit,
  CategorySelectContextInit,
  ConfigContextInit,
  SorterSelectContextInit,
  TabButtonContextInit,
} from "data/initial/context";
import React from "react";
import { CreditCardEventContextModel } from "../../models/Context";
import { CreditCardEventSignalContextInit } from "../initial/context";

export const TabContext = React.createContext(TabButtonContextInit);
export const CategoryContext = React.createContext(CategorySelectContextInit);
export const SorterContext = React.createContext(SorterSelectContextInit);
export const ConfigContext = React.createContext(ConfigContextInit);
export const AttorneyProfileContext = React.createContext(
  AttorneyProfileContextInit
);
export const ApplicantPathContext = React.createContext<string>("");
export const AttorneyPathContext = React.createContext<string>("");
export const ApplicationPathContext = React.createContext<string>("");
export const UserPathContext = React.createContext<string>("");
export const PatentPathContext = React.createContext<string>("");
export const PlanPriceContext = React.createContext<number>(0);
export const ProfileSubmitContext =
  React.createContext<CreditCardEventContextModel>(
    CreditCardEventSignalContextInit
  );
export const CreditCardContext = React.createContext<any>("");
