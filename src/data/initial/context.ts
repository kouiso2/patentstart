import AttorneyProfileInit from 'data/initial';
import TabContextModel, { ConfigContextModel, CreditcardContextModel, PatentCategoryContextModel, SorterContextModel } from "models/Context";
import { configInit } from ".";
import { CreditCardEventContextModel } from '../../models/Context';
import { AttorneyProfileContextModel } from './../../models/Context';

// context default value
export const TabButtonContextInit: TabContextModel = {
  tab: false,
  // 初期値を作成するが、eslintに引っかかる
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setTab: () => { },
};
export const CategorySelectContextInit: PatentCategoryContextModel = {
  patentCategory: "DailyNecessities",
  // 初期値を作成するが、eslintに引っかかる
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setPatentCategory: () => { },
};
export const SorterSelectContextInit: SorterContextModel = {
  sorter: "desc",
  // 初期値を作成するが、eslintに引っかかる
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setSorter: () => { },
};
// context default value
export const ConfigContextInit: ConfigContextModel = {
  config: configInit,
  // 初期値を作成するが、eslintに引っかかる
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setConfig: () => { },
};

export const AttorneyProfileContextInit: AttorneyProfileContextModel = {
  profile: AttorneyProfileInit,
  setProfile: () => { }
}
export const CreditCardEventSignalContextInit: CreditCardEventContextModel = {
  signal: false,
  setSignal: () => { }
}
export const CreditCardContextInit: CreditcardContextModel = {
  credit: "",
  setCredit: () => { }
}

export default TabButtonContextInit;
