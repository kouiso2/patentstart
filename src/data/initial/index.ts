import ProfileCamera from "assets/img/noimage.svg";
import ApplicantModel from "models/Applicant";
import AttorneyModel from "models/Attorney";
import BankInputModel from "models/Bank";
import UserModel from "models/User";
import ApplicationModel from "../../models/Application";
import ConfigModel from "../../models/Config";
import PatentModel from "../../models/Patent";
import { ModalModel } from "./../../models/Modal";

const blob = new Blob();

export const businessTypeInit = {
  individual: true,
  company: false,
};

export const userProfileInit: UserModel = {
  image: ProfileCamera,
  companyName: "",
  companyKana: "",
  personName: "",
  personKatakana: "",
  firstName: "",
  firstNameKatakana: "",
  lastName: "",
  lastNameKatakana: "",
  gender: "male",
  birthday: new Date(),
  kind: "Attorney",
  businessType: businessTypeInit,
  homepage: "",
  email: "",
  phoneNumber: "",
  address: {
    country: "",
    city: "",
    line1: "",
    line2: "",
    postalCode: "",
    state: "",
    town: "",
  },
  addressKatakana: {
    country: "",
    city: "",
    line1: "",
    line2: "",
    state: "",
    town: "",
  },
  self: "",
  tutorial: {
    home: true,
    chat: true,
  },
  stripeId: "",
  history: 0,
  registerNumber: "",
  correspondArea: ["東京都"],
  goodCategory: {
    DailyNecessities: false,
    Transportation: false,
    Science: false,
    Paper: false,
    FixedStructure: false,
    Machine: false,
    Physics: false,
    Electrical: false,
  },
  identification: {
    isSubmitted: false,
    isIdentified: false,
    confirmationFile: {
      front: {
        name: "",
        path: ProfileCamera,
        fullPath: "",
        type: "",
      },
      back: {
        name: "",
        path: ProfileCamera,
        fullPath: "",
        type: "",
      },
    },
    qualificationFile: {
      name: "",
      path: ProfileCamera,
      fullPath: "",
      type: "",
    },
  },
  bankId: "",
};

export const AttorneyProfileInit: AttorneyModel = {
  image: ProfileCamera,
  companyName: "",
  companyKana: "",
  firstName: "",
  firstNameKatakana: "",
  lastName: "",
  lastNameKatakana: "",
  gender: "male",
  birthday: new Date(),
  kind: "Attorney",
  businessType: businessTypeInit,
  history: 0,
  registerNumber: "",
  correspondArea: ["東京都"],
  homepage: "",
  email: "",
  phoneNumber: "",
  goodCategory: {
    DailyNecessities: false,
    Transportation: false,
    Science: false,
    Paper: false,
    FixedStructure: false,
    Machine: false,
    Physics: false,
    Electrical: false,
  },
  address: {
    country: "",
    city: "",
    line1: "",
    line2: "",
    postalCode: "",
    state: "",
    town: "",
  },
  addressKatakana: {
    country: "",
    city: "",
    line1: "",
    line2: "",
    state: "",
    town: "",
  },
  self: "",
  identification: {
    isSubmitted: false,
    isIdentified: false,
    confirmationFile: {
      front: {
        name: "",
        path: ProfileCamera,
        fullPath: "",
        type: "",
      },
      back: {
        name: "",
        path: ProfileCamera,
        fullPath: "",
        type: "",
      },
    },
    qualificationFile: {
      name: "",
      path: ProfileCamera,
      fullPath: "",
      type: "",
    },
  },
  tutorial: {
    home: true,
    chat: true,
  },
  stripeId: "",
  bankId: "",
};
export const ApplicantProfileInit: ApplicantModel = {
  image: ProfileCamera,
  companyName: "",
  companyKana: "",
  personName: "",
  personKatakana: "",
  kind: "Attorney",
  businessType: businessTypeInit,
  homepage: "",
  email: "",
  phoneNumber: "",
  firstName: "",
  firstNameKatakana: "",
  lastName: "",
  lastNameKatakana: "",
  gender: "male",
  birthday: new Date(),
  address: {
    country: "",
    city: "",
    line1: "",
    line2: "",
    postalCode: "",
    state: "",
    town: "",
  },
  addressKatakana: {
    country: "",
    city: "",
    line1: "",
    line2: "",
    state: "",
    town: "",
  },
  self: "",
  tutorial: {
    home: true,
    chat: true,
  },
  stripeId: "",
};

export const configModalInit: ModalModel = {
  isOpen: false,
  form: "Home",
  id: "",
  idSub: "",
  imgUrl: "",
};

export const configInit: ConfigModel = {
  modal: configModalInit,
  path: "",
  id: "",
  normal: {
    id: "",
    path: "",
  },
  room: {
    id: "",
    path: "",
  },
  application: {
    id: "",
    path: "",
  },
  attorney: {
    id: "",
    path: "",
  },
  applicant: {
    id: "",
    path: "",
  },
  user: {
    id: "",
    path: "",
  },
  offer: {
    id: "",
    path: "",
  },
  patent: {
    id: "",
    path: "",
  },
  bank: {
    id: "",
    path: "",
    name: "",
  },
};
export const patentInit: PatentModel = {
  referenceNum: "",
  fillingNum: "",
  knownDate: new Date(),
  fillingDate: new Date(),
  explanation: "",
  developName: "",
  fi: "",
  jplatUrl: "",
  rightHolder: "",
  document: new File([blob], "name", { lastModified: 0 }),
  application: "",
};

export const appInit: ApplicationModel = {
  title: "",
  category: null,
  explanation: "",
  document: ProfileCamera,
  recruit: [],
  status: "Recruit",
  price: 0,
  creater: null,
  attorney: null,
  patent: null,
  maintenancePrice: 0,
  platform: "web",
  subscriptionId: "",
};

export const bankInputInit: BankInputModel = {
  country: "JP",
  currency: "jpy",
  bank_number: "",
  kind: "Normal",
  branch_number: "",
  account_holder_name: "",
  account_holder_type: "individual",
  account_number: "",
};
export default AttorneyProfileInit;
