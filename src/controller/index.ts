import SideBarIconUserActive from 'assets/img/common/account-active.svg';
import SideBarIconUser from 'assets/img/common/account.svg';
import SideBarIconChatActive from 'assets/img/common/chat-active.svg';
import SideBarIconChat from 'assets/img/common/chat.svg';
import SideBarIconHomeActive from 'assets/img/common/home-active.svg';
import SideBarIconHome from 'assets/img/common/home.svg';
import SideBarIconListActive from 'assets/img/common/list-active.svg';
import SideBarIconList from 'assets/img/common/list.svg';
import { tabChoice } from '../data/choice/index';


export function iconSelect(activate: boolean, what: typeof tabChoice[number]) {
  if (activate) {
    switch (what) {
      case 'account':
        return SideBarIconUser;
      case 'chat':
        return SideBarIconChat;
      case 'home':
        return SideBarIconHome;
      case 'list':
        return SideBarIconList;
      default:
        return what;
    }
  } else {
    switch (what) {
      case 'account':
        return SideBarIconUserActive;
      case 'chat':
        return SideBarIconChatActive;
      case 'home':
        return SideBarIconHomeActive;
      case 'list':
        return SideBarIconListActive;
      default:
        return what;
    }
  }
};

export function noValueText(text: any, alternative?: any) {
  if (!text) {
    return "未登録";
  } else {
    return alternative ? alternative : text;
  }
};

export default iconSelect;
