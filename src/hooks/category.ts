import "firebase/auth";
import "firebase/firestore";
import React from "react";
import { categoryObjectChoice } from "../data/choice/index";

type categoryProp = keyof typeof categoryObjectChoice;

export default function useCategory() {
  const [patentCategory, setPatentCategory] =
    React.useState<categoryProp>("DailyNecessities");
  const handleChange = React.useCallback(
    (e: React.ChangeEvent<HTMLSelectElement>) => {
      e.preventDefault();
      const result = e.currentTarget.value;

      if (
        result === "DailyNecessities" ||
        result === "Transportation" ||
        result === "Science" ||
        result === "Paper" ||
        result === "FixedStructure" ||
        result === "Machine" ||
        result === "Physics" ||
        result === "Electrical" ||
        result === "NoUnderstand"
      ) {
        setPatentCategory(result);
      }
    },
    [setPatentCategory]
  );
  return { patentCategory, setPatentCategory, handleChange };
}
