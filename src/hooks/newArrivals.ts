import React from 'react';
export default function useNewArrivals(){
  const [newArrivals, setNewArrivals] = React.useState<number>(0);

  return {
    newArrivals, setNewArrivals
  }
}
