import { ApplicantProfileInit } from 'data/initial/index';
import firebase from 'firebase';
import 'firebase/auth';
import 'firebase/firestore';
import KenAll from 'ken-all';
import ApplicantModel from 'models/Applicant';
import React from 'react';
import { useHistory } from 'react-router';

interface ProfileModel extends ApplicantModel {
  path: string;
  id: string;
}

type StateModel = ProfileModel | firebase.firestore.DocumentData;

export default function useProfile(path?: string) {
  const history = useHistory();
  const [photo, setPhoto] = React.useState<boolean>(false);
  const [profile, setProfile] = React.useState<StateModel>(ApplicantProfileInit);

  const imgInput = React.useRef<HTMLInputElement>(null);

  React.useEffect(() => {
    const auth = firebase.auth().currentUser;
    if (path) {
      const db = firebase.firestore().doc(path);
      db.onSnapshot((snap) => {
        let item: firebase.firestore.DocumentData | undefined = snap.data();
        setProfile({
          path: snap.ref.path,
          id: snap.id,
          ...item,
        });
      });
    } else {
      const db = firebase.firestore().collection('User').doc(auth?.uid);
      db.onSnapshot((snap) => {
        let item: firebase.firestore.DocumentData | undefined = snap.data();
        setProfile({
          path: snap.ref.path,
          id: snap.id,
          ...item,
        });
      });
    }
  }, [setProfile, path]);

  const handleSubmit = React.useCallback((e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const auth = firebase.auth().currentUser;
    const file = imgInput.current!.files![0];
    const db = firebase.firestore().collection('User').doc(auth?.uid);
    async function imgSave() {
      const storageRef = firebase.storage().ref().child(`profile/${auth?.uid}`);
      const imgRef = await storageRef.put(file, { contentType: `${file?.type}` });
      const url = await imgRef.ref.getDownloadURL();
      await db.update({
        ...profile,
        image: url,
        updateDt: firebase.firestore.FieldValue.serverTimestamp()
      });
    };
    if (photo) {
      imgSave();
    };
    return history.push('/applicant/home');
  }, [history, profile, photo]);

  const handleImageChange = React.useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
    e.preventDefault();
    const reader = new FileReader();
    let file = imgInput.current!.files![0];
    reader.onload = () => {
      setProfile({
        ...profile,
        image: reader.result
      })
    }
    reader.readAsDataURL(file);
    setPhoto(true);
  }, [profile, setProfile, setPhoto]);

  //郵便番号で住所を特定する
  const searchAddress = React.useCallback((e: React.KeyboardEvent<HTMLIonInputElement>) => {
    e.preventDefault();
    const inputValue: any = e.currentTarget.value;
    if (inputValue.length === 7) {
      KenAll(inputValue).then((address) => {
        if (address.length !== 0) {
          let state: string = `${address[0][0]}`;
          let city: string = `${address[0][1]}`;
          let line1: string = `${address[0][2]}`;
          setProfile({
            ...profile,
            address: {
              ...profile?.address,
              state: state,
              city: city,
              line1: line1,
              country: "日本"
            }
          })
        }
      })
    }
  }, [profile, setProfile]);

  const handleInputChange = React.useCallback((e: CustomEvent): void => {
    const target = e.target as HTMLIonInputElement;

    setProfile({
      ...profile,
      [target.name]: e.detail.value
    })
  }, [profile, setProfile]);

  const handleAddressChange = React.useCallback((e: CustomEvent): void => {
    const target = e.target as HTMLIonInputElement;

    setProfile({
      ...profile,
      [target.name]: e.detail.value
    })
  }, [profile, setProfile]);

  const handleTextareaChange = React.useCallback((e: CustomEvent): void => {
    const target = e.target as HTMLIonInputElement;

    setProfile({
      ...profile,
      [target.name]: e.detail.value
    })
  }, [profile, setProfile]);

  return { profile, setProfile, imgInput, handleSubmit, handleInputChange, handleAddressChange, handleTextareaChange, handleImageChange, searchAddress }
}
