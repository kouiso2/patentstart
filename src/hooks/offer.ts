import firebase from 'firebase';
import 'firebase/auth';
import 'firebase/firestore';
import React from 'react';
import { useHistory } from 'react-router';

interface OfferModel {
  price: number;
  remark: string;
  attorney: firebase.firestore.DocumentReference<firebase.firestore.DocumentData> | null | null;
  application: firebase.firestore.DocumentReference<firebase.firestore.DocumentData> | null;
}

type StateModel = OfferModel | firebase.firestore.DocumentData;

export default function useOffer(appPath: string, offerPath?: string) {
  const history = useHistory();
  const [roomId, setRoomId] = React.useState<string>("");

  const [offer, setOffer] = React.useState<StateModel>({
    price: 0,
    remark: ""
  });

  const [signal, setSignal] = React.useState<boolean>(false);

  React.useEffect(() => {
    if (offerPath) {
      firebase.firestore().doc(offerPath).onSnapshot((snap) => {
        setOffer({
          ...snap.data(),
          application: snap.data()?.application,
          attorney: snap.data()?.attorney,
        })
      })
    }
  }, [offerPath, setOffer]);

  const handleSubmit = React.useCallback((e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const auth = firebase.auth().currentUser;
    const db = firebase.firestore();
    const appRef = db.doc(appPath);
    const offerRef = db.collection('User').doc(auth?.uid).collection('Offer');
    const attorney = db.collection('User').doc(auth?.uid);
    const roomRef = db.collection('Rooms').doc(roomId);
    const messageRef = roomRef.collection('Messages');
    const timestamp = firebase.firestore.FieldValue.serverTimestamp();
    offerRef.add({
      ...offer,
      createDt: timestamp,
      creater: attorney,
      attorney: attorney,
      application: appRef,
    }).then((offerRes) => {
      roomRef.update({
        offerPath: db.doc(offerRes.path),
        lastOffer: offer.price,
        lastOfferTimestamp: timestamp,
        lastMessageText: offer.price + '円プラン',
        lastOfferMessageTimestamp: timestamp,
      }).catch((error) => {
        window.alert('処理が失敗しました');
        console.error(error, "failed");
      });
      messageRef.doc(offerRes.id).set({
        senderId: auth?.uid,
        state: 'offer',
        text: offer.price,
        timestamp: timestamp
      }).then(() => {
        window.alert("オファーが送信されました、チャットへ戻ります");
        history.push({
          pathname: `/attorney/chat/${roomId}`
        });
      })
        .catch((error) => {
          window.alert('処理が失敗しました');
          console.error(error, "failed");

        })
    }).catch((error) => {
      window.alert('処理が失敗しました');
      console.error(error, "failed");

    });
  }, [roomId, history, appPath, offer]);
  //async await
  // const handleSubmit = React.useCallback(async(e: React.FormEvent<HTMLFormElement>) => {
  //   e.preventDefault();
  //   const auth = firebase.auth().currentUser;
  //   const db = firebase.firestore();
  //   const appRef = db.doc(appPath);
  //   const offerRef = db.collection('User').doc(auth?.uid).collection('Offer');
  //   const attorney = db.collection('User').doc(auth?.uid);
  //   const roomRef = db.collection('Rooms').doc(roomId);
  //   const messageRef = roomRef.collection('Messages');
  //   const timestamp = firebase.firestore.FieldValue.serverTimestamp();
  //   const offerRes = await offerRef.add({
  //     ...offer,
  //     createDt: timestamp,
  //     creater: attorney,
  //     attorney: attorney,
  //     application: appRef,
  //   });
  //   await roomRef.update({
  //     offerPath: db.doc(offerRes.path),
  //     lastOffer: offer.price,
  //     lastOfferTimestamp: timestamp,
  //     lastMessageText: offer.price + '円プラン',
  //     lastOfferMessageTimestamp: timestamp,
  //   });
  //   await messageRef.doc(offerRes.id).set({
  //     senderId: auth?.uid,
  //     state: 'offer',
  //     text: offer.price,
  //     timestamp: timestamp
  //   });
  //   window.alert("オファーが送信されました、チャットへ戻ります");
  //   history.push({
  //     pathname: `/attorney/chat/${roomId}`
  //   });
  // }, [roomId, history, appPath, offer]);

  return { offer, setOffer, handleSubmit, setRoomId, signal, setSignal };
}
