import firebase from "firebase";
import "firebase/auth";
import "firebase/firestore";
import model from "models/Application";
import React from "react";
import { useHistory } from "react-router";
import useCategory from "./category";
import useSorter from "./sorter";
import useTab from "./tab";

type appTypes = model[] | firebase.firestore.DocumentData[];
type docTypes = firebase.firestore.DocumentData[];
interface stateProps {
  app: appTypes;
}

export default function useMatter() {
  const history = useHistory();

  const { patentCategory, setPatentCategory } = useCategory();
  const { sorter, setSorter } = useSorter();
  const { tab, setTab } = useTab();

  const [matter, setMatter] = React.useState<stateProps>();
  const [newArrivals, setNewArrivals] = React.useState<number>(0);

  const matterQuery = React.useCallback(() => {
    const db = firebase.firestore().collectionGroup("Application");

    const dbRef = db.where("status", "==", "Recruit");
    // const test = db.where(dbUser ,'==',)
    return (
      dbRef
        // .orderBy('createDt', sorter).limit(40)
        .orderBy("createDt", sorter)
    );
    // if (tab) {
    //   return (
    //     dbRef
    //       .where("category", "==", patentCategory)
    //       // .orderBy('createDt', sorter).limit(40)
    //       .orderBy("createDt", sorter)
    //   );
    // } else {
    //   return (
    //     dbRef
    //       // .orderBy('createDt', sorter).limit(40)
    //       .orderBy("createDt", sorter)
    //   );
    // }
  }, [sorter]);

  React.useEffect(() => {
    let unmounted = true;

    const query = matterQuery();
    let list: docTypes = [];

    query.onSnapshot((snap: firebase.firestore.QuerySnapshot) => {
      snap.forEach((res: firebase.firestore.DocumentSnapshot) => {
        list.push({
          id: res.id,
          path: res.ref.path,
          ...res.data(),
        });
      });
      //案件数を入れる
      if (unmounted) {
        let num = list.length;

        setMatter({
          ...matter,
          app: list,
        });
        setNewArrivals(num);
      }
    });
    return () => {
      // cleanup
      unmounted = false;
    };
  }, [matter, setMatter, setNewArrivals, patentCategory, matterQuery, sorter]);

  const handleClick = React.useCallback(
    (e: React.MouseEvent<HTMLElement>) => {
      e.preventDefault();
      const id = e.currentTarget.getAttribute("data-id");
      const path = e.currentTarget.getAttribute("data-path");

      history.push({
        pathname: `/attorney/newarrivals/detail/${id}/`,
        state: path,
      });
    },
    [history]
  );

  return {
    matter,
    setMatter,
    handleClick,
    newArrivals,
    patentCategory,
    setPatentCategory,
    sorter,
    setSorter,
    tab,
    setTab,
    matterQuery,
  };
}
