// import { FacebookLogin } from "@capacitor-community/facebook-login";
import { Plugins } from "@capacitor/core";
import "@codetrix-studio/capacitor-google-auth";
import { isPlatform } from "@ionic/react";
import { cfaSignIn } from "capacitor-firebase-auth";
//push notification
import { Push } from "components/PushNotification/app/Push";
import firebase from "firebase";
import "firebase/auth";
import "firebase/firestore";
import React from "react";
import { useHistory } from "react-router";
import { userChoice } from "../data/choice/index";
const { SignInWithApple, Device } = Plugins;

export default function useLogin() {
  const history = useHistory();
  const [email, setEmail] = React.useState<string>("");
  const [password, setPassword] = React.useState<string>("");
  firebase.auth().languageCode = "ja";
  const redirectPage = React.useCallback(() => {
    const auth = firebase.auth().currentUser;
    if (isPlatform("ios") || isPlatform("android")) {
      //iosとandroidの場合はチャットページ一覧に遷移させる
      const push = new Push("");
      push.push();
      firebase
        .firestore()
        .collection("User")
        .doc(auth?.uid)
        .onSnapshot((snap) => {
          const item = snap.data();
          if (item) {
            let kind: typeof userChoice[number] = item?.kind;
            if (kind === "Attorney") {
              history.push("/attorney/chat");
            } else if (kind === "Applicant") {
              history.push("/applicant/chat");
            } else if (kind === "Admin") {
              history.push("/admin");
            } else {
              history.push("/auth");
            }
          } else {
            history.push("/auth/signup/select");
          }
        });
    } else {
      firebase
        .firestore()
        .collection("User")
        .doc(auth?.uid)
        .onSnapshot((snap) => {
          const item = snap.data();
          if (item) {
            let kind: typeof userChoice[number] = item?.kind;
            if (kind === "Attorney") {
              history.push("/attorney");
            } else if (kind === "Applicant") {
              history.push("/applicant");
            } else if (kind === "Admin") {
              history.push("/admin");
            } else {
              history.push("/auth");
            }
          } else {
            history.push("/auth/signup/select");
          }
        });
    }
  }, [history]);

  const handleGoogleLogin = React.useCallback(
    async (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
      e.preventDefault();
      const googleUser = await Plugins.GoogleAuth.signIn();
      const credential = firebase.auth.GoogleAuthProvider.credential(
        googleUser.authentication.idToken
      );
      await firebase.auth().signInWithCredential(credential);
      redirectPage();
    },
    [redirectPage]
  );

  const handleTwitterLogin = React.useCallback(
    async (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
      e.preventDefault();
      cfaSignIn("twitter.com").subscribe((user: firebase.User) => {
        console.log(user.displayName);
        redirectPage();
      });
    },
    [redirectPage]
  );
  // const handleTwitterCommunityLogin = React.useCallback(
  //   async (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
  //     e.preventDefault();
  //     const twitter = new TwitterClass();
  //     twitter
  //       .login()
  //       .then((r) => setTestTw("login")) // { authToken:string, authTokenSecret:string, userName:string, userID:string }
  //       .catch((err) => console.log(err));
  //   },
  //   []
  // );

  const handleFacebookLogin = React.useCallback(
    async (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
      e.preventDefault();
      cfaSignIn("facebook.com").subscribe((user: firebase.User) => {
        console.log(user.displayName);
        redirectPage();
      });
    },
    [redirectPage]
  );

  // const handleFacebookCommunityLogin = React.useCallback(
  //   async (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
  //     e.preventDefault();
  //     const FACEBOOK_PERMISSIONS = ["email", "user_birthday"];
  //     const result = await FacebookLogin.login({
  //       permissions: FACEBOOK_PERMISSIONS,
  //     });
  //     const token = result.accessToken?.token;
  //     const credential = firebase.auth.FacebookAuthProvider.credential(token!);
  //     await firebase.auth().signInWithCredential(credential);
  //     redirectPage();
  //   },

  //   [redirectPage]
  // );

  const handleAppleLogin = React.useCallback(
    async (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
      e.preventDefault();
      const provider = new firebase.auth.OAuthProvider("apple.com");
      let device = await Device.getInfo();

      if (device.platform === "ios") {
        const response = await SignInWithApple.Authorize();
        const credential = provider.credential({
          idToken: response.identityToken,
        });
        await firebase.auth().signInWithCredential(credential);
        // Show the button with SignInWithApple.Authorize()
        console.log("apple login success", response);
      }
    },

    []
  );
  const handleLoginSubmit = React.useCallback(
    (e: React.FormEvent<HTMLFormElement>) => {
      e.preventDefault();
      firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
        .then(() => {
          redirectPage();
        })
        .catch((err) => {
          console.error(err);
          alert("メールアドレスまたはパスワードが間違ってます。");
        });
    },
    [email, password, redirectPage]
  );

  return {
    email,
    setEmail,
    password,
    setPassword,
    handleLoginSubmit,
    handleGoogleLogin,
    handleTwitterLogin,
    handleAppleLogin,
    handleFacebookLogin,
  };
}
