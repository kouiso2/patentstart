/**
 * @modules 保守管理確認ページ
 * route /applicant/patentend/check
 */

import { IAPProduct, InAppPurchase2 } from "@ionic-native/in-app-purchase-2";
import { isPlatform } from "@ionic/react";
import firebase from "firebase";
import "firebase/firestore";
import "firebase/functions";
import useApplication from "hooks/application";
import useAuth from "hooks/auth";
import useOffer from "hooks/offer";
import useProfile from "hooks/profile";
import React from "react";
import { useHistory } from "react-router";

interface PaymentProps {
  kind: "Apply" | "Manage";
  offerPath: string;
}

const usePayment = (props: PaymentProps) => {
  const { offer } = useOffer("", props.offerPath);
  const { app } = useApplication(offer.application?.path);
  const { profile, profileIdentify } = useProfile(offer.attorney?.path);
  const fullName = profile.firstName + profile.lastName;
  const history = useHistory();
  const auth = useAuth();
  const func = firebase.functions();
  const db = firebase.firestore();
  InAppPurchase2.verbosity = InAppPurchase2.DEBUG;

  const priceIosFunc = React.useCallback((price: number) => {
    if (price === 30000) {
      return 29800;
    } else if (price === 50000) {
      return 49800;
    } else if (price === 10000) {
      return price;
    }
  }, []);

  const patentApply = React.useCallback(async () => {
    // 特許情報のcollectionを作成する
    const patentRes = await db
      .doc(profileIdentify.path)
      .collection("Patent")
      .add({
        application: app.path,
        developName: app.title,
        explanation: app.explanation,
      });
    // 申請情報の変更・追記を行う Application collection
    await db.doc(app.path).update({
      price: priceIosFunc(offer.price),
      status: "Apply",
      updateDt: firebase.firestore.FieldValue.serverTimestamp(),
      updater: auth?.path,
      attorney: db.doc(profileIdentify.path),
      patent: patentRes.path,
    });

    return history.push({
      pathname: "/applicant/request/complete",
      state: props.offerPath,
    });
  }, [
    db,
    app,
    offer,
    props.offerPath,
    history,
    auth?.path,
    profileIdentify.path,
    priceIosFunc,
  ]);

  const patentManage = React.useCallback(async () => {
    // 申請情報の変更・追記を行う Application collection
    await db.doc(app.path).update({
      maintenancePrice: priceIosFunc(offer.price),
      status: "Manage",
      updateDt: firebase.firestore.FieldValue.serverTimestamp(),
      updater: auth?.path,
    });

    return history.push({
      pathname: "/applicant/patentend/complete",
      state: props.offerPath,
    });
  }, [
    history,
    props.offerPath,
    app.path,
    auth?.path,
    priceIosFunc,
    offer.price,
    db,
  ]);

  const handlePatent = React.useCallback(async () => {
    if ((props.kind = "Apply")) {
      patentApply();
    } else {
      patentManage();
    }
  }, [props.kind, patentApply, patentManage]);

  const priceCode = React.useMemo(() => {
    if (offer.price === 30000) {
      // スタンダードプラン
      return "jp.patentstart.www.standard";
    } else if (offer.price === 50000) {
      // ハイグレードプラン
      return "jp.patentstart.www.highgrade";
    } else {
      // 保守管理プラン
      return "jp.patentstart.www.maintenance";
    }
  }, []);

  React.useEffect(() => {
    InAppPurchase2.when(priceCode).updated((p: IAPProduct) => {
      if (p.state === "approved" && p.id === priceCode) {
        console.log("ID", p.id);
        handlePatent();
      }
    });
  }, [history, props.offerPath, priceCode, handlePatent]);

  React.useEffect(() => {
    InAppPurchase2.register({
      id: priceCode,
      type: InAppPurchase2.NON_RENEWING_SUBSCRIPTION,
    });
    InAppPurchase2.refresh();
  }, [priceCode]);

  InAppPurchase2.ready(() => {
    console.log("Store is ready");
    console.log("Products: " + JSON.stringify(InAppPurchase2.products));
    console.log(JSON.stringify(InAppPurchase2.get(priceCode)));
  });

  const handleFixClick = React.useCallback(
    (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
      e.preventDefault();
      return history.goBack();
    },
    [history]
  );

  /**@function stripeのApplicantのアカウント作成 */
  const createStripeAccount = React.useCallback(async () => {
    const register = func.httpsCallable("createApplicantAccount");
    if (!auth?.stripeId) {
      await register();
    }
  }, [auth?.stripeId, func]);

  const stripePayment = React.useCallback(async () => {
    const subscription = func.httpsCallable("linkingSubscription");
    const maintenance = func.httpsCallable("linkingMaintenance");
    if (props.kind === "Apply") {
      // メインの決済の場合
      await subscription({
        price: offer.price,
        attorneyPath: profileIdentify.path,
        appPath: app.path,
      });
    } else {
      // 保守の場合
      await maintenance({
        appPath: app.path,
      });
    }
  }, [app, offer, props.kind, func, profileIdentify]);

  const handleSaveClick = React.useCallback(
    async (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
      e.preventDefault();

      //iosアプリ内課金を行う
      if (isPlatform("ios")) {
        const priceItem = InAppPurchase2.get(priceCode);
        InAppPurchase2.order(priceItem).then(
          (p: IAPProduct) => {
            //　決済成功した
            console.log(p);
          },
          (e: any) => {
            // 決済失敗した
            console.error(e);
          }
        );
        // Ios以外の決済なら
      } else {
        try {
          await createStripeAccount();
          await stripePayment();
          await handlePatent();
        } catch (e) {
          window.alert("処理が失敗しました");
        }
      }
    },
    [handlePatent, priceCode, stripePayment, createStripeAccount]
  );

  return {
    handleSaveClick,
    handleFixClick,
    fullName,
  };
};

export default usePayment;
