import firebase from 'firebase';
import 'firebase/firestore';
import React from 'react';
import { useHistory } from 'react-router';
import { appInit } from '../data/initial/index';
import ApplicationModel from '../models/Application';


interface appProp extends ApplicationModel {
  creater: firebase.firestore.DocumentReference<firebase.firestore.DocumentData>;
  path: string;
};
type stateProps = appProp | firebase.firestore.DocumentData;

export default function useApplication(path: string) {
  const history = useHistory();
  const [application, setApplication] = React.useState<stateProps>(appInit);
  const docInput = React.useRef<HTMLInputElement>(null);
  const auth = firebase.auth().currentUser;
  const app = application;
  const setApp = setApplication;

  React.useEffect(() => {
    if (path) {
      firebase.firestore().doc(path).onSnapshot((snap: firebase.firestore.DocumentSnapshot) => {

        setApplication({
          ...snap.data(),
          path: snap.ref.path
        });
      });
    }
  }, [path, setApplication]);

  const handleCheckSubmit = React.useCallback((e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    let docFile = docInput.current!.files![0];

    setApplication({
      ...application,
      document: docFile
    })

    return history.push('/applicant/application/check/');
  }, [application, setApplication, history]);

  // const handleImageChange = React.useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
  //   e.preventDefault();
  //   const reader = new FileReader();
  //   let file = imgInput.current!.files![0];
  //   reader.onload = () => {
  //     setApplication({
  //       ...application,
  //       image: reader.result
  //     })
  //   }
  //   reader.readAsDataURL(file);
  //   setPhoto(true);
  // }, [application, setApplication, setPhoto]);


  const handleFixClick = React.useCallback((e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault();
    return history.goBack();
  }, [history]);

  async function handleSaveClick(e: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
    e.preventDefault();
    const db = firebase.firestore();
    const storage = firebase.storage();

    const dbRef = db.collection('Application');
    const docRef = storage.ref().child(`application/document/${app?.document.name}`);

    const docRes = await docRef.put(app?.document, { contentType: `${app?.document.type}` });

    const docUrl = await docRes.ref.getDownloadURL();

    setApplication({
      ...app,
      document: docUrl
    })

    await dbRef.add({
      ...app,
      document: docUrl,
      createDt: firebase.firestore.FieldValue.serverTimestamp(),
      creater: firebase.firestore().collection('User').doc(auth?.uid)
    });
    history.push('/applicant/home/');
  }
  return {
    app,
    setApp,
    // handleImageChange,
    handleCheckSubmit,
    handleFixClick,
    handleSaveClick,
    docInput
  };

}
