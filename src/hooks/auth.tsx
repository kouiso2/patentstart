import { rootUrlChoice } from 'data/choice/index';
import "firebase/auth";
import "firebase/firestore";
//firebase
import firebase from "firebase/index";
import ApplicantModel from 'models/Applicant';
import AttorneyModel from 'models/Attorney';
import React from 'react';
import { Redirect } from 'react-router';
interface AuthModel extends ApplicantModel, AttorneyModel {
  uid?: string;
  url: typeof rootUrlChoice[number] | "/auth/signup/select";
  redirect: React.ReactNode;
  path: string;
}

type StateModel = AuthModel | firebase.firestore.DocumentData;
export function useAuth() {
  const [auth, setAuth] = React.useState<StateModel>();
  React.useEffect(() => {
    firebase.auth().onAuthStateChanged((login) => {
      if (login?.uid) {
        const db = firebase.firestore();
        const dbRef = db.collection("User").doc(login.uid);
        dbRef.onSnapshot((snap: firebase.firestore.DocumentSnapshot) => {

          const item = snap.data();
          if (item?.kind === "Applicant") {
            //申請者だったら
            setAuth({
              ...item,
              path: snap.ref.path,
              uid: login.uid,
              url: "/applicant",
              redirect: <Redirect to="/applicant" />,
              kind: "Applicant",
            });
          } else if (item?.kind === "Attorney") {
            //弁理士
            setAuth({
              ...item,
              path: snap.ref.path,
              uid: login.uid,
              url: "/attorney",
              redirect: <Redirect to="/attorney" />,
              kind: "Attorney",
            });
          } else if (item?.kind === "Admin") {
            //管理者
            setAuth({
              ...item,
              path: snap.ref.path,
              uid: login.uid,
              url: "/admin",
              redirect: <Redirect to="/admin" />,
              kind: "Admin",
            });
          } else {
            //アカウントがない

            setAuth({
              ...item,
              url: "/auth/signup/select",
              redirect: <Redirect to="/auth/signup/select" />,
              kind: "Auth",
            });
          }
        })
      } else {
        setAuth({
          url: "/",
          redirect: <Redirect to="/auth" />,
          kind: "Auth"
        });
        setAuth({
          url: "/auth",
          redirect: <Redirect to="/" />,
          kind: "Auth"
        });
      }
    });
  }, [auth?.kind]);

  return auth;
}
export default useAuth;
