import 'firebase/auth';
import 'firebase/firestore';
import React from "react";
interface bankProps {
  country: string;
  currency: string;
  routing_number: string;
  account_holder_name: string;
  account_holder_type: string;
  account_number: string;
}

export default function useBank(){
  const [bank, setBank] = React.useState<bankProps>({
    country: "JP",
    currency: "jpy",
    routing_number: "0001054",
    account_holder_name: 'カ）ROND',
    account_holder_type: 'individual',
    account_number: '4695851',
  });

  return {bank, setBank};
}
