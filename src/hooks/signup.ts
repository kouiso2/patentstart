import firebase from 'firebase';
import 'firebase/auth';
import 'firebase/functions';
import React from "react";
import { useHistory } from "react-router";

interface stateProps {
  mainEmail: string;
  checkEmail: string;
  password: string;
  validate: string;
}

export default function useSignup(){
  const history = useHistory();
  const [signup, setSignup] = React.useState<stateProps>({
    mainEmail: "",
    checkEmail: "",
    password: "",
    validate: ""
  });

  async function handleSignupSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    const auth = firebase.auth();
    const actionCodeSettings = {
      url: "http://localhost:8100/signin/",
    };
    if (signup.mainEmail !== signup.checkEmail) {
      setSignup({
        ...signup,
        validate: "確認用のメールアドレスが違います"
      });
      return;
    } else {
      try {
        await auth.createUserWithEmailAndPassword(signup.mainEmail, signup.password);
        await auth.currentUser?.sendEmailVerification(actionCodeSettings);
        return history.push('/auth/signup/select');
      } catch (e) {
        setSignup({
          ...signup,
          validate: "このメールアドレスでは登録できません"
        })
      }
    }
  }

  const validateEmail = React.useCallback(() => {
    if (signup.checkEmail === signup.mainEmail) {
      setSignup({
        ...signup,
        validate: ""
      });
    } else {
      setSignup({
        ...signup,
        validate: "確認用のメールアドレスが違います"
      });
    }
  }, [signup, setSignup]);
  return {signup, setSignup, handleSignupSubmit, validateEmail};
}
