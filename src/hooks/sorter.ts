import 'firebase/auth';
import 'firebase/firestore';
import React from "react";


export default function useSorter() {
  const [sorter, setSorter] = React.useState<any>("asc");
  const handleChange = React.useCallback((e: React.ChangeEvent<HTMLSelectElement>) => {
    e.preventDefault();
    const result = e.currentTarget.value;

    if (
      result === "DailyNecessities" ||
      result === "Transportation" ||
      result === "Science" ||
      result === "Paper" ||
      result === "FixedStructure" ||
      result === "Machine" ||
      result === "Physics" ||
      result === "Electrical"
    ) {
      setSorter(result);
    }
  }, [setSorter]);
  return { sorter, setSorter, handleChange };
}
