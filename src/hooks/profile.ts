import AttorneyProfileInit from "data/initial";
import firebase from "firebase";
import "firebase/auth";
import "firebase/firestore";
import useAuth from "hooks/auth";
import KenAll from "ken-all";
import ApplicantModel from "models/Applicant";
import AttorneyModel from "models/Attorney";
import React from "react";
import { useHistory } from "react-router";
import UserModel from "../models/User";
const phoneUtil =
  require("google-libphonenumber").PhoneNumberUtil.getInstance();
interface ProfileModel extends UserModel {
  path: string;
  id: string;
}

type StateModel = ProfileModel | firebase.firestore.DocumentData;

export default function useProfile(path?: string) {
  //custom-hook
  const history = useHistory();
  const auth = useAuth();

  //state
  const [photo, setPhoto] = React.useState<boolean>(false);
  const [profile, setProfile] = React.useState<StateModel>(AttorneyProfileInit);
  const [signal, setSignal] = React.useState<boolean>(false);
  const [profileIdentify, setProfileIdentify] = React.useState<{
    id: string;
    path: string;
  }>({
    id: "",
    path: "",
  });

  //useRef
  const imgInput = React.useRef<HTMLInputElement>(null);

  function toE164Number(strVal: string) {
    // 半角変換
    const halfVal = strVal.replace(/[！-～]/g, function (tmpStr) {
      // 文字コードをシフト
      return String.fromCharCode(tmpStr.charCodeAt(0) - 0xfee0);
    });
    // 文字の変換
    phoneUtil.parseAndKeepRawInput(halfVal, "JP");
    return halfVal.replace(/[\s+()－―]/gi, "").replace("0", "+81");
  }

  React.useEffect(() => {
    let unmounted = true;
    const auth = firebase.auth().currentUser;
    if (path) {
      const db = firebase.firestore().doc(path);
      db.onSnapshot((snap) => {
        if (unmounted) {
          const item: firebase.firestore.DocumentData | undefined = snap.data();
          console.log(
            "firebase goodCategory",
            item?.goodCategory?.Transportation
          );
          setProfile({
            ...item,
          });
          setProfileIdentify({
            id: snap.id,
            path: snap.ref.path,
          });
        }
      });
    } else {
      const db = firebase.firestore().collection("User").doc(auth?.uid);
      db.onSnapshot((snap) => {
        if (unmounted) {
          const item: firebase.firestore.DocumentData | undefined = snap.data();
          setProfile({
            ...item,
          });
          setProfileIdentify({
            id: snap.id,
            path: snap.ref.path,
          });
        }
      });
    }
    return () => {
      // cleanup
      unmounted = false;
    };
  }, [setProfile, path, setProfileIdentify]);

  const handleSubmit = React.useCallback(
    async (e: React.FormEvent<HTMLFormElement>) => {
      e.preventDefault();
      const func = firebase.functions();
      const createAttorneyAccount = func.httpsCallable("createAttorneyAccount");
      const db = firebase.firestore().collection("User").doc(auth?.uid);
      const file = imgInput.current!.files![0];
      const items =
        auth?.kind === "Attorney"
          ? (profile as AttorneyModel)
          : (profile as ApplicantModel);
      const phoneNumber = toE164Number(items.phoneNumber);
      if (photo) {
        const storageRef = firebase
          .storage()
          .ref()
          .child(`profile/${auth?.uid}`);
        const imgRef = await storageRef.put(file, {
          contentType: `${file?.type}`,
        });
        const url = await imgRef.ref.getDownloadURL();
        await db.update({
          ...items,
          image: url,
          personName: items.firstName + items.lastName,
          personKatakana: items.firstNameKatakana + items.lastNameKatakana,
          countryPhoneNumber: phoneNumber,
          updateDt: firebase.firestore.FieldValue.serverTimestamp(),
        });
      } else {
        await db.update({
          ...items,
          personName: items.firstName + items.lastName,
          personKatakana: items.firstNameKatakana + items.lastNameKatakana,
          countryPhoneNumber: phoneNumber,
          updateDt: firebase.firestore.FieldValue.serverTimestamp(),
        });
      }

      if (auth?.kind === "Attorney") {
        if (!auth.stripeId) {
          try {
            await createAttorneyAccount();
          } catch (e) {
            console.error(e, "failed Attorney Account for stripe");
          }
        }
        return history.push("/attorney/home");
      } else if (auth?.kind === "Applicant") {
        setSignal(true);
        history.push("/applicant/home");
      }
    },
    [auth?.kind, auth?.uid, auth?.stripeId, history, profile, photo, setSignal]
  );

  const handleImageChange = React.useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      e.preventDefault();
      const file = e.target.files![0];
      const type = file.type;
      const size = file.size;
      const limit = 2000000;
      const reader = new FileReader();
      if (type !== "image/png" && type !== "image/jpeg") {
        e.target.value = "";
        alert("pngかjpegの形式でアップロードして下さい");
        return;
      }

      if (size > limit) {
        e.target.value = "";
        alert("プロフィール画像は2MB以下で指定してください");
        return;
      }
      reader.onload = () => {
        setProfile({
          ...profile,
          image: reader.result,
        });
      };
      reader.readAsDataURL(file);
      setPhoto(true);
    },
    [profile, setProfile, setPhoto]
  );

  //郵便番号で住所を特定する
  const searchAddress = React.useCallback(
    (e: React.KeyboardEvent<HTMLIonInputElement>) => {
      e.preventDefault();
      const inputValue: any = e.currentTarget.value;
      if (inputValue.length === 7) {
        KenAll(inputValue).then((address) => {
          if (address.length !== 0) {
            let state: string = `${address[0][0]}`;
            let city: string = `${address[0][1]}`;
            let town: string = `${address[0][2]}`;
            setProfile({
              ...profile,
              address: {
                ...profile?.address,
                state: state,
                city: city,
                town: town,
                country: "日本",
              },
            });
          }
        });
      }
    },
    [profile, setProfile]
  );

  const handleCheckboxClick = React.useCallback(
    (e: React.MouseEvent<HTMLInputElement, MouseEvent>) => {
      e.preventDefault();

      setProfile({
        ...profile,
        goodCategory: {
          ...profile.goodCategory,
          [e.currentTarget.name]: e.currentTarget.checked ? false : true,
        },
      });
    },
    [setProfile, profile]
  );

  const handleInputChange = React.useCallback(
    (e: CustomEvent): void => {
      const target = e.target as HTMLIonInputElement;

      setProfile({
        ...profile,
        [target.name]: e.detail.value,
      });
    },
    [profile, setProfile]
  );

  const handleAddressChange = React.useCallback(
    (e: CustomEvent): void => {
      const target = e.target as HTMLIonInputElement;

      setProfile({
        ...profile,
        [target.name]: e.detail.value,
      });
    },
    [profile, setProfile]
  );

  const handleTextareaChange = React.useCallback(
    (e: CustomEvent): void => {
      const target = e.target as HTMLIonInputElement;

      setProfile({
        ...profile,
        [target.name]: e.detail.value,
      });
    },
    [profile, setProfile]
  );

  return {
    profile,
    setProfile,
    imgInput,
    handleSubmit,
    handleInputChange,
    handleAddressChange,
    handleTextareaChange,
    handleImageChange,
    handleCheckboxClick,
    searchAddress,
    signal,
    setSignal,
    profileIdentify,
  };
}
