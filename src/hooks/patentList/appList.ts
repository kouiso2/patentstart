import firebase from 'firebase';
import 'firebase/auth';
import 'firebase/firestore';
import ApplicationModel from 'models/Application';
import React from 'react';
import { useHistory } from 'react-router';
import useAuth from '../auth';

export interface AppListModel extends ApplicationModel {
  id: string;
  path: string;
  attorneyName: string;
  applicantName: string;
}

type AppListTypes = AppListModel[] | firebase.firestore.DocumentData[];
type FireDocTypes = firebase.firestore.DocumentData[];
interface stateProps {
  app: AppListTypes;
  count: number;
}

export default function usePatentList() {
  const auth = useAuth();
  const history = useHistory();
  const [recruit, setRecruit] = React.useState<stateProps>();
  const [apply, setApply] = React.useState<stateProps>();
  const [manage, setManage] = React.useState<stateProps>();

  const recruitFunc = React.useCallback(() => {
    const user = firebase.auth().currentUser;
    const db = firebase.firestore().collectionGroup('Application');
    let recruitRef: any;
    if (auth?.kind === "Applicant") {
      recruitRef = db.where("creater", '==', firebase.firestore().collection('User').doc(user?.uid))
        .where('status', '==', 'Recruit');
    } else if (auth?.kind === "Attorney") {
      recruitRef = db.where("recruit", 'array-contains', firebase.firestore().collection('User').doc(user?.uid))
        .where('status', '==', 'Recruit');
    };
    recruitRef.onSnapshot((snap: firebase.firestore.QuerySnapshot) => {
      let list: FireDocTypes = [];
      snap.forEach((res: firebase.firestore.DocumentSnapshot) => {
        list.push({
          id: res.id,
          path: res.ref.path,
          attorneyName: res.data()?.attorney,
          ...res.data(),
        });
      });
      setRecruit({
        app: list,
        count: list.length
      });
    })
  }, [setRecruit, auth?.kind]);

  const applyFunc = React.useCallback(() => {
    const user = firebase.auth().currentUser;
    const db = firebase.firestore().collectionGroup('Application');
    let recruitRef: any;
    if (auth?.kind === "Applicant") {
      recruitRef = db.where("creater", '==', firebase.firestore().collection('User').doc(user?.uid))
        .where('status', '==', 'Apply');
    } else if (auth?.kind === "Attorney") {
      recruitRef = db.where("attorney", '==', firebase.firestore().collection('User').doc(user?.uid))
        .where('status', '==', 'Apply');
    };
    recruitRef.onSnapshot((snap: firebase.firestore.QuerySnapshot) => {
      let list: FireDocTypes = [];
      snap.forEach((res: firebase.firestore.DocumentSnapshot) => {
        list.push({
          id: res.id,
          path: res.ref.path,
          applicantName: res.data()?.creater,
          ...res.data()
        });
      });
      setApply({
        app: list,
        count: list.length
      });
    })
  }, [setApply]);

  const manageFunc = React.useCallback(() => {
    const user = firebase.auth().currentUser;
    const db = firebase.firestore().collectionGroup('Application');
    let recruitRef: any;
    if (auth?.kind === "Applicant") {
      recruitRef = db.where("creater", '==', firebase.firestore().collection('User').doc(user?.uid))
        .where('status', '==', 'Manage');
    } else if (auth?.kind === "Attorney") {
      recruitRef = db.where("attorney", '==', firebase.firestore().collection('User').doc(user?.uid))
        .where('status', '==', 'Manage');
    };
    recruitRef.onSnapshot((snap: firebase.firestore.QuerySnapshot) => {
      let list: FireDocTypes = [];
      snap.forEach((res: firebase.firestore.DocumentSnapshot) => {
        list.push({
          id: res.id,
          path: res.ref.path,
          ...res.data()
        });
      });
      setManage({
        app: list,
        count: list.length
      });
    })
  }, [setManage]);

  const handleRecruitClick = React.useCallback((id: string, path: string) => {
    return history.push({
      pathname: `${auth?.url}/recruitment/${id}`,
      state: path
    })
  }, [history, auth?.url]);
  const handleApplyClick = React.useCallback((id: string, path: string) => {
    return history.push({
      pathname: `${auth?.url}/application/apply/${id}`,
      state: path
    })
  }, [history, auth?.url]);
  const handleManageClick = React.useCallback((id: string, path: string) => {
    return history.push({
      pathname: `${auth?.url}/application/management/${id}`,
      state: path
    })
  }, [history, auth?.url]);

  return { recruit, apply, manage, recruitFunc, applyFunc, manageFunc, handleRecruitClick, handleApplyClick, handleManageClick };
}
