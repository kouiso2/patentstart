import firebase from 'firebase';
import 'firebase/auth';
import 'firebase/firestore';
import ApplicationModel from 'models/Application';
import React from 'react';
import { useHistory } from 'react-router';

export interface appForAttorneyProps extends ApplicationModel {
  id: string;
  path: string;
}

type appTypes = appForAttorneyProps[] | firebase.firestore.DocumentData[];
type docTypes = firebase.firestore.DocumentData[];
interface stateProps {
  app: appTypes;
  count: number;
}

export default function usePatentList() {
  const history = useHistory();

  const [recruit, setRecruit] = React.useState<stateProps>();
  const [apply, setApply] = React.useState<stateProps>();
  const [manage, setManage] = React.useState<stateProps>();

  const recruitFunc = React.useCallback(() => {
    const auth = firebase.auth().currentUser;
    const db = firebase.firestore().collectionGroup('Application')
      .where('recruit', 'array-contains', firebase.firestore().collection('User').doc(auth?.uid))
      .where('status', '==', 'Recruit');
    db.onSnapshot((snap: firebase.firestore.QuerySnapshot) => {
      let list: docTypes = [];
      snap.forEach((res: firebase.firestore.DocumentSnapshot) => {
        list.push({
          id: res.id,
          path: res.ref.path,
          ...res.data()
        });
      });
      let num = list.length;
      setRecruit({
        app: list,
        count: num
      });
    })
  }, [setRecruit]);

  const applyFunc = React.useCallback(() => {
    const auth = firebase.auth().currentUser;
    const db = firebase.firestore().collectionGroup('Application')
      .where('attorney', '==', firebase.firestore().collection('User').doc(auth?.uid))
      .where('status', '==', 'Apply');
    db.onSnapshot((snap: firebase.firestore.QuerySnapshot) => {
      let list: docTypes = [];
      snap.forEach((res: firebase.firestore.DocumentSnapshot) => {
        list.push({
          id: res.id,
          path: res.ref.path,
          ...res.data()
        });
      });
      let num = list.length;
      setApply({
        app: list,
        count: num
      });
    })
  }, [setApply]);

  const manageFunc = React.useCallback(() => {
    const auth = firebase.auth().currentUser;
    const db = firebase.firestore().collectionGroup('Application')
      .where('attorney', '==', firebase.firestore().collection('User').doc(auth?.uid))
      .where('status', '==', 'Manage');
    db.onSnapshot((snap: firebase.firestore.QuerySnapshot) => {
      let list: docTypes = [];
      snap.forEach((res: firebase.firestore.DocumentSnapshot) => {
        list.push({
          id: res.id,
          path: res.ref.path,
          ...res.data()
        });
      });
      let num = list.length;
      setManage({
        app: list,
        count: num
      });
    })
  }, [setManage]);

  const handleRecruitClick = React.useCallback((id: string, path: string) => {
    return history.push({
      pathname: `/attorney/newarrivals/detail/${id}`,
      state: path
    })
  }, [history]);
  const handleApplyClick = React.useCallback((id: string, path: string) => {
    return history.push({
      pathname: `/attorney/application/apply/${id}`,
      state: path
    })
  }, [history]);
  const handleManageClick = React.useCallback((id: string, path: string) => {
    return history.push({
      pathname: `/attorney/application/management/${id}`,
      state: path
    })
  }, [history]);

  return { recruit, apply, manage, recruitFunc, applyFunc, manageFunc, handleApplyClick, handleManageClick, handleRecruitClick };
}
