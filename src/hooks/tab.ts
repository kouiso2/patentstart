import React from "react";

export default function useTab() {
  const [tab, setTab] = React.useState<boolean>(false);

  return { tab, setTab };
}
