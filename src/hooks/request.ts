import firebase from 'firebase';
import 'firebase/auth';
import 'firebase/firestore';
import React from 'react';

interface OfferModel {
  price: number;
  remark: string;
  attorney: string;
  application: string;
}

type StateModel = OfferModel | firebase.firestore.DocumentData;

export default function useRequest(path?: string) {

  const [request, setRequest] = React.useState<StateModel>({
    price: 0,
    remark: "",
    attorney: "",
    application: "",
  });

  React.useEffect(() => {
    if (path){
      firebase.firestore().doc(path).onSnapshot((snap) => {
        setRequest({
          ...request,
          ...snap.data(),
          attorney: snap.data()?.attorney?.path,
          application: snap.data()?.application?.path,
        });
      });
    }
  }, [path, request, setRequest]);

  return {request};
}
