import firebase from 'firebase';
import 'firebase/firestore';
import 'firebase/storage';
import Model from 'models/Patent';
import React from 'react';
import { useHistory } from 'react-router';
import { patentInit } from '../data/initial/index';

interface PatentModel extends Model{
  path: string;
  id: string;
}
type StateModel = PatentModel | firebase.firestore.DocumentData;

export default function usePatent(path: string){
  const history = useHistory();
  const [patent, setPatent] = React.useState<StateModel>(patentInit);
  const docInput = React.useRef(null);

  React.useEffect(() => {
    if (path){
      const db = firebase.firestore().doc(path);
      const storage = firebase.storage();
      db.onSnapshot((snap) => {
        const fillingDate: firebase.firestore.Timestamp = snap.data()?.fillingDate;
        const knownDate: firebase.firestore.Timestamp = snap.data()?.knownDate;
        if (snap){
          setPatent({
            ...snap.data(),
            id: snap.id,
            path: snap.ref.path,
            fillingDate: fillingDate?.toDate(),
            knownDate: knownDate?.toDate(),
            document: snap.data()?.document ? storage.refFromURL(snap.data()?.document) : ""
          });
        }
      });
      }
  }, [path]);

  const handleFileChange = React.useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
    e.preventDefault();
    const file = e.target.files![0];
    const type = file.type;
    const size = file.size;
    const limit = 2000000;
    if (type !== "application/pdf") {
      e.target.value = "";
      alert("pdfの形式でアップロードして下さい");
      return;
    }

    if (size > limit) {
      e.target.value = "";
      alert('PDFは2MB以下で指定してください');
      return;
    };

    setPatent({
      ...patent,
      document: file
    })
  }, [patent, setPatent]);

  const handleSubmit = React.useCallback((e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    return history.push({
      pathname: '/attorney/application/check/',
      state: patent.path
    });
  }, [history, patent.path]);

  return {patent, setPatent, docInput, handleFileChange, handleSubmit};
}
