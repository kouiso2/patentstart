/* global $ */

$(function () {
  $('.accordion').each(function (i) {
    //各アコーディオンに固有のクラス名をつける
    $(this).addClass('js-accordion-number' + (i + 1));
  });
  //アコーディオンの一番上だけ開いた状態
  $('.js-accordion-number1 .accordion__contents').css('display', 'block');
  //アコーディオンの一番上の矢印を開いた状態に
  $(this).find('.js-accordion-number1 .accordion__header__arrow').addClass('accordion__header__arrow--active');

  //アコーディオン
  $('.js__accordion__header').on('click', function () {
    //矢印
    $(this).find('.accordion__header__arrow').addClass('accordion__header__arrow--active');
    //コンテンツ
    $(this).siblings('.accordion__contents').slideDown();
  });



  //チャット SP時のハンバーガーメニュー設定
  // $('.MessageInput__inner__hamburger').on('click', function () {
  //   $(this).find('.hamburger--open').fadeToggle('fast');
  //   $(this).find('.hamburger--close').fadeToggle('fast');
  //   $(this).parent().siblings('.MessageInputSP__menu').slideToggle(200);
  //   $(this).parent().siblings('.MessageInputSP__menu').toggleClass('sp__hamburger--active');
  // });
  $(document).on('click', function (e) {
    if (!$(e.target).closest('.MessageInput__inner__hamburger').length) {
      // ターゲット要素の外側をクリックした時の操
      if ($('.MessageInputSP__menu').hasClass('sp__hamburger--active')) {
        $('.MessageInputSP__menu').slideUp(200);
        $('.MessageInputSP__menu').removeClass('sp__hamburger--active');
        $('.hamburger--open').fadeToggle('fast');
        $('.hamburger--close').fadeToggle('fast');
      }
    } else {
      // ターゲット要素をクリックした時の操作
    }
  });


  //   //ArrivalsListChild 「特許名」下の文章が長かったら「・・・もっと見る」を追加

  var $setElm = $('.arrivalsList__child--js .arrivalsList__child__desc--js');
  var cutFigure = '60'; // カットする文字数
  var afterTxt = '…' + '　' + '<span class="read__more">もっと見る</span>'; // 文字カット後に表示するテキスト

  $setElm.each(function () {
    var textLength = $(this).text().length;
    var textTrim = $(this).text().substr(0, (cutFigure))

    if (cutFigure < textLength) {
      $('.arrivalsList__child__desc .read__more').css({ 'display': 'inline-block' });
      $(this).html(textTrim + afterTxt).css({ visibility: 'visible' });
    } else if (cutFigure >= textLength) {
      $(this).css({ visibility: 'visible' });
    }
  });

  //ArrivalsList.tsx 右側の「並び順」が選んだ項目の文字数に応じて幅が可変する
  $('.order__select--js').change(function () {
    var optionLength = $(this).find('option:selected').text().length;
    var optionWidth = optionLength * 12;
    $(this).css('width', optionWidth + 'px');
  })

});
