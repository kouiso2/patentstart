/* global $ */

$(function () {

  //モーダル設定
  $(document).on('click', function (e) {
    if ($('.modal__wrap__all').hasClass('modal__active')) {
      if (!$(e.target).closest('.modal-content-inner').length) {
        // ターゲット要素の外側をクリックした時の操作
        $('.modal__wrap__all').fadeToggle();
        $('.modal__wrap__all').toggleClass('modal__active');
        // $('.modal__wrap__all').removeClass('modal__active');
      } else if ($(e.target).closest('.modal__close').length) {
        // modal__closeをクリックした時の操作
        $('.modal__wrap__all').fadeToggle();
        $('.modal__wrap__all').toggleClass('modal__active');
      } else {

      }
    } else {
      if (!$(e.target).closest('.modal__trigger').length) {
        // ターゲット要素の外側をクリックした時の操作
      } else {
        // ターゲット要素をクリックした時の操作
        $('.modal__wrap__all').fadeToggle();
        $('.modal__wrap__all').toggleClass('modal__active');
      }
    }
  });
});
