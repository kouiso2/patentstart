//test
import ApplicantContact from "pages/Applicant/Contact";
import ContactReportComplete from "pages/Applicant/ContactReportComplete";
import ApplicantReport from "pages/Applicant/Report";
// common
////Account
import AttorneyAccountPageList from "pages/Attorney/Account/AccountPageList";
import BankAccountCheck from "pages/Attorney/Account/Bank/Check";
import BankAccountComplete from "pages/Attorney/Account/Bank/Complete";
import BankAccount from "pages/Attorney/Account/Bank/Index";
import BankAccountChange from "pages/Attorney/Account/Bank/Update";
import Proceeds from "pages/Attorney/Account/Proceeds";
//Attorney
////Account
import AttorneyProfile from "pages/Attorney/Account/Profile/Index";
import AttorneyProfileChange from "pages/Attorney/Account/Profile/Update";
////Application
import ApplicationList from "pages/Attorney/Home";
////NewArrivals
import ArrivalsComplete from "pages/Attorney/NewArrivals/Complete";
import ArrivalsDetail from "pages/Attorney/NewArrivals/Detail";
import ArrivalsList from "pages/Attorney/NewArrivals/Index";
////Chat
import AttorneyOffer from "pages/Attorney/Offer/Index";
import ApplicationInfoCheck from "pages/Attorney/Patent/Check";
import ApplicationInfoChange from "pages/Attorney/Patent/Update";
import Login from "pages/Auth/Login";
import Applying from "pages/Common/Application/Applying";
import Management from "pages/Common/Application/Management";
import Chat from "pages/Common/Chat/Index";
////Chat
import ChatList from "pages/Common/Chat/List";
import SaveFolder from "pages/Common/Chat/SaveFolder";
import PwChangeComplete from "pages/Common/PwChange/Complete";
import { PasswordUpdate } from "pages/Common/PwChange/Index";
import IdentificationComplete from "pages/Identification/Complete";
import Identification from "pages/Identification/Index";
import Reject from 'pages/Identification/Reject';
import allRouteProps from 'routes/routes';

//Auth→ログインしていない状態のページ
//Admin→システム管理者のページ
//Applicant→申請者
//Attorney→弁護士
//common→申請者・弁理士共通のコンポーネント

// const themeType = ["/auth", "/admin", "/attorney", "/applicant"];

export const routes: allRouteProps[] = [
   {
    path: "/",
    component: Login,
  },
  // 本人確認してない場合のページ
  {
    path: "/reject",
    component: Reject,
  },

  // 29
  {
    path: "/home",
    component: ApplicationList,
  },
  {
    collapse: true,
    path: "/application",
    views: [
      // 30
      {

        path: "/apply/:id",
        component: Applying,
      },
      // 33
      {

        path: "/management/:id",
        component: Management,
      },
      // 31
      {

        path: "/update/:id",
        component: ApplicationInfoChange,
      },
      // 32
      {

        path: "/check",
        component: ApplicationInfoCheck,
      },
    ]
  },

  // 35
  {
    collapse: true,
    path: "/chat",
    views: [
      {
        path: "/",
        component: ChatList,
      },
      {
        path: "/:id",
        component: Chat,
      },
    ]
  },
  // アルバム
  {
    path: "/album",
    collapse: true,
    views: [
      {
        //トークルームのid
        path: "/:id",
        component: SaveFolder,
      },
    ]
  },

  // 39
  {
    path: "/offer",
    collapse: true,
    views: [
      {
        //トークルームのid
        path: "/:id",
        component: AttorneyOffer,
      },
    ]
  },


  // 44
  {
    collapse: true,
    path: "/newarrivals",
    views: [
      // 46
      {
        path: "/",
        component: ArrivalsList,
      },
      // 46
      {
        path: "/detail/:id",
        component: ArrivalsDetail,
      },
      //39
      {
        path: "/offer/:id",
        component: AttorneyOffer,
      },
      // 47
      {
        path: "/complete",
        component: ArrivalsComplete,
      },
    ]
  },
  // 48
  {
    path: "/account",
    component: AttorneyAccountPageList,
  },
  {
    collapse: true,
    path: "/profile",
    views: [
      // 49
      {
        path: "/",
        component: AttorneyProfile,
      },
      // 50
      {
        path: "/update",
        component: AttorneyProfileChange,
      },
      // 51

    ]
  },
  {
    collapse: true,
    path: "/identification",
    views: [
      {
        //52
        path: "/",
        component: Identification,
      },
      // 53
      {
        path: "/complete",
        component: IdentificationComplete,
      },
    ]
  },
  {
    path: "/pwchange",
    collapse: true,
    views: [
      // 54
      {
        path: "/",
        component: PasswordUpdate
      },
      {
        //55
        path: "/complete",
        component: PwChangeComplete
      }
    ],
  },
  // 56
  {

    path: "/proceeds",
    component: Proceeds,
  },
  {
    collapse: true,
    path: "/bank",
    views: [

      // 58, 59
      {
        path: "/",
        component: BankAccount,
      },
      // 60
      {
        path: "/update",
        component: BankAccountChange,
      },
      // 61
      {
        path: "/check",
        component: BankAccountCheck,
      },
      // 62
      {
        path: "/complete",
        component: BankAccountComplete,
      },
    ]
  },
  // 100
  {
    path: "/report",
    collapse: true,
    views: [
      {
        // 100
        path: "/:id",
        component: ApplicantReport
      },
      {
        // 完了ページ
        path: "/complete",
        component: ContactReportComplete
      }
    ],
  },
  // 101
  {
    path: "/contact",
    collapse: true,
    views: [
      {
        path: "/",
        component: ApplicantContact,
      },
      {
        path: "/complete",
        component: ContactReportComplete,
      },
    ]
  },
];

export default routes;
