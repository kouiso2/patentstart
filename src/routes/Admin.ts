//test
import ApplicantProfile from "pages/Admin/ApplicantProfile";
import AttorneyProfile from "pages/Admin/AttorneyProfile";
import ContactList from "pages/Admin/ContactList";
import ReportList from "pages/Admin/ReportList";
// Admin
import UserListApplicant from "pages/Admin/UserListApplicant";
import UserListAttorney from "pages/Admin/UserListAttorney";
import allRouteProps from 'routes/routes';



//Auth→ログインしていない状態のページ
//Admin→システム管理者のページ
//Applicant→申請者
//Attorney→弁護士
//common→申請者・弁理士共通のコンポーネント

// const themeType = ["/auth", "/admin", "/attorney", "/applicant"];

export const routes: allRouteProps[] = [


  {
    path: "/applicant-list",
    component: UserListApplicant,
  },
  {
    path: "/attorney-list",
    component: UserListAttorney,
  },
  {
    path: "/contact-list",
    component: ContactList,
  },
  {
    path: "/report-list",
    component: ReportList,
  },
  {
    path: "/applicant",
    collapse: true,
    views: [
      {
        path: "/profile",
        component: ApplicantProfile,
      },
    ],
  },
  {
    path: "/attorney",
    collapse: true,
    views: [
      {
        path: "/profile",
        component: AttorneyProfile,
      },
    ],
  },
];

export default routes;
