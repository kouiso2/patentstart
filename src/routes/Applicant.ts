//test
////Account
import ApplicantAccountPageList from "pages/Applicant/Account/AccountPageList";
import ApplicantProfile from 'pages/Applicant/Account/Profile/Index';
//Applicant
////Account
import ProfileChange from "pages/Applicant/Account/Profile/Update";
import ApplicationCreateCheck from "pages/Applicant/Application/Check";
////Application
import ApplicationCreate from "pages/Applicant/Application/Create";
import Recruitment from "pages/Applicant/Application/Recruitment";
import ApplicantContact from "pages/Applicant/Contact";
import ContactReportComplete from "pages/Applicant/ContactReportComplete";
////Application
import ApplicationList from "pages/Applicant/Home";
import ApplicantReport from "pages/Applicant/Report";
import PatentCheck from "pages/Applicant/Request/Check";
import PatentComplete from "pages/Applicant/Request/Complete";
//// Chat
import ApplicantOffer from "pages/Applicant/Request/Index";
import PatentEndCheck from "pages/Applicant/Request/PatentEndCheck";
import PatentEndComplete from "pages/Applicant/Request/PatentEndComplete";
import Login from "pages/Auth/Login";
import Applying from "pages/Common/Application/Applying";
import Management from "pages/Common/Application/Management";
import Chat from "pages/Common/Chat/Index";
////Chat
import ChatList from "pages/Common/Chat/List";
import SaveFolder from "pages/Common/Chat/SaveFolder";
// common
import PwChangeComplete from "pages/Common/PwChange/Complete";
import { PasswordUpdate } from "pages/Common/PwChange/Index";
import allRouteProps from 'routes/routes';

//Auth→ログインしていない状態のページ
//Admin→システム管理者のページ
//Applicant→申請者
//Attorney→弁護士
//common→申請者・弁理士共通のコンポーネント

// const themeType = ["/auth", "/admin", "/attorney", "/applicant"];

export const routes: allRouteProps[] = [
  {
    path: "/",
    component: Login,
  },
  // 1
  {
    path: "/home",
    component: ApplicationList,
  },
  // 2
  {
    path: "/recruitment/:id",
    component: Recruitment,
  },
  {
    path: "/application",
    collapse: true,
    views: [
      // 7
      {

        path: "/management/:id",
        component: Management,
      },
      // 9
      {
        path: "/apply/:id",
        component: Applying,
      },
      // 9
      {
        path: "/create",
        component: ApplicationCreate,
      },
      // 10
      {
        path: "/check",
        component: ApplicationCreateCheck,
      },
    ]
  },
  // 11
  {
    path: "/chat",
    collapse: true,
    views: [
      {
        path: "/",
        component: ChatList,
      },
      // 12~15,19,20
      {
        //トークルームのid
        path: "/:id",
        component: Chat,
      },
    ]
  },
  // 22
  {
    path: "/album",
    collapse: true,
    views: [
      {
        //トークルームのid
        path: "/:id",
        component: SaveFolder,
      },
    ]
  },
  // 16
  {
    path: "/offer",
    collapse: true,
    views: [

      //16
      {
        path: "/:id",
        component: ApplicantOffer,
      },

    ]
  },
  // 16
  {
    path: "/request",
    collapse: true,
    views: [

      // 17
      {
        path: "/check",
        component: PatentCheck,
      },
      // 18
      {
        path: "/complete",
        component: PatentComplete,
      },

    ]
  },

  {
    path: "/patentend",
    collapse: true,
    views: [

      // 17
      {
        path: "/check",
        component: PatentEndCheck,
      },
      // 18
      {
        path: "/complete",
        component: PatentEndComplete,
      },

    ]
  },

  // 23
  {
    path: "/account",
    component: ApplicantAccountPageList,
  },
  {
    path: "/profile",
    collapse: true,
    views: [
      // 24
      {
        path: "/",
        component: ApplicantProfile
      },
      //25
      {
        path: "/update",
        component: ProfileChange
      },
    ],
  },
  {
    path: "/pwchange",
    collapse: true,
    views: [
      // 27
      {
        path: "/",
        component: PasswordUpdate
      },
      {
        //28
        path: "/complete",
        component: PwChangeComplete
      }
    ],
  },
  // 100
  {
    path: "/report",
    collapse: true,
    views: [
      {
        // 100
        path: "/:id",
        component: ApplicantReport
      },
      {
        // 完了ページ
        path: "/complete",
        component: ContactReportComplete
      }
    ],
  },
  // 101
  {
    path: "/contact",
    collapse: true,
    views: [
      {
        // 100
        path: "/",
        component: ApplicantContact
      },
      {
        // 完了ページ
        path: "/complete",
        component: ContactReportComplete
      }
    ],
  },
];

export default routes;
