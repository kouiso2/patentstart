//test
import NewRegistrationComplete from "pages/Auth/Complete";
import AuthComplete from 'pages/Auth/Contact';
import Login from "pages/Auth/Login";
//step
import Step1 from "pages/Auth/PwReset/Step1";
import Step2 from "pages/Auth/PwReset/Step2";
import Step3 from "pages/Auth/PwReset/Step3";
import Step4 from "pages/Auth/PwReset/Step4";
import SignupSelect from "pages/Auth/Select";
// import Signup from "pages/Auth/Signup/Index";
import NewRegistrationSelect from 'pages/Auth/Signup';
// Auth
import Top from "pages/Auth/Top";
import allRouteProps from 'routes/routes';


//Auth→ログインしていない状態のページ
//Admin→システム管理者のページ
//Applicant→申請者
//Attorney→弁護士
//common→申請者・弁理士共通のコンポーネント

// const themeType = ["/auth", "/admin", "/attorney", "/applicant"];

export const authRoutes: allRouteProps[] = [

  // 追加
  {
    path: "/contact",
    component: AuthComplete,
  },
  //XD70
  {
    path: "/top",
    component: Top,
  },
  //XD71
  {
    path: "/login",
    component: Login,
  },
  {
    path: "/signup",
    collapse: true,
    views: [
      {
        path: "/",
        component: NewRegistrationSelect,
      },
      //XD77
      {
        path: "/complete",
        component: NewRegistrationComplete,
      },
      {
        path: "/select",
        component: SignupSelect,
      },
      //XD76
    ]
  },
  {
    path: "/pwreset",
    collapse: true,
    views: [
      //XD72
      {
        path: "/first",
        component: Step1,
      },
      {
        path: "/second",
        component: Step2,
      },
      {
        path: "/third",
        component: Step3,
      },
      {
        path: "/fourth",
        component: Step4,
      },
    ]
  }
];

export default authRoutes;
