export interface oneRouteProps {
  path: string;
  component: React.FC;
}

export interface allRouteProps {
  path: string;
  collapse?: boolean;
  component?: React.FC;
  views?: allRouteProps[];
}

export default allRouteProps;
