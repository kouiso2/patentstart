import 'assets/css/ArrivalsList.css';
// css
import 'assets/css/Home.css';
import 'assets/css/style.css';
import 'firebase/firestore';
import useMatter from 'hooks/matter';
import React from 'react';
import { patentCategoryJpn } from 'translate';
import { AppFireTypes } from '../pages/Applicant/Home';
interface Props {
  value: AppFireTypes;
  key: number | undefined
}

// commonComponent
export const AllArrivalsListChild: React.FC<Props> = ({ value }, { key }) => {
  const {
    handleClick,
  } = useMatter();
  return (
    <article className="arrivalsList__child arrivalsList__child--js" key={key} data-id={value.id} data-path={value.path} onClick={handleClick}>
      <div className="arrivalsList__child__top">
        <div className="arrivalsList__child__info">
          <time className="child__info info--time"></time>
          {/* <span className="child__info info--register">登録A</span> */}
          <span className="child__info info--cat">{patentCategoryJpn(value.category)}</span>
        </div>
        <h5 className="arrivalsList__child__name">{value.title}</h5>
        <p className="arrivalsList__child__desc arrivalsList__child__desc--js">{value.explanation}</p>
      </div>
      <div className="arrivalsList__child__bottom">
        <h6 className="arrivalsList__child__bottom__text">
          <span className="arrivalsList__child__bottom__text--number">
            {value.recruit?.length}
          </span>人の弁理士が応募しています
    </h6>
        <span></span>
      </div>
    </article>
  )
};

export const MatchArrivalsListChild: React.FC<Props> = ({ value }, { key }) => {
  const {
    handleClick,
  } = useMatter();
  return (
    <article className="arrivalsList__child arrivalsList__child--js" key={key} data-id={value.id} data-path={value.path} onClick={handleClick}>
      <div className="arrivalsList__child__top">
        <div className="arrivalsList__child__info">
          <time className="child__info info--time"></time>
          {/* <span className="child__info info--register">登録B</span> */}
          <span className="child__info info--cat">{patentCategoryJpn(value.category)}</span>
        </div>
        <h5 className="arrivalsList__child__name">{value.title}</h5>
        <p className="arrivalsList__child__desc arrivalsList__child__desc--js">{value.explanation}</p>
      </div>
      <div className="arrivalsList__child__bottom">
        <h6 className="arrivalsList__child__bottom__text">
          <span className="arrivalsList__child__bottom__text--number">
            {value.recruit?.length}
          </span>人の弁理士が応募しています
              </h6>
        <span></span>
      </div>
    </article>
  )
}

export const NoneArrivalsListChild: React.FC = () => {
  return (
    <div className="arrivalsList--noResult">
      現在はありません
    </div>
  )
}

export default AllArrivalsListChild;
