import { IonRouterLink } from '@ionic/react';
import 'assets/css/ExploreContainer.css';
import React from 'react';
import { Link } from 'react-router-dom';


interface ContainerProps {
  name: string;
}

const ExploreContainer: React.FC<ContainerProps> = ({ name }) => {
  return (
    <div className="container">
      <strong>{name}</strong>
      <p>Explore <IonRouterLink target="_blank" rel="noopener noreferrer" href="https://ionicframework.com/docs/components/"><Link to='/auth/login/'>UI Components</Link></IonRouterLink></p>
    </div>
  );
};


export default ExploreContainer;
