//ionic
import React from 'react';
export const SingleTerms: React.FC = () => {
  return (
    <div className="auth__undertext">
      <a href="https://patentstart.jp/terms.html" className="auth__undertext__link">ご利用規約</a>
      <a href="https://patentstart.jp/privacy.html" className="auth__undertext__link">プライバシーポリシー</a>
    </div>
  );
}

export const MultiTerms: React.FC = () => {
  return (
    <React.StrictMode>
      <div className="auth__undertext--select pc">

        <a href="https://patentstart.jp/terms.html" className="auth__undertext__link">ご利用規約</a>
        <a href="https://patentstart.jp/privacy.html" className="auth__undertext__link">プライバシーポリシー</a>
      </div>
      <div className="auth__undertext--select sp">
        <a href="https://patentstart.jp/terms.html" className="auth__undertext__link">ご利用規約</a>
        <a href="https://patentstart.jp/privacy.html" className="auth__undertext__link">プライバシーポリシー</a>
      </div>
    </React.StrictMode>
  )
}

export default SingleTerms;
