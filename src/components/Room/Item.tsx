//css
import 'assets/css/Chat.css';
import ProfileNoImage from 'assets/img/noimage.svg';
import ChatListProf from 'assets/img/nowloading.svg';
import { statusChoice } from 'data/choice';
//firebase
import firebase from 'firebase';
import 'firebase/firestore';
import useAuth from 'hooks/auth';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { RootState } from 'store/Combine';
import { applicationConfigAction, attorneyConfigAction, roomConfigAction } from 'store/Config/action';


type Room = {
  id: string;
  application: string;
  applicantId: string;
  applicantUnreadNumber: number;
  attorneyId: string;
  attorneyUnreadNumber: number;
  patentStatus: typeof statusChoice[number];
  lastMessageText: string;
  timestamp: any;
};
interface Props {
  room: Room;
}

/**
* RoomItem:
* RoomListコンポーネント内から呼び出される。RoomListが複数のトークルームを表示する役割を持つのに対して、RoomItemは1つ1つのトークルームを表示している
* roomはRoomListコンポーネントで渡されるprops。RoomList内で自分に関係のあるチャットルームの情報を複数取得した後、RoomItemに1つずつ情報を渡している
*/
const RoomItem: React.FC<Props> = ({ room }) => {
  const history = useHistory();
  const auth = useAuth();
  const [user, setUser] = React.useState({ id: "", personName: "", path: "" });
  const [profImage, setProfImage] = React.useState<string>("");
  const [unreadNumber, setUnreadNumber] = React.useState<number>(0);
  const status = room.patentStatus;
  const dispatch = useDispatch();
  const config = useSelector((state: RootState) => state.config);

  /**
  * React.useEffect:
  * Cloud Firestoreから条件を指定してデータを取得。ここではログインユーザーが申請者(Applicant)か弁理士(Attorney)かによって条件分岐して取得
  * firestoreから取得するのは、チャットルームのトーク相手のプロフィール画像,トーク相手のユーザ情報,自分の未読数
  * 上記のそれぞれをsetProfImage,setUser,setUnreadNumber関数を使うことでstate更新
  */
  React.useEffect(() => {
    const db = firebase.firestore();
    if (auth?.kind === "Applicant") {
      db.collection("User")
        .doc(room.attorneyId)
        .get()
        .then((doc) => {
          doc.data()?.image
            ? setProfImage(doc.data()?.image)
            : setProfImage(ProfileNoImage);
          setUser({
            id: doc.id,
            personName: doc.data()?.personName,
            path: doc.ref.path,
          });
        })
        .catch((error) => { });

      db.collection("Rooms")
        .doc(room.id)
        .onSnapshot((doc) => {
          setUnreadNumber(doc.data()?.applicantUnreadNumber);
        });
    } else if (auth?.kind === "Attorney") {
      db.collection("User")
        .doc(room.applicantId)
        .get()
        .then((doc) => {
          //プロフィール画像があったらそちらをセット。ない場合はデフォルトの画像をセット。
          doc.data()?.image
            ? setProfImage(doc.data()?.image)
            : setProfImage(ChatListProf);
          setUser({
            id: doc.id,
            personName: doc.data()?.personName,
            path: doc.ref.path,
          });
        })
        .catch((error) => { });

      db.collection("Rooms")
        .doc(room.id)
        .onSnapshot((doc) => {
          setUnreadNumber(doc.data()?.attorneyUnreadNumber);
        });
    }
  }, [
    auth?.kind,
    setProfImage,
    setUser,
    room.applicantId,
    room.attorneyId,
    room.id,
  ]);

  const StatusBadge = React.useMemo(() => {
    if (status === "Recruit") {
      return (
        <span className="chatlist__child__desc__top--status status--recruiting">
          募集中
        </span>
      );
    } else if (status === "Manage") {
      return (
        <span className="chatlist__child__desc__top--status status--managing">
          管理中
        </span>
      );
    } else if (status === "Apply") {
      return (
        <span className="chatlist__child__desc__top--status status--applying">
          申請中
        </span>
      );
    } else {
      return;
    }
  }, [status]);

  /**
  * return(戻り値):
  * 1つのトークルームを表示。
  * 具体的に表示しているのは、トーク相手のプロフィール画像,トーク相手の名前,ステータスバッジ(募集中・管理中・申請中),トークルームの最新のメッセージ,未読数
  */
  return (
    <div className="chatlist__child" style={{ marginBottom: "1rem" }} onClick={() => {
      dispatch(roomConfigAction(
        {
          ...config.room,
          id: room.id,
        }
      ));
      dispatch(applicationConfigAction({
        ...config.application,
        path: room.application
      }));
      dispatch(attorneyConfigAction({
        ...config.attorney,
        path: user.path
      }));
      history.push({
        pathname: `${auth?.url + "/chat/" + room.id}`,
      });
    }}>
      <figure className="chatlist__child__thum" style={{ backgroundImage: `url(${profImage})` }}></figure>
      <article className="chatlist__child__desc">
        <div className="chatlist__child__desc__top">
          <h4 className="chatlist__child__desc__top--name">
            {user.personName}
          </h4>
          {StatusBadge}
          {/*
            ]status--recruiting →募集中
            status--managing →管理中
            status--applying →申請中
            */}
        </div>
        {/* <p className="chatlist__child__desc--message">最新の弁理士からのメッセージが表示されます最新の弁理士からのメッセージが</p> */}
        <p className="chatlist__boxRead">{room.lastMessageText}</p>
      </article>
      {/* メッセージ件数が0の場合は数字が表示されないようにする。 */}
      {(unreadNumber !== 0) ? <span className="chatlist__child--notification">{unreadNumber}</span> : <span></span>}
    </div>
  );
};

export default RoomItem;
