import 'assets/css/Chat.css';
//firebase
import firebase from 'firebase';
import useAuth from 'hooks/auth';
import React from 'react';
import RoomItem from './Item';

export type Room = {
  id: string,
  applicantId: string,
  attorneyId: string,
  patentStatus: string,
  lastMessageText: string,
  timestamp: any,
  application: string;
}

/**
* RoomList:
* Cloud Firestoreから取得した複数のチャットルームを表示する。
*/
const RoomList: React.FC = () => {
  const auth = useAuth();

  const [rooms, setRooms] = React.useState<Room[]>([]);

  const roomItems = React.useMemo(() => {
    return rooms.map((room: any, index: any) => {
      const roomDoc = room;
      return (
        <RoomItem
          room={roomDoc}
          // エラー回避用のkey
          key={index}
        />
      );
    })
  }, [rooms]);

  /**
  * React.useEffect:
  * Cloud Firestoreから条件を指定してデータを取得。ここでは自分のユーザIDを元に、自分に関係するトークルームの情報を一気に取得している
  * firestoreから取得したクエリを元に、RoomListコンポーネントの冒頭で宣言したroomsをsetRooms関数で更新
  */
  React.useEffect(() => {
    if (auth?.uid) {
      const db = firebase.firestore();
      const roomsRef = db.collection("Rooms");
      let query: any;


      if (auth?.kind === "Applicant") {
        query = roomsRef.where('applicantId', '==', auth?.uid);
      } else if (auth?.kind === "Attorney") {
        query = roomsRef.where('attorneyId', '==', auth?.uid);
      }
      // let query = roomsRef.where('applicantId', '==', auth?.uid).orderBy('lastMessageTimestamp', 'desc');
      // setQuery(roomsRef.where('applicantId', '==', auth?.uid).orderBy('lastMessageTimestamp', 'desc'));

      query.onSnapshot((snap: firebase.firestore.QuerySnapshot) => {
        setRooms(
          snap.docs.map((doc) => ({
            application: doc.data()?.application?.path,
            id: doc.id,
            applicantId: doc.data().applicantId,
            applicantUnreadNumber: doc.data().applicantUnreadNumber,
            attorneyId: doc.data().attorneyId,
            attorneyUnreadNumber: doc.data().attorneyUnreadNumber,
            patentStatus: doc.data().patentStatus,
            lastMessageText: doc.data().lastMessageText,
            timestamp: doc.data().timestamp
          }))
        );

        snap.docChanges().forEach(async function (change: any) {
          if (change.type === 'modified') {

            if (auth?.kind === "Applicant") {
              query = roomsRef.where('applicantId', '==', auth?.uid);
            } else if (auth?.kind === "Attorney") {
              query = roomsRef.where('attorneyId', '==', auth?.uid);
            }

          }
        });
      });

    }
  }, [auth?.uid, auth?.kind]);

  /**
  * return(戻り値):
  * return内のroomItemsの部分で、firestoreから取得した複数のトークルームを表示している
  * roomItemsについては、RoomListコンポーネントの最初の方にあるReact.useMemoを参照
  */
  return (
    <div className="chatlist__wrap">
      <h3 className="chatlist__wrap__title pc">チャット一覧</h3>
      {roomItems}
    </div>
  );
};

export default RoomList;
