//react
import { IonItem, IonRouterLink, IonThumbnail, isPlatform } from '@ionic/react';
//css
import 'assets/css/Chat.css';
import Excel from 'assets/img/ms-excel-icon.svg';
import PowerPoint from 'assets/img/ms-powerpoint-icon.svg';
import Word from 'assets/img/ms-word-icon.svg';
//img
import PDF from 'assets/img/pdf-icon.png';
import { UserPathContext } from 'data/context';
//firebase
import firebase from 'firebase';
import "firebase/storage";
import useAuth from 'hooks/auth';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import { RootState } from 'store/Combine';
import { modalConfigAction, pathConfigAction } from 'store/Config/action';
import { userChoice } from '../../data/choice/index';

type Message = {
  id: string,
  senderId: string,
  state: string,
  filePath: string,
  text: string,
  timestamp: any,
}
type Props = {
  isLastItem: boolean,
  message: Message,
  roomId: string,
  lastOfferTimestamp: any,
  profImage: string,
}
document.addEventListener("DOMContentLoaded", function () {
  document.body.addEventListener('click', function () {
    // const elm = (document.querySelector(".normalImageMenu") as HTMLElement);
    // elm.style.display="none";
    const elms = document.querySelectorAll(".normalImageMenu");
    for (let i = 0; i < elms.length; i++) {
      const elm = elms[i] as HTMLElement;
      elm.style.display = "none";
    }
  });
});

interface RequestButtonModel {
  kind: typeof userChoice[number];
  id: string;
  offerPath: firebase.firestore.DocumentReference<firebase.firestore.DocumentData>;
}

const RequestButton = React.memo((props: RequestButtonModel) => {
  const { kind, id, offerPath } = props;
  if (kind === "Applicant") {
    return (
      <Link to={{
        pathname: `/applicant/offer/${id}`,
        state: offerPath?.path
      }}><button className="offer__button">依頼する</button></Link>
    );
  } else {
    return (
      <span></span>
    )
  }
})

/**
* MessageItem:
* MessageListコンポーネント内から呼び出される。MessageListが複数のメッセージを表示する役割を持つのに対して、MessageItemは1つ1つのメッセージを表示している
* MessageListコンポーネントから渡されるpropsは以下の通り
* message:トークルームに表示するメッセージの情報を保持している
* roomId:トークルームのID
* isLastItem:一番最後のメッセージの場合にtrueになる変数
* lastOfferTimestamp:弁理士のオファーメッセージで最新のもののタイムスタンプ
* profImage:トークルームの相手のプロフィール画像
*/
const MessageItem: React.FC<Props> = ({ message, roomId, isLastItem, lastOfferTimestamp, profImage }) => {
  const auth = useAuth();
  const [file, setFile] = React.useState("");
  const dispatch = useDispatch();
  const config = useSelector((state: RootState) => state.config);
  const { id } = useParams<{ id: string }>();
  const [offerPath, setOfferPath] = React.useState<firebase.firestore.DocumentReference<firebase.firestore.DocumentData> | null>();
  const path = React.useContext(UserPathContext);

  const messageId = message.id;
  const senderId = message.senderId;
  const filePath = message.filePath;
  const text = message.text;
  const timestamp = message.timestamp;
  let state = message.state;

  /**
  * renderFile():
  * 表示するメッセージのstate(種類)によって、トークルームに表示するJSXを条件分岐で変更してくれる関数
  * stateの内容は、normalFile(通常の画像),albumFile(アルバムに保存されている画像),video(動画),pdf(PDFファイル),word(Wordファイル),excel(Excelファイル),powerpoint(PowerPointファイル)
  */
  function renderFile() {
    if (state === "normalFile") {
      return (
        <img className="imgNormalFile chat__img" src={`${file}`} width={200}
          alt=""

          onClick={(e) => {
            e.preventDefault();
            dispatch(modalConfigAction({
              ...config.modal,
              isOpen: true,
              form: "Chat",
              id: roomId,
              idSub: messageId,
              imgUrl: file,
            }));
          }}
        />
      );
    } else if (state === "albumFile") {
      return (
        <img src={`${file}`} alt="" width={200} className="imgNormalFile chat__img"
        />
      );
    } else if (state === "video") {
      return (
        <video src={`${file}`} width={200} controls muted className={`imgNormalFile chat__img ${messageStyle()}`} style={{ marginRight: "0" }}
        ></video>
      );

    } else if (state === "pdf") {
      return (

        <div className="chat-bubble">
          <img src={PDF} alt="" />
          <IonRouterLink href={`${file}/`} target="_blank" rel="noopener noreferrer">{text}</IonRouterLink>
        </div>

      );
    } else if (state === "word") {
      return (

        <div className="chat-bubble">
          <img src={Word} alt="" />
          <IonRouterLink href={`${file}/`} target="_blank" rel="noopener noreferrer">{text}</IonRouterLink>
        </div>


      );
    } else if (state === "excel") {
      return (

        <div className="chat-bubble">
          <img src={Excel} alt="" />
          <IonRouterLink href={`${file}/`} target="_blank" rel="noopener noreferrer">{text}</IonRouterLink>
        </div>


      );
    } else if (state === "powerpoint") {
      return (


        <div className="chat-bubble">
          <img src={PowerPoint} alt="" />
          <IonRouterLink href={`${file}/`} target="_blank" rel="noopener noreferrer">{text}</IonRouterLink>
        </div>
      );
    }
  }

  /**
  * messageStyle():
  * 自分のメッセージの場合は右に吹き出しを表示。相手のメッセージの場合は左に表示。
  */
  function messageStyle() {
    let user = firebase.auth().currentUser;
    if (user) {
      if (user.uid === senderId) {
        return "Message__item--me";
      } else {
        return "Message__item--you"
      }
    }
  }

  // const ref = React.useRef<null | HTMLIonItemElement>(null);

  /**
  * React.useEffect():
  * メッセージがファイル形式の時に、トークルームに表示するファイルを実際にFirebase Storageからダウンロードしてくる
  * ダウンロードが成功したら、MessageItemコンポーネントの冒頭で宣言したfileをsetFile関数で更新
  */
  React.useEffect(() => {

    if (state === "normalFile" || state === "albumFile" || state === "video" || state === "pdf" || state === "word" || state === "excel" || state === "powerpoint") {
      var storageRef = firebase.storage().ref();

      storageRef.child(filePath).getDownloadURL().then(function (url) {
        setFile(url);
      }).catch(function (error) {

      });
    }

  }, [setFile, state, filePath, isLastItem]);


  React.useEffect(() => {
    firebase.firestore().collection("Rooms").doc(roomId).onSnapshot((snap) => {
      if (snap.data()?.offerPath) {
        setOfferPath(snap.data()?.offerPath);
      }
    })
  }, [setOfferPath, roomId]);

  /**
  * return(戻り値):
  * 表示するメッセージの種類によって条件分岐
  * メッセージの種類(state)はtext(通常のテキストメッセージ),offer(弁理士のオファーメッセージ),それ以外(ファイル)に分けられる
  */
  if (state === "text") {//メッセージが通常のテキストの場合
    return (
      <IonItem className={`Message__item ${messageStyle()}`} id={messageId} lines="none">
        {/*
        Message__item 基本のスタイル←これは外さない
        Message__item--you chat-bubbleの左にプロフと左側に矢印を表示
        Message__item--me chat-bubbleを右寄せ 右側の矢印はなし
        me--arrow hcat-bubble右寄せの時に矢印を右側に表示
        Message__item--offer オファー時の赤色のスタイル
        Message__item--canceled オファー時の灰色のスタイル

         */}

        <IonThumbnail className="Message__item__thum">
          <figure style={{
            backgroundImage: `url(${profImage})`
          }}
            onClick={() => {
              dispatch(pathConfigAction(path));
              dispatch(modalConfigAction({
                ...config.modal,
                isOpen: true,
                id: id,
                form: "OpponentProfile",
                imgUrl: profImage
              }));
            }}
          ></figure>
        </IonThumbnail>

        <p className="chat-bubble">{text}</p>
        {/* <figure style={{ backgroundImage: `url(${PDF})`, width: "30px", height: "30px" }}></figure> */}
      </IonItem>
    );
  } else if (state === "offer" && lastOfferTimestamp?.toDate().toString() === timestamp?.toDate().toString()) {//メッセージが弁理士のオファーメッセージの場合
    return (
      <IonItem className={`Message__item Message__item--offer ${messageStyle()}`} id={messageId} lines="none">
        <IonThumbnail className="Message__item__thum"
          onClick={() => {
            dispatch(pathConfigAction(path));
            dispatch(modalConfigAction({
              ...config.modal,
              isOpen: true,
              id: id,
              form: "OpponentProfile",
              imgUrl: profImage
            }));
          }}
        >
          <figure style={{
            backgroundImage: `url(${profImage})`
          }}></figure>
        </IonThumbnail>



        {
          <p className="chat-bubble">
            {/* 弁理士太郎様より<br className="sp" /> */}
            <span className="offer__plan">{text}</span>プランのオファーが来ました。<br />
            <RequestButton kind={auth?.kind} offerPath={offerPath!} id={id} />
          </p>
        }
        {/* <figure style={{ backgroundImage: `url(${PDF})`, width: "30px", height: "30px" }}></figure> */}
      </IonItem>
    );
  } else if (state === "offer" && lastOfferTimestamp?.toDate().toString() !== timestamp?.toDate().toString()) {//メッセージが弁理士のオファーメッセージの場合
    return (
      <IonItem className={`Message__item Message__item--canceled ${messageStyle()}`} id={messageId} lines="none">
        <IonThumbnail className="Message__item__thum"
          onClick={() => {
            dispatch(pathConfigAction(path));
            dispatch(modalConfigAction({
              ...config.modal,
              isOpen: true,
              id: id,
              form: "OpponentProfile",
              imgUrl: profImage
            }));
          }}
        >
          <figure style={{
            backgroundImage: `url(${profImage})`
          }}></figure>
        </IonThumbnail>

        {
          <p className="chat-bubble">
            {/* 弁理士太郎様より<br className="sp" /> */}
            <span className="offer__plan">{text}</span>プランのオファーが来ました。<br />
            {(auth?.kind === 'Applicant' && !(isPlatform("ios") || isPlatform("android"))) ? <button className="offer__button" disabled>依頼する</button> : <span></span>}</p>
        }
        {/* <figure style={{ backgroundImage: `url(${PDF})`, width: "30px", height: "30px" }}></figure> */}
      </IonItem>
    );
  }else if (state === "complete") {//メッセージが申請完了のメッセージの場合
    return (
      <IonItem className={`Message__item Message__item--offer ${messageStyle()}`} id={messageId} lines="none">
      <IonThumbnail className="Message__item__thum"
        onClick={() => {
          dispatch(pathConfigAction(path));
          dispatch(modalConfigAction({
            ...config.modal,
            isOpen: true,
            id: id,
            form: "OpponentProfile",
            imgUrl: profImage
          }));
        }}
      >
        <figure style={{
          backgroundImage: `url(${profImage})`
        }}></figure>
      </IonThumbnail>
    
      {
        <p className="chat-bubble">
          {/* 弁理士太郎様より<br className="sp" /> */}
          <span className="offer__plan">{text}</span><br />
          {
            (auth?.kind === 'Applicant') ? 
              <Link to="/applicant/patentend/check/"><button className="offer__button">手続きを始める</button></Link> : 
              <button className="offer__button" disabled style={{ backgroundColor: "#ccc", color: "#222" }}>手続きを始める</button>
          }
        </p>
      }
      {/* <figure style={{ backgroundImage: `url(${PDF})`, width: "30px", height: "30px" }}></figure> */}
    </IonItem>
    );
  }
  else {//メッセージがファイルの場合。renderFile関数を使ってファイルを表示する。
    return (
      <IonItem className={`Message__item ${messageStyle()}`} id={messageId} lines="none">
        <IonThumbnail className="Message__item__thum"
          onClick={() => {
            dispatch(pathConfigAction(path));
            dispatch(modalConfigAction({
              ...config.modal,
              isOpen: true,
              id: id,
              form: "OpponentProfile",
              imgUrl: profImage
            }));
          }}
        >
          <figure style={{
            backgroundImage: `url(${profImage})`
          }}></figure>

        </IonThumbnail>
        {renderFile()}
        {/* <figure style={{ backgroundImage: `url(${PDF})`, width: "30px", height: "30px" }}></figure> */}
      </IonItem>
    );
  }
};
export default MessageItem;
