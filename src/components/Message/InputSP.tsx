import { isPlatform } from '@ionic/react';
import { makeStyles } from '@material-ui/core/styles';
import HamburgerClose from 'assets/img/message/chat-hambuger-close.svg';
import HamburgerOpen from 'assets/img/message/chat-hambuger-open.svg';
import MessageMenuButton from 'components/Button/Message/Button';
import MessageCompleteButton from 'components/Button/Message/Complete';
import FileUploadButton from 'components/Button/Message/FileUpload';
import MessageOfferButton from 'components/Button/Message/Offer';
import useApplication from 'hooks/application';
import useAuth from 'hooks/auth';
import React from 'react';
import { useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import { RootState } from 'store/Combine';
import MessageSubmitButton from '../Button/Message/Submit';
// import { gravatarPath } from '../gravatar';
import MessageText from './Text';



const useStyles = makeStyles({
  root: {
    gridRow: 2,
    // margin: '26px',
  },
});

// $(function () {
//   //チャット SP時のハンバーガーメニュー設定
//   $('.MessageInput__inner__hamburger').on('click', function () {
//     $(this).find('.hamburger--open').fadeToggle('fast');
//     $(this).find('.hamburger--close').fadeToggle('fast');
//     $(this).parent().siblings('.MessageInputSP__menu').slideToggle(200);
//     $(this).parent().siblings('.MessageInputSP__menu').toggleClass('sp__hamburger--active');
//   });
//   $(document).on('click', function (e) {
//     if (!$(e.target).closest('.MessageInput__inner__hamburger').length) {
//       // ターゲット要素の外側をクリックした時の操
//       if ($('.MessageInputSP__menu').hasClass('sp__hamburger--active')) {
//         $('.MessageInputSP__menu').slideUp(200);
//         $('.MessageInputSP__menu').removeClass('sp__hamburger--active');
//         $('.hamburger--open').fadeToggle('fast');
//         $('.hamburger--close').fadeToggle('fast');
//       }
//     } else {
//       // ターゲット要素をクリックした時の操作
//     }
//   });
// });

/**
* MessageInput:
* チャットルームのインプットエリアを表示する。(スマホ時)
*/
const MessageInput: React.FC = () => {
  const inputEl = React.useRef(null);
  const [text, setText] = React.useState('');
  const [filePath, setFilePath] = React.useState('');
  const [state, setState] = React.useState('text');
  const [file, setFile] = React.useState();
  const [showHamburgerIcon, setShowHamburgerIcon] = React.useState(true);
  const { id } = useParams<{ id: string }>();
  const classes = useStyles();
  const auth = useAuth();
  const config = useSelector((state: RootState) => state.config);
  const { app } = useApplication(config.application.path);
  const history = useHistory();
  console.log(app);
  const handleAppDetail = React.useCallback(() => {
    if (app.status === "Apply") {
      // 申請中
      console.log("申請中")
      return history.push({
        pathname: `/attorney/application/apply/${app.path.slice(-20)}/`,
        state: app.path
      });
    } else if (app.status === "Recruit") {
      // 応募中
      console.log("応募中")
      return history.push({
        pathname: `/attorney/newarrivals/detail/${app.path.slice(-20)}/`,
        state: app.path
      });
    } else {
      // 管理中
      console.log("管理中")
      return history.push({
        pathname: `/attorney/application/management/${app.path.slice(-20)}/`,
        state: app.path
      });
    }
  }, [app, history]);

  /**
  * return(戻り値):
  * 以下のコンポーネントを表示している
  * MessageText:チャットルームで実際にメッセージを入力するテキストエリア
  * FileUploadButton:ファイルアップロードができるボタン
  * MessageMenuButton:
  * MessageSubmitButton:メッセージを送信できるボタン
  */
  return (
    // <div className={classes.root}>
    <section className={`${classes.root} MessageInput sp`} style={{ paddingBottom: '1.6rem' }}>
      <div className="MessageInput__inner">
        <figure className="MessageInput__inner__hamburger">
          {showHamburgerIcon
            ? <img className="hamburger--open" src={HamburgerOpen} alt="" onClick={() => {
              setShowHamburgerIcon(false);
            }} />
            : <img className="hamburger--close" src={HamburgerClose} alt="" onClick={() => {
              setShowHamburgerIcon(true);
            }} />
          }
        </figure>
        <div className="MessageInput__inner__text">
          <MessageText
            inputEl={inputEl}
            setText={setText}
            text={text}
          />
        </div>

        <div className="MessageInput__inner__button">
          <MessageSubmitButton
            inputEl={inputEl}
            setText={setText}
            text={text}
            filePath={filePath}
            setState={setState}
            state={state}
            file={file}
          />
        </div>
      </div>

      <div className="MessageInputSP__menu" style={{ 'display': (showHamburgerIcon ? 'none' : 'block') }}>
        <div className="MessageInputSP__menu__inner">
          {/* 画像を送る */}
          <FileUploadButton
            setText={setText}
            setFilePath={setFilePath}
            setState={setState}
            setFile={setFile}
          />
          {/* アルバムから送る */}
          <MessageMenuButton roomId={id} />

          {(auth?.kind === "Attorney" && !(isPlatform("ios") || isPlatform("android"))) ? <MessageCompleteButton /> : <React.StrictMode></React.StrictMode>}
          {/* <div className={`MessageInputSP__menu__child ${(auth?.kind === "Applicant" || (isPlatform("ios") || isPlatform("android"))) ? `MessageInputSP__menu__child—-applicant` : ``}`}>
            <figure className="MessageInputSP__menu__child__img"><img src={MessageDocsButtonSP} onClick={handleAppDetail} alt="" />
              <p className="MessageInputSP__menu__child__img__caption">案件詳細</p>
            </figure>
          </div> */}

          {(auth?.kind === "Attorney" && !(isPlatform("ios") || isPlatform("android"))) ? <MessageOfferButton roomId={id} /> : <React.StrictMode></React.StrictMode>}
        </div>
      </div>
    </section>
  );
};

export default MessageInput;
