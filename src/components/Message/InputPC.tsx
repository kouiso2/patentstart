import { makeStyles } from '@material-ui/core/styles';
import MessageMenuButton from 'components/Button/Message/Button';
import CompleteButton from 'components/Button/Message/Complete';
import FileUploadButton from 'components/Button/Message/FileUpload';
import { default as MessageOfferButton } from 'components/Button/Message/Offer';
import MessageSubmitButton from 'components/Button/Message/Submit';
import useAuth from 'hooks/auth';
import React from 'react';
import { useParams } from 'react-router-dom';
import MessageText from './Text';



const useStyles = makeStyles({
  root: {
    gridRow: 2,
    // margin: '26px',
  },

});

/**
* MessageInput:
* チャットルームのインプットエリアを表示する。(PC時)
*/
const MessageInput: React.FC = () => {
  const inputEl = React.useRef(null);
  const [text, setText] = React.useState('');
  const [filePath, setFilePath] = React.useState('');
  const [state, setState] = React.useState('text');
  const [file, setFile] = React.useState();
  const { id } = useParams<{ id: string }>();

  const classes = useStyles();
  const auth = useAuth();

  const OfferBtn = React.useMemo(() => {
    if (auth?.kind === "Attorney") {
      return (
        <MessageOfferButton roomId={id} />
      );
    } else {
      return;
    }
  }, [auth?.kind, id]);

  /**
  * return(戻り値):
  * 以下のコンポーネントを表示している
  * MessageText:チャットルームで実際にメッセージを入力するテキストエリア
  * FileUploadButton:ファイルアップロードができるボタン
  * MessageMenuButton:アルバムページにリンクするボタン(名前が分かりにくい・・・)
  * MessageSubmitButton:メッセージを送信できるボタン
  */
  return (
    // <div className={classes.root}>
    <div className={`${classes.root} MessageInput pc`} >
      <div className="MessageInput__inner">
        <div className="MessageInput__inner__text">
          <MessageText
            // ここのinputをクリックした時にアニメーション(ボーダーとplacceholderの動き)が出るけどキャンセルしたい 川勝
            inputEl={inputEl}
            setText={setText}
            text={text}
          />
        </div>
        <div className="MessageInput__inner__menu">
          {/* <Avatar src={avatarPath} /> */}
          <FileUploadButton
            setText={setText}
            setFilePath={setFilePath}
            setState={setState}
            setFile={setFile}
          />

          <MessageMenuButton roomId={id} />
          {OfferBtn}
          {
            (auth?.kind === 'Attorney') ? 
              <CompleteButton /> : 
              <></>
          }

        </div>

        <div className="MessageInput__inner__button">
          <MessageSubmitButton
            inputEl={inputEl}
            setText={setText}
            text={text}
            filePath={filePath}
            setState={setState}
            state={state}
            file={file}
          />
        </div>


      </div>
    </div>
  );
};

export default MessageInput;
