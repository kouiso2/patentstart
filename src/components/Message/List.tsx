import { IonContent, IonList } from '@ionic/react';
import { makeStyles } from '@material-ui/core/styles';
import ProfileNoImage from 'assets/img/noimage.svg';
//firebase
import firebase from 'firebase';
import 'firebase/firestore';
import useAuth from 'hooks/auth';
import React from 'react';
import { useParams } from 'react-router-dom';
import MessageItem from './Item';



// function deleteMessage(id: string) {
//   var messageItem = document.getElementById(id);
//   // If an element for that message exists we delete it.
//   if (messageItem) {
//     messageItem.remove();
//   }
// }


const useStyles = makeStyles({
  root: {
    gridRow: 1,
    overflow: 'auto',
    width: '100%',
  },
});


/**
* MessageList:
* Cloud Firestoreから取得した複数のメッセージを表示する
* リアルタイムにメッセージを表示するために、React.useEffect内でonSnapshotを使ってメッセージを更新している
*/
const MessageList: React.FC = () => {
  const auth = useAuth();

  type Message = {
    id: string,
    senderId: string,
    state: string,
    filePath: string,
    text: string,
    timestamp: any,
  }

  const [messages, setMessages] = React.useState<Message[]>([]);
  const [lastOfferTimestamp, setLastOfferTimestamp] = React.useState();
  const [profImage, setProfImage] = React.useState("");
  const { id } = useParams<{ id: string }>();

  const classes = useStyles();

  React.useEffect(() => {
    if (auth) {
      const db = firebase.firestore();
      const roomsRef = db.collection("Rooms").doc(id);
      const messagesRef = db.collection("Rooms").doc(id).collection("Messages");
      const usersRef = db.collection("User");
      const query = messagesRef.orderBy('timestamp', 'asc');
      setMessages([]);
      roomsRef.get()
        .then((doc: any) => {
          //弁理士が最後にオファーしたプランを取得しておく。(最後にオファーしたプラン以外はボタンを押せなくするため。)
          setLastOfferTimestamp(doc.data()?.lastOfferTimestamp);
          //チャット相手のプロフィール画像を設定。ない場合はデフォルトのアイコン画像。
          if (auth?.kind === "Applicant") {
            //自分が申請者の場合、相手は弁理士なのでattorneyIdでユーザー情報取得
            usersRef.doc(doc.data().attorneyId).get()
              .then((userDoc) => {
                userDoc.data()?.image ? setProfImage(userDoc.data()?.image) : setProfImage(ProfileNoImage);
                //申請者(自分)の未読数を0に設定
                roomsRef.update({ applicantUnreadNumber: 0 });
              }).catch((error) => {
              });
          } else if (auth?.kind === "Attorney") {
            usersRef.doc(doc.data().applicantId).get()
              .then((userDoc) => {
                userDoc.data()?.image ? setProfImage(userDoc.data()?.image) : setProfImage(ProfileNoImage);
                //弁理士(自分)の未読数を0に設定
                roomsRef.update({ attorneyUnreadNumber: 0 });
              }).catch((error) => {
              });
          }
        });
      query.onSnapshot((snap: firebase.firestore.QuerySnapshot) => {
        snap.docChanges().forEach(function (change) {
          if (change.type === 'added') {
            var message = change.doc.data();
            setMessages(
              messages => [{ id: change.doc.id, senderId: message.senderId, state: message.state, filePath: message.filePath, text: message.text, timestamp: message.timestamp }, ...messages]
            );
          }
          if (change.type === 'modified') {
            (document.querySelector('.inner__content') as HTMLIonContentElement)?.scrollToBottom(500);
          }
        });
      });
    }
    return () => {
      // cleanup
    }
    // Lintの設定にすると無限ループが発生する
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [auth?.kind, id, setMessages, setLastOfferTimestamp]);

  // const length = messages.length;
  var headerMessageId = "";
  if (messages[0] !== undefined) {
    headerMessageId = messages[0].id;
  }

  /**
  * return(戻り値):
  * Cloud Firestoreから取得した複数のメッセージ(messages)を展開して、それぞれのメッセージを表示
  * 1つ1つのメッセージについては、MessageItemコンポーネントで表示していく
  */
  return (
    <IonContent className="MessageWrap inner__content"
      scrollY={true}
    >
      <IonList className={"Message__inner Message " + classes.root}>
        {messages.slice(0).reverse().map((message, index) => {
          const key = index;
          const messageDoc = message;
          const isLastItem = headerMessageId === messageDoc.id;

          return (
            <MessageItem
              key={key}
              message={messageDoc}
              roomId={id}
              isLastItem={isLastItem}
              lastOfferTimestamp={lastOfferTimestamp}
              profImage={profImage}
            />
          );

        })}

      </IonList>
    </IonContent>
  );
};

export default MessageList;
