import { TextField } from '@material-ui/core';
import React from 'react';


type Props = {
  inputEl: any,
  setText: any,
  text: string
}

/**
* MessageText:
* チャットルームのテキストエリアを表示する
* MessageInputコンポーネント内で呼び出される
* MessageInputコンポーネントからpropsとしてtextとsetTextが渡される
*/
const MessageText: React.FC<Props> = ({ inputEl, setText, text }) => {
  const [isComposed, setIsComposed] = React.useState(false);

  /**
  * return(戻り値):
  * Cloud Firestoreから取得した複数のメッセージ(messages)を展開して、それぞれのメッセージを表示
  * 1つ1つのメッセージについては、MessageItemコンポーネントで表示していく
  * テキストエリアが変化する度にonChange内でsetTextを呼び出し、textを更新
  */
  return (
    <TextField
      autoFocus
      // id="filled-basic"
      placeholder="メッセージを入力"
      variant="filled"
      InputLabelProps={{
        shrink: true, //アニメーションをキャンセル
      }}
      label={'メッセージを入力'}
      fullWidth={true}
      inputRef={inputEl}
      onChange={(e: any) => setText(e.target.value)}
      onKeyDown={(e: any) => {
        if (isComposed) return;

        const { target } = e;
        // TextArea要素以外の場合は終了
        if (!(target instanceof HTMLTextAreaElement)) {
          return;
        }

        const text = target.value;
        if (text === '') return;
      }}
      onCompositionStart={() => setIsComposed(true)}
      onCompositionEnd={() => setIsComposed(false)}
      value={text}
    />
  );
};

export default MessageText;
