


//firebase
import firebase from 'firebase';
import React from 'react';
import { useParams } from 'react-router-dom';
import AlbumItem from './Item';


async function updateState(roomId: string, messageId: string) {
  const db = firebase.firestore();
  const stateRef = db.collection("Rooms").doc(roomId).collection("Messages").doc(messageId);
  return await stateRef.update({
    state: "normalFile"
  }).catch(function (error: any) {
    console.error('Error writing new message to database', error);
  });
}

interface Props {
  changeHead?: (isState: boolean) => any;
  selectNum?: (select: number) => any;
}

/**
* AlbumList:
* SaveFolderコンポーネントから呼び出される。
* ヘッダーに表示されるボタンや、Cloud Firestoreから取得した画像(アルバムに保存されているもの)を複数表示する
* また、ボタンをクリックした時に呼び出される関数などが定義されている
* changeHeadとselectNumの説明
*/
const AlbumList: React.FC<Props> = ({ changeHead, selectNum }) => {
  const [images, setImages] = React.useState([{ id: "", state: "", filePath: "" }]);
  const [selectedImgNum, setSelectedImgNum] = React.useState(0);
  const [HeadTextFlag, setHeadTextFlag] = React.useState(true);
  const { id } = useParams<{ id: string }>();
  let isState = true;
  if (selectNum) {
    selectNum(selectedImgNum)
  }

  /**
  * handleSelect():
  * ヘッダー部分にデフォルトで表示されている「選択」ボタンをクリックした時に呼び出される関数
  * クリック後はヘッダーが切り替わり、選択されている写真の件数と、「キャンセル」「保存」「削除」ボタンが表示される
  */
  function handleSelect() {
    setSelectedImgNum(0);
    setHeadTextFlag(!HeadTextFlag);
    const cancelBtn = (document.querySelector(".AlbumList__cancel__button") as HTMLElement);
    const deleteBtn = (document.querySelector(".AlbumList__delete__button") as HTMLElement);
    const selectBtn = (document.querySelector(".AlbumList__select__button") as HTMLElement);
    //追加分
    const saveBtn = (document.querySelector(".AlbumList__save__button") as HTMLElement);
    const selectText = (document.querySelector(".album__text") as HTMLElement);
    selectText.style.display = "initial";
    saveBtn.style.display = "initial";
    //追加分
    if (changeHead) {
      changeHead(isState);
    }
    cancelBtn.style.display = "initial";
    deleteBtn.style.display = "initial";
    selectBtn.style.display = "none";

    let checkBoxes = document.querySelectorAll(".AlbumItem__checkbox:checked");
    for (let i = 0; i < checkBoxes.length; i++) {
      let checkbox = checkBoxes[i] as HTMLInputElement;
      checkbox.checked = false;
    }
    let labels = document.querySelectorAll(".album__link__checkbox");
    for (let i = 0; i < labels.length; i++) {
      let label = labels[i] as HTMLElement;
      label.style.display = "initial";
    }
  }

  /**
  * handleDelete():
  * ヘッダー部分の「削除」ボタンをクリックした時に呼び出される関数
  * 写真を1枚以上選択した状態で「削除」ボタンをクリックすると、写真が保存用フォルダから削除される
  */
  function handleDelete() {
    setHeadTextFlag(!HeadTextFlag);
    setSelectedImgNum(0);
    const cancelBtn = (document.querySelector(".AlbumList__cancel__button") as HTMLElement);
    const deleteBtn = (document.querySelector(".AlbumList__delete__button") as HTMLElement);
    const selectBtn = (document.querySelector(".AlbumList__select__button") as HTMLElement);
    //追加分
    const saveBtn = (document.querySelector(".AlbumList__save__button") as HTMLElement);
    saveBtn.style.display = "none";
    //追加分
    cancelBtn.style.display = "none";
    deleteBtn.style.display = "none";
    selectBtn.style.display = "initial";
    if (changeHead) {
      changeHead(false);
    }
    let labels = document.querySelectorAll(".album__link__checkbox");
    for (let i = 0; i < labels.length; i++) {
      let label = labels[i] as HTMLElement;
      label.style.display = "none";
    }
    let checkBoxesChecked = document.querySelectorAll(".AlbumItem__checkbox:checked");
    for (let i = 0; i < checkBoxesChecked.length; i++) {
      let checkbox = checkBoxesChecked[i] as HTMLInputElement;
      checkbox.checked = false;
      checkbox.style.display = "none";
      updateState(id, checkbox.value);
    }
  }

  /**
  * handleCancel():
  * ヘッダー部分の「キャンセル」ボタンをクリックした時に呼び出される関数
  * クリック後はヘッダーが切り替わり、デフォルトの「選択」ボタンが表示された状態になる
  */
  function handleCancel() {
    setHeadTextFlag(!HeadTextFlag);

    setSelectedImgNum(0);
    const cancelBtn = (document.querySelector(".AlbumList__cancel__button") as HTMLElement);
    const deleteBtn = (document.querySelector(".AlbumList__delete__button") as HTMLElement);
    const selectBtn = (document.querySelector(".AlbumList__select__button") as HTMLElement);
    //追加分
    const saveBtn = (document.querySelector(".AlbumList__save__button") as HTMLElement);
    saveBtn.style.display = "none";
    //追加分
    if (changeHead) {
      changeHead(false);
    }
    cancelBtn.style.display = "none";
    deleteBtn.style.display = "none";
    selectBtn.style.display = "initial";

    let checkboxes = document.querySelectorAll(".AlbumItem__checkbox");
    for (let i = 0; i < checkboxes.length; i++) {
      let checkbox = checkboxes[i] as HTMLInputElement;
      checkbox.style.display = "none";
      checkbox.checked = false;
    }
    let labels = document.querySelectorAll(".album__link__checkbox");
    for (let i = 0; i < labels.length; i++) {
      let label = labels[i] as HTMLElement;
      label.style.display = "none";
    }
  }

  /**
  * handleSave():
  * ヘッダー部分の「保存」ボタンをクリックした時に呼び出される関数
  * 写真を1枚以上選択した状態で「保存」ボタンをクリックすると、写真がローカルのコンピュータに保存される
  */
  function handleSave() {
    setHeadTextFlag(!HeadTextFlag);

    setSelectedImgNum(0);
    const cancelBtn = (document.querySelector(".AlbumList__cancel__button") as HTMLElement);
    const deleteBtn = (document.querySelector(".AlbumList__delete__button") as HTMLElement);
    const selectBtn = (document.querySelector(".AlbumList__select__button") as HTMLElement);
    const saveBtn = (document.querySelector(".AlbumList__save__button") as HTMLElement);
    saveBtn.style.display = "none";
    cancelBtn.style.display = "none";
    deleteBtn.style.display = "none";
    selectBtn.style.display = "initial";
    if (changeHead) {
      changeHead(false);
    }
    //DOMが属性として持っているurlを取得して、ローカルに画像をダウンロードする。
    let childs = document.querySelectorAll(".AlbumItem__checkbox:checked");
    for (let i = 0; i < childs.length; i++) {
      let child = childs[i] as HTMLInputElement;
      let parent = child.closest('.album__link') as HTMLElement;

      //ローカルに保存する処理
      let url = parent.style.backgroundImage.replace(/^url\(["']?/, '').replace(/["']?\)$/, '');
      let xhr = new XMLHttpRequest();
      xhr.responseType = 'blob';
      xhr.onload = ((event) => {
        //blobがファイルのデータ
        let blob = xhr.response;
        //aタグを作る
        let link = document.createElement('a');
        // ファイルデータに紐づくダウンロードリンクを設定する
        link.href = URL.createObjectURL(blob);
        //第二引数はファイル名
        link.setAttribute('download', 'image');
        // link.download = 'photo.jpg';
        link.click();
      });
      xhr.open('GET', url);
      xhr.send();
    }

    let checkboxes = document.querySelectorAll(".AlbumItem__checkbox");
    for (let i = 0; i < checkboxes.length; i++) {
      let checkbox = checkboxes[i] as HTMLInputElement;
      checkbox.style.display = "none";
      checkbox.checked = false;
    }
    let labels = document.querySelectorAll(".album__link__checkbox");
    for (let i = 0; i < labels.length; i++) {
      let label = labels[i] as HTMLElement;
      label.style.display = "none";
    }
  }

  /**
  * React.useEffect():
  * Cloud Firestoreから条件を指定してデータを取得。ここではチャットルームのidを指定して、その中でstateフィールドがalbumFileになっているデータだけ取得
  * firestoreから取得したクエリを元に、AlbumListの冒頭で宣言したimagesをsetImages関数で更新
  */
  React.useEffect(() => {
    const db = firebase.firestore();
    const messagesRef = db.collection("Rooms").doc(id).collection("Messages");
    const query = messagesRef.where('state', '==', 'albumFile');

    query.onSnapshot((snap: firebase.firestore.QuerySnapshot) => {
      setImages(
        snap.docs.map((doc) => ({ id: doc.id, state: doc.data().state, filePath: doc.data().filePath }))
      );
    });

  }, [id]);
  const length = images.length;

  /**
  * return(戻り値):
  * 「選択」「キャンセル」「保存」「削除」ボタンと、AlbumItemコンポーネントの表示
  */
  return (
    <React.StrictMode>
      <div className="album__button__wrap">
        <p className="album__text album__text__pc">{HeadTextFlag ? "保存用フォルダ" : selectedImgNum + "件選択中"}</p>

        <button className="AlbumList__select__button album__button__select" onClick={() => handleSelect()}>選択</button>
        <button className="AlbumList__cancel__button album__button__cancel" style={{ display: "none" }} onClick={() => handleCancel()}>キャンセル</button>
        <button className="AlbumList__save__button album__button__save" style={{ display: "none" }} onClick={() => handleSave()}>保存</button>
        <button className="AlbumList__delete__button album__button__delete" style={{ display: "none" }} onClick={() => handleDelete()}>削除</button>
      </div>
      <div className="album">

        {images.slice(0).reverse().map((image, index) => {
          const key = index;
          const id = (index + 1).toString();
          const imageDoc = image;
          const isLastItem = length === index + 1;

          return (
            <AlbumItem
              key={key}
              id={id}
              image={imageDoc}
              isLastItem={isLastItem}
              setSelectedImgNum={setSelectedImgNum}
            />
          );
        })}

      </div>
    </React.StrictMode>
  );
};

export default AlbumList;
