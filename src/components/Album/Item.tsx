import 'assets/css/Message.css';
//firebase
import firebase from 'firebase';
import "firebase/storage";
import React from 'react';

type Props = {
  isLastItem: boolean,
  id: string,
  image: any,
  setSelectedImgNum: any,
}

/**
* AlbumItem:
* AlbumListコンポーネント内から呼び出される。AlbumListが複数画像を表示する役割を持つのに対して、AlbumItemは1つ1つの画像を表示している
* AlbumListから渡されるpropsは以下の通り
* id:複数ある画像やチェックボックスをそれぞれ紐付け、区別するために使用する
* image:アルバムページに表示する画像の情報を保持している
* setSelectedImgNum:AlbumListコンポーネントの最初の方で宣言されたstateを更新するための関数。選択されている画像の数を保持する
*/
const AlbumItem: React.FC<Props> = ({ isLastItem, id, image, setSelectedImgNum }) => {
  const [photo, setPhoto] = React.useState();
  const messageId = image.id;
  const state = image.state;
  const filePath = image.filePath;

  /**
  * React.useEffect():
  * アルバムページに表示する画像を実際にFirebase Storageからダウンロードしてくる
  * ダウンロードが成功したら、AlbumItemコンポーネントの冒頭で宣言したphotoをsetPhoto関数で更新
  */
  React.useEffect(() => {
    if (state === "albumFile") {
      var storageRef = firebase.storage().ref();

      storageRef.child(filePath).getDownloadURL().then(function (url) {
        setPhoto(url);
      }).catch(function (error) {

      });

    }
  }, [setPhoto, isLastItem, state, filePath]);

  /**
  * return(戻り値):
  * Storageからダウンロードしてきた画像と、画像上に表示するチェックボックスを表示
  * チェックボックsをクリックした時の挙動はonChange内に記述
  */
  return (
    <label className="album__link chat__img" style={{ backgroundImage: `url(${`${photo}`})` }}
    >
      {/* id,htmlforの数字書き換える感じでお願いします！ */}
      <input id={id} type="checkbox" className="AlbumItem__checkbox" style={{ display: "none" }} name="delete" value={messageId}
        onChange={(e) => {
          if (e.target.checked) {
            setSelectedImgNum((num: number) => {
              return num + 1;
            });
          } else {
            setSelectedImgNum((num: number) => {
              return num - 1;
            });
          }
        }}
      />
      <label htmlFor={id} className="album__link__checkbox" style={{ display: "none" }} ></label>
    </label>
  );
};

export default AlbumItem;
