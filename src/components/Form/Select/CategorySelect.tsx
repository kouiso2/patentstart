import { categoryObjectChoice } from "data/choice";
import { CategoryContext } from 'data/context';
import React from 'react';


type categoryKeyOnly = typeof categoryObjectChoice;

export const CategorySelect: React.FC = () => {
  const { patentCategory, setPatentCategory } = React.useContext(CategoryContext);
  const categoryOption = React.useMemo(() => {
    return (Object.keys(categoryObjectChoice) as (keyof categoryKeyOnly)[]).map((value, key: number) => (
      <option value={value} key={key}>{Object.values(categoryObjectChoice)[key]}</option>
    ));
  }, []);

  const handleChange = React.useCallback((e: React.ChangeEvent<HTMLSelectElement>) => {
    e.preventDefault();
    const result = e.currentTarget.value;

    if (
      result === "DailyNecessities" ||
      result === "Transportation" ||
      result === "Science" ||
      result === "Paper" ||
      result === "FixedStructure" ||
      result === "Machine" ||
      result === "Physics" ||
      result === "Electrical" ||
      result === "NoUnderstand"
    ) {
      setPatentCategory(result);
    }
  }, [setPatentCategory]);

  return (
    <div className="arrivalsList__catOrder__cat"  >
      {/* 「全案件一覧」のタブで表示しているときはarrivalsList__catOrder__catにdisplay__noneを付け足して非表示にする */}
      <select className="arrivalsList__catOrder__cat__select" name="category" id="" onChange={handleChange} value={patentCategory}>
        {categoryOption}
      </select>
    </div>

  )
}

export default CategorySelect;
