import OrderIcon from 'assets/img/common/icon-order.svg';
import { sorterChoice } from "data/choice";
import React from 'react';
import { SorterContext } from '../../../data/context/index';

type categoryKeyOnly = typeof sorterChoice;

export const SorterSelect: React.FC = () => {
  const { sorter, setSorter } = React.useContext(SorterContext);
  const sorterOption = React.useMemo(() => {
    return (Object.keys(sorterChoice) as (keyof categoryKeyOnly)[]).map((value, key: number) => (
      <option value={value} key={key}>{Object.values(sorterChoice)[key]}</option>
    ));
  }, []);

  const handleChange = React.useCallback((e: React.ChangeEvent<HTMLSelectElement>) => {
    e.preventDefault();
    const result = e.currentTarget.value;

    if (
      result === "desc" ||
      result === "asc"
    ) {
      setSorter(result);
    }
  }, [setSorter]);

  return (
    <div className="arrivalsList__catOrder__order">
      <img src={OrderIcon} alt="" />
      <select className="arrivalsList__catOrder__order__select order__select--js" style={{ "color": "black" }} name="order" id="" value={sorter} onChange={handleChange}>
        <option value="default">全案件一覧</option>
        {sorterOption}
      </select>
    </div>
  )
}

export default SorterSelect;
