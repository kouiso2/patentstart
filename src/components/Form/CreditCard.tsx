import { IonInput } from '@ionic/react';
import {
  CardCvcElement,
  CardExpiryElement,
  CardNumberElement,
  Elements,
  useElements, useStripe
} from '@stripe/react-stripe-js';
//stripe
import { loadStripe } from '@stripe/stripe-js';
import { stripePublickeys } from 'data/choice';
import { ProfileSubmitContext } from 'data/context';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/functions';
//firebase
import firebase from 'firebase/index';
import useAuth from 'hooks/auth';
import React from 'react';

interface Props {
  children?: React.ReactNode;
}

const stripePromise = loadStripe(stripePublickeys.live);

export const CreditCardForm: React.FC<Props> = ({ children }) => {
  const stripe = useStripe();
  const elements = useElements();
  const { signal, setSignal } = React.useContext(ProfileSubmitContext);

  const auth = useAuth();

  const [fullname, setFullname] = React.useState<string>('');
  const [cardNumber, setCardNumber] = React.useState<boolean>(false);

  const handleSubmit = React.useCallback(async () => {
    // e.preventDefault();
    //あとで直す
    if (cardNumber) {
      if (!fullname) {
        return;
      }
    }

    const func = firebase.functions();
    const createAccount = func.httpsCallable("createApplicantAccount");
    const attach = func.httpsCallable("attachDefaultNewCredit");
    if (!auth?.stripeId) {
      await createAccount();
    };

    const tokenRes = await stripe?.createToken(elements?.getElement(CardNumberElement)!)
    try {
      await attach(tokenRes?.token?.id);
    } catch (e) {
      window.alert("カード登録に失敗しました");
      console.error(e)
    }
  }, [auth?.stripeId, elements, stripe, cardNumber, fullname]);

  if (signal && cardNumber) {
    handleSubmit();
    setSignal(false);
  }
  return (
    <div className="payment--card--new" >
      {children}
      <div className="payment--card--new__wrap">
        <p className="payment--card--new__text">カード名義</p>
        <IonInput
          placeholder="SHINSEI TARO"
          autocomplete="cc-name"
          className="payment--card--new__input"
          type="text"
          onIonChange={(e) => {
            e.preventDefault();
            setFullname(e.detail.value!);
          }}
          required={false} />
        <p className="payment--card--new__text">カード番号</p>
        <CardNumberElement onChange={(e) => {
          e.complete ? setCardNumber(true) : setCardNumber(false);
        }} className="payment--card--new__input" />
        <p className="payment--card--new__text">有効期限</p>
        <div className="number">
          <CardExpiryElement className="payment--card--new__input number__input" />
        </div>
        <p className="payment--card--new__text">セキュリティ番号</p>
        <div className="number">
          <CardCvcElement className="payment--card--new__input" />
        </div>
      </div>
    </div>
  );
};

const CheckoutForm: React.FC<Props> = ({ children }) => (
  <Elements stripe={stripePromise}>
    <CreditCardForm>
      {children}
    </CreditCardForm>
  </Elements>
);
export default CheckoutForm;
