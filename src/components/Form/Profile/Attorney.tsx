import { IonDatetime, IonInput, IonRouterLink, IonTextarea } from '@ionic/react';
import ProfileCamera from 'assets/img/noimage.svg';
import { categoryObjectChoice, prefectureChoice } from 'data/choice';
import useProfile from 'hooks/profile';
import React from 'react';
import { patentCategoryJpn } from 'translate';
type categoryKeyOnly = keyof typeof categoryObjectChoice;
export const AttorneyProfileForm: React.FC = () => {
  const {
    profile,
    setProfile,
    imgInput,
    handleSubmit,
    handleImageChange,
    searchAddress } = useProfile();

  const categoryCheckbox = React.useMemo(() => {
    return (Object.keys(profile.goodCategory) as (categoryKeyOnly)[]).map((category: categoryKeyOnly, key: number) => {
      console.log(category, profile.goodCategory[category]);
      console.log(profile.goodCategory.Transportation);
      return (
        <label htmlFor={String(key)} className="form__classification" key={String(key)} >
          <input
            type="checkbox"
            className=""
            id={String(key)}
            value={category}
            name={category}
            defaultChecked={profile.goodCategory[category]}
            onClick={(e) => {
              setProfile({
                ...profile,
                goodCategory: {
                  ...profile.goodCategory,
                  [category]: e.currentTarget.checked,
                }
              });
            }}
          />
          {
            profile.goodCategory[category] ? <div className="form__classification__checkbox--active"></div> : <div className="form__classification__checkbox"></div>
          }
          <div className="">
            <p className="form__classification__title">{patentCategoryJpn(category)}</p>
          </div>
        </label>
      )
    })


  }, [setProfile, profile]);
  return (
    <form onSubmit={handleSubmit}>
      <div className="form__profile">
        <label className="form__profile__img" htmlFor="img">
          <figure className="camera" style={{ backgroundImage: `url(${profile.image ? profile.image : ProfileCamera})` }}></figure>
        </label>
        <input name="image" type="file" className="form__profile__input" id="img" ref={imgInput} onChange={handleImageChange} />
        <div className="form__profile__text">
          <p className="heading">プロフィール画像</p>
          <span className="disc">アプリ内のサムネイルに設定されます。<br />※2MB以下の画像を選択してください</span>
        </div>
      </div>
      <div className="">
        <div className="form__title__wrap__flex">
          <span className="form__title">会社名・組織名</span>
          <span className="form__badge--required">必須</span>
        </div>
        <IonInput
          className="form__input"
          name="companyName"
          type="text"
          placeholder="株式会社 特許申請"
          value={profile.companyName}
          onIonChange={(e) => {
            setProfile({
              ...profile,
              companyName: e.detail.value
            });
          }}
          required={true} />
      </div>
      <div className="">
        <div className="form__title__wrap__flex">
          <span className="form__title">会社名・組織名（フリガナ）</span>
          <span className="form__badge--required">必須</span>
        </div>
        <IonInput
          autocomplete="organization-title"
          className="form__input"
          name="companyKana"
          type="text"
          placeholder="カブシキカイシャ トッキョシンセイ"
          value={profile.companyKana}
          onIonChange={(e) => {
            setProfile({
              ...profile,
              companyKana: e.detail.value
            })
          }}
          required={true}
          pattern="(?=.*?[\u30A1-\u30FC])[\u30A1-\u30FC\s]*" />
      </div>
      <div className="">
        <div className="form__title__wrap__flex">
          <span className="form__title">担当者名 姓</span>
          <span className="form__badge--required">必須</span>
        </div>
        <IonInput
          autocomplete="family-name"
          name="firstName"
          onIonChange={(e) => {
            setProfile({
              ...profile,
              firstName: e.detail.value
            })
          }}
          value={profile.firstName}
          placeholder="特許太郎"
          className="form__input"
          type="text"
          required={true} />
      </div>
      <div className="">
        <div className="form__title__wrap__flex">
          <span className="form__title">担当者名 姓（フリガナ）</span>
          <span className="form__badge--required">必須</span>
        </div>
        <IonInput
          autocomplete="family-name"
          name="firstNameKatakana"
          onIonChange={(e) => {
            setProfile({
              ...profile,
              firstNameKatakana: e.detail.value
            })
          }}
          value={profile.firstNameKatakana}
          placeholder="トッキョタロウ"
          className="form__input"
          type="text"
          required={true} pattern="(?=.*?[\u30A1-\u30FC])[\u30A1-\u30FC\s]*" />
      </div>

      {/* 追加 */}
      <div className="">
        <div className="form__title__wrap__flex">
          <span className="form__title">担当者名 名</span>
          <span className="form__badge--required">必須</span>
        </div>
        <IonInput
          autocomplete="given-name"
          name="lastName"
          onIonChange={(e) => {
            setProfile({
              ...profile,
              lastName: e.detail.value
            })
          }}
          value={profile.lastName}
          placeholder="特許太郎"
          className="form__input"
          type="text"
          required={true} />
      </div>
      <div className="">
        <div className="form__title__wrap__flex">
          <span className="form__title">担当者名 名（フリガナ）</span>
          <span className="form__badge--required">必須</span>
        </div>
        <IonInput
          autocomplete="given-name"
          name="lastNameKatakana"
          onIonChange={(e) => {
            setProfile({
              ...profile,
              lastNameKatakana: e.detail.value
            })
          }}
          value={profile.lastNameKatakana}
          placeholder="トッキョタロウ"
          className="form__input"
          type="text"
          required={true} pattern="(?=.*?[\u30A1-\u30FC])[\u30A1-\u30FC\s]*" />
      </div>


      <div className="">
        <div className="form__title__wrap__flex mt-30">
          <span className="form__title">担当者 性別</span>
          <span className="form__badge--required">必須</span>
        </div>
        <div className="form__title__wrap__flex">
          <input className="radioBtn__input" type="radio" id="genderMale" name="gender" value="male" onClick={(e) => {
            setProfile({
              ...profile,
              gender: e.currentTarget.value
            });
            console.log(profile.gender)
          }} checked={profile.gender === "male" ? true : false} />
          <label htmlFor="genderMale" className="form__input__company radioBtn__label"></label>
          <label htmlFor="genderFemale" className="form__text__company">男性</label>
        </div>
        <div className="form__title__wrap__flex">
          <input className="radioBtn__input" type="radio" id="genderFemale" name="gender" value="female" onClick={(e) => {
            setProfile({
              ...profile,
              gender: e.currentTarget.value
            });
            console.log(profile.gender)
          }} checked={profile.gender === "female" ? true : false} />
          <label htmlFor="company" className="radioBtn__label form__input__company"></label>
          <label htmlFor="company" className="form__text__company">女性</label>
        </div>
      </div>
      <div className="">
        <div className="form__title__wrap__flex mt-30">
          <span className="form__title">担当者 生年月日</span>
          <span className="form__badge--required">必須</span>
        </div>
        <IonDatetime name="birthday" value={profile.birthday} className="form__input" displayFormat="YYYY/MM/DD" placeholder="生年月日を選択" max="2007-12-31" onIonChange={(e) => {
          e.preventDefault();
          setProfile({
            ...profile,
            birthday: e.detail.value
          })
        }}></IonDatetime>
      </div>
      <div className="">
        <span className="form__title mt-30">弁理士歴</span>
        <IonInput
          name="history"
          onIonChange={(e): void => {
            setProfile({
              ...profile,
              history: parseInt(e.detail.value!, 10)
            })
          }}
          value={profile.history}
          placeholder="3年"
          className="form__input"
          type="number" />
      </div>
      <div className="">
        <span className="form__title mt-30">弁理士登録番号</span>
        <IonInput
          name="registerNumber"
          onIonChange={(e) => {
            setProfile({
              ...profile,
              registerNumber: e.detail.value
            })
          }}
          value={profile.registerNumber}
          placeholder="123412345456"
          className="form__input"
          type="text" />
      </div>
      <div className="">
        <span className="form__title mt-30">対応エリア</span>
        <IonInput
          multiple={true}
          name="correspondArea"
          onIonChange={(e): void => {
            prefectureChoice.forEach((value) => {
              if (value === e.detail.value) {
                setProfile({
                  ...profile,
                  correspondArea: e.detail.value
                })
              }
            })
          }}
          value={profile.correspondArea}
          placeholder="東京都、神奈川県、埼玉県、千葉県"
          className="form__input"
          type="text" />
      </div>
      <div className="">
        <span className="form__title mt-30">ホームページ・SNS等</span>
        <IonInput
          autocomplete="url"
          name="homepage"
          onIonChange={(e) => {
            setProfile({
              ...profile,
              homepage: e.detail.value
            })
          }}
          value={profile.homepage}
          placeholder="https://tokkyo-shinsei.jp"
          className="form__input"
          type="url" />
      </div>
      <div className="">
        <div className="form__title__wrap__flex">
          <span className="form__title">メールアドレス</span>
          <span className="form__badge--required">必須</span>
        </div>
        <IonInput
          autocomplete="email"
          name="email"
          onIonChange={(e) => {
            setProfile({
              ...profile,
              email: e.detail.value
            })
          }}
          value={profile.email}
          placeholder="tokkyo-shinsei@tokkyo.co.jp"
          className="form__input"
          type="email"
          required={true} />
      </div>
      <div className="">
        <div className="form__title__wrap__flex  mt-30">
          <span className="form__title">電話番号</span><span className="form__badge--required">必須</span>
        </div>
        <IonInput
          autocomplete="tel"
          name="tel"
          onIonChange={(e) => {
            setProfile({
              ...profile,
              phoneNumber: e.detail.value
            })
          }}
          value={profile.phoneNumber}
          placeholder="09011112222"
          className="form__input"
          type="tel"
          required={true} />
      </div>
      <div className="form__title__wrap__flex">
        <span className="form__title">得意な国際特許分類<span className="form__title__12">（複数選択可）</span></span>
      </div>
      {categoryCheckbox}

      <span className="form__classification--undertext">カテゴリーは「<IonRouterLink className="form__classification--undertext__link" href="https://www.jpo.go.jp/system/patent/gaiyo/bunrui/ipc/ipc8wk.html" target="_blank">国際特許分類</IonRouterLink>」に従い、分野を選択してください。</span>
      <div className="form__title__wrap">
        <div className="form__title__wrap__flex mt-30 mb-10">
          <span className="form__title form__title__20">住所</span>
          <span className="form__badge--required">必須</span>
        </div>

        <div className="form--address">
          <span className="mark">〒</span>
          <IonInput
            autocomplete="postal-code"
            className="address"
            max="7"
            type="text"
            value={profile.address?.postalCode}
            onIonChange={(e) => {
              setProfile({
                ...profile,
                address: {
                  ...profile.address,
                  postalCode: e.detail.value
                }
              })
            }}
            onKeyUp={searchAddress}
            required={true} />
          <button type="button" className="number">郵便番号検索</button>
        </div>
        <IonInput
          name="state"
          autocomplete="address-level1"
          onIonChange={(e) => {
            setProfile({
              ...profile,
              address: {
                ...profile.address,
                state: e.detail.value
              }
            })
          }}
          value={profile.address?.state}
          placeholder="都道府県"
          className="form__input"
          type="text"
          required={true} />
        <IonInput
          autocomplete="address-level2"
          name="city"
          onIonChange={(e) => {
            setProfile({
              ...profile,
              address: {
                ...profile.address,
                city: e.detail.value
              }
            })
          }}
          value={profile.address?.city}
          placeholder="市区町村"
          className="form__input"
          type="text"
          required={true} />
        <IonInput
          autocomplete="address-level3"
          name="town"
          onIonChange={(e) => {
            setProfile({
              ...profile,
              address: {
                ...profile.address,
                town: e.detail.value
              }
            })
          }}
          value={profile.address?.town}
          placeholder="町名・字"
          className="form__input"
          type="text"
          required={true} />
        <IonInput
          autocomplete="address-level4"
          name="line1"
          onIonChange={(e) => {
            setProfile({
              ...profile,
              address: {
                ...profile.address,
                line1: e.detail.value
              }
            })
          }}
          value={profile.address?.line1}
          placeholder="番地・号"
          className="form__input"
          type="text"
          required={true} />
        <IonInput
          autocomplete="address-level4"
          name="line2"
          onIonChange={(e) => {
            setProfile({
              ...profile,
              address: {
                ...profile.address,
                line2: e.detail.value
              }
            })
          }}
          value={profile.address?.line2}
          placeholder="建物など"
          className="form__input"
          type="text"
          required={true} />
      </div>
      <div className="form__title__wrap__flex mt-30 mb-10">
        <span className="form__title form__title__20">住所（フリガナ）</span>
        <span className="form__badge--required">必須</span>
      </div>
      <IonInput
        name="state"
        autocomplete="address-level1"
        onIonChange={(e) => {
          setProfile({
            ...profile,
            addressKatakana: {
              ...profile.addressKatakana,
              state: e.detail.value
            }
          })
        }}
        value={profile.addressKatakana?.state}
        placeholder="トドウフケン"
        className="form__input"
        type="text"
        pattern="(?=.*?[\u30A1-\u30FC])[\u30A1-\u30FC\s]*"
        required={true} />
      <IonInput
        autocomplete="address-level2"
        name="city"
        onIonChange={(e) => {
          setProfile({
            ...profile,
            addressKatakana: {
              ...profile.addressKatakana,
              city: e.detail.value
            }
          })
        }}
        value={profile.addressKatakana?.city}
        placeholder="シクチョウソン"
        className="form__input"
        type="text"
        pattern="(?=.*?[\u30A1-\u30FC])[\u30A1-\u30FC\s]*"
        required={true} />
      <IonInput
        autocomplete="address-level3"
        name="town"
        onIonChange={(e) => {
          setProfile({
            ...profile,
            addressKatakana: {
              ...profile.addressKatakana,
              town: e.detail.value
            }
          })
        }}
        value={profile.addressKatakana?.town}
        placeholder="チョウメイ・アザ"
        className="form__input"
        type="text"
        pattern="(?=.*?[\u30A1-\u30FC])[\u30A1-\u30FC\s]*"
        required={true}
      />
      <IonInput
        autocomplete="address-level4"
        name="line1"
        onIonChange={(e) => {
          setProfile({
            ...profile,
            addressKatakana: {
              ...profile.addressKatakana,
              line1: e.detail.value
            }
          })
        }}
        value={profile.addressKatakana?.line1}
        placeholder="バンチ・ゴウ"
        className="form__input"
        type="text"
        required={true}
      />
      <IonInput
        autocomplete="address-level4"
        name="line2"
        onIonChange={(e) => {
          setProfile({
            ...profile,
            addressKatakana: {
              ...profile.addressKatakana,
              line2: e.detail.value
            }
          })
        }}
        value={profile.addressKatakana?.line2}
        placeholder="タテモノなど"
        className="form__input"
        type="text"
        pattern="(?=.*?[\u30A1-\u30FC])[\u30A1-\u30FC\s]*"
        required={true} />
      <span className="form__title mt-30">自己紹介</span>
      <IonTextarea
        name="self"
        onIonChange={(e) => {
          e.preventDefault();
          setProfile({
            ...profile,
            self: e.detail.value
          })
        }}
        className="form__input form__textarea"
        placeholder="簡単な自己紹介など"
        value={profile.self}
      />
      <div className=" SP__btn__center SP__btn__update">
        <button className="Btn__base SP__btn" type="submit">
          変更する
        </button>
      </div>
    </form>
  )
}

export default AttorneyProfileForm;
