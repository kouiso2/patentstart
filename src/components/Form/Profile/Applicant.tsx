import { IonInput, IonTextarea } from "@ionic/react";
// img
import ProfileCamera from 'assets/img/noimage.svg';
import useProfile from 'hooks/profile';
import React from 'react';

export const ApplicantProfileForm: React.FC = () => {

  const {
    profile,
    setProfile,
    imgInput,
    handleSubmit,
    handleImageChange,
  } = useProfile();

  return (
    <form onSubmit={handleSubmit} id="test-id">
      <div className="form__profile">
        <label className="form__profile__img" htmlFor="img">
          <figure className="camera" style={{ backgroundImage: `url(${profile.image ? profile.image : ProfileCamera})` }}></figure>
        </label>
        <input name="image" type="file" className="form__profile__input" id="img" ref={imgInput} onChange={handleImageChange} />
        <div className="form__profile__text">
          <p className="heading">プロフィール画像</p>
          <span className="disc">アプリ内のサムネイルに設定されます。<br />※2MB以下の画像を選択してください</span>
        </div>
      </div>

      <div className="">
        <div className="form__title__wrap__flex mt-30">
          <span className="form__title">法人・個人</span>
          <span className="form__badge--required">必須</span>
        </div>
        <div className="form__title__wrap__flex">
          <input className=" radioBtn__input" type="radio" id="individual" name="individual" onClick={() => {
            setProfile({
              ...profile,
              businessType: {
                individual: true,
                company: false
              }
            });

          }} checked={profile.businessType.individual} />
          <label htmlFor="individual" className="form__input__company radioBtn__label"></label>
          <label htmlFor="individual" className="form__text__company">個人</label>
        </div>
        <div className="form__title__wrap__flex">
          <input className="radioBtn__input" type="radio" id="company" name="company" onClick={() => {
            setProfile({
              ...profile,
              businessType: {
                individual: false,
                company: true
              }
            });

          }} checked={profile.businessType.company} />
          <label htmlFor="company" className="radioBtn__label form__input__company"></label>
          <label htmlFor="company" className="form__text__company">法人</label>
        </div>
      </div>

      <div className="">
        <div className="form__title__wrap__flex mt-30">
          <span className="form__title">会社名・組織名</span>
        </div>
        <IonInput
          autocomplete="organization-title"
          name="companyName"
          placeholder="株式会社特許申請"
          className="form__input"
          type="text"
          value={profile.companyName}
          onIonChange={(e) => {
            setProfile({
              ...profile,
              companyName: e.detail.value
            });
          }}
        />
      </div>
      <div className="">
        <div className="form__title__wrap__flex mt-30">
          <span className="form__title">会社名・組織名（フリガナ）</span>
        </div>
        <IonInput
          autocomplete="organization-title"
          name="companyKana"
          placeholder="カブシキカイシャトッキョシンセイ"
          className="form__input"
          type="text"
          pattern="(?=.*?[\u30A1-\u30FC])[\u30A1-\u30FC\s]*"
          value={profile.companyKana}
          onIonChange={(e) => {
            setProfile({
              ...profile,
              companyKana: e.detail.value
            })
          }} />
      </div>
      <div className="">
        <div className="form__title__wrap__flex">
          <span className="form__title">担当者名 姓</span>
          <span className="form__badge--required">必須</span>
        </div>
        <IonInput
          autocomplete="family-name"
          name="firstName"
          onIonChange={(e) => {
            setProfile({
              ...profile,
              firstName: e.detail.value
            })
          }}
          value={profile.firstName}
          placeholder="特許太郎"
          className="form__input"
          type="text"
          required={true} />
      </div>
      <div className="">
        <div className="form__title__wrap__flex">
          <span className="form__title">担当者名 姓（フリガナ）</span>
          <span className="form__badge--required">必須</span>
        </div>
        <IonInput
          autocomplete="family-name"
          name="firstNameKatakana"
          onIonChange={(e) => {
            setProfile({
              ...profile,
              firstNameKatakana: e.detail.value
            })
          }}
          value={profile.firstNameKatakana}
          placeholder="トッキョタロウ"
          className="form__input"
          type="text"
          required={true}
          pattern="(?=.*?[\u30A1-\u30FC])[\u30A1-\u30FC\s]*" />
      </div>

      {/* 追加 */}
      <div className="">
        <div className="form__title__wrap__flex">
          <span className="form__title">担当者名 名</span>
          <span className="form__badge--required">必須</span>
        </div>
        <IonInput
          autocomplete="given-name"
          name="lastName"
          onIonChange={(e) => {
            setProfile({
              ...profile,
              lastName: e.detail.value
            })
          }}
          value={profile.lastName}
          placeholder="特許太郎"
          className="form__input"
          type="text"
          required={true} />
      </div>
      <div className="">
        <div className="form__title__wrap__flex">
          <span className="form__title">担当者名 名（フリガナ）</span>
          <span className="form__badge--required">必須</span>
        </div>
        <IonInput
          autocomplete="given-name"
          name="lastNameKatakana"
          onIonChange={(e) => {
            setProfile({
              ...profile,
              lastNameKatakana: e.detail.value
            })
          }}
          value={profile.lastNameKatakana}
          placeholder="トッキョタロウ"
          className="form__input"
          type="text"
          required={true}
          pattern="(?=.*?[\u30A1-\u30FC])[\u30A1-\u30FC\s]*" />
      </div>
      <div className="">
        <span className="form__title mt-30">ホームページ・SNS等</span>
        <IonInput
          autocomplete="url"
          name="homepage"
          placeholder="https://shinsei-example.co.jp"
          className="form__input"
          type="url"
          value={profile.homepage}
          onIonChange={(e) => {
            setProfile({
              ...profile,
              homepage: e.detail.value
            })
          }}
        />
      </div>
      <div className="">
        <div className="form__title__wrap__flex mt-30">
          <span className="form__title">メールアドレス</span>
          <span className="form__badge--required">必須</span>
        </div>
        <IonInput
          name="email"
          autocomplete="email"
          placeholder="example@shinsei.co.jp"
          className="form__input"
          type="email"
          required={true}
          value={profile.email}
          onIonChange={(e) => {
            setProfile({
              ...profile,
              email: e.detail.value
            })
          }}
        />
      </div>
      <div className="">
        <div className="form__title__wrap__flex mt-30">
          <span className="form__title">電話番号</span>
          <span className="form__badge--required">必須</span>
        </div>
        <IonInput
          autocomplete="tel"
          name="phoneNumber"
          placeholder="000-0000-0000"
          className="form__input"
          type="tel"
          required={true}
          value={profile.phoneNumber}
          onIonChange={(e) => {
            setProfile({
              ...profile,
              phoneNumber: e.detail.value
            })
          }}
        />
      </div>
      <span className="form__title mt-30">自己紹介</span>
      <IonTextarea
        name="self"
        className="form__input form__textarea"
        placeholder="簡単な自己紹介を入力"
        onIonChange={(e) => {
          setProfile({
            ...profile,
            self: e.detail.value
          })
        }}
        value={profile.self} />
      <div className=" SP__btn__center SP__btn__update">
        <button className="Btn__base SP__btn" type="submit">
          変更する
        </button>
      </div>
    </form>
  );
}

export default ApplicantProfileForm;
