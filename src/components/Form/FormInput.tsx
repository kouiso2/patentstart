import 'assets/css/SideBar.css';

interface Props {
  // children: React.ReactNode;
  placeholder: string;
  title: string;
  required: boolean;
}

const FormInput: React.FC<Props> = ({ placeholder, title, required }) => {
  return (
    <div className="form__title__wrap">
      <span className="form__title">{title}</span>
      {/* 必須の場合true */}
      {required && <span className="form__badge--required">必須</span>}
      <input placeholder={placeholder} className="form__input" type="text" />
    </div>
  );
};
{/* <FormInput placeholder="place" title="カード名義" required={false} /> */ }



export default FormInput;
