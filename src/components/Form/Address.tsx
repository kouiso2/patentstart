
const Address: React.FC = () => {
  return (
    <div className="">
      <span className="form__title form__title__20">住所</span>
      <div className="form--address">
        <span className="mark">〒</span>
        <input className="address" type="text" />
        <button className="number">郵便番号検索</button>
      </div>
      <input placeholder="番地まで" className="form__input" type="text" />
      <input placeholder="建物など" className="form__input" type="text" />
    </div>
  );
};


export default Address;
