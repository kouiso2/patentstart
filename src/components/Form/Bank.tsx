import { IonInput } from '@ionic/react';
import {
  Elements,
  useStripe
} from '@stripe/react-stripe-js';
//stripe
import { loadStripe } from '@stripe/stripe-js';
import { stripePublickeys } from 'data/choice';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/functions';
//firebase
import firebase from 'firebase/index';
import React from 'react';
import { useHistory } from 'react-router';
let zenginCode = require('zengin-code');

interface bankProps {
  country: string;
  currency: string;
  bank_number: string;
  branch_number: string;
  account_holder_name: string;
  account_holder_type: string;
  account_number: string;
}

const StripeForm: React.FC = () => {
  const history = useHistory();

  const stripe = useStripe();

  const [bank, setBank] = React.useState<bankProps>({
    country: "JP",
    currency: "jpy",
    bank_number: "0001",
    branch_number: "054",
    account_holder_name: 'カ）ROND',
    account_holder_type: 'individual',
    account_number: '4695851',
  });

  async function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();

    const func = firebase.functions();
    const regist = func.httpsCallable('createAttorneyBank');

    const res = await stripe?.createToken('bank_account', {
      country: bank.country,
      currency: bank.currency,
      routing_number: bank.bank_number + bank.branch_number,
      account_holder_name: bank.account_holder_name,
      account_holder_type: bank.account_holder_type,
      account_number: bank.account_number,
    });
    await regist(res?.token?.id);
    return history.push('/attorney/bank/check');
  }

  const bankList = React.useMemo(() => {
    return Object.values(zenginCode).map((element: any) => {
      return <option value={element.name} className="form__select__option">{element.name}</option>
    });
  }, []);

  return (
    <form method="post" onSubmit={handleSubmit}>
      <div className="form__title__wrap__flex">
        <span className="form__title__20">入金先口座を変更する</span><span className="form__badge--required">必須</span>
      </div>

      <p className="form--bank__formtype">銀行</p>
      <label htmlFor="1" className="form__select--deco">
        <select className="form__select" name="銀行" id="1" onChange={(e) => {
          e.preventDefault();
          setBank({
            ...bank,
            bank_number: e.target.value
          });

        }}>
          <option value="" className="form__select__option">選択してください</option>
          {bankList}
        </select>
      </label>

      <p className="form--bank__formtype">口座種別</p>
      <label htmlFor="1" className="form__select--deco">
        <select className="form__select" name="銀行" id="1" value={bank?.account_holder_type} onChange={(e) => {
          setBank({
            ...bank,
            account_holder_type: e.target.value
          });

        }}>
          <option value="" className="form__select__option">選択してください</option>
          <option value="普通預金" className="form__select__option">普通預金</option>
          <option value="当座預金" className="form__select__option">当座預金</option>
          <option value="貯蓄預金" className="form__select__option">貯蓄預金</option>
        </select>
      </label>
      <p className="form--bank__formtype">支店コード</p>
      <IonInput
        placeholder="例)000"
        className="form__input"
        type="text"
        onIonChange={(e) => {
          e.preventDefault();
          // setBank({
          //   ...bank,
          //   fingerprint: e.detail.value
          // });

        }} />
      <p className="form--bank__formtype">口座番号</p>
      <IonInput
        placeholder="例)000000"
        className="form__input"
        type="text"
        value={bank?.account_number}
        onIonChange={(e) => {
          e.preventDefault();
          setBank({
            ...bank,
            account_number: e.detail.value!
          });

        }} />
      <p className="form--bank__formtype">口座名義</p>
      <IonInput
        placeholder="例)ベンリシ タロウ"
        className="form__input"
        type="text"
        value={bank?.account_holder_name}
        onIonChange={(e) => {
          e.preventDefault();
          setBank({
            ...bank,
            account_holder_name: e.detail.value!
          });

        }} />
      <p className="form--bank__undertext">25日までに変更した場合、翌月の支払いは新しい口座に振り込まれます。26日以降の場合は翌々月以降となります。</p>
      <div className=" SP__btn__center">
        <button className="Btn__base SP__btn" type="submit">
          内容を確認する
        </button>
      </div>
    </form>
  );
};
const stripePromise = loadStripe(stripePublickeys.test);
const BankForm: React.FC = () => (
  <Elements stripe={stripePromise}>
    <StripeForm />
  </Elements>
);
export default BankForm;
