// img
import ProfileCamera from 'assets/img/noimage.svg';
//firebase
import firebase from 'firebase';
import 'firebase/firestore';
import 'firebase/storage';
import React from 'react';

interface stateProps {
  url: any;
  name: string;
  type: string;
}

interface actionProps {
  type: "IMAGE" | "NAME" | "TYPE";
  payload: any;
}

interface Props {
  thumbnailUrl?: string;
}

export let initialImage: stateProps = {
  url: ProfileCamera,
  name: "",
  type: ""
}

export function photoReducer(state: stateProps, action: actionProps) {
  switch (action.type) {
    case "IMAGE":
      return {
        ...state,
        url: action.payload
      }
    case "NAME":
      return {
        ...state,
        name: action.payload
      }
    case "TYPE":
      return {
        ...state,
        type: action.payload
      }
    default:
      return state;
  }
}

export const ProfileImg: React.FC = () => {
  const [photo, setPhoto] = React.useState<stateProps>({
    url: ProfileCamera,
    name: "",
    type: ""
  });
  const imgInput = React.useRef<HTMLInputElement>(null);

  async function handleChange(e: React.ChangeEvent<HTMLInputElement>) {
    e.preventDefault();
    const reader = new FileReader();
    let file = imgInput.current!.files![0];
    reader.onload = () => {
      setPhoto({
        ...photo,
        url: reader.result,
        type: file.type,
        name: file.name
      });
    }
    reader.readAsDataURL(file);
  }

  React.useEffect(() => {
    const auth = firebase.auth().currentUser;
    const storage = firebase.storage().ref();
    const storageRef = storage.child("profile/" + `${auth!.uid}`);
    storageRef.getDownloadURL().then((snap: firebase.firestore.DocumentSnapshot) => {
      setPhoto({
        ...photo,
        url: snap
      });
    });
  }, [setPhoto, photo]);

  return (
    <div className="form__profile__wrap">
      <div className="form__profile">
        <label className="form__profile__img" htmlFor="img">
          <figure className="camera" style={{ backgroundImage: `url(${photo?.url})` }}></figure>
        </label>
        <input type="file" className="form__profile__input" id="img" ref={imgInput} onChange={handleChange} />
        <div className="form__profile__text">
          <p className="heading">プロフィール画像</p>
          <span className="disc">アプリ内のサムネイルに設定されます。<br />※2MB以下の画像を選択してください</span>
        </div>
      </div>
    </div>
  );
};


export default ProfileImg;
