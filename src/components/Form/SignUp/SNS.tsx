import 'assets/css/Btn.css';
// css
import 'assets/css/style.css';
import React from 'react';




export const SignUpSNS: React.FC = () => {
  return (
    <form action="">
      {/* <SnsBtn /> */}
      <div className="auth--regist--undertext">
        <span className="auth--regist__disc">「登録」ボタンをクリックすることで、<span className="auth--link--primary">ご利用規約</span>・<span className="auth--link--primary">プライバシーポリシー</span>に同意したものとみなします。</span>
      </div>
      <div className="btn--center">
        <button className="Btn__base PC__btn" type="submit">新規登録</button>
      </div>
    </form>
  );
};

export default SignUpSNS;
