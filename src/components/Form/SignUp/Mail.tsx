//ionic
import { IonInput } from "@ionic/react";
//css
import 'assets/css/Btn.css';
import React from 'react';



interface Props {
  main: string;
  check: string;
}

export const SignUpMail: React.FC = () => {
  //メール メインと確認用
  const [email, setEmail] = React.useState<Props>({
    main: "",
    check: ""
  });
  //パスワード メインと確認用
  const [password, setPassword] = React.useState<string>();
  //確認用emailが合っていたときとそうでない時のstate
  const [complete, setComplete] = React.useState<boolean>();
  //mailフォームにするか、snsフォームにするか


  async function handleMailSignupSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    // return await firebase.auth().sendSignInLinkToEmail(email.main, password);
  }

  return (
    <form action="post" className="container" onSubmit={handleMailSignupSubmit}>
      <p className="auth__input__text">メールアドレス</p>
      <IonInput
        type="email"
        className="auth__input"
        name="email"
        id="email"
        value={email.main}
        autocomplete="email"
        onIonChange={(e): void => {
          e.preventDefault();
          setEmail({
            ...email,
            main: e.detail.value!
          });
        }} />
      <p className="auth__input__text">メールアドレス（確認用）</p>
      <IonInput
        type="email"
        className="auth__input"
        name="email"
        id="email"
        value={email.check}
        autocomplete="email"
        onIonChange={(e): void => {
          e.preventDefault();
          setEmail({
            ...email,
            check: e.detail.value!
          });
        }} />
      <p className="auth__input__text">パスワード</p>
      <IonInput
        className="auth__input"
        type="password"
        name="password"
        id="password"
        placeholder=""
        value={password}
        autocomplete="new-password"
        onIonChange={(e): void => {
          e.preventDefault();
          setPassword(e.detail.value!);
        }} />
      <span className="auth--regist__disc">※半角英数字を含む8〜20桁で入力して下さい。</span>
      <div className="btn--center">
        <button className="Btn__base PC__btn" type="submit">新規登録</button>
      </div>
    </form>
  );
}
