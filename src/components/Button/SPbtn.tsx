import 'assets/css/Btn.css';

const SPbtn: React.FC = ({ children }) => {

  return (
    <div className=" SP__btn__center">
      <button className="Btn__base SP__btn">
        {/* {children} */}ボタン
      </button>
    </div>
  );
};

export default SPbtn;
