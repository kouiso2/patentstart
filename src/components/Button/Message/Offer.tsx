import { IconButton } from '@material-ui/core';
import Hoverimg from 'assets/img/message/offer-hover.svg';
import MessageOffer from 'assets/img/message/pc-message-offer.svg';
import MessageOfferButtonSP from 'assets/img/message/sp-message-offer.svg';
import firebase from 'firebase';
import React from 'react';
import { Link } from 'react-router-dom';

interface Props {
  hoverFlag?: boolean;
  roomId: string;
}

/**
* MessageOfferButton:
* 弁理士からのオファーページにリンクするボタン。チャットルームのインプットエリア内に表示される
* MessageInputコンポーネント内で呼び出される
*/
const MessageOfferButton: React.FC<Props> = ({ roomId }) => {
  const [hover, setHover] = React.useState<boolean>(false);
  const toggle = () => setHover(!hover);

  const [app, setApp] = React.useState<string>("");

  React.useEffect(() => {
    firebase.firestore().collection("Rooms").doc(roomId).onSnapshot((snap) => {
      setApp(snap.data()?.application.path)
    })
  }, [setApp, roomId]);

  return (
    <React.StrictMode>
      <Link to={{
        pathname: "/attorney/offer/" + roomId + "/",
        state: app
      }} className="MessageInputSP__menu__child">
        <IconButton
          component="label"
        >
          {/* pcの時 */}
          <div className="pc hover__img__position">
            <img className={hover ? 'isHoverShow hover__img hover__img--3' : 'isHoverHide hover__img hover__img--3'} src={Hoverimg} alt="" />
            <img src={MessageOffer} alt=""
              onMouseEnter={toggle}
              onMouseLeave={toggle}
            />
          </div>
          {/* spの時 */}
          <figure className="MessageInputSP__menu__child__img sp"><img src={MessageOfferButtonSP} alt="" />
            <p className="MessageInputSP__menu__child__img__caption">オファーする</p>
          </figure>
        </IconButton>
      </Link>
    </React.StrictMode>
  );

};

export default MessageOfferButton;
