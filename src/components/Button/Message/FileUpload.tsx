import { isPlatform } from '@ionic/react';
import { IconButton } from '@material-ui/core';
import Hoverimg from 'assets/img/message/addImg-hover.svg';
import InsertPhoto from 'assets/img/message/pc-message-album.svg';
import MessagePhotoButtonSP from 'assets/img/message/sp-message-photo.svg';
import Compressor from 'compressorjs';
import useAuth from 'hooks/auth';
import React from 'react';
// import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';

type Props = {
  setText: any,
  setFilePath: any,
  setState: any,
  setFile: any
}

/**
* FileUploadButton:
* ファイルをアップロードするボタンが表示される。また、ファイルアップロード時のバリデーションやファイルの圧縮などの処理も記述されている
* MessageInputコンポーネント内で呼び出される
*/
const FileUploadButton: React.FC<Props> = ({ setText, setFilePath, setState, setFile }) => {
  const auth = useAuth();
  const [hover, setHover] = React.useState<boolean>(false);
  const toggle = () => setHover(!hover);
  
  //現在の時刻を文字列として取得する関数
  function getNowDateTimeStr() {
    const date = new Date();
    const Y = date.getFullYear();
    const M = ("00" + (date.getMonth() + 1)).slice(-2);
    const D = ("00" + date.getDate()).slice(-2);
    const h = ("00" + date.getHours()).slice(-2);
    const m = ("00" + date.getMinutes()).slice(-2);
    const s = ("00" + date.getSeconds()).slice(-2);

    return Y + '-' + M + '-' + D + '_' + h + ':' + m + ':' + s;
  }

  /**
  * return(戻り値):
  * ファイルのアップロードボタンを表示している
  * また、ファイルアップロード時のファイル形式に応じたバリデーションや圧縮の処理を施している。
  */
  return (
    <IconButton className={`menu__fileUpload MessageInputSP__menu__child ${(auth?.kind === "Applicant" || (isPlatform("ios") || isPlatform("android"))) ? `MessageInputSP__menu__child—-applicant` : ``}`} component="label"
    //   onClick={() => {
    // saveMessage({ state: "text" ,text });
    // setText('');
    // inputEl.current.focus();z
    //   }}
    >
      {/* <figure className="menu__fileUpload"> */}
      {/* pcの時 */}
      <div className="pc hover__img__position">
        <img className={hover ? 'isHoverShow hover__img hover__img--2' : 'isHoverHide hover__img hover__img--2'} src={Hoverimg} alt="" />
        <img src={InsertPhoto} alt=""
          onMouseEnter={toggle}
          onMouseLeave={toggle}
        />
      </div>

      {/* spの時 */}
      <figure className="MessageInputSP__menu__child__img sp"><img src={MessagePhotoButtonSP} alt="" />
        <p className="MessageInputSP__menu__child__img__caption">画像追加</p>
      </figure>

      <input
        type="file"
        style={{ display: "none" }}
        onChange={(e) => {
          var file = e.target.files![0];
          if (file !== undefined) {
            var type = file.type; // MIMEタイプ
            var size = file.size; // ファイル容量（byte）
          } else {
            return;
          }
          var limit1M = 1000000; // byte, 1MB
          var limit10M = 10000000; // byte, 10MB

          //決められたファイル形式以外ははじくようにする。
          if (type !== "image/png" && type !== "image/jpeg" && type !== "video/mp4" && type !== "video/quicktime" && type !== "application/pdf" &&
            type !== "application/msword" && type !== "application/vnd.openxmlformats-officedocument.wordprocessingml.document" &&
            type !== "application/vnd.ms-excel" && type !== "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" &&
            type !== "application/vnd.ms-powerpoint" && type !== "application/vnd.openxmlformats-officedocument.presentationml.presentation") {
            e.target.value = "";
            alert('ファイルはpng,jpeg,mp4,mov,pdf,docx,xlsx,pptxのいずれかの形式でアップロードしてください');
            return;
          }

          //画像の場合
          if (type === "image/png" || type === "image/jpeg") {

            new Compressor(file, {//画像圧縮
              quality: 0.6,
              success(result: Blob): void {
                if (result.size > limit1M) {
                  e.target.value = "";
                  alert('画像サイズは1MB以下で指定してください');
                  return;
                }
                setState('normalFile');
                setText(file.name);
                setFilePath("message/" + auth?.uid + '/' + getNowDateTimeStr() + '_' + file.name);
                setFile(result);
              },
              maxWidth: 1000,
              maxHeight: 400,
              mimeType: type,
              error(err: Error): void { //エラー処理

              },
            });

          } else {//画像以外の場合
            if (type === "video/mp4" || type === "video/quicktime") {
              if (size > limit10M) {
                e.target.value = "";
                alert('動画サイズは10MB以下で指定してください');
                return;
              }
              setState('video');
            }
            if (type === "application/pdf") {
              if (size > limit10M) {
                e.target.value = "";
                alert('PDFは10MB以下で指定してください');
                return;
              }
              setState('pdf');
            }
            if (type === "application/msword" || type === "application/vnd.openxmlformats-officedocument.wordprocessingml.document") {
              if (size > limit1M) {
                e.target.value = "";
                alert('Wordファイルは1MB以下で指定してください');
                return;
              }
              setState('word');
            }
            if (type === "application/vnd.ms-excel" || type === "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
              if (size > limit1M) {
                e.target.value = "";
                alert('Excelファイルは1MB以下で指定してください');
                return;
              }
              setState('excel');
            }
            if (type === "application/vnd.ms-powerpoint" || type === "application/vnd.openxmlformats-officedocument.presentationml.presentation") {
              if (size > limit1M) {
                e.target.value = "";
                alert('PowerPointファイルは1MB以下で指定してください');
                return;
              }
              setState('powerpoint');
            }

            setText(file.name);
            setFilePath("message/" + auth?.uid + '/' + getNowDateTimeStr() + '_' + file.name);
            setFile(file);
          }

        }}
      />
      {/* </figure> */}
    </IconButton>
  );
};

export default FileUploadButton;
