import { isPlatform } from '@ionic/react';
import { IconButton } from '@material-ui/core';
import Hoverimg from 'assets/img/message/complete-hover.svg';
import MessageMenu from 'assets/img/message/pc-complete.svg';
import CompleteSP from 'assets/img/message/sp-complete.svg';
//firebase
import firebase from 'firebase';
import 'firebase/firestore';
import "firebase/storage";
import useAuth from 'hooks/auth';
import React from 'react';
import { Link, useParams } from 'react-router-dom';

const db = firebase.firestore();
const roomsRef = db.collection("Rooms");

const saveMessage = (senderId: string, roomId: string) => {
  const messagesRef = roomsRef.doc(roomId).collection("Messages");
  return messagesRef.add({
    senderId: senderId,
    state: 'complete',
    text: '申請完了の手続きをしますか？',
    timestamp: firebase.firestore.FieldValue.serverTimestamp()
  }).catch(function (error: any) {
    console.error('Error writing new message to database', error);
  });
}
const updateApplicantRoom = (roomId: string) => {
  return roomsRef.doc(roomId).update({
    lastMessageText: '申請完了の手続きをしますか？',
    lastMessageTimestamp: firebase.firestore.FieldValue.serverTimestamp(),
    applicantUnreadNumber: firebase.firestore.FieldValue.increment(1),
  }).catch(function (error: any) {
    console.error('Error writing update room to database', error);
  });
}
const updateAttorneyRoom = (roomId: string) => {
  return roomsRef.doc(roomId).update({
    lastMessageText: '申請完了の手続きをしますか？',
    lastMessageTimestamp: firebase.firestore.FieldValue.serverTimestamp(),
    attorneyUnreadNumber: firebase.firestore.FieldValue.increment(1),
  }).catch(function (error: any) {
    console.error('Error writing update room to database', error);
  });
}

/**
* MessageCompleteButton:
* 弁理士が申請を完了するページにリンクするボタン。チャットルームのインプットエリア内に表示される
* MessageInputコンポーネント内で呼び出される
*/
const MessageCompleteButton: React.FC = () => {
  const auth = useAuth();
  interface ParamTypes {
    id: string
  }
  const { id } = useParams<ParamTypes>();
  const roomId = id;
  const [hover, setHover] = React.useState<boolean>(false);
  const toggle = () => setHover(!hover);

  /**
  * return(戻り値):
  * 申請完了送信ボタンを表示する
  * onClick内では、Cloud Firestoreにデータを保存する処理
  */
  return (
    <Link to="#"
      className={`MessageInputSP__menu__child ${(auth?.kind === "Applicant" || (isPlatform("ios") || isPlatform("android"))) ? `MessageInputSP__menu__child—-applicant` : ``}`}
    >
      <IconButton
        component="label"
        onClick={() => {
          if (window.confirm('申請完了のメッセージを送りますか？')) {
            // OKが押された際に実行する処理
            saveMessage(auth?.uid!, roomId);
            if (auth?.kind === "Applicant") {
              //roomコレクションのlastMessageTextとlastMessageTimestampを更新する。
              //ログインユーザーが申請者だから、弁理士側の未読数(attorneyUnreadNumber)を+1する。
              updateAttorneyRoom(roomId);
            } else if (auth?.kind === "Attorney") {
              //roomコレクションのlastMessageTextとlastMessageTimestampを更新する。
              //ログインユーザーが弁理士だから、申請者側の未読数(applicantUnreadNumber)を+1する。
              updateApplicantRoom(roomId);
            }
          }
        }}
      >
        {/* pcの時 */}
        <div className="pc hover__img__position">
          <img className={hover ? 'isHoverShow hover__img hover__img--4' : 'isHoverHide hover__img hover__img--4'} src={Hoverimg} alt="" />
          <img src={MessageMenu} alt=""
            onMouseEnter={toggle}
            onMouseLeave={toggle}
          />
        </div>
        {/* spの時 */}
        <figure className="MessageInputSP__menu__child__img sp"><img src={CompleteSP} alt="" />
          <p className="MessageInputSP__menu__child__img__caption">申請を完了する</p>
        </figure>
      </IconButton>
    </Link>
  );
};

export default MessageCompleteButton;
