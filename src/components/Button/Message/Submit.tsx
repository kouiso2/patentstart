import { IconButton } from '@material-ui/core';
import SendIconRed from 'assets/img/message/message-submit-button-red.svg';
import SendIcon from 'assets/img/message/message-submit-button.svg';
//firebase
import firebase from 'firebase';
import 'firebase/firestore';
import "firebase/storage";
import useAuth from 'hooks/auth';
import React from 'react';
import { useParams } from 'react-router-dom';




const db = firebase.firestore();
const roomsRef = db.collection("Rooms");

const saveMessage = (state: string, text: string, filePath: string, senderId: string, roomId: string) => {
  const messagesRef = roomsRef.doc(roomId).collection("Messages");
  return messagesRef.add({
    senderId: senderId,
    state: state,
    text: text,
    filePath: filePath,
    timestamp: firebase.firestore.FieldValue.serverTimestamp()
  }).catch(function (error: any) {
    console.error('Error writing new message to database', error);
  });
}
const updateApplicantRoom = (roomId: string, text: string) => {
  return roomsRef.doc(roomId).update({
    lastMessageText: text,
    lastMessageTimestamp: firebase.firestore.FieldValue.serverTimestamp(),
    applicantUnreadNumber: firebase.firestore.FieldValue.increment(1),
  }).catch(function (error: any) {
    console.error('Error writing update room to database', error);
  });
}
const updateAttorneyRoom = (roomId: string, text: string) => {
  return roomsRef.doc(roomId).update({
    lastMessageText: text,
    lastMessageTimestamp: firebase.firestore.FieldValue.serverTimestamp(),
    attorneyUnreadNumber: firebase.firestore.FieldValue.increment(1),
  }).catch(function (error: any) {
    console.error('Error writing update room to database', error);
  });
}
// const saveImage = ({image}: {image: any}) => {
//   const storageRef = firebase.storage().ref();
//   const imgRef = storageRef.child('message/'+image.name);
//   imgRef.put(image).then(function(snap) {
//     imgRef.getDownloadURL().then(function(url){
//       setText(url);
//     });
//   });;
// }

type Props = {
  inputEl: any,
  setText: any,
  text: string,
  filePath: string,
  setState: any,
  state: string,
  file: any,
}

/**
* MessageSubmitButton:
* チャットルームのインプットエリア内のメッセージ送信ボタンを表示する
* MessageInputコンポーネント内で呼び出される
*/
const MessageSubmitButton: React.FC<Props> = ({ inputEl, setText, text, filePath, setState, state, file }) => {
  const auth = useAuth();

  interface ParamTypes {
    id: string
  }
  const { id } = useParams<ParamTypes>();
  const roomId = id;

  async function saveFile(file: any) {
    const storageRef = firebase.storage().ref();
    const imgRef = storageRef.child(filePath);
    imgRef.put(file).then(function () {
      saveMessage(state, text, filePath, auth?.uid!, roomId);
    });
    // .then(function(snap) {
    //   imgRef.getDownloadURL().then(function(imgUrl){
    //     setUrl(imgUrl);
    //   });
    // });;
  }

  /**
  * return(戻り値):
  * メッセージ送信ボタンを表示する
  * onClick内では、送信するメッセージの種類に応じた処理を記述している
  * メッセージの種類が通常のテキストの場合はCloud Firestoreに保存。ファイルの場合はFirebase StorageとCloud Firestoreに保存するという処理を施している
  */
  return (
    <IconButton
      disabled={text === ''}
      onClick={() => {
        if (state === "text") {
          saveMessage(state, text, filePath, auth?.uid!, roomId);
        } else if (state === "normalFile" || state === "video" || state === "pdf" || state === "word" || state === "excel" || state === "powerpoint") {
          saveFile(file);
        }
        if (auth?.kind === "Applicant") {
          //roomコレクションのlastMessageTextとlastMessageTimestampを更新する。
          //ログインユーザーが申請者だから、弁理士側の未読数(attorneyUnreadNumber)を+1する。
          updateAttorneyRoom(roomId, text);
        } else if (auth?.kind === "Attorney") {
          //roomコレクションのlastMessageTextとlastMessageTimestampを更新する。
          //ログインユーザーが弁理士だから、申請者側の未読数(applicantUnreadNumber)を+1する。
          updateApplicantRoom(roomId, text);
        }

        setText('');
        setState('text');
        inputEl.current.focus();
      }}
    >
      {(text === '') ? <img className="SendIconRed" src={SendIcon} alt="" /> : <img className="SendIconRed" src={SendIconRed} alt="" />}

    </IconButton>
  );
};

export default MessageSubmitButton;
