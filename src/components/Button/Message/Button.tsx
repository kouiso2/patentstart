import { isPlatform } from '@ionic/react';
import { IconButton } from '@material-ui/core';
import Hoverimg from 'assets/img/message/album-hover.svg';
import MessageMenu from 'assets/img/message/pc-message-menu.svg';
import MessageAlbumButtonSP from 'assets/img/message/sp-message-album.svg';
import useAuth from 'hooks/auth';
import React from 'react';
import { Link } from 'react-router-dom';
type Props = {
  roomId: string,
}

/**
* MessageMenuButton:
* アルバムページにリンクするボタンを表示する(名前が分かりにくい・・・)
* MessageInputコンポーネント内で呼び出される
*/
const MessageMenuButton: React.FC<Props> = ({ roomId }) => {
  const auth = useAuth();
  const [hover, setHover] = React.useState<boolean>(false);
  const toggle = () => setHover(!hover);

  return (
    <Link to={auth?.url + "/album/" + roomId + "/"}
      className={`MessageInputSP__menu__child ${(auth?.kind === "Applicant" || (isPlatform("ios") || isPlatform("android"))) ? `MessageInputSP__menu__child—-applicant` : ``}`}
    >
      <IconButton
        component="label"
      >
        {/* pcの時 */}
        <div className="pc hover__img__position">
          <img className={hover ? 'isHoverShow hover__img hover__img--1' : 'isHoverHide hover__img hover__img--1'} src={Hoverimg} alt="" />
          <img src={MessageMenu} alt=""
            onMouseEnter={toggle}
            onMouseLeave={toggle}
          />
        </div>
        {/* spの時 */}
        <figure className="MessageInputSP__menu__child__img sp"><img src={MessageAlbumButtonSP} alt="" />
          <p className="MessageInputSP__menu__child__img__caption">アルバム</p>
        </figure>
      </IconButton>
    </Link>
  );
};

export default MessageMenuButton;
