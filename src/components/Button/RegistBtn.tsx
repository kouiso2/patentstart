//css
import 'assets/css/Btn.css';
import React from 'react';
import { Link } from 'react-router-dom';



const RegistBtn: React.FC = () => {

  return (
    <div className="btn--center">
      <Link to="/auth/signup/"><button className="Btn__base PC__btn" >新規登録</button></Link>
    </div>
  );
};

export default RegistBtn;
