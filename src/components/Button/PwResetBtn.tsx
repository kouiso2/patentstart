import 'assets/css/Btn.css';
import React from 'react';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';


const PwResetBtn: React.FC = () => {
  const history = useHistory();

  const handleClick = React.useCallback((e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault();
    return history.push('/auth/login');
  }, [history]);

  return (
    <div style={{ marginTop: "4rem" }}>
      <div className="btn--center">
        <button className="Btn__base PC__btn" onClick={handleClick}>次へ</button>
      </div>
      <Link to="/auth/top/" style={{ textAlign: 'center', marginTop: '20px' }} className="auth--regist__disc">トップページに戻る</Link>
    </div>
  );
};

export default PwResetBtn;
