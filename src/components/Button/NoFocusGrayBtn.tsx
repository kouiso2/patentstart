import 'assets/css/Btn.css';



const NoFocusGrayBtn: React.FC = () => {
  return (
    <div className="btn--center btn--regist">
      <button id="FocusBtn" className="Btn__base PC__btn btn--regist__btn">次へ</button>
    </div>
  );
};

export default NoFocusGrayBtn;
