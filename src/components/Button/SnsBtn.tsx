//google
// エラーが出るのでコメントアウト
// import "@codetrix-studio/capacitor-google-auth";
import 'assets/css/Btn.css';
//css
import 'assets/css/style.css';
import React from 'react';
import useLogin from '../../hooks/login';

const SnsBtn: React.FC = () => {
  const { handleGoogleLogin, handleTwitterLogin, handleAppleLogin, handleFacebookLogin } = useLogin();
  return (
    <React.StrictMode>
      <div className="btn--center">
        <button className="btn--sns btn--sns--google" onClick={handleGoogleLogin}>Googleでサインイン</button>
      </div>
      <div className="btn--center">
        <button className="btn--sns btn--sns--twitter" onClick={handleTwitterLogin}>Twitterでサインイン</button>
      </div>
      <div className="btn--center">
        <button className="btn--sns btn--sns--facebook" onClick={handleFacebookLogin}>Facebookでサインイン</button>
      </div>
      <div className="btn--center">
        <button className="btn--sns btn--sns--apple" onClick={handleAppleLogin}>Appleでサインイン</button>
      </div>
    </React.StrictMode>
  );
};
export default SnsBtn;
