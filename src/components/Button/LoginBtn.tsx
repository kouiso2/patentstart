import 'assets/css/Btn.css';
import React from 'react';
import { Link } from 'react-router-dom';



const LoginBtn: React.FC = () => {

  return (
    <div className="btn--center">
      <Link to="/auth/login/"><button type="button" className="Btn__base__login PC__btn">ログイン</button></Link>
    </div>
  );
};

export default LoginBtn;
