import 'assets/css/Btn.css';




const PCbtn: React.FC = ({ children }) => {

  return (
    <div className=" SP__btn__center">
      <button className="Btn__base PC__btn">{children}</button>
    </div>
  );
};

export default PCbtn;
