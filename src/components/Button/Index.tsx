import { IonRouterLink } from '@ionic/react';
import 'assets/css/Btn.css';
import React from 'react';

export const BtnColor = [
  'primary',
  'white',
  'gray'
] as const;
export const BtnSize = [
  '278',
  '280',
  '100per'
] as const;
export const BtnType = [
  'sns',
  'two',
  'one'
] as const;

interface BtnProps {
  children?: React.ReactNode;
  color?: typeof BtnColor[number];
  size?: typeof BtnSize[number];
  href: string;
  type: typeof BtnType[number];
}

export const CommonBtn: React.FC<BtnProps> = ({ children, color, size, href, type }) => {

  const BtnSize = React.useMemo(() => {
    if (size === '278') {
      return (
        ' SP__btnMin '
      );
    } else if (size === '280') {
      return (
        ' PC__btn '
      );
    } else {
      return (
        ' SP__btn '
      );
    }
  }, []);
  const BtnColor = React.useMemo(() => {
    if (color === 'primary') {
      return (
        ' Btn__base '
      );
    } else if (color === 'white') {
      return (
        ' Btn__base__login '
      );
    } else {
      return (
        ' Btn__base__gray '
      );
    }
  }, []);
  const BtnType = React.useMemo(() => {
    if (type === 'sns') {
      return (
        <React.StrictMode>
          <div className="btn--center">
            <button className="btn--sns btn--sns--google">Googleでサインイン</button>
          </div>
        </React.StrictMode>
      );
    } else if (type === 'two') {
      return (
        <div className="bank__info__btn-box">
          <button className="bank__info__btn">訂正する</button>
          <button className="bank__info__btn">{children}</button>
        </div>
      );
    } else {
      return (
        <div className="btn--center">
          {/* <a href={href} type="submit" className={BtnColor + BtnSize}>{children}</a> */}
          <IonRouterLink href={href + "/"} className={BtnColor + BtnSize}>{children}</IonRouterLink>
        </div>
      );
    }
  }, []);
  return (
    <React.StrictMode>
      {BtnType}
    </React.StrictMode>
  );
};



export default CommonBtn;
