import {
  Plugins,

  PushNotification,

  PushNotificationActionPerformed, PushNotificationToken
} from '@capacitor/core';
import firebase from 'firebase';
import 'firebase/firestore';
import React from 'react';

const { PushNotifications } = Plugins;
const INITIAL_STATE = {
  notifications: [{ id: 'id', title: "Test Push", body: "This is my first push notification", auth: [] }],
};

export class Push extends React.Component {
  state: any = {};
  props: any = {};
  constructor(props: any) {
    super(props);
    this.state = { ...INITIAL_STATE };
  }

  push() {
    // Capacitor.isPluginAvailable('PushNotifications');

    console.log('push func test');
    // Register with Apple / Google to receive push via APNS/FCM
    PushNotifications.register();
    // PushNotifications.requestPermission().then(result => {
    //   if (result.granted) {
    //     // Register with Apple / Google to receive push via APNS/FCM
    //     PushNotifications.register();
    //   } else {
    //     // Show some error
    //     console.log('request denied')
    //   }
    // });

    // On succcess, we should be able to receive notifications
    PushNotifications.addListener('registration',
      (token: PushNotificationToken) => {
        //Cloud Firestoreにpush通知のためのtokenを保存する
        const auth = firebase.auth().currentUser;
        firebase.firestore().collection('User').doc(auth?.uid).update({
          token: token.value
        });
        // alert('Push registration success, token: ' + token.value);
      }
    );

    // Some issue with your setup and push will not work
    PushNotifications.addListener('registrationError',
      (error: any) => {
        // alert('Error on registration: ' + JSON.stringify(error));
      }
    );

    // Show us the notification payload if the app is open on our device
    PushNotifications.addListener('pushNotificationReceived',
      (notification: PushNotification) => {
        let notif = this.state.notifications;
        notif.push({ id: notification.id, title: notification.title, body: notification.body })
        this.setState({
          notifications: notif
        })
      }
    );

    // Method called when tapping on a notification
    PushNotifications.addListener('pushNotificationActionPerformed',
      (notification: PushNotificationActionPerformed) => {
        let notif = this.state.notifications;
        notif.push({ id: notification.notification.data.id, title: notification.notification.data.title, body: notification.notification.data.body })
        this.setState({
          notifications: notif
        })
      }
    );
  }

  componentDidMount() {
    this.push();
  }

  render() {
    // const { notifications } = this.state;
    return (
      <div>
        {/* <h1>プッシュ通知テスト</h1>
        {notifications && notifications.map((notif: any) =>
          <div key={notif.id}>
            <div>
              <h3>{notif.title}</h3>
              <p>{notif.body}</p>
            </div>
          </div>
        )} */}
      </div>
    );
  };
}
export default Push;
