import React from 'react';
import 'assets/css/style.css';



const CompleteWrap: React.FC<{}> = ({ children }) => {
  return (
    // スタイルはstyle.scssに記載。
    <div className="complete complete__wrap">
      {/* display__noneで非表示 */}

      <h3 className="complete__title">依頼が完了しました</h3>
      <p className="complete__desc">依頼完了のメッセージを記載する依頼完了のメッセージを記載する依頼完了のメッセージを記載する依頼完了のメッセージを記載する依頼完了のメッセージを記載する依頼完了のメッセージを記載する</p>
      <button className="complete__button">チャットを送る</button>
      <button className="complete__button display__none">ホームへ戻る</button>
    </div>
  );
};

export default CompleteWrap;
