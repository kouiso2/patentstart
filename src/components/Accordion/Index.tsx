import 'assets/css/accordion.css';
import AccordionContentsAbout from "components/Accordion/AccordionContents/About";
import AccordionContentsApplicantInfo from "components/Accordion/AccordionContents/ApplicantInfo";
import AccordionContentsAttorneyInfo from "components/Accordion/AccordionContents/AttorneyInfo";
import AccordionContentsDetail from "components/Accordion/AccordionContents/PatentDetail";
import AccordionContentsPlan from "components/Accordion/AccordionContents/Plan";
import React from 'react';


export const AcdType = [
  'About',
  'ApplicantInfo',
  'AttorneyInfo',
  'Detail',
  'Plan'
] as const;

type Props = {
  children?: React.ReactNode;
  type: typeof AcdType[number];
}


const Accordion: React.FC<Props> = ({ type }) => {
  const accordionType = React.useMemo(() => {

    if (type === 'ApplicantInfo') {
      return (
        <AccordionContentsApplicantInfo applicantPath={""} />
      );
    } else if (type === 'AttorneyInfo') {
      return (
        <AccordionContentsAttorneyInfo attorneyPath={""} />
      );
    } else if (type === 'Detail') {
      return (
        <AccordionContentsDetail patentPath={""} />
      );
    } else if (type === 'About') {
      return (
        <AccordionContentsAbout appPath={""} />
      );
    } else {
      return (
        <AccordionContentsPlan price={0} />
      );
    }

  }, [type]);
  return (
    <div className="accordion accordion__wrap">
      {accordionType}
    </div>
  );
};

export default Accordion;
