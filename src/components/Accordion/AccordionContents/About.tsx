import { IonRouterLink } from '@ionic/react';
import AcdArrow from 'assets/img/common/acd__arrow.svg';
import InfoIcon from 'assets/img/common/info__icon.svg';
import firebase from 'firebase';
import 'firebase/storage';
import useApplication from 'hooks/accordionApplication';
import React from 'react';
import { noValueText } from '../../../controller/index';

interface Props {
  appPath: string;
}
const AccordionContentsAbout: React.FC<Props> = ({ appPath }) => {

  const { app } = useApplication(appPath);
  const [tab, setTab] = React.useState<boolean>(false);

  const content = React.useMemo(() => {
    //content
    if (tab) {
      return (
        <div className="accordion__contents accordion__contents--active">
          <div className="accordion__contents__about">
            <p className="accordion__contents__commonText accordion__contents__commonText--mb12">{app.explanation}</p>
            <h4 className="accordion__contents__commonTitle">添付PDFファイル</h4>
            <ul className="accordion__contents__commonFilelist">
              <li className="accordion__contents__commonFilelist__child">
                <IonRouterLink className="accordion__contents__commonFilelist__child__text" href={app.document} target="_blank">{noValueText(app.document, firebase.storage().refFromURL(app.document)?.name)}</IonRouterLink>
              </li>
            </ul>
          </div>
        </div>
      )
    }
  }, [tab, app]);

  const headerAllow = React.useMemo(() => {
    //↑accordion__header__arrow--activeで180度回転
    if (tab) {
      return <button className="accordion__header" onClick={(e) => {
        e.preventDefault();
        setTab(false);
      }}>
        <figure className="accordion__header__img">
          <img src={InfoIcon} alt="Information" />
        </figure>
        <h3 className="accordion__header__title">依頼内容</h3>
        <img
          className="accordion__header__arrow accordion__header__arrow--active"
          src={AcdArrow}
          alt="↑"
        />
      </button>
    } else {
      return <button className="accordion__header" onClick={(e) => {
        e.preventDefault();
        setTab(true);
      }}>
        <figure className="accordion__header__img">
          <img src={InfoIcon} alt="Information" />
        </figure>
        <h3 className="accordion__header__title">依頼内容</h3>
        <img
          className="accordion__header__arrow accordion__header__arrow"
          src={AcdArrow}
          alt="↑"
        />
      </button>

    }
  }, [tab, setTab]);

  return (
    <React.StrictMode>
      {headerAllow}
      {/* contents */}
      {content}
    </React.StrictMode>
  );
};

export default AccordionContentsAbout;
