import { isPlatform } from '@ionic/react';
import AcdArrow from 'assets/img/common/acd__arrow.svg';
import PlanIcon from 'assets/img/common/plan__icon.svg';
import React from 'react';

interface Props {
  price: number;
}

const AccordionContentsPlan: React.FC<Props> = ({ price }) => {
  const [tab, setTab] = React.useState<boolean>(false);

  const priceIosFunc = React.useCallback((price: number) => {
    if (price === 30000) {
      return "スタンダード";
    } else if (price === 50000) {
      return "ハイグレード";
    }
  }, []);

  const priceIosVariable = React.useMemo(() => {
    return isPlatform('ios') ? priceIosFunc(price) : price;
  }, [price, priceIosFunc]);

  const content = React.useMemo(() => {
    //content
    if (tab) {
      return (
        <div className="accordion__contents accordion__contents--active">
          <p className="accordion__contents__priceText">お客様は<span className="accordion__contents__priceText__red">{priceIosVariable}プラン</span>に入っています.</p>
          <p className="accordion__contents__caution">※お引き落としは毎月25日です。など、何かあればここに説明文を記載する。</p>
        </div>
      )
    }
  }, [tab, priceIosVariable]);

  const headerAllow = React.useMemo(() => {
    //↑accordion__header__arrow--activeで180度回転
    if (tab) {
      return <button className="accordion__header" onClick={(e) => {
        e.preventDefault();
        setTab(false);
      }}>
        <figure className="accordion__header__img">
          <img src={PlanIcon} alt="Information" />
        </figure>
        <h3 className="accordion__header__title">プラン</h3>
        <img
          className="accordion__header__arrow accordion__header__arrow--active"
          src={AcdArrow}
          alt="↑"
        />
      </button>
    } else {
      return <button className="accordion__header" onClick={(e) => {
        e.preventDefault();
        setTab(true);
      }}>
        <figure className="accordion__header__img">
          <img src={PlanIcon} alt="Information" />
        </figure>
        <h3 className="accordion__header__title">プラン</h3>
        <img
          className="accordion__header__arrow accordion__header__arrow"
          src={AcdArrow}
          alt="↑"
        />
      </button>
    }
  }, [tab, setTab]);

  return (
    <React.StrictMode>
      {headerAllow}
      {content}
    </React.StrictMode>
  );
};

export default AccordionContentsPlan;
