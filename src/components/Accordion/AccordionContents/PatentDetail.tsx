import { IonRouterLink } from '@ionic/react';
import EditIcon from 'assets/img/attorneyedit.svg';
import AcdArrow from 'assets/img/common/acd__arrow.svg';
import DetailIcon from 'assets/img/common/detail__icon.svg';
import { noValueText } from 'controller';
import 'firebase/storage';
import useAuth from 'hooks/auth';
import React from 'react';
import { Link, useParams } from 'react-router-dom';
import '../../../assets/css/style.css';
import usePatent from '../../../hooks/patent';

interface Props {
  patentPath: string;
}

const AccordionContentsDetail: React.FC<Props> = ({ patentPath }) => {
  const { id } = useParams<{ id: string }>();
  const auth = useAuth();
  const { patent } = usePatent(patentPath);
  const [tab, setTab] = React.useState<boolean>(false);
  const content = React.useMemo(() => {
    //content
    if (tab) {
      return (
        <div className="accordion__contents accordion__contents--active">
          <p className="accordion__contents__commonText accordion__contents__commonText--mb12">{noValueText(patent.explanation)}</p>
          <h4 className="accordion__contents__commonTitle">申請情報</h4>
          <ul className="accordion__contents__table">
            {/* 1 */}
            <li className="accordion__contents__table__child">
              <p className="accordion__contents__table__child__title">文献番号</p>
              <p className="accordion__contents__table__child__text">{noValueText(patent.referenceNum)}</p>
            </li>
            {/* 2 */}
            <li className="accordion__contents__table__child">
              <p className="accordion__contents__table__child__title">出願番号</p>
              <p className="accordion__contents__table__child__text">{noValueText(patent.fillingNum)}</p>
            </li>
            {/* 3 */}
            <li className="accordion__contents__table__child">
              <p className="accordion__contents__table__child__title">出願日</p>
              <p className="accordion__contents__table__child__text">{patent.fillingDate?.getFullYear() + "-" + ("00" + (patent.fillingDate?.getMonth() + 1)).slice(-2) + "-" + ("00" + patent.fillingDate?.getDate()).slice(-2)}</p>
            </li>
            {/* 4 */}
            <li className="accordion__contents__table__child">
              <p className="accordion__contents__table__child__title">公知日</p>
              <p className="accordion__contents__table__child__text">{patent.knownDate?.getFullYear() + "-" + ("00" + (patent.knownDate?.getMonth() + 1)).slice(-2) + "-" + ("00" + patent.knownDate?.getDate()).slice(-2)}</p>
            </li>
            {/* 5 */}
            <li className="accordion__contents__table__child">
              <p className="accordion__contents__table__child__title">発明の名称</p>
              <p className="accordion__contents__table__child__text">{noValueText(patent.developName)}</p>
            </li>
            {/* 6 */}
            <li className="accordion__contents__table__child">
              <p className="accordion__contents__table__child__title">出願人・権利者</p>
              <p className="accordion__contents__table__child__text">{noValueText(patent.rightHolder)}</p>
            </li>
            {/* 7 */}
            <li className="accordion__contents__table__child">
              <p className="accordion__contents__table__child__title">FI</p>
              <p className="accordion__contents__table__child__text">{noValueText(patent.fi)}</p>
            </li>
            {/* 8 */}
            <li className="accordion__contents__table__child">
              <p className="accordion__contents__table__child__title">J-plat pat URL</p>
              <p className="accordion__contents__table__child__text">{noValueText(patent.jplatUrl)}</p>
            </li>
          </ul>
          <h4 className="accordion__contents__commonTitle">申請書類</h4>
          <ul className="accordion__contents__commonFilelist">
            <li className="accordion__contents__commonFilelist__child">
              <IonRouterLink className="accordion__contents__commonFilelist__child__text" >{noValueText(patent.document, patent.document.name)}</IonRouterLink>
            </li>
          </ul>
        </div>
      )
    }
  }, [tab, patent]);

  const editLink = React.useMemo(() => {
    if (auth?.kind !== "Attorney") {
      return;
    };
    return (<Link to={{
      pathname: `/attorney/application/update/${id}`,
      state: patentPath,
    }} className="accordion__edit__text">編集する
      <img src={EditIcon} alt="" /></Link>)
  }, [auth, patentPath, id]);

  const headerAllow = React.useMemo(() => {
    //↑accordion__header__arrow--activeで180度回転
    if (tab) {
      return <button className="accordion__header" onClick={(e) => {
        e.preventDefault();
        setTab(false);
      }}>
        <figure className="accordion__header__img">
          <img src={DetailIcon} alt="Information" />
        </figure>
        <h3 className="accordion__header__title">特許の詳細</h3>
        {editLink}
        <img
          className="accordion__header__arrow accordion__header__arrow--active"
          src={AcdArrow}
          alt="↑"
        />
      </button>
    } else {
      return <button className="accordion__header" onClick={(e) => {
        e.preventDefault();
        setTab(true);
      }}>
        <figure className="accordion__header__img">
          <img src={DetailIcon} alt="Information" />
        </figure>
        <h3 className="accordion__header__title">特許の詳細</h3>
        {editLink}
        <img
          className="accordion__header__arrow accordion__header__arrow"
          src={AcdArrow}
          alt="↑"
        />
      </button>
    }
  }, [tab, setTab, editLink]);


  return (
    <React.StrictMode>
      {headerAllow}
      {/* contents */}
      {content}
    </React.StrictMode>
  );
};

export default AccordionContentsDetail;
