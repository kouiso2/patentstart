/* eslint-disable jsx-a11y/anchor-has-content */
import AcdArrow from 'assets/img/common/acd__arrow.svg';
import InfoIcon from 'assets/img/common/info__icon.svg';
import useApplication from 'hooks/application';
import React from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../../../store/Combine';

const AccordionContentsAbout: React.FC = () => {
  const config = useSelector((state: RootState) => state.config);
  const { app } = useApplication(config.application.path);
  const [tab, setTab] = React.useState<boolean>(false);

  const headerAllow = React.useMemo(() => {
    //↑accordion__header__arrow--activeで180度回転
    if (tab) {
      return <button className="accordion__header" onClick={(e) => {
        e.preventDefault();
        setTab(false);
      }}>
        <figure className="accordion__header__img">
          <img src={InfoIcon} alt="Information" />
        </figure>
        <h3 className="accordion__header__title">依頼内容</h3>
        <img
          className="accordion__header__arrow accordion__header__arrow--active"
          src={AcdArrow}
          alt="↑"
        />
      </button>
    } else {
      return <button className="accordion__header" onClick={(e) => {
        e.preventDefault();
        setTab(true);
      }}>
        <figure className="accordion__header__img">
          <img src={InfoIcon} alt="Information" />
        </figure>
        <h3 className="accordion__header__title">依頼内容</h3>
        <img
          className="accordion__header__arrow accordion__header__arrow"
          src={AcdArrow}
          alt="↑"
        />
      </button>
    }
  }, [tab, setTab]);

  return (
    <React.StrictMode>
      {headerAllow}
      {/* contents */}
      <div className="accordion__contents accordion__contents--active">
        <div className="accordion__contents__about">
          <p className="accordion__contents__commonText accordion__contents__commonText--mb12">{app?.explanation}</p>
          <h4 className="accordion__contents__commonTitle">添付PDFファイル</h4>
          <ul className="accordion__contents__commonFilelist">
            <li className="accordion__contents__commonFilelist__child">
              <a target="_blank" className="accordion__contents__commonFilelist__child__text" href={app?.document} rel="noreferrer"></a>
            </li>
          </ul>
        </div>
      </div>
    </React.StrictMode>
  );
};

export default AccordionContentsAbout;
