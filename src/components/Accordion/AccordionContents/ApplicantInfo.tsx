import { IonRouterLink } from '@ionic/react';
import AcdArrow from 'assets/img/common/acd__arrow.svg';
import AttorneyInfo from 'assets/img/common/user__icon.svg';
import 'firebase/firestore';
import useProfile from 'hooks/profile';
import React from 'react';

interface Props {
  applicantPath: string;
}

const AccordionContentsApplicantInfo: React.FC<Props> = ({ applicantPath }) => {
  const { profile } = useProfile(applicantPath);
  const [tab, setTab] = React.useState<boolean>(false);

  const content = React.useMemo(() => {
    //content
    if (tab) {
      return (
        <div className="accordion__contents accordion__contents--active">
          {/* プロフィール */}
          <div className="accordion__contents__profile">
            <figure className="accordion__contents__profile__img" style={{ backgroundImage: `url(${profile?.image})` }}></figure>
            <div className="accordion__contents__profile__text">
              <h4 className="accordion__contents__commonTitle accordion__contents__profile__text__kanji">{profile?.companyName}</h4>
              <p className="accordion__contents__profile__text__kana">{profile?.companyKana}</p>
              <div className="accordion__contents__profile__text__flex">
                <span className="accordion__contents__profile__text__flex__child--applicant dislay__active">法人</span>
                {/* dislay__activeで非表示。なければ非表示 */}
              </div>
            </div>
          </div>
          {/* end プロフィール */}
          <div className="accordion__contents__flex">
            <div className="accordion__contents__flex__child">
              <h4 className="accordion__contents__commonTitle">担当者</h4>
              <p className="accordion__contents__commonText accordion__contents__commonText--mb12">
                <span className="main__patent__genre">{profile?.firstName + profile?.lastName}</span></p>
            </div>
            <div className="accordion__contents__flex__child">
              <h4 className="accordion__contents__commonTitle">ホームページ・SNS等</h4>
              <IonRouterLink href={profile?.homepage + "/"} target="_blank" className="accordion__contents__commonText accordion__contents__commonText--mb12 accordion__contents__externalLink">{profile?.homepage}</IonRouterLink>
            </div>
          </div>

          <h4 className="accordion__contents__commonTitle">自己紹介</h4>
          <p className="accordion__contents__commonText accordion__contents__commonText--mb12">{profile?.self}</p>
        </div>
      )
    }
  }, [profile, tab]);

  const headerAllow = React.useMemo(() => {
    //↑accordion__header__arrow--activeで180度回転
    if (tab) {
      return <button className="accordion__header" onClick={(e) => {
        e.preventDefault();
        setTab(false);
      }}>
        <figure className="accordion__header__img">
          <img src={AttorneyInfo} alt="Information" />
        </figure>
        <h3 className="accordion__header__title">申請者の情報</h3>
        <img
          className="accordion__header__arrow accordion__header__arrow--active"
          src={AcdArrow}
          alt="↑"
        />
      </button>
    } else {
      return <button className="accordion__header" onClick={(e) => {
        e.preventDefault();
        setTab(true);
      }}>
        <figure className="accordion__header__img">
          <img src={AttorneyInfo} alt="Information" />
        </figure>
        <h3 className="accordion__header__title">申請者の情報</h3>
        <img
          className="accordion__header__arrow accordion__header__arrow"
          src={AcdArrow}
          alt="↑"
        />
      </button>
    }
  }, [tab, setTab]);

  return (
    <React.StrictMode>
      {headerAllow}
      {/* contents */}
      {content}
    </React.StrictMode>
  );
};

export default AccordionContentsApplicantInfo;
