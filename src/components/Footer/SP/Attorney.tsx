import { IonRouterLink, isPlatform } from '@ionic/react';
import 'assets/css/FooterSP.css';
import SideBarIconUserActive from 'assets/img/common/account-active.svg';
import SideBarIconUser from 'assets/img/common/account.svg';
import SideBarIconChatActive from 'assets/img/common/chat-active.svg';
import SideBarIconChat from 'assets/img/common/chat.svg';
import SideBarIconHomeActive from 'assets/img/common/home-active.svg';
import SideBarIconHome from 'assets/img/common/home.svg';
import SideBarIconListActive from 'assets/img/common/list-active.svg';
import SideBarIconList from 'assets/img/common/list.svg';
//firebase
import firebase from 'firebase';
import 'firebase/firestore/';
import useAuth from 'hooks/auth';
import React from 'react';
import { FooterChoice } from '../../../data/choice/index';


interface Props {
  page: typeof FooterChoice[number];
}


const AttorneyFooterSP: React.FC<Props> = ({ page }) => {
  const auth = useAuth();
  const [totalUnreadNumber, setTotalUnreadNumber] = React.useState(0);

  React.useEffect(() => {
    let unmounted = false;
    if (auth?.uid) {
      const db = firebase.firestore();
      const roomsRef = db.collection("Rooms").where('attorneyId', "==", auth?.uid);

      roomsRef.onSnapshot((snap: firebase.firestore.QuerySnapshot) => {
        setTotalUnreadNumber(0);
        snap.docs.forEach((doc) => {
          setTotalUnreadNumber((num) => {
            return doc.data().attorneyUnreadNumber + num;
          });
        });
      });
    }
    return () => {
      // cleanup
      // エラー回避
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      unmounted = true;
    }
  }, [auth?.uid, setTotalUnreadNumber]);

  return (
    <footer className="footerSP__wrap">
      <div className="attorney__footerSP__inner footerSP">

        {(page === "home") ?
          <IonRouterLink className="footerSP__menuItem footerSP__menuItem--active" href="/attorney/home/">
            <figure className="footerSP__menuItem__img">
              <img className="footerSP__menuItem__img img--active" src={SideBarIconHomeActive} alt="" />
            </figure>
            <span className="footerSP__menuItem__text">ホーム</span>
          </IonRouterLink> :
          <IonRouterLink className="footerSP__menuItem footerSP__menuItem--active" href="/attorney/home/">
            <figure className="footerSP__menuItem__img">
              <img className="footerSP__menuItem__img img--active" src={SideBarIconHome} alt="" />
            </figure>
            <span className="footerSP__menuItem__text">ホーム</span>
          </IonRouterLink>
        }

        {
          (page === "chat") ?
            <IonRouterLink className="footerSP__menuItem" href="/attorney/chat/">
              <figure className="footerSP__menuItem__img">
                <img className="footerSP__menuItem__img img--active" src={SideBarIconChatActive} alt="" />
              </figure>
              <span className="footerSP__menuItem__text">チャット</span>
              {(totalUnreadNumber !== 0) ? <p className="footerSP__menuItem--notice"><span className="footerSP__menuItem--notice--text">{totalUnreadNumber}</span></p> : <span></span>}
            </IonRouterLink>
            :
            <IonRouterLink className="footerSP__menuItem" href="/attorney/chat/">
              <figure className="footerSP__menuItem__img">
                <img className="footerSP__menuItem__img img--active" src={SideBarIconChat} alt="" />
              </figure>
              <span className="footerSP__menuItem__text">チャット</span>
              {(totalUnreadNumber !== 0) ? <p className="footerSP__menuItem--notice"><span className="footerSP__menuItem--notice--text">{totalUnreadNumber}</span></p> : <span></span>}
            </IonRouterLink>
        }


        {//ios・androidの場合と、それ以外で条件分岐
          (isPlatform("ios") || isPlatform("android")) ?
            <React.StrictMode></React.StrictMode> :
            <React.StrictMode>
              {(page === "list") ?
                <IonRouterLink className="footerSP__menuItem" href="/attorney/newarrivals/">
                  <figure className="footerSP__menuItem__img">
                    <img className="footerSP__menuItem__img img--active" src={SideBarIconListActive} alt="" />
                  </figure>
                  <span className="footerSP__menuItem__text">新着リスト</span>
                </IonRouterLink>
                :
                <IonRouterLink className="footerSP__menuItem" href="/attorney/newarrivals/">
                  <figure className="footerSP__menuItem__img">
                    <img className="footerSP__menuItem__img img--active" src={SideBarIconList} alt="" />
                  </figure>
                  <span className="footerSP__menuItem__text">新着リスト</span>
                </IonRouterLink>
              }
            </React.StrictMode>
        }


        {(page === "account") ?
          <IonRouterLink className="footerSP__menuItem" href={(isPlatform("ios") || isPlatform("android")) ? "/attorney/profile/" : "/attorney/account/"}>
            <figure className="footerSP__menuItem__img">
              <img className="footerSP__menuItem__img img--active" src={SideBarIconUserActive} alt="" />
            </figure>
            <span className="footerSP__menuItem__text">アカウント</span>
          </IonRouterLink>
          :
          <IonRouterLink className="footerSP__menuItem" href={(isPlatform("ios") || isPlatform("android")) ? "/attorney/profile/" : "/attorney/account/"}>
            <figure className="footerSP__menuItem__img">
              <img className="footerSP__menuItem__img img--active" src={SideBarIconUser} alt="" />
            </figure>
            <span className="footerSP__menuItem__text">アカウント</span>
          </IonRouterLink>
        }

      </div>
    </footer>
  );
};

export default AttorneyFooterSP;
