import { IonRouterLink, isPlatform } from '@ionic/react';
import 'assets/css/FooterSP.css';
import SideBarIconUserActive from 'assets/img/common/account-active.svg';
import SideBarIconUser from 'assets/img/common/account.svg';
import SideBarIconChatActive from 'assets/img/common/chat-active.svg';
import SideBarIconChat from 'assets/img/common/chat.svg';
import SideBarIconHomeActive from 'assets/img/common/home-active.svg';
import SideBarIconHome from 'assets/img/common/home.svg';
import { FooterChoice } from 'data/choice/index';
//firebase
import firebase from 'firebase';
import useAuth from 'hooks/auth';
import React from 'react';
interface Props {
  page: typeof FooterChoice[number];
}


const ApplicantFooterSP: React.FC<Props> = ({ page }) => {
  const auth = useAuth();
  const [totalUnreadNumber, setTotalUnreadNumber] = React.useState(0);
  // console.log(auth?.uid);

  React.useEffect(() => {
    if (auth?.uid) {
      const db = firebase.firestore();
      const roomsRef = db.collection("Rooms").where('applicantId', "==", auth?.uid);

      roomsRef.onSnapshot((snap: firebase.firestore.QuerySnapshot) => {
        setTotalUnreadNumber(0);
        snap.docs.forEach((doc) => {
          setTotalUnreadNumber((num) => {
            return doc.data().applicantUnreadNumber + num;
          });
        });

      });
    }
  }, [auth?.uid, setTotalUnreadNumber]);
  return (
    <footer className="footerSP__wrap sp">
      <div className="applicant__footerSP__inner footerSP">
        {(page === "home") ?
          <IonRouterLink className="footerSP__menuItem footerSP__menuItem--active" href="/applicant/home/">
            <figure className="footerSP__menuItem__img">
              <img className="footerSP__menuItem__img img--active" src={SideBarIconHomeActive} alt="" />
            </figure>
            <span className="footerSP__menuItem__text">ホーム</span>
          </IonRouterLink> :
          <IonRouterLink className="footerSP__menuItem footerSP__menuItem--active" href="/applicant/home/">
            <figure className="footerSP__menuItem__img">
              <img className="footerSP__menuItem__img img--active" src={SideBarIconHome} alt="" />
            </figure>
            <span className="footerSP__menuItem__text">ホーム</span>
          </IonRouterLink>
        }

        {(page === "chat") ?
          <IonRouterLink className="footerSP__menuItem" href="/applicant/chat/">
            <figure className="footerSP__menuItem__img">
              <img className="footerSP__menuItem__img img--active" src={SideBarIconChatActive} alt="" />
            </figure>
            <span className="footerSP__menuItem__text">チャット</span>
            {(totalUnreadNumber !== 0) ? <p className="footerSP__menuItem--notice"><span className="footerSP__menuItem--notice--text">{totalUnreadNumber}</span></p> : <span></span>}
          </IonRouterLink>
          :
          <IonRouterLink className="footerSP__menuItem" href="/applicant/chat/">
            <figure className="footerSP__menuItem__img">
              <img className="footerSP__menuItem__img img--active" src={SideBarIconChat} alt="" />
            </figure>
            <span className="footerSP__menuItem__text">チャット</span>
            {(totalUnreadNumber !== 0) ? <p className="footerSP__menuItem--notice"><span className="footerSP__menuItem--notice--text">{totalUnreadNumber}</span></p> : <span></span>}
          </IonRouterLink>
        }

        {(page === "account") ?
          <IonRouterLink className="footerSP__menuItem" href={(isPlatform("ios") || isPlatform("android")) ? "/applicant/profile/" : "/applicant/account/"}>
            <figure className="footerSP__menuItem__img">
              <img className="footerSP__menuItem__img img--active" src={SideBarIconUserActive} alt="" />
            </figure>
            <span className="footerSP__menuItem__text">アカウント</span>
          </IonRouterLink>
          :
          <IonRouterLink className="footerSP__menuItem" href={(isPlatform("ios") || isPlatform("android")) ? "/applicant/profile/" : "/applicant/account/"}>
            <figure className="footerSP__menuItem__img">
              <img className="footerSP__menuItem__img img--active" src={SideBarIconUser} alt="" />
            </figure>
            <span className="footerSP__menuItem__text">アカウント</span>
          </IonRouterLink>
        }


      </div>
    </footer>
  );
};

export default ApplicantFooterSP;
