import { IonRouterLink } from '@ionic/react';
//css
import 'assets/css/FooterSP.css';
import iconSelect from 'controller/index';
import { tabChoice } from 'data/choice';
import useAuth from 'hooks/auth';
import React from 'react';
import tabJpn from 'translate';
import { FooterChoice, layoutsChoice } from '../../data/choice/index';
import ApplicantFooterSP from './SP/Applicant';
import AttorneyFooterSP from './SP/Attorney';


interface Props {
  activate: boolean;
  what: typeof tabChoice[number];
}

export const FooterContext = React.createContext<Props>({
  what: "home",
  activate: true
});



export const TabButton: React.FC<Props> = ({ activate, what }) => {
  return (
    <IonRouterLink className="footerSP__menuItem">
      <figure className="footerSP__menuItem__img">
        {/* <img className="sidebar__btn__icon" src={SideBarIconUserActive} alt="" /> */}
        <img className="footerSP__menuItem__img" src={iconSelect(activate, what)} alt="" />
      </figure>
      <span className="footerSP__menuItem__text">{tabJpn(what)}</span>
    </IonRouterLink>
  );
}

export const MainFooter: React.FC = () => {
  return (
    <footer className="footer ion-no-border">
      <small className="footer--copyright">copylight ©︎ 2021.</small>
    </footer>
  );
};
export const Footer: React.FC<{ format: typeof layoutsChoice[number], page: typeof FooterChoice[number] }> = ({ format, page }) => {
  const auth = useAuth();

  const tabFooter = React.useMemo(() => {
    if (format === 'Tabs') {
      if (auth?.kind === 'Applicant') {
        return (
          <div className="inner--auth--sp--margin sp">
            <ApplicantFooterSP page={page} />
          </div>
        )
      } else {
        return (
          <div className="inner--auth--sp--margin sp">
            <AttorneyFooterSP page={page} />
          </div>
        )
      }

    } else {
      return;
    }
  }, [format, auth?.kind, page]);

  return (
    <React.StrictMode>
      {tabFooter}
      <MainFooter />
    </React.StrictMode>
  );
};

export default Footer;
