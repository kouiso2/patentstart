import { TabContext } from 'data/context';
import React from 'react';

interface Props {
  platform: "sp" | "pc";
}

export const TabButton: React.FC<Props> = ({ platform }) => {
  const { tab, setTab } = React.useContext(TabContext);

  const tabMatch = React.useMemo(() => {
    if (tab) {
      return (
        <button className="arrivalsList__tabs--child match active__tab">条件に合う案件一覧</button>
      )
    } else {
      return (
        <button onClick={(e: React.MouseEvent<HTMLButtonElement, MouseEvent>): void => {
          e.preventDefault();
          setTab(true);
        }} className="arrivalsList__tabs--child match">条件に合う案件一覧</button>
      )
    }
  }, [tab, setTab]);

  const tabAll = React.useMemo(() => {
    if (tab) {
      return (
        <button onClick={(e: React.MouseEvent<HTMLButtonElement, MouseEvent>): void => {
          e.preventDefault();
          setTab(false);
        }} className="arrivalsList__tabs--child all">全案件一覧</button>
      )
    } else {
      return (
        <button className="arrivalsList__tabs--child all active__tab">全案件一覧</button>
      )
    }
  }, [tab, setTab]);

  const platformTab = React.useMemo(() => {
    if (platform === "pc") {
      return (
        <div className="arrivalsList__tabs pc" >
          {tabMatch}
          {tabAll}
        </div >
      )
    } else {
      return (
        <div className="arrivalsList__tabs sp" >
          {tabMatch}
          {tabAll}
        </div >
      )
    }
  }, [platform, tabAll, tabMatch]);
  return (
    <React.StrictMode>
      {platformTab}
    </React.StrictMode>
  )
}
