import React from 'react';
export const RecruitBadge: React.FC = () => {
  return (
    <span className="applicationList__status">募集中</span>
  );
};
export const ApplyBadge: React.FC = () => {
  return (
    <span className="applicationList__status applicationList__status--sinsei">申請中</span>
  );
};
export const ManageBadge: React.FC = () => {
  return (
    <span className="applicationList__status applicationList__status--kanri">管理中</span>
  );
};
export const AttorneyCertifiedBadge: React.FC = () => {
  return (
    <span className="modal__contents__profile__text__flex--ver1">弁理士資格証明書提出済</span>
  );
}
export const IdentificationBadge: React.FC = () => {
  return (
    <span className="modal__contents__profile__text__flex--ver1">本人認証済み</span>
  );
}
