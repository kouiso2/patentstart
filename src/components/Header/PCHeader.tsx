import 'assets/css/Header.css';
import HeaderImg from 'assets/img/common/logo.svg';
import 'firebase/auth';
import useAuth from 'hooks/auth';
import React from 'react';
import { useHistory } from 'react-router-dom';


const PCHeader: React.FC = () => {
  const history = useHistory();
  const auth = useAuth();

  const handleClick = React.useCallback((e: React.MouseEvent<HTMLImageElement, MouseEvent>) => {
    e.preventDefault();
    if (auth?.kind === "Attorney") {
      history.push('/attorney/home')
    } else if (auth?.kind === "Applicant") {
      history.push('/applicant/home');
    } else if (auth?.kind === "Admin") {
      history.push('/admin/contact-list/');
    } else {
      history.push('/auth/login');
    }

  }, [history, auth?.kind]);
  return (
    <header className="header__base header__home pc">
      <figure className="header__home--logo" onClick={handleClick} >
        <img src={HeaderImg} alt="header logo" />
      </figure>
    </header>
  );
};

export default PCHeader;
