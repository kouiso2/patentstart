//css
import 'assets/css/Header.css';
//img
import InfoIcon from 'assets/img/common/info__icon.svg';
import React from 'react';
import { Link } from 'react-router-dom';


export const HeaderChat: React.FC = () => {

  return (
    <React.StrictMode>
      {/* PC */}
      <header className="header__base header__chat pc">
        <div className="header__desc">
          <p className="header__desc--bold"><b>弁理士名</b></p>
          <p className="header__desc--text">シンプルケース</p>
        </div>
        <img className="header__chat--info hover__pointer" src={InfoIcon} alt="Information" />
      </header>
      {/* SP */}
      <header className="header__base header__chat sp">
        <Link to="/auth/login/" className="header__backButton">
          <i className="header__backButton--icon fas fa-angle-left"></i>
          <span className="header__backButton--text">戻る</span>
        </Link>
        <div className="header__desc">
          <p className="header__desc--bold"><b>弁理士名</b></p>
          <p className="header__desc--text">シンプルケース</p>
        </div>
        <img className="header__chat--info hover__pointer" src={InfoIcon} alt="Information" />
      </header>
    </React.StrictMode>
  );
};

export default HeaderChat;
