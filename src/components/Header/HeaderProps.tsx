//css
import 'assets/css/Header.css';
///img
import InfoIcon from 'assets/img/common/info__icon.svg';
import HeaderImg from 'assets/img/common/logo.svg';
import useAuth from 'hooks/auth';
import React from 'react';
import {
  Link,
  useHistory
} from 'react-router-dom';
import { HeaderContext } from './index';


const HeaderProps: React.FC = () => {
  const history = useHistory();
  const handleFixClick = React.useCallback((e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault();
    return history.goBack();
  }, [history]);
  const auth = useAuth();

  const LogoUrl = React.useMemo(() => {
    if (auth?.kind === "Applicant") {
      return (
        "/applicant/home/"
      )
    } else if (auth?.kind === "Attorney") {
      return (
        "/attorney/home/"
      )
    }
    else {
      return (
        "/auth/login/"
      )
    }
  }, [auth?.kind]);

  const part = React.useContext(HeaderContext);
  const { title, cases, left, right, center } = part;
  const rightButton = React.useMemo(() => {
    if (right === "close") {
      return (
        <i onClick={handleFixClick} className="close__icon hover__pointer fal fa-times"></i>
      );
    } else if (right === "info") {
      return (
        <img className="hover__pointer" src={InfoIcon} alt="Information" />
      );
    } else {
      return;
    }
  }, [right, handleFixClick]);
  const leftButton = React.useMemo(() => {
    if (left === "back") {
      return (
        <div className="header__backclose">
          <button
            onClick={handleFixClick}
            className="header__backButton">
            <i className="header__backButton--icon fas fa-angle-left"></i>
            <span className="header__backButton--text">戻る</span>
          </button>
        </div>
      );
    } else {
      return;
    }
  }, [left, handleFixClick]);
  const centerText = React.useMemo(() => {
    if (center === "title") {
      return (
        <div className="header--center__text--Xcenter">
          <p className="title max--textOverflow">{title}</p>
        </div>
      );
    } else if (center === "logo") {
      return (

        <figure className="header__home--logo">
          <Link to={LogoUrl}>
            <img src={HeaderImg} alt="header logo" />
          </Link>
        </figure>

      );
    } else {
      return (
        <div className="header__desc header--center__text--Xcenter">
          <p className="header__desc--bold max--textOverflow">{title}</p>
          <p className="header__desc--text max--textOverflow">{cases}</p>
        </div>
      );
    }
  }, [center, cases, title, LogoUrl]);

  return (
    <header className="header__base sp">
      {leftButton}
      {centerText}
      {rightButton}
    </header>
  );
};

export default HeaderProps;
