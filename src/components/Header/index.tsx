import React from 'react';
import { centerTextChoice, leftButtonChoice, rightButtonChoice } from '../../data/choice/index';
import HeaderProps from './HeaderProps';
import PCHeader from './PCHeader';


interface Props {
  title?: string | number;
  cases?: string;
  left: typeof leftButtonChoice[number];
  center: typeof centerTextChoice[number];
  right: typeof rightButtonChoice[number];
}

export const HeaderContext = React.createContext<Props>({
  title: "",
  cases: "",
  left: "none",
  center: "title",
  right: "none"
});

export const Header: React.FC<Props> = ({ title, cases, left, center, right }) => {

  return (
    <HeaderContext.Provider value={{ title, cases, left, center, right }}>
      <PCHeader />
      <HeaderProps />
    </HeaderContext.Provider>
  );
};

export default Header;
