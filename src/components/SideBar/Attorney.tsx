import { isPlatform } from '@ionic/react';
import 'assets/css/SideBar.css';
import SideBarIconUserActive from 'assets/img/common/account-active.svg';
import SideBarIconUser from 'assets/img/common/account.svg';
import SideBarIconChatActive from 'assets/img/common/chat-active.svg';
import SideBarIconChat from 'assets/img/common/chat.svg';
import SideBarIconHomeActive from 'assets/img/common/home-active.svg';
import SideBarIconHome from 'assets/img/common/home.svg';
import SideBarIconListActive from 'assets/img/common/list-active.svg';
import SideBarIconList from 'assets/img/common/list.svg';
import ProfileImg from 'assets/img/noimage.svg';
import firebase from 'firebase';
import useAuth from 'hooks/auth';
import React from 'react';
import { Link } from "react-router-dom";
import { SideBarChoice } from '../../data/choice/index';

interface Props {
  page: typeof SideBarChoice[number];
}
export const AttoreySideBar: React.FC<Props> = ({ page }) => {
  const auth = useAuth();
  const [totalUnreadNumber, setTotalUnreadNumber] = React.useState(0);

  React.useEffect(() => {
    if (auth?.uid) {
      const db = firebase.firestore();
      const roomsRef = db.collection("Rooms").where('attorneyId', "==", auth?.uid);
      roomsRef.onSnapshot((snap: firebase.firestore.QuerySnapshot) => {
        setTotalUnreadNumber(0);
        snap.docs.forEach((doc) => {
          setTotalUnreadNumber((num) => {
            return doc.data().attorneyUnreadNumber + num;
          });
        });
      });
    }
  }, [auth?.uid, setTotalUnreadNumber]);

  return (
    <aside className="sidebar__wrap">
      <div className="sidebar__wrap__inner">
        <div className="user">
          <figure className="user-img" style={{ backgroundImage: `url(${auth?.image ? auth?.image : ProfileImg})` }}></figure>
          <div className="user-item">
            <p className="user-item_company">{auth?.companyName}</p>
            <p className="user-item_name">{auth?.personName}</p>
          </div>
        </div>


        {(page === "home") ?
          <Link to="/attorney/home/" className="sidebar__btn sidebar__btn--active "><img className="sidebar__btn__icon" src={SideBarIconHomeActive} alt="" /><span className="sidebar__btn__text">ホーム</span>
          </Link> :

          <Link to="/attorney/home/" className="sidebar__btn"><img className="sidebar__btn__icon" src={SideBarIconHome} alt="" /><span className="sidebar__btn__text">ホーム</span>
          </Link>
        }

        {(page === "chat") ?
          <Link to="/attorney/chat/" className="sidebar__btn sidebar__btn--active "><img className="sidebar__btn__icon" src={SideBarIconChatActive} alt="" /><span className="sidebar__btn__text">チャット</span>
            {(totalUnreadNumber !== 0) ? <p className="sidebar__btn--notice"><span className="sidebar__btn--notice--Text">{totalUnreadNumber}</span></p> : <span></span>}
          </Link> :

          <Link to="/attorney/chat/" className="sidebar__btn"><img className="sidebar__btn__icon" src={SideBarIconChat} alt="" /><span className="sidebar__btn__text">チャット</span>
            {(totalUnreadNumber !== 0) ? <p className="sidebar__btn--notice"><span className="sidebar__btn--notice--Text">{totalUnreadNumber}</span></p> : <span></span>}
          </Link>
        }

        {//ios・androidの場合と、それ以外で条件分岐
          (isPlatform("ios") || isPlatform("android")) ?
            <React.StrictMode></React.StrictMode> :
            <React.StrictMode>
              {(page === "list") ?
                <Link to="/attorney/newarrivals/" className="sidebar__btn sidebar__btn--active "><img className="sidebar__btn__icon" src={SideBarIconListActive} alt="" /><span className="sidebar__btn__text">新着リスト</span>
                </Link> :
                <Link to="/attorney/newarrivals/" className="sidebar__btn"><img className="sidebar__btn__icon" src={SideBarIconList} alt="" /><span className="sidebar__btn__text">新着リスト</span>
                </Link>
              }
            </React.StrictMode>
        }

        {(page === "account") ?
          <Link to={(isPlatform("ios") || isPlatform("android")) ? "/attorney/profile/" : "/attorney/account/"} className="sidebar__btn sidebar__btn--active "><img className="sidebar__btn__icon" src={SideBarIconUserActive} alt="" /><span className="sidebar__btn__text">アカウント</span>
          </Link> :
          <Link to={(isPlatform("ios") || isPlatform("android")) ? "/attorney/profile/" : "/attorney/account/"} className="sidebar__btn"><img className="sidebar__btn__icon" src={SideBarIconUser} alt="" /><span className="sidebar__btn__text">アカウント</span>
          </Link>
        }

        <h2 className="sidebar__other--title">その他・お問い合わせ</h2>
        {/* <p className="sidebar__other--btn account__other--sp__btn"
        >
          アプリの使い方
      </p> */}
        <a href="https://patentstart.jp/privacy.html" target="_blank" rel="noreferrer" className="sidebar__other--btn account__other--sp__btn">
          プライバシーポリシー
        </a>
        <Link to="/attorney/contact/" className="sidebar__other--btn account__other--sp__btn">
          お問い合わせ
        </Link>
      </div>
    </aside>
  );
};

export default AttoreySideBar;
