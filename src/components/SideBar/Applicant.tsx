//ionic
//css
import { isPlatform } from '@ionic/react';
import 'assets/css/SideBar.css';
import SideBarIconUserActive from 'assets/img/common/account-active.svg';
// img
import SideBarIconUser from 'assets/img/common/account.svg';
import SideBarIconChatActive from 'assets/img/common/chat-active.svg';
import SideBarIconChat from 'assets/img/common/chat.svg';
import SideBarIconHomeActive from 'assets/img/common/home-active.svg';
import SideBarIconHome from 'assets/img/common/home.svg';
import ProfileImg from 'assets/img/noimage.svg';
import { SideBarChoice } from 'data/choice';
import firebase from 'firebase';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/storage';
import useAuth from 'hooks/auth';
import React from 'react';
import { Link } from "react-router-dom";

interface Props {
  page: typeof SideBarChoice[number];
}

const ApplicantSideBar: React.FC<Props> = ({ page }) => {
  const auth = useAuth();

  const [totalUnreadNumber, setTotalUnreadNumber] = React.useState(0);

  React.useEffect(() => {
    if (auth?.uid) {
      const db = firebase.firestore();
      const roomsRef = db.collection("Rooms").where('applicantId', "==", auth?.uid);
      roomsRef.onSnapshot((snap: firebase.firestore.QuerySnapshot) => {
        setTotalUnreadNumber(0);
        snap.docs.forEach((doc) => {
          setTotalUnreadNumber((num) => {
            return doc.data().applicantUnreadNumber + num;
          });
        });

      });
    }
  }, [auth?.uid, setTotalUnreadNumber]);

  const Name = React.useMemo(() => {
    if (auth?.businessType.company) {
      return <div className="user-item">
        <p className="user-item_company">{auth?.companyName}</p>
        <p className="user-item_name">{auth?.personName}</p>
      </div>
    } else if (auth?.businessType.individual) {
      return <div className="user-item">
        <p className="user-item_company">{auth?.personName}</p>
        <p className="user-item_name">{auth?.personKatakana}</p>
      </div>
    }
  }, [auth?.businessType.company, auth?.businessType.individual, auth?.companyName, auth?.personKatakana, auth?.personName]);

  return (
    <aside className="sidebar__wrap">
      <div className="sidebar__wrap__inner">
        <div className="user">
          <figure className="user-img" style={{ backgroundImage: `url(${auth?.image ? auth?.image : ProfileImg})` }}></figure>
          {/* style={{ background: 'url(../src/assets/img/demo.jpg)' }} */}
          {Name}

        </div>

        {(page === "home") ?
          <Link to="/applicant/home/" className="sidebar__btn sidebar__btn--active "><img className="sidebar__btn__icon" src={SideBarIconHomeActive} alt="" /><span className="sidebar__btn__text">ホーム</span>
          </Link> :

          <Link to="/applicant/home/" className="sidebar__btn"><img className="sidebar__btn__icon" src={SideBarIconHome} alt="" /><span className="sidebar__btn__text">ホーム</span>
          </Link>
        }

        {(page === "chat") ?
          <Link to="/applicant/chat/" className="sidebar__btn sidebar__btn--active "><img className="sidebar__btn__icon" src={SideBarIconChatActive} alt="" /><span className="sidebar__btn__text">チャット</span>
            {(totalUnreadNumber !== 0) ? <p className="sidebar__btn--notice"><span className="sidebar__btn--notice--Text">{totalUnreadNumber}</span></p> : <span></span>}
          </Link> :

          <Link to="/applicant/chat/" className="sidebar__btn"><img className="sidebar__btn__icon" src={SideBarIconChat} alt="" /><span className="sidebar__btn__text">チャット</span>
            {(totalUnreadNumber !== 0) ? <p className="sidebar__btn--notice"><span className="sidebar__btn--notice--Text">{totalUnreadNumber}</span></p> : <span></span>}
          </Link>
        }

        {(page === "account") ?
          <Link to={(isPlatform("ios") || isPlatform("android")) ? "/applicant/profile/" : "/applicant/account/"} className="sidebar__btn sidebar__btn--active "><img className="sidebar__btn__icon" src={SideBarIconUserActive} alt="" /><span className="sidebar__btn__text">アカウント</span>
          </Link> :

          <Link to={(isPlatform("ios") || isPlatform("android")) ? "/applicant/profile/" : "/applicant/account/"} className="sidebar__btn"><img className="sidebar__btn__icon" src={SideBarIconUser} alt="" /><span className="sidebar__btn__text">アカウント</span>
          </Link>
        }








        <h2 className="sidebar__other--title">その他・お問い合わせ</h2>
        {/* <p className="sidebar__other--btn account__other--sp__btn"

        >
          アプリの使い方
      </p> */}
        <a href="https://patentstart.jp/privacy.html" target="_blank" rel="noreferrer" className="sidebar__other--btn account__other--sp__btn">
          プライバシーポリシー
        </a>
        {/* <Link to="/applicant/contact/" className="sidebar__other--btn account__other--sp__btn">
          よくある質問
      </Link> */}
        <Link to="/applicant/contact/" className="sidebar__other--btn account__other--sp__btn">お問い合わせ</Link>
      </div>
    </aside>
  );
};

export default ApplicantSideBar;
