import useAuth from 'hooks/auth';
import React from 'react';
import { SideBarChoice } from '../../data/choice/index';
import AdminSideBar from './Admin';
import ApplicantSideBar from './Applicant';
import AttorneySideBar from './Attorney';

interface Props {
  page: typeof SideBarChoice[number];
}

const Sidebar: React.FC<Props> = ({ page }) => {
  const auth = useAuth();
  const test = React.useMemo(() => {
    if (auth?.kind === "Applicant") {
      return (
        <ApplicantSideBar page={page} />
      );
    } else if (auth?.kind === "Attorney") {
      return (
        <AttorneySideBar page={page} />
      );
    } else if (auth?.kind === "Admin") {
      return (
        <AdminSideBar />
      );
    } else {
      return (
        <React.StrictMode></React.StrictMode>
      );
    }
  }, [auth, page]);

  return test;
};

export default Sidebar;
