import 'assets/css/SideBar.css';
import SideBarIconUser from 'assets/img/common/account.svg';
import ProfileImg from 'assets/img/noimage.svg';
import useAuth from 'hooks/auth';
import React from 'react';
import { Link } from "react-router-dom";

interface Props {

}
export const AdminSideBar: React.FC<Props> = () => {
  const auth = useAuth();

  return (
    <aside className="sidebar__wrap">
      <div className="sidebar__wrap__inner">
        <div className="user">
          <figure className="user-img" style={{ backgroundImage: `url(${auth?.image ? auth?.image : ProfileImg})` }}></figure>
          <div className="user-item">
            <p className="user-item_company">{auth?.companyName}</p>
            <p className="user-item_name">{auth?.personName}</p>
          </div>
        </div>
        <Link to="/admin/applicant-list/" className="sidebar__btn"><img className="sidebar__btn__icon" src={SideBarIconUser} alt="" /><span className="sidebar__btn__text">申請者一覧</span></Link>
        {/* <Link to="/admin/attorney-list/" className="sidebar__btn"><img className="sidebar__btn__icon" src={SideBarIconUser} alt="" /><span className="sidebar__btn__text">弁理士一覧</span></Link> */}
        <a href="/admin/attorney-list/" className="sidebar__btn"><img className="sidebar__btn__icon" src={SideBarIconUser} alt="" /><span className="sidebar__btn__text">弁理士一覧</span></a>
        
        <h2 className="sidebar__other--title">その他</h2>

        <Link to="/admin/contact-list/" className="sidebar__other--btn account__other--sp__btn">お問い合わせ一覧</Link>
        <Link to="/admin/report-list/" className="sidebar__other--btn account__other--sp__btn">通報一覧</Link>
        {/* <Link to="/admin/attorney-approval-list/" className="sidebar__other--btn account__other--sp__btn">弁理士の本人確認</Link> */}
      </div>
    </aside>
  );
};

export default AdminSideBar;