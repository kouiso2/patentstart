import { IonRouterLink } from '@ionic/react';
import 'assets/css/modal.css';
import ProfileNoImage from 'assets/img/noimage.svg';
import { AttorneyCertifiedBadge } from 'components/Badge';
import "firebase/auth";
import 'firebase/firestore';
import useAuth from 'hooks/auth';
import useProfile from 'hooks/profile';
import React from 'react';
import { useSelector } from 'react-redux';
import { patentCategoryJpn } from 'translate';
import { RootState } from '../../store/Combine';
import { IdentificationBadge } from '../Badge';

//XD5
const ModalAttorneyProf: React.FC = () => {
  const config = useSelector((state: RootState) => state.config);
  const { profile, profileIdentify } = useProfile(config.path);
  const auth = useAuth();

  const currentCategory = React.useMemo(() => {
    return Object.keys(profile.goodCategory).map((value: any, key: number) => {
      if (Object.values(profile.goodCategory)[key]) {
        return (
          <span key={key}>{patentCategoryJpn(value)}/</span>
        );
      } else {
        return null;
      }
    })
  }, [profile]);

  const identifyBadge = React.useMemo(() => {
    if (profile.identification?.isIdentified) {
      return (
        <IdentificationBadge />
      )
    }
  }, [profile]);

  const submissionBadge = React.useMemo(() => {
    if (profile.identification.isSubmitted) {
      return (
        <AttorneyCertifiedBadge />
      )
    }
  }, [profile]);

  const reportLink = React.useMemo(() => {
    if (auth?.kind === "Applicant") {
      return (<IonRouterLink className="modal__contents__ButtonWrap--report" href={`${auth?.url}/report/${profileIdentify.id}`}>このユーザーを報告する</IonRouterLink>);
    }
  }, [auth?.kind, auth?.url, profileIdentify]);

  return (
    <div className="modal__contents__inside">
      {/* プロフィール */}
      <div className="modal__contents__profile">
        <figure className="modal__contents__profile__img" style={{ backgroundImage: `url(${profile.image ? profile.image : ProfileNoImage})` }}></figure>
        <div className="modal__contents__profile__text">
          <h4 className="modal__contents__profile__text__kanji">{profile.firstName + profile.lastName}</h4>
          <p className="modal__contents__profile__text__kana">{profile.personKatakana}</p>
          <div className="modal__contents__profile__text__flex">
            {submissionBadge}
            {identifyBadge}
            {/*
            modal__contents__profile__text__flex--ver1 弁理士の資格　白背景に赤い文字＆赤い枠線
            modal__contents__profile__text__flex--ver2　申請者のプロフ　赤い背景に白い文字
             */}

            {/* dislay__noneのクラス名追加で非表示 */}
          </div>
        </div>
      </div>
      {/* end プロフィール */}
      <div className="modal__contents__flex">
        <div className="modal__contents__flex__child">
          <h4 className="modal__contents__commonTitle1">得意な国際特許分類</h4>
          <p className="modal__contents__commonText1 modal__contents__commonText--mb12">
            {currentCategory}
          </p>
        </div>
        {
          profile.homepage ?
            <div className="modal__contents__flex__child">
              <h4 className="modal__contents__commonTitle1">WEB</h4>
              <IonRouterLink href={profile.homepage} target="_blank" className="modal__contents__commonText1 modal__contents__commonText--mb12 modal__contents__externalLink">{profile.homepage}</IonRouterLink>
            </div> : <React.StrictMode></React.StrictMode>
        }

      </div>

      <h4 className="modal__contents__commonTitle2">自己紹介</h4>
      <p className="modal__contents__commonText2">{profile.self}</p>
      <div className="modal__contents__ButtonWrap ">
        <button className="modal__contents__ButtonWrap--button display__none">この弁理士とチャットする</button>
        {/* display__noneで非表示 */}
        {reportLink}
      </div>
    </div>
  );
};

export default ModalAttorneyProf;
