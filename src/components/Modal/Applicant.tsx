import { IonRouterLink } from '@ionic/react';
import 'assets/css/modal.css';
import ProfileNoImage from 'assets/img/noimage.svg';
import "firebase/auth";
import 'firebase/firestore';
import useAuth from 'hooks/auth';
import useProfile from 'hooks/profile';
import React from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../../store/Combine';

const ModalApplicantProf: React.FC = () => {
  const config = useSelector((state: RootState) => state.config);
  const { profile, profileIdentify } = useProfile(config.path);
  const auth = useAuth();

  const reportLink = React.useMemo(() => {
    if (auth?.kind === "Attorney") {
      return (<IonRouterLink className="modal__contents__ButtonWrap--report" href={`${auth?.url}/report/${profileIdentify.id}`}>このユーザーを報告する</IonRouterLink>);
    }
  }, [auth?.kind, auth?.url, profileIdentify]);

  return (
    <div className="modal__contents__inside">
      {/* dislay__noneのクラス名追加で非表示 */}
      {/* プロフィール */}
      <div className="modal__contents__profile">
        <figure className="modal__contents__profile__img" style={{ backgroundImage: `url(${profile.image ? profile.image : ProfileNoImage})` }}></figure>
        <div className="modal__contents__profile__text">
          <h4 className="modal__contents__profile__text__kanji">{profile.companyName}</h4>
          <p className="modal__contents__profile__text__kana">{profile.companyKana}</p>
          <div className="modal__contents__profile__text__flex">
            <span className="modal__contents__profile__text__flex--ver2">
              {profile.businessType.company === true ? "法人" : "個人"}
            </span>
            {/*
            modal__contents__profile__text__flex--ver1 弁理士の資格　白背景に赤い文字＆赤い枠線
            modal__contents__profile__text__flex--ver2　申請者のプロフ　赤い背景に白い文字
             */}

          </div>
        </div>
      </div>
      {/* end プロフィール */}
      <div className="modal__contents__flex">
        <div className="modal__contents__flex__child">
          <h4 className="modal__contents__commonTitle1">担当者名</h4>
          <p className="modal__contents__commonText1 modal__contents__commonText--mb12">{profile.firstName + profile.lastName}</p>
        </div>
        {
          profile.homepage ?
            <div className="modal__contents__flex__child">
              <h4 className="modal__contents__commonTitle1">ホームページ・SNS等</h4>
              <IonRouterLink href={`${profile.homepage}`} target="_blank" className="modal__contents__commonText1 modal__contents__commonText--mb12 modal__contents__externalLink">{profile.homepage}</IonRouterLink>
            </div> : <React.StrictMode></React.StrictMode>
        }

      </div>

      <h4 className="modal__contents__commonTitle2">自己紹介</h4>
      <p className="modal__contents__commonText2">{profile.self}</p>
      <div className="modal__contents__ButtonWrap ">
        {reportLink}
      </div>
    </div>
  );
};

export default ModalApplicantProf;
