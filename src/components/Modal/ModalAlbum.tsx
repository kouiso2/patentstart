import 'assets/css/modal.css';
import CheckMarkRed from 'assets/img/common/icon_checkmark-red.svg';
//firebase
import firebase from 'firebase';
import React from 'react';


interface Props {
  roomId: string;
  messageId: string;
  imgUrl: string;
}

const ModalAlbum: React.FC<Props> = ({ roomId, messageId, imgUrl }) => {

  function handleDownload(e: React.MouseEvent<HTMLButtonElement>) {

    let xhr = new XMLHttpRequest();
    xhr.responseType = 'blob';
    xhr.onload = ((event) => {
      //blobがファイルのデータ
      let blob = xhr.response;
      //aタグを作る
      let link = document.createElement('a');
      // ファイルデータに紐づくダウンロードリンクを設定する
      link.href = URL.createObjectURL(blob);
      //第二引数はファイル名
      link.setAttribute('download', 'image');
      // link.download = 'photo.jpg';
      link.click();
    });
    xhr.open('GET', imgUrl);
    xhr.send();

    //保存しました。表示
    document.getElementById('modal__album__img__saved--div')?.classList.remove('display__none');
    // var img = document.getElementById('modal__album__img__body');
    // (img as HTMLImageElement).src = imgUrl;
  }

  function handleSave() {
    const db = firebase.firestore();
    const stateRef = db.collection("Rooms").doc(roomId).collection("Messages").doc(messageId);
    return stateRef.update({
      state: "albumFile",
    }).then(function () {
      document.getElementById('modal__album__img__saved--div')?.classList.remove('display__none');
    }).catch(function (error: any) {
      console.error('Error writing new message to database', error);
    });
  }

  return (
    <div className="modal__album__inner">
      <figure className="modal__album__img">
        <img id="modal__album__img__body" src={imgUrl} alt="" />
        <div id="modal__album__img__saved--div" className="modal__album__img__saved display__none">
          {/* dislay__none のクラス名追加で非表示 */}
          <img className="modal__album__img__saved--checkmark" src={CheckMarkRed} alt="" />
          <p className="modal__album__img__saved--text">保存しました</p>
        </div>
      </figure>
      <div className="modal__album__button">
        <button className="modal__album__button--save" onClick={handleDownload}>本体に保存</button>
        <button className="modal__album__button--save" onClick={handleSave}>保存用フォルダに保存</button>
        {/* <button className="modal__album__button--delete">この画像を削除する</button> */}
      </div>
    </div>
  );
};

export default ModalAlbum;
