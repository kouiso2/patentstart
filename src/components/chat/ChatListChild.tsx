import "assets/css/Chat.css";
import firebase from "firebase";
import useProfile from "hooks/profile";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "store/Combine";
import { modalConfigAction, pathConfigAction } from "store/Config/action";
import ChatListProf from "../../assets/img/nowloading.svg";

interface Props {
  appPath: string;
  appId: string;
}
export type Room = {
  id: string;
  applicantId: string;
  attorneyId: string;
  patentStatus: string;
  lastMessageText: string;
  timestamp: any;
  application: string;
};
const ChatListChild: React.FC<Props> = ({ appId, appPath }) => {
  //未読メッセージ数を取得する
  const { profile, profileIdentify } = useProfile(appPath);
  const dispatch = useDispatch();
  const config = useSelector((state: RootState) => state.config);
  const [, setUnreadNumber] = React.useState<number>(0);
  const [, setRoom] = React.useState<Room>();
  React.useEffect(() => {
    const db = firebase.firestore().collection("Rooms").doc(appId);
    db.onSnapshot((doc: firebase.firestore.DocumentSnapshot) => {
      setUnreadNumber(doc.data()?.applicantUnreadNumber);
      setRoom({
        id: doc.id,
        applicantId: doc.data()?.applicantId,
        attorneyId: doc.data()?.attorneyId,
        patentStatus: doc.data()?.patentStatus,
        lastMessageText: doc.data()?.lastMessageText,
        timestamp: doc.data()?.timestamp,
        application: doc.data()?.application?.path,
      });
    });
  }, [setUnreadNumber, setRoom, appId]);
  // const StatusBadge = React.useMemo(() => {
  //   if (status === 'Recruit') {
  //     return (
  //       <span className="chatlist__child__desc__top--status status--recruiting">募集中</span>
  //     )
  //   } else if (status === 'Manage') {
  //     return (
  //       <span className="chatlist__child__desc__top--status status--managing">管理中</span>
  //     )
  //   } else if (status === 'Apply') {
  //     return (
  //       <span className="chatlist__child__desc__top--status status--applying">申請中</span>
  //     )
  //   } else {
  //     return;
  //   }
  // }, [status]);
  return (
    <div
      className="chatlist__child"
      style={{
        transition: "none",
        cursor: "auto",
        pointerEvents: "none",
      }}
    >
      <figure
        className="chatlist__child__thum"
        style={{
          backgroundImage: `url(${profile.image ? profile.image : ChatListProf
            })`,
        }}
        onClick={() => {
          dispatch(
            modalConfigAction({
              ...config.modal,
              isOpen: true,
              form: "OpponentProfile",
            })
          );
          dispatch(pathConfigAction(profileIdentify.path));
        }}
      ></figure>
      <article className="chatlist__child__desc">
        <div className="chatlist__child__desc__top">
          <h4 className="chatlist__child__desc__top--name">
            {profile.firstName + profile.lastName}
          </h4>
          {/*
            ]status--Recruit →募集中
             status--Manage →管理中
             status--Apply →申請中
             */}
        </div>
        {/* <p className="chatlist__child__desc--message">{room?.lastMessageText}</p> */}
      </article>
      {/* メッセージ件数が0の場合は数字が表示されないようにする。 */}
      {/* { unreadNumber !== 0 ? <span className="chatlist__child--notification">{unreadNumber}</span> : <span></span>} */}
    </div>
  );
};
export default ChatListChild;
