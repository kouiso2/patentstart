// import React, { useEffect, useRef } from 'react';
import { IonRouterLink } from '@ionic/react';
import 'assets/css/Chat.css';

const CircleBtn: React.FC = () => {
  return (
    <div className="chatNormal">
      <label className="chatNormal__input"><input type="text" /></label>
      <div className="chatNormal__menu">
        <div className="chatNormal__menu__list">
          <label className="chatNormal__menu__list--item" htmlFor=""><input type="file" /><img src="" alt="" /></label>
          <IonRouterLink className="chatNormal__menu__list--item" href="/auth/login/"><img src="" alt="" /></IonRouterLink>
        </div>
      </div>
    </div>
  );
};

export default CircleBtn;
