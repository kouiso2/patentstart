import 'assets/css/Chat.css';


const ChatlistWrap: React.FC = ({ children }) => {
  return (
    <div className="chatlist__wrap">
      <h3 className="chatlist__wrap__title pc">チャット一覧</h3>
      {children}
    </div>

  );
};

export default ChatlistWrap;
