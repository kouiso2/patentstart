import 'assets/css/style.css';
import React from 'react';
export interface Props {
  page: number;
  totalPages: number;
  handlePagination: (page: number) => void;
}
export const PaginationComponent: React.FC<Props> = ({
  page,
  totalPages,
  handlePagination,
}) => {

  return (
    <ul className="pagination__box">

      {/* 最初のページなら非表示 */}
      {page === 1 ? <React.StrictMode></React.StrictMode> :
        <React.StrictMode>
          <button
            onClick={() => handlePagination(1)}
            type="button"
            className="pagination__item topend"
          >
            &lt;
            最初へ
          </button>
          <div className="pagination--dot">...</div>
          <button
            onClick={() => handlePagination(page - 1)}
            type="button"
            className="pagination__item"
          >
            {page === 1 ? page : page - 1}
          </button>
        </React.StrictMode>
      }


      <button
        onClick={() => handlePagination(page)}
        type="button"
        className="pagination__item"
        id="active"
      >
        {page}
      </button>

      {totalPages - 1 === page - 1 ?
        <React.StrictMode></React.StrictMode>
        :
        // 最後のページ以外はこっちを表示
        <button
          onClick={() => handlePagination(page + 1)}
          type="button"
          className="pagination__item"
        >
          {page + 1}
        </button>
      }


      {/* 1ページ目だったら表示 */}
      {page !== 1 ? <React.StrictMode></React.StrictMode> :
        <button
          onClick={() => handlePagination(page + 2)}
          type="button"
          className="pagination__item"
        >
          {page + 2}
        </button>
      }

      {totalPages - 1 === page - 1 ?
        <React.StrictMode></React.StrictMode>
        :
        // 最後のページ以外はこっちを表示
        <React.StrictMode>
          <div className="pagination--dot">...</div>
          <button
            onClick={() => handlePagination(totalPages)}
            type="button"
            className="pagination__item topend"
          >
            最後へ
            &gt;
          </button>
        </React.StrictMode>
      }

    </ul>
  );
};

export const ThirdPagination: React.FC<Props> = ({
  page,
  totalPages,
  handlePagination,
}) => {
  return (
    <ul className="pagination__box">
      <button
        onClick={() => handlePagination(1)}
        type="button"
        className="pagination__item"
        id={page === 1 ? "active" : ""}
      >
        1
      </button>
      <button
        onClick={() => handlePagination(2)}
        type="button"
        className="pagination__item"
        id={page === 2 ? "active" : ""}
      >
        2
      </button>
      <button
        onClick={() => handlePagination(3)}
        type="button"
        className="pagination__item"
        id={page === 3 ? "active" : ""}
      >
        3
      </button>
    </ul>
  );
}
export const Pagination = PaginationComponent;
