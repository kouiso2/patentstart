import { ApplyBadge, ManageBadge } from 'components/Badge';
import useApplicantPatentList from 'hooks/patentList/applicant';
import useAttorneyPatentList from 'hooks/patentList/attorney';
import { default as useApplicantProfile, default as useAttorneyProfile } from 'hooks/profile';
import React from 'react';
interface Props {
  item: any;
  kind: "Apply" | "Manage";
}

export const AttorneyApplicationItem: React.FC<Props> = ({ item, kind }) => {
  const { handleApplyClick, handleManageClick } = useAttorneyPatentList();
  const { profile } = useAttorneyProfile(item.creater?.path);

  const handleClick = React.useCallback((id: string, path: string) => {
    if (kind === "Apply") {
      return handleApplyClick(id, path);
    } else {
      handleManageClick(id, path);
    }
  }, [kind, handleManageClick, handleApplyClick]);

  const badge = React.useMemo(() => {
    if (kind === "Apply") {
      return (
        <ApplyBadge />
      )
    } else {
      return (
        <ManageBadge />
      )
    }
  }, [kind]);

  return (
    <div className="applicationList__bg" onClick={() => {
      handleClick(item.id, item.path);
    }}>
      <div>
        <div className="applicationList__statusWrap">
          {badge}
          <span className="applicationList__name">{profile.firstName + profile.lastName}</span>
        </div>
        <p className="applicationList__text">{item.title}</p>
      </div>
    </div>
  )
};
export const ApplicantApplicationItem: React.FC<Props> = ({ item, kind }) => {
  const { handleApplyClick, handleManageClick } = useApplicantPatentList();
  const { profile } = useApplicantProfile(item.attorney?.path);

  const handleClick = React.useCallback((id: string, path: string) => {
    if (kind === "Apply") {
      return handleApplyClick(id, path);
    } else {
      handleManageClick(id, path);
    }
  }, [kind, handleManageClick, handleApplyClick]);

  const badge = React.useMemo(() => {
    if (kind === "Apply") {
      return (
        <ApplyBadge />
      )
    } else {
      return (
        <ManageBadge />
      )
    }
  }, [kind]);

  return (
    <div className="applicationList__bg" onClick={() => {
      handleClick(item.id, item.path);
    }}>
      <div>
        <div className="applicationList__statusWrap">
          {badge}
          <span className="applicationList__name">{profile.firstName + profile.lastName}</span>
        </div>
        <p className="applicationList__text">{item.title}</p>
      </div>
    </div>
  )
};

export const NoneApplicationItem: React.FC = () => {
  return (
    <div className="recruitment__chatNone recruitment__spNone">
      <article className="chatlist__child__des recruitment__chatNone__message">
        <div className="chatlist__child__desc__top">
        </div>
        <p className="chatlist__child__desc--message recruitment__chatNone__message--pr">現在はありません</p>
      </article>
    </div>
  )
}

export default AttorneyApplicationItem;
